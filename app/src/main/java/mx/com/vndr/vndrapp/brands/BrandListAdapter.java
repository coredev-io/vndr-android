package mx.com.vndr.vndrapp.brands;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Brand;

public class BrandListAdapter extends RecyclerView.Adapter<BrandListAdapter.BrandListViewHolder> {

    private List<Brand> brandList;
    private OnBrandListClick listener;

    public BrandListAdapter(List<Brand> brandList, OnBrandListClick listener) {
        this.brandList = brandList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public BrandListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BrandListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand_list, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull BrandListViewHolder holder, final int position) {
        holder.brandNameTextView.setText(brandList.get(position).getBrandName());
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Brand selectedBrand = brandList.get(position);
                listener.onClick(selectedBrand);
            }
        });
    }

    @Override
    public int getItemCount() {
        return brandList.size();
    }

    public class BrandListViewHolder extends RecyclerView.ViewHolder{

        ConstraintLayout layout;
        TextView brandNameTextView;

        public BrandListViewHolder(@NonNull View itemView) {
            super(itemView);
            layout = itemView.findViewById(R.id.layout_item_brand_list);
            brandNameTextView = itemView.findViewById(R.id.txt_brand_name);
        }
    }

    public interface OnBrandListClick{
        void onClick(Brand brand);
    }
}
