package mx.com.vndr.vndrapp.catalogs.mainCatalog;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.itemList.Item;
import mx.com.vndr.vndrapp.customviews.itemList.ItemListAdapter;
import mx.com.vndr.vndrapp.customviews.itemList.ItemListPresenter;

public class MainCatalogInteractor {

    private final String TAG = "MainCatalogInteractor";

    private List<Item> itemList;
    private ItemListAdapter adapter;
    private OnItemClickListener listener;


    public MainCatalogInteractor() {
        itemList = new ArrayList<>();
        addItems();
        createAdapter();
    }

    public void setListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    private OnItemClickListener getListener() {
        if (listener == null) throw new NullPointerException("MainCatalogInteractor.OnItemClickListener is null");
        else return listener;
    }

    private void addItems(){
        itemList.add(
                new Item(
                        "Asociar una marca a mi perfil",
                        "Selecciona las marcas que vendes",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try{
                                    getListener().onAssociateBrandClick();
                                } catch (NullPointerException e){
                                    Log.e(TAG, e.getMessage());
                                    e.printStackTrace();
                                }

                            }
                        }
                )
        );

        itemList.add(
                new Item(
                        "Dar de alta/editar una marca nueva",
                        "Marca no incluida en lista general",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getListener().onNewBrandClick();
                            }
                        }
                )
        );

        itemList.add(
                new Item(
                        "Dar de alta/editar un Catálogo",
                        "Catálogo no incluido en una Marca",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getListener().onEditCatalog();
                            }
                        }
                )
        );

        itemList.add(
                new Item(
                        "Dar de alta/editar un Producto",
                        "Mercancía por Catálogo",
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getListener().onEditProduct();
                            }
                        }
                )
        );

    }

    private void createAdapter(){
        ItemListPresenter itemListPresenter = new ItemListPresenter(itemList);
        adapter = new ItemListAdapter(itemListPresenter);
    }

    public ItemListAdapter getAdapter() {
        return adapter;
    }

    interface OnItemClickListener{
        void onAssociateBrandClick();
        void onNewBrandClick();
        void onEditCatalog();
        void onEditProduct();
    }


}
