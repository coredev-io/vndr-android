package mx.com.vndr.vndrapp.cobros;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.TicketMovementActivity;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.cobros.movements.AddMovementActivity;
import mx.com.vndr.vndrapp.cobros.movements.NewMovement;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.cobros.movements.MovementTypeActivity;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;

import static android.view.View.GONE;

public class DetailCxcActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener, APIVndrCxc.CXCResponse, VndrListAdapter.OnCardItemSelected, MaterialDialog.ListCallbackSingleChoice {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textView;
    ProgressBar progressBar;
    FloatingActionButton fab;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_cxc);

        toolbar = findViewById(R.id.tb_cxc_detail);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener((view -> onBackPressed()));

        toolbar.setOnMenuItemClickListener(this);

        recyclerView = findViewById(R.id.rv_cxc_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textView = findViewById(R.id.txt_cxc_detail);
        textView.setVisibility(View.GONE);
        progressBar = findViewById(R.id.pb_detail_cxc);
        progressBar.setVisibility(GONE);
        fab = findViewById(R.id.floatingActionButton3);

        if (getIntent().getBooleanExtra("isStatusQuebranto",false)){
            fab.setVisibility(GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(null);
        getData();
    }

    private void getData(){
        showProgress();
        new APIVndrCxc(Volley.newRequestQueue(this))
                .getCxCDetail(this, getIntent().getStringExtra("id_cxc"));
    }

    @Override
    public void onMoveSelected(CxCMovement movement) {
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
    }

    @Override
    public void cxcSuccess(List<VndrList> list) {
        dismissError();
        dismissProgress();
        VndrListAdapter adapter = new VndrListAdapter(list);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void cxcError(String messageError) {
        dismissProgress();
        showError(messageError);
    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(GONE);
    }

    private void showError(String messageError){
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
    }

    private void dismissError(){
        textView.setVisibility(GONE);
    }

    private void hideAddMovementButton(){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cxc_menu,menu);
        return true;
    }

    public void onAddMovementClick(View view){
        MovementType.Query movementSelected = MovementType.Query.current.getSelected();
        if (movementSelected != null)
            goToPickMovementType(movementSelected);
        else
            askForMovementType();


    }

    private void askForMovementType(){
        new MaterialDialog.Builder(this)
                .title("Registra cobro o cargo")
                .items(R.array.items_movement_types)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    if (which == 0)
                        goToPickMovementType(MovementType.Query.A);
                    else goToPickMovementType(MovementType.Query.C);
                    return false;
                })
                .show();
    }

    private void navigateToShareInformation(NotificationAlertData notificationAlert){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setNotificationAlertData(notificationAlert);

        alertFragment.show(getSupportFragmentManager(),"alert");
    }

    private void askForNotificationToShare(){
        if (NotificationAlertData.SelectedCxC.current.getDataList().isEmpty()){
            Toast.makeText(this, "No tienes notificaciones para compartir", Toast.LENGTH_LONG).show();
            return;
        }

        List<String> items = new ArrayList<>();

        for (NotificationAlertData notificationAlert : NotificationAlertData.SelectedCxC.current.getDataList()) {
            Log.e("TAG",notificationAlert.getTitle());
            items.add(notificationAlert.getTitle());
        }
        new MaterialDialog.Builder(this)
                .title("Notificaciones de cuenta")
                .items(items)
                .itemsCallbackSingleChoice(-1, this)
                .show();
    }

    @Override
    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
        NotificationAlertData alert = NotificationAlertData.SelectedCxC.current.getDataList().get(which);
        navigateToShareInformation(alert);
        return true;
    }

    private void goToPickMovementType(MovementType.Query query){
        Intent intent = new Intent(this, MovementTypeActivity.class)
                .putExtra("query", query);
        startActivityForResult(intent, 0);
    }

    private void goToAddMovement(){
        startActivity( new Intent(
                this,
                AddMovementActivity.class
        ));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 0){
            MovementType movementType = (MovementType) data.getSerializableExtra("movementType");
            NewMovement.me.getMovement().setType(movementType);
            goToAddMovement();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menu_config_cxc){
            new AdminCxcFragment().show(getSupportFragmentManager(), "fm");
            return true;
        } else if (menuItem.getItemId() ==  R.id.menu_share_cxc){
            askForNotificationToShare();
        }
        return false;
    }


}
