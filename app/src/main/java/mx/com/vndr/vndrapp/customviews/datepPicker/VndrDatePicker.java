package mx.com.vndr.vndrapp.customviews.datepPicker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import java.util.Calendar;
import java.util.Date;

public class VndrDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private final String TAG = "VndrDatePicker";
    private DatePickerDialog dialog;
    private Date currentDate;
    private Date minDate;
    private Date maxDate;
    private OnSelectedDate onSelectedDateListener;

    public VndrDatePicker() {

    }

    public VndrDatePicker setOnSelectedDateListener(OnSelectedDate onSelectedDateListener) {
        this.onSelectedDateListener = onSelectedDateListener;
        return this;
    }

    public VndrDatePicker setcurrentDate(Date currentDate) {
        this.currentDate = currentDate;
        return this;
    }

    public VndrDatePicker setMinDate(Date minDate) {
        this.minDate = minDate;
        return this;
    }

    public VndrDatePicker setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
        return this;
    }

    public void show(FragmentManager fm){
        super.show(fm, TAG);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();

        if (currentDate != null)
            calendar.setTime(currentDate);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        dialog = new DatePickerDialog(getContext(),this, year, month, day);

        if (minDate != null)
            dialog.getDatePicker().setMinDate(minDate.getTime());

        if (maxDate != null)
            dialog.getDatePicker().setMaxDate(maxDate.getTime());

        return dialog;

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(i, i1, i2);
        Log.d(TAG, "Sected date: " +  calendar.getTime().toString());

        if (onSelectedDateListener != null)
            onSelectedDateListener.selectedDate(calendar.getTime());
        else
            Log.e(TAG, "OnSelectedDateListener is null");
    }

    public interface OnSelectedDate {
        void selectedDate(Date date);
    }
}
