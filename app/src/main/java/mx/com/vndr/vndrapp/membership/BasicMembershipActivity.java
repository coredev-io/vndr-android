package mx.com.vndr.vndrapp.membership;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import mx.com.vndr.vndrapp.R;

public class BasicMembershipActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basic_membership);
    }

    public void onClick(View view){
        onBackPressed();
    }

}
