package mx.com.vndr.vndrapp.models;

public class ProductClass {
    private String classification, id;

    public ProductClass(String classification, String id) {
        this.classification = classification;
        this.id = id;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
