package mx.com.vndr.vndrapp.api.inventory;

import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mx.com.vndr.vndrapp.api.VndrRequest;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_SUPPLIERS;

public class APISupplier {

    private final static String TAG = "APISupplier";

    private RequestQueue queue;

    public APISupplier(RequestQueue queue) {
        this.queue = queue;
    }

    public void addSupplier(@NonNull Supplier supplier, @NonNull OnSupplierResponse onSupplierResponse){
        new VndrRequest(queue)
                .setRequestParams(supplier.getParamsRequest())
                .post(URL_SUPPLIERS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onSupplierResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        onSupplierResponse.onSuccess();
                    }
                });
    }

    public void updateSupplier(@NonNull Supplier supplier, @NonNull OnSupplierResponse onSupplierResponse){
        String url = URL_SUPPLIERS + "/" + supplier.getId();
        new VndrRequest(queue)
                .setRequestParams(supplier.getParamsRequest())
                .put(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onSupplierResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        onSupplierResponse.onSuccess();
                    }
                });
    }

    public void getSuppliers(@NonNull OnSupplierListResponse onSupplierResponse){
        new VndrRequest(queue)
                .get(URL_SUPPLIERS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onSupplierResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.has("suppliers")){
                                List<Supplier> suppliers = Supplier.parseSuppliersList(jsonObject.getJSONArray("suppliers"));

                                if (suppliers.isEmpty())
                                    onSupplierResponse.onEmptyList();
                                else
                                    onSupplierResponse.onSuccess(suppliers);
                            }

                            else{
                                if (jsonObject.getInt("statusCode") == 404)
                                    onSupplierResponse.onEmptyList();
                                else
                                    onSupplierResponse.onError(jsonObject.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onSupplierResponse.onError("Ocurrió un error al parsear los datos.");
                        }
                    }
                });
    }

    public interface OnSupplierResponse {
        void onSuccess();
        void onError(String messageError);
    }

    public interface OnSupplierListResponse {
        void onSuccess(List<Supplier> suppliers);
        void onEmptyList();
        void onError(String messageError);
    }


}
