package mx.com.vndr.vndrapp.catalogs.associateBrand;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public class AssociateBrandPresenter implements AssociateBrandInteractor.OnGetDataFinishListener {

    AssociateBrandInteractor interactor;
    AssociateBrandView brandView;

    public AssociateBrandPresenter(AssociateBrandInteractor interactor, AssociateBrandView brandView) {
        this.interactor = interactor;
        this.brandView = brandView;
    }

    public void getData(){
        brandView.showProgress();
        interactor.getBrands(this);
    }

    @Override
    public void onError(String error) {
        brandView.dismissProgress();
        brandView.showRequestError(error);
    }

    @Override
    public void onSuccess(CheckedItemAdapter adapter) {
        brandView.dismissProgress();
        brandView.setAdapter(adapter);
    }

    @Override
    public void onCheckedItemClick(Brand brand) {
        brandView.showModal(brand);
    }

}
