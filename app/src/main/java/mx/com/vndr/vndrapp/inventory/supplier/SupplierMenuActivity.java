package mx.com.vndr.vndrapp.inventory.supplier;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.inventory.productsPurchase.ProductsPurchaseMainActivity;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.ShoppingOrdersActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class SupplierMenuActivity extends AppCompatActivity {


    Toolbar toolbar;
    RecyclerView recyclerView;
    Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_menu);
        supplier = Supplier.Selected.current.getSupplier();
        setupUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setTitle(supplier.getName());
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_supplier_menu);
        recyclerView = findViewById(R.id.rv_supplier_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuSupplier1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToSupplierActivity));

        options = Arrays.asList(getResources().getStringArray(R.array.menuSupplier2));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToShoppingOrders));

        options = Arrays.asList(getResources().getStringArray(R.array.menuSupplier3));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateTo));

        /*
        options = Arrays.asList(getResources().getStringArray(R.array.menuSupplier4));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));

        options = Arrays.asList(getResources().getStringArray(R.array.menuSupplier5));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));
         */

        return optionList;
    }

    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }

    private void navigateToSupplierActivity(){
        startActivityForResult(
                new Intent(
                        this,
                        SupplierActivity.class
                ).putExtra("supplier", supplier), 1);
    }

    private void navigateToShoppingOrders(){
        ShoppingOrdersActivity.Query query = ShoppingOrdersActivity.Query.BY_SUPPLIER;
        query.setSupplierId(supplier.getId());
        startActivity(new Intent(this, ShoppingOrdersActivity.class)
                .putExtra("query", query));
    }

    private void navigateTo(){
        startActivity(new Intent(this, ProductsPurchaseMainActivity.class).putExtra("supplier", supplier));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK){
            supplier = (Supplier) data.getSerializableExtra("supplier");
        }
    }
}
