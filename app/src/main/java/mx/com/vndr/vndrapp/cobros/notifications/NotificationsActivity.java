package mx.com.vndr.vndrapp.cobros.notifications;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;

public class NotificationsActivity extends AppCompatActivity implements APIVndrCxc.NotificationsResponse, NotificationsAdapter.OnNotificationSelected, TabLayout.BaseOnTabSelectedListener {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textView;
    TabLayout tabLayout;
    TextView notificationsSum;

    private APIVndrCxc.NotificationsQuery selectedQuery = APIVndrCxc.NotificationsQuery.A;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        setupUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData(selectedQuery);
    }

    private void getData(APIVndrCxc.NotificationsQuery query){
        showLoading();
        selectedQuery = query;
        new APIVndrCxc(Volley.newRequestQueue(this))
                .getNotifications(this, query);
    }

    @Override
    public void onNotificacionClick(Notification notification) {
        Notification.Selected.current.setNotificationSelected(notification);
        navigateToNotificationDetail();
    }

    @Override
    public void notifications(List<Notification> notifications) {
        dismissLoading();
        NotificationsAdapter adapter = new NotificationsAdapter(notifications);
        adapter.setOnNotificationSelected(this);
        recyclerView.setAdapter(adapter);

        if (selectedQuery.equals(APIVndrCxc.NotificationsQuery.A))
            updateNotificationsTotal(Notification.totalNotificationsNoticesAmount);
        else
            updateNotificationsTotal(Notification.totalNotificationsRequirementsAmount);
    }

    @Override
    public void notificationsError(String messageError) {
        showError(messageError);
        updateNotificationsTotal("$0.00");
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_notifications);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_notifications);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        progressBar = findViewById(R.id.pb_notifications);

        textView = findViewById(R.id.txt_notifications);
        tabLayout = findViewById(R.id.tabLayout_notifications);
        tabLayout.addOnTabSelectedListener(this);

        notificationsSum = findViewById(R.id.txt_notificacions_sum);


        hideItems();
    }

    private void hideItems(){
        progressBar.setVisibility(View.GONE);
        textView.setVisibility(View.GONE);
    }

    private void showLoading(){
        textView.setVisibility(View.GONE);  //  dismiss error textview
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
    }

    private void showError(String messageError){
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
        progressBar.setVisibility(View.GONE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void navigateToNotificationDetail(){
        Intent intent = new Intent(this, NotificationDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        if (tab.getPosition() == 0)
            getData(APIVndrCxc.NotificationsQuery.A);
        else
            getData(APIVndrCxc.NotificationsQuery.R);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        recyclerView.setAdapter(null);
        updateNotificationsTotal("$0.00");
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void updateNotificationsTotal(String amount){
        notificationsSum.setText(
               amount
        );
    }
}
