package mx.com.vndr.vndrapp.analytics.collections;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.cobros.movements.MovementsActivity;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class CollectionMenuActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collection_menu);
        setupUI();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_collection_menu);
        recyclerView = findViewById(R.id.rv_collection_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuCollectionOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::askForTypeCartera));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCollectionOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::askForTypeRecuperacion));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCollectionOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToResumeCollection));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCollectionOption4));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToMovements));

        return optionList;
    }

    public void askForTypeCartera(){
        new MaterialDialog.Builder(this)
                .items("Cobranza actual","Cuentas por cobrar")
                .itemsCallback((dialog, itemView, position, text) -> {
                    if (position == 0)
                        navigateToCobranzaActual(StatusCollectionActivity.Query.CURRENT);
                    else
                        navigateToStatusCobranza();
                })
                .show();
    }

    public void askForTypeRecuperacion(){
        new MaterialDialog.Builder(this)
                .items("Cartera actual","Proyección de recuperación")
                .itemsCallback((dialog, itemView, position, text) -> {
                    if (position == 0) {
                        navigateToCobranzaActual(StatusCollectionActivity.Query.CXC_REC);
                    }
                    else
                        navigateToProjection();
                })
                .show();
    }

    public void navigateToCobranzaActual(StatusCollectionActivity.Query query){
        startActivity(new Intent(this, StatusCollectionActivity.class).putExtra("query",query));
    }

    public void navigateToStatusCobranza(){
        startActivity(new Intent(this, StatusCxcActivity.class));
    }

    public void navigateToProjection(){
        startActivity(new Intent(this, ProjectionActivity.class));
    }

    public void navigateToResumeCollection(){
        startActivity(new Intent(this, CollectionResumeActivity.class));
    }

    public void navigateToMovements(){
        startActivity(new Intent(this, MovementsActivity.class));
    }

    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }
}