package mx.com.vndr.vndrapp.products;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.itemSingleList.ItemSingleListAdapter;
import mx.com.vndr.vndrapp.customviews.itemSingleList.OnSingleListClickItemListener;
import mx.com.vndr.vndrapp.models.ProductClass;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_CLASS;

public class ProductClassificationActivity extends AppCompatActivity implements OnSingleListClickItemListener {

    RecyclerView recyclerView;
    Toolbar toolbar;
    ItemSingleListAdapter adapter;
    List<ProductClass> classList = new ArrayList<>();
    List<String> items = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_classification);
        setUpUI();
        getClasses();
    }


    private void setUpUI(){
        recyclerView = findViewById(R.id.rv_product_class);
        toolbar = findViewById(R.id.tb_product_class);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    }


    private void getClasses(){
        StringRequest request = new StringRequest(Request.Method.GET, URL_CLASS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    processResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }


    private void processResponse(String response) throws JSONException {
        JSONArray array = new JSONArray(response);

        for (int i = 0; i <  array.length(); i++){
            JSONObject object = array.getJSONObject(i);

            ProductClass productClass = new ProductClass(
                    object.getString("standard_classification"),
                    object.getString("_id")
            );

            classList.add(productClass);
            items.add(object.getString("standard_classification"));
        }

        adapter = new ItemSingleListAdapter(items, this);
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void onClickListener(String item, int pos) {
        ProductClass productClass = classList.get(pos);

        startActivity(
                new Intent(
                        this,
                        PickProductActivity.class
                ).putExtra("activity", ProductEnum.QUERY_PRODUCTS_BY_CLASS)
                .putExtra("idCatalog",productClass.getId())
        );
    }
}
