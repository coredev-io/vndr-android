package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.PODetailActivity;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class InvoiceDateActivity extends AppCompatActivity implements CalendarView.OnDateChangeListener {

    Toolbar toolbar;
    CalendarView calendarView;
    ProgressBar progressBar;
    Button button;
    TextView textView;
    Date date;
    PurchaseInvoice purchaseInvoice;
    InvoiceDateType type = InvoiceDateType.invoice;
    boolean isFutureDate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_date);

        setupUI();
        purchaseInvoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();
        if (getIntent().hasExtra("type"))
            type = (InvoiceDateType) getIntent().getSerializableExtra("type");
        customTitles();

    }

    private void customTitles(){
        switch (type) {
            case invoice:
                getSupportActionBar().setTitle("Fecha de la factura");
                textView.setText("Selecciona fecha de la factura");
                break;
            case reception:
                getSupportActionBar().setTitle("Fecha de recepción");
                textView.setText("Selecciona la fecha de recepción de la mercancía");
                break;
            case payment:
                getSupportActionBar().setTitle("Fecha de pago");
                textView.setText("Selecciona la fecha de pago");
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    /**
     * UI Methods
     */

    private void setupUI(){
        toolbar = findViewById(R.id.tb_invoice_date);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        calendarView = findViewById(R.id.cv_invoice_date);
        button = findViewById(R.id.btn_invoice_date);
        progressBar = findViewById(R.id.pb_invoice_date);
        textView = findViewById(R.id.detail_invoice_date);
        calendarView.setOnDateChangeListener(this);

        date = new Date();  //hoy
        calendarView.setDate(date.getTime());
        //calendarView.setMinDate(date.getTime());

    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
        Calendar c = Calendar.getInstance();
        c.set(i, i1, i2);
        date = c.getTime();
        if (type != InvoiceDateType.invoice)
            validateDates();
    }

    private void validateDates(){
        isFutureDate = date.after(new Date());
        if (isFutureDate)
            makeButtonNotSendedAppearance();
        else
            makeButtonSendedAppearance();
    }

    private void makeButtonNotSendedAppearance(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            button.setBackgroundTintList(ContextCompat.getColorStateList(this,R.color.colorPrimary));
        }

        if (type == InvoiceDateType.reception) {
            button.setText("Aún no la recibo");
        }

        if (type == InvoiceDateType.payment) {
            button.setText("Aún no la pago");
        }
    }

    private void makeButtonSendedAppearance(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            button.setBackgroundTintList(ContextCompat.getColorStateList(this,R.color.colorAccent));
        }
        button.setText("Siguiente");
    }


    /**
     * Action UI Methods
     */

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }


    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    public void onClick(View view){
        appendInvoiceData();
    }

    public void navigateToNextView(){
        switch (type){
            case invoice:
                startActivityForResult(
                        new Intent(this, InvoiceDateActivity.class)
                                .putExtra("type", InvoiceDateType.reception),
                        1);
                break;
            case reception:
                startActivityForResult(
                        new Intent(this, InvoicePaymentMethodActivity.class),
                        1);
                break;
            case payment:
                if (isFutureDate) {
                    startActivityForResult(
                            new Intent(this, InvoiceReviewActivity.class),
                            1);
                } else {
                    startActivityForResult(
                            new Intent(this, InvoicePaymentTypeActivity.class),
                            1);
                }
                break;
        }
    }

    public void appendInvoiceData(){
        showLoading();
        Map<String, String> params = new HashMap<>();

        switch (type){
            case invoice:
                params.put("invoice_date", VndrDateFormat.dateToWSFormat(date));
                break;
            case reception:
                params.put("reception_date", VndrDateFormat.dateToWSFormat(date));
                params.put("reception_status", String.valueOf(!isFutureDate));
                break;
            case payment:
                params.put("payment_date", VndrDateFormat.dateToWSFormat(date));
                break;
        }

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .patch(URL_PURCHASE_INVOICE + "/" + purchaseInvoice.getPurchaseId() , new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismissLoading();
                        Dialogs.showAlert(InvoiceDateActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismissLoading();
                        navigateToNextView();
                    }
                });
    }

    public enum InvoiceDateType {
        invoice, reception, payment
    }
}
