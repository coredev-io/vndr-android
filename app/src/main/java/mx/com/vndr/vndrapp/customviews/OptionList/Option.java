package mx.com.vndr.vndrapp.customviews.OptionList;

public class Option {

    private String title;
    private String subtitle;
    private OnClickListener listener;
    private String headerTitle;
    private boolean isHeader = false;

    public Option(String title) {
        this.title = title;
    }

    public Option(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public Option(String title, String subtitle, OnClickListener listener) {
        this.title = title;
        this.subtitle = subtitle;
        this.listener = listener;
    }

    public Option(String headerTitle, boolean isHeader) {
        this.headerTitle = headerTitle;
        this.isHeader = isHeader;
    }

    public void setListener(OnClickListener listener) {
        this.listener = listener;
    }

    public OnClickListener getListener() {
        return listener;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getHeaderTitle() {
        return headerTitle;
    }

    public boolean isHeader() {
        return isHeader;
    }
}
