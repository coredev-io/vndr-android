package mx.com.vndr.vndrapp.api.inventory;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class ShoppingOrder implements Serializable {

    private String purchaseId;
    private String purchaseNumber;
    private String statusLabel;
    private String statusCode;
    private Double totalOrderAmount;
    private Double orderAmount;
    private Date orderDate;
    private Date sendDate;
    private Supplier supplier;
    private Double shippingCost;
    private List<ShoppingCartProduct> productList = new ArrayList<>();
    private NotificationAlertData notificationAlertData;

    public ShoppingOrder(JSONObject jsonObject) throws JSONException, ParseException {
        if (jsonObject.has("purchase_id"))
            this.purchaseId = jsonObject.getString("purchase_id");
        else if (jsonObject.has("_id"))
            this.purchaseId = jsonObject.getString("_id");
        this.purchaseNumber = String.valueOf(jsonObject.getInt("purchase_number"));
        this.statusLabel = jsonObject.getString("status_label");
        this.totalOrderAmount = jsonObject.getDouble("total_order_amount");
        this.orderDate = VndrDateFormat.stringToDate(jsonObject.getString("purchase_date"));
        this.supplier = new Supplier(jsonObject.getJSONObject("supplier"));
        this.statusCode = jsonObject.getString("status_code");
        this.shippingCost = jsonObject.getDouble("shipping_cost");
        this.orderAmount = jsonObject.getDouble("order_amount");
    }

    public ShoppingOrder() {
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getPurchaseNumber() {
        return "#" + purchaseNumber;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public String getTotalOrderAmount() {
        return NumberFormat.getCurrencyInstance().format(totalOrderAmount);
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public String getOrderDateStr() {
        return VndrDateFormat.dateToString(orderDate);
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setTotalOrderAmount(Double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Double getShippingCost() {
        return shippingCost;
    }

    public String getShippingCostDecimalFormat(){
        if (shippingCost == 0)
            return "";
        else
            return new DecimalFormat("#.00").format(shippingCost);
    }

    public String getShippingCostFormat() {
        return NumberFormat.getCurrencyInstance().format(shippingCost);
    }

    public void setTotalMerch(){
        double merchTotal = 0.0;
        for (ShoppingCartProduct cartProduct: getProductList()){
            merchTotal += cartProduct.getSubtotal();
        }
        this.orderAmount = merchTotal;
    }

    public String getTotalMerchFormat(){
        return NumberFormat.getCurrencyInstance().format(orderAmount);
    }

    public void setShippingCost(Double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public List<ShoppingCartProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ShoppingCartProduct> productList) {
        this.productList = productList;
    }

    public NotificationAlertData getNotificationAlertData() {
        return notificationAlertData;
    }

    public void setNotificationAlertData(NotificationAlertData notificationAlertData) {
        this.notificationAlertData = notificationAlertData;
    }

    public Supplier getSupplier() {
        if (supplier == null)
            return new Supplier();
        else
            return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public byte[] getCreateUpdateOrderBody(boolean isUpdate) throws JSONException {
        JSONObject params = new JSONObject();
        JSONArray products = new JSONArray();

        for (ShoppingCartProduct product : productList) {

            JSONObject object = new JSONObject();
            object.put("product",product.getProduct().getIdProduct());
            object.put("quantity", product.getProductsCount());
            object.put("purchase_price", product.getBuyPrice());
            object.put("iva", 0);
            object.put("notes",product.getNotes());

            products.put(object);
        }

        if (isUpdate)
            params.put("purchase_id", getPurchaseId());
        params.put("supplier_id", getSupplier().getId());
        params.put("shipping_cost", getShippingCost());
        params.put("products", products);
        return params.toString().getBytes();
    }

    public Map<String, String> getUpdateOrderDateBody(){
        Map<String, String> params = new HashMap<>();
        params.put("purchase_id", getPurchaseId());
        params.put("order_date", VndrDateFormat.dateToWSFormat(getOrderDate()));
        Log.e("TAG", params.toString());
        return params;
    }

    public Map<String, String> getUpdateSendOrderDateBody(){
        Map<String, String> params = new HashMap<>();
        params.put("purchase_id", getPurchaseId());
        params.put("sending_date", VndrDateFormat.dateToWSFormat(getSendDate()));
        return params;
    }

    public enum Selected {
        current;

        private ShoppingOrder shoppingOrder;

        public ShoppingOrder getShoppingOrder() {
            return shoppingOrder;
        }

        public void setShoppingOrder(ShoppingOrder shoppingOrder) {
            this.shoppingOrder = shoppingOrder;
        }
    }
}
