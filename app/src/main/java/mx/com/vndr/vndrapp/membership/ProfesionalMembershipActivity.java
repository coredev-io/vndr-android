package mx.com.vndr.vndrapp.membership;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrMembership;
import mx.com.vndr.vndrapp.registro.InitialSurveyActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class ProfesionalMembershipActivity extends AppCompatActivity {

    private final int SIGNUP_SURVEY_INTENT = 1;

    private boolean closeAction = false;
    private boolean buyAndCloseAction = false;

    private VndrMembership vndrMembership;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profesional_membership);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary, this.getTheme()));

        closeAction = getIntent().getBooleanExtra("closeAction", false);
        buyAndCloseAction = getIntent().getBooleanExtra("buyAndCloseAction", false);
        vndrMembership = VndrMembership.getInstance();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void onClick(View view){
        if (closeAction) { this.finish(); }
        else {
            vndrMembership.purchaseLicense(this, new VndrMembership.PurchaseResult() {
                @Override
                public void onPurchaseLicenseSuccess() {
                    if (buyAndCloseAction){ ProfesionalMembershipActivity.this.finish(); }
                    else { navigateToSurvey(); }
                }

                @Override
                public void onPurchaseLicenseFail(String messageError) {
                    if (messageError.isEmpty()){ return; }
                    Dialogs.showAlert(ProfesionalMembershipActivity.this, messageError);
                }
            });
        }
    }

    private void navigateToSurvey(){
        startActivityForResult(new Intent(this, InitialSurveyActivity.class), SIGNUP_SURVEY_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNUP_SURVEY_INTENT && resultCode == RESULT_OK){
            this.finish();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        vndrMembership.endMembershipConnection();
    }
}
