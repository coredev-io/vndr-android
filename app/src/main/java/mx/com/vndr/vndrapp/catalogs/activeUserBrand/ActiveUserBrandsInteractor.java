package mx.com.vndr.vndrapp.catalogs.activeUserBrand;

import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItem;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemPresenter;
import mx.com.vndr.vndrapp.models.Brand;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ACTIVE_USER_BRANDS;

public class ActiveUserBrandsInteractor {

    private String token;
    private RequestQueue queue;
    private CheckedItemAdapter adapter;
    private List<Brand> brandList;
    private List<CheckedItem> items;


    public ActiveUserBrandsInteractor(String token, RequestQueue queue) {
        this.token = token;
        this.queue = queue;
        brandList = new ArrayList<>();
        items = new ArrayList<>();
    }

    public void getData(final OnRequestFinish listener){
        StringRequest request = new StringRequest(Request.Method.GET, URL_ACTIVE_USER_BRANDS , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    processResponse(response,listener);
                    createAdapter();
                    listener.onSuccess(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");

                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        queue.add(request);
    }

    private void processResponse(String response, final OnRequestFinish listener) throws JSONException {
        JSONArray array = new JSONArray(response);

        if (array.length() == 0){
            listener.onEmptyBrands();
        }

        for (int i = 0; i < array.length(); i++){
            JSONObject object = array.getJSONObject(i);


            final Brand brand = new Brand(object.getString("brand_name"), object.getString("_id"));

            CheckedItem item = new CheckedItem(object.getString("brand_name"), false, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onItemClick(brand);
                }
            });

            if (object.has("active_in_user")){
                item.setChecked(object.getBoolean("active_in_user"));
                brand.setActiveInUser(true);
            }


            items.add(item);
            brandList.add(brand);


        }

    }


    private void createAdapter(){
        adapter = new CheckedItemAdapter(
                new CheckedItemPresenter(
                        items
                )
        );
    }

    interface OnRequestFinish{
        void onSuccess(CheckedItemAdapter adapter);
        void onError(String error);
        void onEmptyBrands();
        void onItemClick(Brand brand);
    }
}
