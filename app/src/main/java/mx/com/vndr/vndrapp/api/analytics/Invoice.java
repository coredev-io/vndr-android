package mx.com.vndr.vndrapp.api.analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.NumberFormat;

import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class Invoice implements Serializable {

    String id;
    String supplierName;
    String invoiceNumber;
    String invoiceDate;
    String receptionDate;
    String amount;
    String status;
    String statusCode;
    String iva;
    String totalInvoice;
    String paymentMethod;
    String notes;

    public Invoice(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getString("_id");
        supplierName = jsonObject.getString("supplier");
        invoiceNumber = "#" + jsonObject.getString("invoice_document_number");
        invoiceDate = VndrDateFormat.stringDateParamsFormatToAppFormat(jsonObject.getString("invoice_date"));
        receptionDate = VndrDateFormat.stringDateParamsFormatToAppFormat(jsonObject.getString("reception_date"));
        amount = NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("amount"));
        status = jsonObject.getString("label");
        statusCode = jsonObject.getString("status_code");
    }

    public Invoice(JSONObject ticketObject, boolean isInvoice) throws JSONException {
        id = ticketObject.getString("_id");
        supplierName = ticketObject.getJSONObject("supplier").getString("supplier_name");
        invoiceNumber = "#" + ticketObject.getString("invoice_document_number");
        invoiceDate = VndrDateFormat.stringDateParamsFormatToAppFormat(ticketObject.getString("invoice_date"));
        receptionDate = VndrDateFormat.stringDateParamsFormatToAppFormat(ticketObject.getString("reception_date"));
        amount = NumberFormat.getCurrencyInstance().format(ticketObject.getDouble("invoice_amount"));
        iva = NumberFormat.getCurrencyInstance().format(ticketObject.getDouble("iva"));
        totalInvoice = NumberFormat.getCurrencyInstance().format(ticketObject.getDouble("total_invoice_amount"));
        paymentMethod = ticketObject.getString("payment_method_description");
        notes = ticketObject.getString("notes");
    }

    public String getId() {
        return id;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public String getReceptionDate() {
        return receptionDate;
    }

    public String getAmount() {
        return amount;
    }

    public String getStatus() {
        return status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getIva() {
        return iva;
    }

    public String getTotalInvoice() {
        return totalInvoice;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public String getNotes() {
        return notes;
    }
}
