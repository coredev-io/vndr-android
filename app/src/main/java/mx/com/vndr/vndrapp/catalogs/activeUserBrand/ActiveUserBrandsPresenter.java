package mx.com.vndr.vndrapp.catalogs.activeUserBrand;

import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public class ActiveUserBrandsPresenter implements ActiveUserBrandsInteractor.OnRequestFinish {
    private ActiveUserBrandView view;
    private ActiveUserBrandsInteractor interactor;
    private CatalogActivity activity;

    public ActiveUserBrandsPresenter(ActiveUserBrandView view, ActiveUserBrandsInteractor interactor, CatalogActivity activity) {
        this.view = view;
        this.interactor = interactor;
        this.activity = activity;

        if (activity.equals(CatalogActivity.CATALOG_ACTIVITY)){
            view.setToolbarTitle("Catalogo nuevo");
        } else {
            view.setToolbarTitle("Producto en catálgo");
        }


    }

    public void getActiveUserBrands(){
        interactor.getData(this);
        view.showProgress();
    }

    @Override
    public void onSuccess(CheckedItemAdapter adapter) {
        view.setAdapter(adapter);
        view.dismissProgress();
    }

    @Override
    public void onError(String error) {
        view.dismissProgress();
        view.showError(error);
    }

    @Override
    public void onEmptyBrands() {
        view.showEmptyBrands();
    }

    @Override
    public void onItemClick(Brand brand) {
        view.navigateToCatalogList(brand,activity);
    }
}
