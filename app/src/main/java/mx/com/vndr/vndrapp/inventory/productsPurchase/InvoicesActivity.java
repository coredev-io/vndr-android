package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.api.inventory.Supplier;

public class InvoicesActivity extends AppCompatActivity implements VndrRequest.VndrResponse, PurchaseInvoceAdapter.OnPurchaseInvoceSelected  {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textView;
    Supplier supplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoices);
        setupUI();

        if (getIntent().hasExtra("supplier")){
            supplier = (Supplier) getIntent().getSerializableExtra("supplier");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getInvoices();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_invoices);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_invoices);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textView = findViewById(R.id.txt_empty_invoices);
        progressBar = findViewById(R.id.pb_invoices);
    }

    private void showLoader(){
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        recyclerView.setAdapter(null);
    }

    private void dismissLoader(){
        progressBar.setVisibility(View.GONE);
    }

    private void showMessageError(String messageError) {
        progressBar.setVisibility(View.GONE);
        textView.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
        textView.setText(messageError);
    }

    private void showData(){
        textView.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }


    /**
     * WS Data
     */
    private void getInvoices(){
        showLoader();
        String url = URL_PURCHASE_INVOICE;

        if (supplier != null) {
            url += "?supplier=" + supplier.getId();
        }

        new VndrRequest(Volley.newRequestQueue(this))
                .get(url, this);
    }

    @Override
    public void error(String message) {
        showMessageError(message);
    }

    @Override
    public void success(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONArray jsonArray = jsonObject.getJSONArray("invoices");

            if (jsonArray.length() == 0) {
                dismissLoader();
                showMessageError("No tienes facturas registradas");
                return;
            }

            List<PurchaseInvoice> purchaseInvoices = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                PurchaseInvoice purchaseInvoice = new PurchaseInvoice(
                        jsonArray.getJSONObject(i)
                );
                purchaseInvoices.add(purchaseInvoice);
            }
            recyclerView.setAdapter(
                    new PurchaseInvoceAdapter(purchaseInvoices, this)
                            .setOnShoppingOrderSelected(this)
            );
            dismissLoader();
            showData();

        } catch (JSONException e) {
            e.printStackTrace();
            showMessageError("Ocurrió un error al procesar la solicitud.");
        } catch (ParseException e) {
            e.printStackTrace();
            showMessageError("Ocurrió un error al procesar la solicitud.");
        }
    }


    @Override
    public void onSelected(PurchaseInvoice purchaseInvoice) {
        if (purchaseInvoice.getStatusCode().equals("I00")) {
            startActivity(
                    new Intent(this, CartRegisterPurchaseActivity.class)
                            .putExtra("purchase_invoice", purchaseInvoice)
            );
        } else {

            PurchaseInvoice.Selected.current.setPurchaseInvoice(purchaseInvoice);

            startActivity(
                    new Intent(this, PurchaseInvoiceDetailActivity.class)
            );
        }
    }
}