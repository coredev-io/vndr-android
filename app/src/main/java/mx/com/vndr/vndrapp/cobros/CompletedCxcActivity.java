package mx.com.vndr.vndrapp.cobros;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCCard;
import mx.com.vndr.vndrapp.api.cxc.CxCQuery;
import mx.com.vndr.vndrapp.api.cxc.CxCStatus;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;

import static android.view.View.GONE;

public class CompletedCxcActivity extends AppCompatActivity implements APIVndrCxc.CXCResponse, VndrListAdapter.OnCardItemSelected {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textView;
    ProgressBar progressBar;
    CxCQuery query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_cxc);
        toolbar = findViewById(R.id.tb_cxc_completed);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener((view -> onBackPressed()));

        recyclerView = findViewById(R.id.rv_completed_cxc);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textView = findViewById(R.id.txt_mssg_completed_cxc);
        textView.setVisibility(View.GONE);
        progressBar = findViewById(R.id.pb_completed_cxc);
        progressBar.setVisibility(GONE);

        query = (CxCQuery) getIntent().getSerializableExtra("query");

    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(null);
        getData(query);
    }

    private void getData(CxCQuery query){
        showProgress();
        APIVndrCxc apiCxc = new APIVndrCxc(Volley.newRequestQueue(this));

        switch (query){
            case GENERAL:
                apiCxc.getCompledtCxcList(this);
                break;
            case CUSTOMER:
                apiCxc.getCustomerCompledtCxcList(this, query.getQueryParam());
                break;
        }
    }


    @Override
    public void cxcSuccess(List<VndrList> list) {
        dismissProgress();
        VndrListAdapter adapter = new VndrListAdapter(list);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void cxcError(String messageError) {
        dismissProgress();
        showError(messageError);
    }

    @Override
    public void onCxCSelected(CxCCard cxc) {
        Intent intent = new Intent(this, DetailCxcActivity.class);
        intent.putExtra("id_cxc", cxc.getId());

        if (cxc.getStatus().equals(CxCStatus.QUEBRANTO))
            intent.putExtra("isStatusQuebranto", true);

        startActivity(intent);
    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(GONE);
    }

    private void showError(String messageError){
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
    }

    private void dismissError(){
        textView.setVisibility(GONE);
    }
}
