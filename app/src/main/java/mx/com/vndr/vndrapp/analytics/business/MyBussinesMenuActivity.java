package mx.com.vndr.vndrapp.analytics.business;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.PickPaymentMethodActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.analytics.business.MovementAnalyticsActivity.Query;
import mx.com.vndr.vndrapp.analytics.collections.StatusCollectionActivity;
import mx.com.vndr.vndrapp.api.cxc.Movement;
import mx.com.vndr.vndrapp.cobros.movements.MovementsActivity;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class MyBussinesMenuActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bussines_menu);
        setupUI();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_bussiness_menu);
        recyclerView = findViewById(R.id.rv_bussiness_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuBussinessOption1));
        optionList.add(new Option(options.get(0), options.get(1), () -> navigateToMovements(Query.BY_INCOME)));

        options = Arrays.asList(getResources().getStringArray(R.array.menuBussinessOption2));
        optionList.add(new Option(options.get(0), options.get(1), () -> navigateToMovements(Query.BY_EXPENSES)));

        options = Arrays.asList(getResources().getStringArray(R.array.menuBussinessOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::askForQueryType));
        /*
        options = Arrays.asList(getResources().getStringArray(R.array.menuBussinessOption4));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));
        */

        return optionList;
    }

    public void askForQueryType(){
        new MaterialDialog.Builder(this)
                .items("Resumen general","Movimientos por medio de pago")
                .itemsCallback((dialog, itemView, position, text) -> {
                    if (position == 0){
                        Query query = Query.BY_CASH;
                        query.setPaymentMethodCode(null);
                        navigateToMovements(query);
                    }

                    else
                        askForPaymentMethod();
                })
                .show();
    }

    public void navigateToMovements(Query query){
        startActivity(new Intent(this, MovementAnalyticsActivity.class).putExtra("query", query));
    }


    private void askForPaymentMethod(){
        startActivity(
                new Intent(
                        this,
                        PickPaymentMethodActivity.class
                ).putExtra("isAnalytics", true)
        );
    }
/*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9 && resultCode == RESULT_OK){
            Query query = Query.BY_CASH;
            query.setPaymentMethodCode(data.getStringExtra("code"));
            navigateToMovements(query);
        }
    }

 */


    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }
}