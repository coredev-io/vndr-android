package mx.com.vndr.vndrapp.util;

import android.os.Build;


import com.google.firebase.auth.FirebaseAuth;

import java.util.Map;

public class VndrSecure {

    public static void appendDeviceInfor(Map<String, String> stringMap){
        stringMap.put("os","Android");
        stringMap.put("osVersion", String.valueOf(android.os.Build.VERSION.SDK_INT));
        stringMap.put("phoneModel", android.os.Build.MODEL);
        stringMap.put("phoneManufacturer", Build.MANUFACTURER);
        stringMap.put("appInstanceId", FirebaseAuth.getInstance().getUid());
    }
}
