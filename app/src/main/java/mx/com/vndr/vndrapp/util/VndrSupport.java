package mx.com.vndr.vndrapp.util;

import android.content.Intent;
import android.net.Uri;

public class VndrSupport {

    public static Intent getEmailSupportIntent(){
        String[] addresses = new String[]{"soporte@vndr.com.mx"};
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, addresses);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Ayuda a usuarios vndr+");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Cuéntanos cómo podemos ayudarte.");
        return emailIntent;
    }
}
