package mx.com.vndr.vndrapp.api.inventory;

import android.os.Build;
import android.telephony.PhoneNumberUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Supplier implements Serializable {

    private String id;
    private String brandId;
    private String name;
    private String salesEmail;
    private String salesPhoneNumber;
    private String contactName;
    private String contactEmail;
    private String contactPhoneNumber;
    private String website;

    public Supplier() {

    }

    public Supplier(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getString("_id");
        this.brandId = jsonObject.getString("brand_id");
        this.name = jsonObject.getString("supplier_name");
        this.salesEmail = jsonObject.getString("sales_email");
        this.salesPhoneNumber = jsonObject.getString("phone");
        this.contactName = jsonObject.getString("contact_name");
        this.contactEmail = jsonObject.getString("contact_email");
        this.contactPhoneNumber = jsonObject.getString("contact_phone");
        this.website = jsonObject.getString("website");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalesEmail() {
        return salesEmail;
    }

    public void setSalesEmail(String salesEmail) {
        this.salesEmail = salesEmail;
    }

    public String getSalesPhoneNumber() {
        return salesPhoneNumber;
    }

    public String getSalesPhoneNumberFormatted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return PhoneNumberUtils.formatNumber(salesPhoneNumber,"MX");
        else
            return PhoneNumberUtils.formatNumber(salesPhoneNumber);
    }

    public void setSalesPhoneNumber(String salesPhoneNumber) {
        this.salesPhoneNumber = salesPhoneNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getContactPhoneNumber() {
        return contactPhoneNumber;
    }

    public void setContactPhoneNumber(String contactPhoneNumber) {
        this.contactPhoneNumber = contactPhoneNumber;
    }

    public String getInitials(){
        if (name.length() >= 2)
            return (String.valueOf(name.charAt(0)) + name.charAt(1)).toUpperCase();
        else
            return String.valueOf(name.charAt(0)).toUpperCase();
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Map<String, String> getParamsRequest(){
        Map<String, String> params = new HashMap<>();
        params.put("brand_id", brandId);
        params.put("supplier_name", name);
        params.put("sales_email", salesEmail);
        params.put("phone", salesPhoneNumber);
        params.put("website", website);
        params.put("contact_name", contactName);
        params.put("contact_email", contactEmail);
        params.put("contact_phone", contactPhoneNumber);
        return params;
    }

    public static List<Supplier> parseSuppliersList(JSONArray jsonArray) throws JSONException {
        List<Supplier> suppliers = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            suppliers.add(
                    new Supplier(
                            jsonArray.getJSONObject(i)
                    )
            );
        }

        return suppliers;
    }

    public enum Selected {
        current;

        private Supplier supplier;

        public Supplier getSupplier() {
            return supplier;
        }

        public void setSupplier(Supplier supplier) {
            this.supplier = supplier;
        }
    }
}
