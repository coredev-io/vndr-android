package mx.com.vndr.vndrapp.customviews.VndrList;

import mx.com.vndr.vndrapp.api.analytics.Invoice;
import mx.com.vndr.vndrapp.api.analytics.PurchaseOrder;
import mx.com.vndr.vndrapp.api.cxc.CxCCard;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.customviews.VndrPieChart;

public class VndrList {

    private VndrListType type;
    private String sectionTitle;
    private SimpleList3 simpleList3;
    private SimpleList2 simpleList2;
    private CxCCard cxc;
    private CxCMovement movement;
    private String emptyMessage;
    private ProductAnalytic productAnalytic;
    private VndrPieChart chart;
    private PurchaseOrder purchaseOrder;
    private Invoice invoice;

    public VndrList(VndrPieChart chart) {
        this.type = VndrListType.CHART;
        this.chart = chart;
    }

    public VndrList(ProductAnalytic productAnalytic) {
        this.type = VndrListType.PRODUCT_ANALYTIC;
        this.productAnalytic = productAnalytic;
    }

    public VndrList(SimpleList3 simpleList3) {
        this.type = VndrListType.HEADER;
        this.simpleList3 = simpleList3;
    }

    public VndrList(SimpleList2 simpleList2) {
        this.type = VndrListType.SIMPLE_LIST2;
        this.simpleList2 = simpleList2;
    }

    public VndrList(String sectionTitle) {
        this.type = VndrListType.SECTION_TITLE;
        this.sectionTitle = sectionTitle;
    }

    public VndrList(CxCCard cxc) {
        this.type = VndrListType.CARD_DETAIL;
        this.cxc = cxc;
    }

    public VndrList(CxCMovement movement) {
        this.type = VndrListType.CARD_MOVEMENT;
        this.movement = movement;
    }

    public VndrList(String emptyMessage, boolean isEmptyMessage){
        this.type = VndrListType.EMPTY_LIST;
        this.emptyMessage = emptyMessage;
    }

    public VndrList(PurchaseOrder purchaseOrder) {
        this.type = VndrListType.PURCHASE_ORDER;
        this.purchaseOrder = purchaseOrder;
    }

    public VndrList(Invoice invoice) {
        this.type = VndrListType.INVOICE;
        this.invoice = invoice;
    }

    public VndrList(VndrListType type){
        this.type = VndrListType.SEPARATOR;
    }



    public VndrListType getType() {
        return type;
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public SimpleList3 getSimpleList3() {
        return simpleList3;
    }

    public CxCCard getCxc() {
        return cxc;
    }

    public CxCMovement getMovement() {
        return movement;
    }

    public String getEmptyMessage() {
        return emptyMessage;
    }

    public SimpleList2 getSimpleList2() {
        return simpleList2;
    }

    public ProductAnalytic getProductAnalytic() {
        return productAnalytic;
    }

    public VndrPieChart getChart() {
        return chart;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public Invoice getInvoice() {
        return invoice;
    }
}