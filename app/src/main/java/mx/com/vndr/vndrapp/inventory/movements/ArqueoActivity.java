package mx.com.vndr.vndrapp.inventory.movements;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.NumberFormat;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.TicketType;
import mx.com.vndr.vndrapp.models.Product;

public class ArqueoActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView brandName, catalog, productName, sku;
    TextView size, color, descripcion;
    ProgressBar progressBar;
    TextView availableStock, inventoryAdjustment;
    EditText currentStock;

    Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_arqueo);
        product = (Product) getIntent().getSerializableExtra("product");
        setUpUI();
    }

    private void setUpUI(){
        brandName = findViewById(R.id.txt_brand_name_arqueo);
        productName = findViewById(R.id.product_name_arqueo);
        catalog = findViewById(R.id.txt_catalog_name_arqueo);
        sku = findViewById(R.id.sku_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        progressBar = findViewById(R.id.pb_inventory_arqueo);
        availableStock = findViewById(R.id.available_stock);
        inventoryAdjustment = findViewById(R.id.inventory_adjustment);
        currentStock = findViewById(R.id.current_stock);

        brandName.setText(product.getBrandName());
        catalog.setText(product.getCatalog().getCatalogName());
        productName.setText(product.getProductName());
        sku.setText(product.getProduct_key());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_inventory_arqueo);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        currentStock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int stock = currentStock.getText().toString().isEmpty() ? 0 : Integer.parseInt(currentStock.getText().toString());

                inventoryAdjustment.setText(
                        String.valueOf(
                                stock - Integer.parseInt(availableStock.getText().toString())
                        )
                );
            }
        });

        availableStock.setText(String.valueOf(product.getInventory().getCurrentStock()));
        inventoryAdjustment.setText(
                String.valueOf(
                        Integer.parseInt(currentStock.getText().toString()) - Integer.parseInt(availableStock.getText().toString())
                )
        );

    }

    public void onClick(View view){
        startActivityForResult(new Intent(this, InventoryMovementReviewActivity.class)
                        .putExtra("product", product)
                        .putExtra("count", Integer.parseInt(currentStock.getText().toString()))
                        //.putExtra("count", Integer.parseInt(currentStock.getText().toString()))
                        .putExtra("isArqueo", true)
                        .putExtra("type", new TicketType("null","Ajuste por arqueo"))
                , 0
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 0) {
            this.finish();
        }
    }
}