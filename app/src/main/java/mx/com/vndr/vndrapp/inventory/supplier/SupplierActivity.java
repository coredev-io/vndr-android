package mx.com.vndr.vndrapp.inventory.supplier;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APISupplier;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.customviews.DropdownTextView;
import mx.com.vndr.vndrapp.customviews.EditTextPhoneNumber;
import mx.com.vndr.vndrapp.util.Dialogs;

public class SupplierActivity extends AppCompatActivity implements TextWatcher, APISupplier.OnSupplierResponse {

    Toolbar toolbar;
    Button button;
    TextInputLayout textInputLayoutName;
    TextInputLayout textInputLayoutEmail;
    TextInputLayout textInputLayoutPhoneNumber;
    TextInputLayout textInputLayoutCellphoneNumber;
    EditText editTextName;
    EditText editTextEmail;
    EditTextPhoneNumber editTextPhoneNumber;
    EditText editTextWeb;
    EditText editTextContactName;
    EditText editTextContactEmail;
    EditTextPhoneNumber editTextContacCellPhone;
    ProgressBar progressBar;
    Supplier supplier;
    boolean editMode;
    boolean updateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier);
        setupUI();

        //  Mostrar objeto si se contiene
        if (getIntent().hasExtra("supplier")){
            supplier = (Supplier) getIntent().getSerializableExtra("supplier");
            displayData();
            disableFields();
            button.setEnabled(true);
            button.setText("Editar");
            editMode = true;
        }


    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_supplier);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        button = findViewById(R.id.btn_supplier);
        textInputLayoutName = findViewById(R.id.il_supplier_name);
        textInputLayoutEmail = findViewById(R.id.il_supplier_email);
        textInputLayoutPhoneNumber = findViewById(R.id.il_supplier_phone);
        textInputLayoutCellphoneNumber = findViewById(R.id.il_contact_supplier_cell_phone);
        editTextName = findViewById(R.id.supplier_name);
        editTextEmail = findViewById(R.id.supplier_email);
        editTextPhoneNumber = findViewById(R.id.supplier_phone_number);
        editTextWeb = findViewById(R.id.supplier_web);
        editTextContactName = findViewById(R.id.contact_supplier_name);
        editTextContactEmail = findViewById(R.id.contact_supplier_email);
        editTextContacCellPhone = findViewById(R.id.contact_supplier_cell_phone);
        progressBar = findViewById(R.id.pb_supplier);


        button.setEnabled(false);

        editTextName.addTextChangedListener(this);
        editTextEmail.addTextChangedListener(this);
        editTextPhoneNumber.addTextChangedListener(this);
        editTextContacCellPhone.addTextChangedListener(this);
    }

    public void onSupplierButtonClick(View view){

        if (editMode) {
            button.setText("Actualizar");
            enableFields();
            editMode = false;
            updateMode = true;
            return;
        }

        if (supplier == null)
            supplier = new Supplier();

        supplier.setBrandId("627ea866299e570018c59bd5");
        supplier.setName(editTextName.getText().toString());
        supplier.setSalesEmail(editTextEmail.getText().toString());
        supplier.setSalesPhoneNumber(editTextPhoneNumber.getRawPhoneNumber());
        supplier.setContactName(editTextContactName.getText().toString());
        supplier.setContactEmail(editTextContactEmail.getText().toString());
        supplier.setContactPhoneNumber(editTextContacCellPhone.getRawPhoneNumber());
        supplier.setWebsite(editTextWeb.getText().toString());

        showLoader();

        if (updateMode) {
            new APISupplier(Volley.newRequestQueue(this))
                    .updateSupplier(
                            supplier,
                            this
                    );
            return;
        }

        new APISupplier(Volley.newRequestQueue(this))
                .addSupplier(
                        supplier,
                        this
                );
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().isEmpty()){
            getCurrentTextInputLayout().setError("Este campo no puede estar vacío");
        } else
            getCurrentTextInputLayout().setError("");

        if (getCurrentTextInputLayout() == textInputLayoutEmail && !isValidaEmail()){
            textInputLayoutEmail.setError("Debes ingresar un email válido.");
        }


        if (areEmptyRequiredFields() || !isValidaEmail()){
            button.setEnabled(false);
        } else
            button.setEnabled(true);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private TextInputLayout getCurrentTextInputLayout(){
        if (editTextName.isFocused())
            return textInputLayoutName;
        else if (editTextEmail.isFocused())
            return textInputLayoutEmail;
        else if (editTextPhoneNumber.isFocused())
            return textInputLayoutPhoneNumber;
        else if (editTextContacCellPhone.isFocused())
            return textInputLayoutCellphoneNumber;
        else
            return new TextInputLayout(this);
    }

    private boolean areEmptyRequiredFields(){
        if (editTextName.getText().toString().isEmpty())
            return true;
        else if (editTextEmail.getText().toString().isEmpty())
            return true;
        else if (editTextPhoneNumber.getText().toString().isEmpty())
            return true;
        else if (editTextContacCellPhone.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private boolean isValidaEmail(){
        if (editTextEmail.getText().toString().contains("@"))
            return true;
        else
            return false;
    }

    @Override
    public void onSuccess() {
        dismissLoader();
        String message = (updateMode) ? "El proveedor se actualizó correctamente" : "Se creó el proveedor correctamente";
        Dialogs.showAlert(this, message, (dialog, which) -> {
            this.setResult(RESULT_OK,new Intent().putExtra("supplier", supplier));
            this.finish();
        });
    }

    @Override
    public void onError(String messageError) {
        dismissLoader();
        Dialogs.showAlert(this, messageError);
    }

    private void showLoader(){
        button.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        disableFields();
    }

    private void dismissLoader(){
        button.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        enableFields();
    }

    private void disableFields(){
        editTextName.setEnabled(false);
        editTextEmail.setEnabled(false);
        editTextPhoneNumber.setEnabled(false);
        editTextWeb.setEnabled(false);
        editTextContactName.setEnabled(false);
        editTextContactEmail.setEnabled(false);
        editTextContacCellPhone.setEnabled(false);
    }

    private void enableFields(){
        editTextName.setEnabled(true);
        editTextEmail.setEnabled(true);
        editTextPhoneNumber.setEnabled(true);
        editTextWeb.setEnabled(true);
        editTextContactName.setEnabled(true);
        editTextContactEmail.setEnabled(true);
        editTextContacCellPhone.setEnabled(true);
    }

    private void displayData(){
        editTextName.setText(supplier.getName());
        editTextEmail.setText(supplier.getSalesEmail());
        editTextPhoneNumber.setText(supplier.getSalesPhoneNumber());
        editTextContactName.setText(supplier.getContactName());
        editTextContactEmail.setText(supplier.getContactEmail());
        editTextContacCellPhone.setText(supplier.getContactPhoneNumber());
        editTextWeb.setText(supplier.getWebsite());
    }

}
