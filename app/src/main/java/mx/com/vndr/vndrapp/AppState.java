package mx.com.vndr.vndrapp;

import android.content.Context;
import android.content.SharedPreferences;

public enum AppState {
    NEW, ENROLLED, NOT_LICENSED;

    private static final String TAG = "AppState";
    public static AppState getAppState(Context context){
        SharedPreferences preferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        return AppState.valueOf(preferences.getString(TAG, NEW.toString()));
    }

    public static void setAppState(Context context, AppState appState){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putString(TAG, appState.toString())
                .apply();
    }
}
