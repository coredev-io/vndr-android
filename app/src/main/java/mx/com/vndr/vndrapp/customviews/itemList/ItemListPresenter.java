package mx.com.vndr.vndrapp.customviews.itemList;

import java.util.List;

public class ItemListPresenter {

    private List<Item> itemList;

    public ItemListPresenter(List<Item> itemList) {
        this.itemList = itemList;
    }

    public void onBindItemRowViewAtPosition(ItemRowView rowView, int position){
        Item item = itemList.get(position);
        rowView.setTitle(item.getTitle());
        rowView.setDescription(item.getSubtitle());
        rowView.setOnClickListener(item.getListener());
    }

    public int getItemCount(){
        return  itemList.size();
    }
}
