package mx.com.vndr.vndrapp.registro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import mx.com.vndr.vndrapp.R;

public class PrivacyAndOpActivity extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_and_op);
        toolbar = findViewById(R.id.tb_privacy_op);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        customUI();
    }

    private void customUI(){
        boolean readMode = getIntent().getBooleanExtra("readMode", false);
        if (readMode){
            Button button = findViewById(R.id.btn_privacy_op);
            button.setText("Atrás");
        }
    }

    public void navigateToNoticePrivacy(View view){
        startActivity(new Intent(this, NoticePrivacyActivity.class));
    }

    public void navigateToTyC(View view){
        startActivity(new Intent(this, TyCActivity.class));
    }

    public void onClickAcceptTyC(View view){
        this.setResult(RESULT_OK);
        this.finish();;
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_CANCELED);
        this.finish();
    }
}
