package mx.com.vndr.vndrapp.inventory.rules;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_INVENTORY;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

import me.abhinay.input.CurrencyEditText;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.InventoryRules;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.util.Dialogs;

public class InventoryRulesActivity extends AppCompatActivity implements  TextWatcher, View.OnTouchListener {

    Toolbar toolbar;
    TextView brandName, catalog, productName, sku;
    TextView size, color, descripcion;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    Button button;

    TextView availableStock, inventoryAdjustment, acquisitionCostInitial, acquisitionTotalInitial;
    EditText currentStock, optimalConfig, buyBack, minimalConfig;
    CurrencyEditText acquisitionCost, iva, acquisitionTotal;


    Product product;
    InventoryRules inventoryRules;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_rules);

        product = (Product) getIntent().getSerializableExtra("product");
        setUpUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    private void setUpUI(){
        brandName = findViewById(R.id.txt_brand_name_rules);
        productName = findViewById(R.id.product_name_rules);
        catalog = findViewById(R.id.txt_catalog_name_rules);
        sku = findViewById(R.id.sku_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        progressBar = findViewById(R.id.pb_inventory_rules);
        linearLayout = findViewById(R.id.ll_inventory_rules);
        button = findViewById(R.id.btn_inventory_rules);
        availableStock = findViewById(R.id.available_stock);
        inventoryAdjustment = findViewById(R.id.inventory_adjustment);
        acquisitionCostInitial = findViewById(R.id.acquisition_cost_initial);
        acquisitionTotalInitial = findViewById(R.id.acquisition_total_initial);
        currentStock = findViewById(R.id.current_stock);
        optimalConfig = findViewById(R.id.config_optimum);
        buyBack = findViewById(R.id.config_buy_back);
        minimalConfig = findViewById(R.id.config_minimal);
        acquisitionCost = findViewById(R.id.acquisition_cost);
        iva = findViewById(R.id.iva);
        acquisitionTotal = findViewById(R.id.acquisition_total);

        brandName.setText(product.getBrandName());
        catalog.setText(product.getCatalog().getCatalogName());
        productName.setText(product.getProductName());
        sku.setText(product.getProduct_key());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_inventory_rules);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        acquisitionCost.setCurrency("$");
        iva.setCurrency("$");
        acquisitionTotal.setCurrency("$");

        currentStock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int stock = currentStock.getText().toString().isEmpty() ? 0 : Integer.parseInt(currentStock.getText().toString());

                inventoryAdjustment.setText(
                        String.valueOf(
                                stock - Integer.parseInt(availableStock.getText().toString())
                        )
                );
                acquisitionCostInitial.setText(NumberFormat.getCurrencyInstance().format(
                        acquisitionCost.getCleanDoubleValue() * stock
                ));
                acquisitionTotalInitial.setText(NumberFormat.getCurrencyInstance().format(
                        acquisitionTotal.getCleanDoubleValue() * stock
                ));

            }
        });

        acquisitionCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                acquisitionTotal.setText(NumberFormat.getCurrencyInstance().format(acquisitionCost.getCleanDoubleValue() + iva.getCleanDoubleValue()));
                acquisitionCostInitial.setText(NumberFormat.getCurrencyInstance().format(
                        acquisitionCost.getCleanDoubleValue() * Integer.parseInt(currentStock.getText().toString())
                ));
                acquisitionTotalInitial.setText(NumberFormat.getCurrencyInstance().format(
                        acquisitionTotal.getCleanDoubleValue() * Integer.parseInt(currentStock.getText().toString())
                ));
            }
        });

        iva.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                acquisitionTotal.setText(NumberFormat.getCurrencyInstance().format(acquisitionCost.getCleanDoubleValue() + iva.getCleanDoubleValue()));
                acquisitionTotalInitial.setText(NumberFormat.getCurrencyInstance().format(
                        acquisitionTotal.getCleanDoubleValue() * Integer.parseInt(currentStock.getText().toString())
                ));
            }
        });

        optimalConfig.addTextChangedListener(this);
        buyBack.addTextChangedListener(this);
        minimalConfig.addTextChangedListener(this);

        currentStock.setOnTouchListener(this);
        buyBack.setOnTouchListener(this);
        minimalConfig.setOnTouchListener(this);
        optimalConfig.setOnTouchListener(this);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.GONE);
    }

    private void dismisLoading(){
        progressBar.setVisibility(View.GONE);
        linearLayout.setVisibility(View.VISIBLE);
    }

    private void getData(){
        showLoading();
        new VndrRequest(Volley.newRequestQueue(this))
                .get( URL_INVENTORY + "/" + product.getIdProduct(), new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismisLoading();
                        Dialogs.showAlert(InventoryRulesActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismisLoading();
                        try {
                            inventoryRules = new InventoryRules(new JSONObject(response).getJSONObject("inventory"));
                            fillData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Dialogs.showAlert(InventoryRulesActivity.this, "Ocurrió un problema al procesar la solicitud");
                        }
                    }
                });
    }

    private void fillData(){

        availableStock.setText(String.valueOf(inventoryRules.getInitialCalculatedStock()));
        optimalConfig.setText(String.valueOf(inventoryRules.getOptimalConfiguration()));
        buyBack.setText(String.valueOf(inventoryRules.getBuyBackConfiguration()));
        minimalConfig.setText(String.valueOf(inventoryRules.getMinimunConfiguration()));
        acquisitionCost.setText(NumberFormat.getCurrencyInstance().format(inventoryRules.getAcquisitionCost()));
        iva.setText(NumberFormat.getCurrencyInstance().format(inventoryRules.getIva()));
        acquisitionTotal.setEnabled(false);


        if (inventoryRules.isRegisterIsValid()) {
            currentStock.setText(String.valueOf(inventoryRules.getInitialStock()));
            button.setText("Actualizar");
            inventoryAdjustment.setText(String.valueOf(inventoryRules.getInitialAdjustedStock()));
            currentStock.setEnabled(false);
            acquisitionCost.setEnabled(false);
            iva.setEnabled(false);
            acquisitionTotal.setText(NumberFormat.getCurrencyInstance().format(inventoryRules.getAcquisitionTotal()));
            acquisitionCostInitial.setText(NumberFormat.getCurrencyInstance().format(inventoryRules.getAcquisitionCostInitial()));
            acquisitionTotalInitial.setText(NumberFormat.getCurrencyInstance().format(inventoryRules.getAcquisitionTotalInitial()));

        } else {
            button.setText("Dar de alta");
            currentStock.setText(String.valueOf(0));
            acquisitionTotal.setText(NumberFormat.getCurrencyInstance().format(acquisitionCost.getCleanDoubleValue() + iva.getCleanDoubleValue()));
            acquisitionCostInitial.setText(NumberFormat.getCurrencyInstance().format(
                    acquisitionCost.getCleanDoubleValue() * Integer.parseInt(currentStock.getText().toString())
            ));
            acquisitionTotalInitial.setText(NumberFormat.getCurrencyInstance().format(
                    acquisitionTotal.getCleanDoubleValue() * Integer.parseInt(currentStock.getText().toString())
            ));
            inventoryAdjustment.setText(
                    String.valueOf(
                            Integer.parseInt(currentStock.getText().toString()) - Integer.parseInt(availableStock.getText().toString())
                    )
            );
        }
    }

    public void onClickButton(View view){


        if (inventoryRules.isRegisterIsValid()) {
            showLoading();
            patch();
        } else {
            Dialogs.showAlert(this,
                    "Las reglas de gestión pueden cambiarse en cualquier momento \n" +
                            "El inventario cambiará con la opción o con ajustes posteriores \n" +
                            "El valor unitario actual aparecerá en la pantalla de consulta de inventario",
                    (materialDialog, dialogAction) -> {
                        showLoading();
                        alta();
                    });
        }
    }


    private void alta(){

        Map<String, String> params = new HashMap<>();
        params.put("units", currentStock.getText().toString());
        params.put("acquisition_cost", String.valueOf(acquisitionCost.getCleanDoubleValue()));
        params.put("acquisition_iva", String.valueOf(iva.getCleanDoubleValue()));
        params.put("acquisition_total", String.valueOf(acquisitionTotal.getCleanDoubleValue()));
        params.put("acquisition_cost_initial", acquisitionCostInitial.getText().toString().replace("$","").replace(",", ""));
        params.put("acquisition_total_initial", acquisitionTotalInitial.getText().toString().replace("$","").replace(",", ""));
        params.put("config_optimum", optimalConfig.getText().toString());
        params.put("config_buy_back", buyBack.getText().toString());
        params.put("config_minimal", minimalConfig.getText().toString());


        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .post( URL_INVENTORY + "/" + product.getIdProduct(), new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismisLoading();
                        Dialogs.showAlert(InventoryRulesActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismisLoading();
                        try {
                            JSONObject jsonObject = new JSONObject(response).getJSONObject("ticket");
                            startActivityForResult(new Intent(InventoryRulesActivity.this, InventorySuccessActivity.class)
                                            .putExtra("ticket", jsonObject.getInt("ticket"))
                                    , 0);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    private void patch(){

        Map<String, String> params = new HashMap<>();
        params.put("config_optimum", optimalConfig.getText().toString());
        params.put("config_buy_back", buyBack.getText().toString());
        params.put("config_minimal", minimalConfig.getText().toString());


        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .patch( URL_INVENTORY + "/" + product.getIdProduct(), new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismisLoading();
                        Dialogs.showAlert(InventoryRulesActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismisLoading();
                        Dialogs.showAlert(InventoryRulesActivity.this, "Las reglas de inventario se actualizaron correctamente.", (materialDialog, dialogAction) -> {
                            InventoryRulesActivity.this.finish();
                        });
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            this.finish();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        boolean isEnabled =
                !optimalConfig.getText().toString().isEmpty() &&
                !buyBack.getText().toString().isEmpty() &&
                !minimalConfig.getText().toString().isEmpty();
        button.setEnabled(isEnabled);

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        EditText editText = (EditText) view;
        editText.onTouchEvent(motionEvent);
        editText.setSelection(editText.getText().length());
        return true;
    }
}