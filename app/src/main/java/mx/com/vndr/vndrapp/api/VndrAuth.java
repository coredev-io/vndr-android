package mx.com.vndr.vndrapp.api;

import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.IntConsumer;

import mx.com.vndr.vndrapp.AppState;
import mx.com.vndr.vndrapp.registro.SignUpData;
import mx.com.vndr.vndrapp.util.VndrSecure;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_DELETE_SESSION;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_USER;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_VALIDATE_DATA_USER;

public class VndrAuth {

    private static final String TAG = "VndrAuth";

    private static VndrAuth instance;

    private FirebaseAuth firebaseAuth;
    private VndrUser vndrUser;
    private RequestQueue requestQueue;
    private CreateUserResult onCompleteCreateUser;

    private SignUpData signUpData;

    private VndrAuth(){
        firebaseAuth = FirebaseAuth.getInstance();
        requestQueue = Volley.newRequestQueue(firebaseAuth.getApp().getApplicationContext());
    }

    public static VndrAuth getInstance(){
        synchronized (VndrAuth.class) {
            if(instance == null){
                instance = new VndrAuth();
            }
        }
        return instance;
    }

    /**
     * Crea un nuevo usuario en la plataforma de vndr+ siguiendo el flujo:
     * 1. Crea usuario en firebase
     * 2. Obtiene token de sesión
     * 3. Se crea usuario en BD
     * 4. Se crea sesión activa
     * @param callback
     */
    public void createUserWithSignUpData(CreateUserResult callback){
        onCompleteCreateUser = callback;
        signUpData = SignUpData.getInstance();

        if (signUpData == null){
            callback.onFailureCreateUser("Ocurrió un error inesperado");
            return;
        }

        // crear usuario en firebase

        firebaseAuth.createUserWithEmailAndPassword(signUpData.getEmail(), signUpData.getPwd())
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        Log.i(TAG, "Firebase user created successfull");

                        vndrUser = new VndrUser();
                        vndrUser.sendEmailVerification();
                        vndrUser.updateUserProfile(signUpData.getName() + " " + signUpData.getLastName());
                        vndrUser.getSessionTokenFromFirebase(new VndrUser.VndrUserCallback() {
                            @Override
                            public void onVndrUserCallbackSuccess() {
                                createVndrUser();
                            }

                            @Override
                            public void onVndrUserCallbackError(String messageError) {
                                onCompleteCreateUser.onFailureCreateUser(messageError);
                            }
                        });
                    } else {
                        Log.e(TAG, "Failed to create new firebase user. Reason: ");
                        Log.e(TAG, task.getException().getMessage());
                        onCompleteCreateUser.onFailureCreateUser(task.getException().getMessage());
                    }
                });

    }

    private void createVndrUser(){
        Map<String, String> params = new HashMap<>();

        params.put("name", signUpData.getName());
        params.put("lastName", signUpData.getLastName());
        params.put("mail", signUpData.getEmail());
        params.put("mobilePhone", signUpData.getMobilePhone());
        params.put("zipCode", signUpData.getZipCode());
        VndrSecure.appendDeviceInfor(params);

        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .post(URL_USER, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        signUpData = null;
                        onCompleteCreateUser.onFailureCreateUser(message);
                    }

                    @Override
                    public void success(String response) {
                        vndrUser.setEmail(signUpData.getEmail());
                        signUpData = null;
                        vndrUser.createVndrSession(new VndrUser.VndrUserCallback() {
                            @Override
                            public void onVndrUserCallbackSuccess() {
                                onCompleteCreateUser.onUserCreatedSuccessfull();
                            }

                            @Override
                            public void onVndrUserCallbackError(String messageError) {
                                onCompleteCreateUser.onFailureCreateUser(messageError);
                                //  Aquí debería mandar al login
                            }
                        });
                    }
                });
    }

    /**
     * Inicio de sesión en la plataforma vndr+
     * @param email
     * @param pwd
     * @param loginResult
     */
    public void login(String email, String pwd, LoginResult loginResult){
        firebaseAuth.signInWithEmailAndPassword(email, pwd).addOnCompleteListener(task -> {
            if (task.isSuccessful()){

                if (!firebaseAuth.getCurrentUser().isEmailVerified()){
                    loginResult.emailNotVerified();
                    return;
                }

                vndrUser = new VndrUser();
                vndrUser.createVndrSession(new VndrUser.VndrUserCallback() {
                    @Override
                    public void onVndrUserCallbackSuccess() {
                        loginResult.loginSuccess();
                    }

                    @Override
                    public void callbackErrorStatusCode(int statusCode) {
                        if (statusCode == 403)
                            loginResult.loginBlocked();
                    }

                    @Override
                    public void onVndrUserCallbackLicenseError(String message, VndrMembership.LicenseVndrStatus status) {
                        loginResult.loginLicenseError(message, status);
                    }

                    @Override
                    public void onVndrUserCallbackError(String messageError) {
                        loginResult.loginError(messageError);
                    }
                });
            } else {
                try {
                    throw task.getException();
                } catch(FirebaseAuthInvalidUserException e) {
                    AppState.setAppState(firebaseAuth.getApp().getApplicationContext(), AppState.NEW);
                    VndrUser.setEmail("",firebaseAuth.getApp().getApplicationContext());
                    VndrUser.setName("", firebaseAuth.getApp().getApplicationContext());
                    loginResult.loginError("El email o la contraseña no son válidos, por favor inténtalo de nuevo. Asegúrate que ambos sean correctos.");
                } catch(FirebaseAuthInvalidCredentialsException e) {
                    AppState.setAppState(firebaseAuth.getApp().getApplicationContext(), AppState.NEW);
                    VndrUser.setEmail("",firebaseAuth.getApp().getApplicationContext());
                    VndrUser.setName("", firebaseAuth.getApp().getApplicationContext());
                    loginResult.loginError("El email o la contraseña no son válidos, por favor inténtalo de nuevo. Asegúrate que ambos sean correctos.");
                } catch(Exception e) {
                    FirebaseCrashlytics.getInstance().recordException(e);
                    loginResult.loginError("Ocurrió un error inesperado, por favor inténtalo de nuevo");
                }

            }
        });
    }

    /**
     * Cerrar sesión en vndr+
     * @param result
     */
    public void logout(VndrAuthResult result){
        new VndrRequest(requestQueue)
                .post(URL_DELETE_SESSION, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        if (result != null)
                            result.onVndrAuthError(message);
                    }

                    @Override
                    public void success(String response) {
                        firebaseAuth.signOut();
                        vndrUser = null;
                        if (result != null)
                            result.onVndrAuthSuccess();

                    }
                });
    }

    /**
     * Obtiene el usuario activo en la aplicación.
     * @return VndrUser
     */
    public VndrUser getCurrentUser(){
        if (vndrUser == null)
            return new VndrUser();
        else
            return vndrUser;
    }

    /**
     * Valida la información del usaurio
     * si la validación es exitosa envía un email
     * al usuario para resetear la contraseña.
     * @param mail
     * @param phone
     * @param zipCode
     * @param callback
     */
    public void sendPwdResetEmail(String mail, String phone, String zipCode, VndrAuthResult callback){
        //  Validate data
        Map<String, String> params = new HashMap<>();
        params.put("mail",mail);
        params.put("mobilePhone", phone);
        params.put("zipCode",zipCode);

        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .post(URL_VALIDATE_DATA_USER, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.onVndrAuthError(message);
                    }

                    @Override
                    public void success(String response) {
                        firebaseAuth.sendPasswordResetEmail(mail);
                        callback.onVndrAuthSuccess();
                    }
                });
    }

    public interface CreateUserResult{
        void onUserCreatedSuccessfull();
        void onFailureCreateUser(String messageError);
    }

    public interface LoginResult{
        void loginSuccess();
        void loginError(String message);
        void loginLicenseError(String message, VndrMembership.LicenseVndrStatus status);
        void loginBlocked();
        void emailNotVerified();
    }

    public interface VndrAuthResult{
        void onVndrAuthError(String messageError);
        void onVndrAuthSuccess();
    }

}
