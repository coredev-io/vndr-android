package mx.com.vndr.vndrapp.registro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;

import mx.com.vndr.vndrapp.R;

public class NoticePrivacyActivity extends AppCompatActivity {

    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_privacy);

        toolbar = findViewById(R.id.tb_notice_privacy);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    public void onClick(View view){
        onBackPressed();
    }
}
