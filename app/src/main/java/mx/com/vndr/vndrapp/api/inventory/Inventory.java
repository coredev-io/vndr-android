package mx.com.vndr.vndrapp.api.inventory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Inventory implements Serializable {

    int currentStock = 0;
    int availableStock = 0;
    int pendingStock = 0;
    int orderPendingIncomplete = 0;
    int orderPendingTotal = 0;
    int orderShippingPersonal = 0;
    int orderShippingCourier = 0;
    int currentCalculatedStock = 0;
    int optimalConfiguration = 0;
    int buyBackConfiguration = 0;
    int minimunConfiguration = 0;
    int pendingOrder = 0;
    double acquisitionCost = 0;

    public Inventory(JSONObject jsonObject) throws JSONException {
        currentStock = jsonObject.getInt("current_stock");
        availableStock = jsonObject.getInt("available_stock");
        orderPendingTotal = jsonObject.getInt("order_pending_total");
        orderPendingIncomplete = jsonObject.getInt("order_pending_incomplete");
        orderShippingPersonal = jsonObject.getInt("order_shipping_personal");
        orderShippingCourier = jsonObject.getInt("order_shipping_courier");
        pendingStock = jsonObject.getInt("pending_stock_in");
        currentCalculatedStock = jsonObject.getInt("current_calculated_stock");
        optimalConfiguration = jsonObject.getInt("config_optimum");
        buyBackConfiguration = jsonObject.getInt("config_buy_back");
        minimunConfiguration = jsonObject.getInt("config_minimal");
        pendingOrder = jsonObject.getInt("pending_order");

        if (jsonObject.has("acquisition_cost")){
            acquisitionCost = jsonObject.getDouble("acquisition_cost");
        }
    }

    public int getCurrentStock() {
        return currentStock;
    }

    public int getAvailableStock() {
        return availableStock;
    }

    public int getPendingStock() {
        return pendingStock;
    }

    public int getOrderPendingIncomplete() {
        return orderPendingIncomplete;
    }

    public int getorderPendingTotal() {
        return orderPendingTotal;
    }

    public int getOrderShippingPersonal() {
        return orderShippingPersonal;
    }

    public int getOrderShippingCourier() {
        return orderShippingCourier;
    }

    public int getCurrentCalculatedStock() {
        return currentCalculatedStock;
    }

    public int getOptimalConfiguration() {
        return optimalConfiguration;
    }

    public int getBuyBackConfiguration() {
        return buyBackConfiguration;
    }

    public int getMinimunConfiguration() {
        return minimunConfiguration;
    }

    public int getPendingOrder() {
        return pendingOrder;
    }

    public double getAcquisitionCost() {
        return acquisitionCost;
    }
}
