package mx.com.vndr.vndrapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrMembership;
import mx.com.vndr.vndrapp.registro.SignUpActivity;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";

    private final int SPLASH_TIME = 1000;
    private final int SIGNUP_INTENT = 1;

    private CountDownTimer timer;

    private LinearLayout linearLayoutRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        linearLayoutRegistro = findViewById(R.id.linearLayoutRegistro);

        VndrMembership.getInstance();

        timer = new CountDownTimer(SPLASH_TIME,100){

            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {
                if (AppState.getAppState(SplashActivity.this).equals(AppState.NEW)){
                    linearLayoutRegistro.setVisibility(View.VISIBLE);
                } else {
                    navigateToSignInActivity(linearLayoutRegistro);
                }
            }
        };
        timer.start();
    }

    public void navigateToSignInActivity(View view){
        startActivity(new Intent(SplashActivity.this, SignInActivity.class));
        this.finish();
    }

    public void navigateToSignUpActivity(View v){
        startActivityForResult(new Intent(SplashActivity.this, SignUpActivity.class), SIGNUP_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SIGNUP_INTENT && resultCode == RESULT_OK){
            this.finish();
        }
    }
}
