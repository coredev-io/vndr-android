package mx.com.vndr.vndrapp.analytics.inventory;

import static android.view.View.GONE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import java.util.Calendar;
import java.util.Date;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.TicketMovementActivity;
import mx.com.vndr.vndrapp.api.analytics.APIPurchase;
import mx.com.vndr.vndrapp.api.analytics.APIShoppingSummary;
import mx.com.vndr.vndrapp.api.analytics.Invoice;
import mx.com.vndr.vndrapp.api.analytics.PurchaseOrder;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.PODetailActivity;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class ShoppingSummaryActivity extends AppCompatActivity implements VndrDatePicker.OnSelectedDate, APIShoppingSummary.OnShoppingSummaryResponse, TabLayout.OnTabSelectedListener, VndrListAdapter.OnCardItemSelected {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textViewDateInit;
    TextView textViewDateEnd;
    TextView textViewError;
    ProgressBar progressBar;
    Date minDate;
    Date maxDate;
    boolean isRequestminDate;
    Query query;
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_summary);
        setupUI();
        setCurrentDate();
        query = (Query) getIntent().getSerializableExtra("query");

        if (query.equals(Query.BY_SUPPLIER)){
            tabLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    private void getData(){
        showProgress();
        recyclerView.setAdapter(null);
        if (query.equals(Query.PRODUCT_BY_SUPPLIER)){
            new APIShoppingSummary(Volley.newRequestQueue(this))
                    .getProductsBySupplierResume(this, minDate, maxDate);
        } else if (query.equals(Query.BY_BRAND)){
            new APIShoppingSummary(Volley.newRequestQueue(this))
                    .getResumeByBrand(this, minDate, maxDate);
        } else {
            new APIShoppingSummary(Volley.newRequestQueue(this))
                    .getShoppingSummary(this, minDate, maxDate, query);
        }


    }


    private void setCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date());

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);

        maxDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH, 1); //Set first day of month

        minDate = calendar.getTime();

        //  Display on UI
        textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
        textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
    }

    private void checkValidDateRange(){
        //  Previene que la fecha inicial sea mayor a la fecha final
        //  En dado caso fecha final se pone igual a la inicial
        if (minDate.compareTo(maxDate) > 0){
            maxDate = minDate;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
    }

    //  UI Methods
    private void setupUI(){
        toolbar = findViewById(R.id.tb_shopping_orders_resume);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_shopping_orders_resume);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewDateInit = findViewById(R.id.txt_shopping_orders_date_init);
        textViewDateInit.setOnClickListener(view -> {
            isRequestminDate = true;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewDateEnd = findViewById(R.id.txt_shopping_orders_date_end);
        textViewDateEnd.setOnClickListener(view -> {
            isRequestminDate = false;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(maxDate)
                    .setMinDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewError = findViewById(R.id.txt_shopping_orders_resume_error);
        textViewError.setVisibility(GONE);

        progressBar = findViewById(R.id.pb_shopping_orders_resume);
        progressBar.setVisibility(GONE);

        tabLayout = findViewById(R.id.tabl_query);
        tabLayout.addOnTabSelectedListener(this);

    }

    private void showProgress(){
        dismissError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissProgress();
        recyclerView.setAdapter(null);
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }

    private void dismissError(){
        textViewError.setVisibility(GONE);
    }

    @Override
    public void selectedDate(Date date) {
        if (isRequestminDate){
            minDate = date;
            textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
            checkValidDateRange();
        }
        else{
            maxDate = date;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
        getData();
    }

    @Override
    public void onSuccess(VndrListAdapter adapter) {
        adapter.setListener(this);
        dismissProgress();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {
        showError(error);
    }

    @Override
    public void onPurchaseOrderSelected(PurchaseOrder purchaseOrder) {

        Intent intent = (new Intent(this, PODetailActivity.class));
        intent.putExtra("isCompleted", purchaseOrder.isCompleted());
        intent.putExtra("isNotSended", purchaseOrder.isNotSended());
        intent.putExtra("id", purchaseOrder.getId());
        startActivity(intent);
    }

    @Override
    public void onInvoiceSelected(Invoice invoice) {
        /*
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
         */
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (tab.getPosition() == 1){
            query = Query.PRODUCT_BY_SUPPLIER;
        } else {
            query = Query.BY_SUPPLIER;
        }

        getData();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        recyclerView.setAdapter(null);

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public enum Query {
        ALL, BY_SUPPLIER, PRODUCT_BY_SUPPLIER, BY_BRAND
    }
}