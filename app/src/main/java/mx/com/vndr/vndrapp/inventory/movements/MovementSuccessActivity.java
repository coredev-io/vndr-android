package mx.com.vndr.vndrapp.inventory.movements;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mx.com.vndr.vndrapp.R;

public class MovementSuccessActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_success);


        textView = findViewById(R.id.txt_inventory_success);

        textView.setText(String.valueOf(getIntent().getIntExtra("ticket", 0)));
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_OK);
        this.finish();
    }

    public void onClickFinish(View view){
        this.setResult(RESULT_OK);
        this.finish();
    }
}