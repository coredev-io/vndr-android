package mx.com.vndr.vndrapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.customer.MainCustomerActivity;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.CustomerViewHolder> {


    List<Customer> items;
    OnCustomerSelected customerSelectedListener;


    public CustomerAdapter(List<Customer> items, OnCustomerSelected customerSelectedListener) {
        this.items = items;
        this.customerSelectedListener = customerSelectedListener;
    }

    @NonNull
    @Override
    public CustomerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_customer_item_card, parent, false);
        return new CustomerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CustomerViewHolder holder, final int position) {


        holder.name.setText(items.get(position).getCustomerFullName());
        holder.initials.setText(items.get(position).getInitials());
        holder.phone.setText(items.get(position).getPhone());
        holder.email.setText(items.get(position).getEmail());
        holder.item.setOnClickListener((view -> customerSelectedListener.onSelected(items.get(position))));

        /*
        boolean main = ((Activity) context).getIntent().getBooleanExtra("isMain",false);
        if (!main){
            holder.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context,ShoppingCartActivity.class);
                    intent.putExtra("customer",items.get(position));
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            });
        } else {
            holder.item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, MainCustomerActivity.class);
                    intent.putExtra("customer",items.get(position));
                    context.startActivity(intent);
                    ((Activity) context).finish();
                }
            });
        }
         */


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CustomerViewHolder extends RecyclerView.ViewHolder{

        TextView name, initials, phone, email;
        View item;

        public CustomerViewHolder(@NonNull View itemView) {
            super(itemView);
            item = itemView;
            name = itemView.findViewById(R.id.name_customer_item);
            initials = itemView.findViewById(R.id.initials_customer_item);
            phone = itemView.findViewById(R.id.phone_customer_item);
            email = itemView.findViewById(R.id.email_customer_item);
        }
    }

    public interface OnCustomerSelected {
        void onSelected(Customer customer);
    }
}
