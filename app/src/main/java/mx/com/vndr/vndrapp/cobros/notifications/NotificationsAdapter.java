package mx.com.vndr.vndrapp.cobros.notifications;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import java.util.List;

import mx.com.vndr.vndrapp.R;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder> {

    private static final String TAG = "NotificationAdapter";
    private List<Notification> items;
    private Context context;
    private OnNotificationSelected onNotificationSelected;

    public NotificationsAdapter(List<Notification> items) {
        this.items = items;
    }

    public void setOnNotificationSelected(OnNotificationSelected onNotificationSelected) {
        this.onNotificationSelected = onNotificationSelected;
    }

    @NonNull
    @Override
    public NotificationsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new NotificationsViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notification,parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsViewHolder holder, int position) {
        Notification notification = items.get(position);

        holder.status.setText(notification.getDescriptionType());
        GradientDrawable shapeType = (GradientDrawable) holder.status.getBackground();
        if (notification.getType().equals(NotificationType.N001) && !notification.isRedAlert()){
            shapeType.setColor(context.getResources().getColor(R.color.colorAlertLow));
        } else if (notification.getType().equals(NotificationType.N001) && notification.isRedAlert()){
            shapeType.setColor(context.getResources().getColor(R.color.colorAlertMax));
        } else {
            shapeType.setColor(context.getResources().getColor(R.color.colorRed));
        }
        holder.nombre.setText(notification.getCustomerName());
        holder.fechaPago.setText(notification.getPaymentDate());
        holder.orderNumber.setText(notification.getOrderNumber());
        holder.amount.setText(notification.getAmount());

        if (onNotificationSelected == null){
            Log.d(TAG, "No se implementó el listener OnNotificationSelected");
            return;
        }

        holder.materialCardView.setOnClickListener(
                view ->
                        onNotificationSelected.onNotificacionClick(notification)
        );

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class NotificationsViewHolder  extends RecyclerView.ViewHolder {

        TextView status;
        TextView nombre;
        TextView fechaPago;
        TextView orderNumber;
        TextView amount;
        MaterialCardView materialCardView;

        public NotificationsViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.txt_tag);
            nombre = itemView.findViewById(R.id.txt_customer_name);
            fechaPago = itemView.findViewById(R.id.txt_notification_date);
            amount = itemView.findViewById(R.id.txt_notification_amount);
            orderNumber = itemView.findViewById(R.id.txt_order_number);
            materialCardView = itemView.findViewById(R.id.card_notification);
        }
    }
    public interface OnNotificationSelected {
        void onNotificacionClick(Notification notification);
    }
}
