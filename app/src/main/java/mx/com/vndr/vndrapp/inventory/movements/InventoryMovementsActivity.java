package mx.com.vndr.vndrapp.inventory.movements;

import static android.view.View.GONE;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_TICKET;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.inventory.InventoryMovements;
import mx.com.vndr.vndrapp.cobros.movements.MovementsAdapter;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class InventoryMovementsActivity extends AppCompatActivity implements VndrDatePicker.OnSelectedDate, VndrRequest.VndrResponse, InventoryMoveAdapter.OnClickCardListener {

    RecyclerView recyclerView;
    Toolbar toolbar;
    TextView textViewDateInit,
            textViewDateEnd,
            textViewError;
    ProgressBar progressBar;
    boolean isRequestminDate;
    Date minDate, maxDate;

    List<InventoryMovements> inventoryMovementsList;

    InventoryMoveAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_movements);

        setupUI();

        //  Sett max and min Date of current month
        setCurrentDate();

    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(null);
        getMovements();
    }

    private void getMovements(){
        showProgress();

        String url = URL_TICKET;
        url += "?initial_date=" + VndrDateFormat.dateToWSFormat(minDate);
        url += "&final_date=" + VndrDateFormat.dateToWSFormat(maxDate);

        new VndrRequest(Volley.newRequestQueue(this))
                .get(url, this);

    }

    @Override
    public void selectedDate(Date date) {
        if (isRequestminDate){
            minDate = date;
            textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
            checkValidDateRange();
        }
        else{
            maxDate = date;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
        getMovements();

    }

    // Logic methods

    private void setCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date());

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);

        maxDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH, 1); //Set first day of month

        minDate = calendar.getTime();

        //  Display on UI
        textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
        textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
    }

    private void checkValidDateRange(){
        //  Previene que la fecha inicial sea mayor a la fecha final
        //  En dado caso fecha final se pone igual a la inicial
        if (minDate.compareTo(maxDate) > 0){
            maxDate = minDate;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
    }

    //  UI Methods
    private void setupUI(){
        toolbar = findViewById(R.id.tb_moves);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_moves);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewDateInit = findViewById(R.id.txt_moves_date_init);
        textViewDateInit.setOnClickListener(view -> {
            isRequestminDate = true;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewDateEnd = findViewById(R.id.txt_moves_date_end);
        textViewDateEnd.setOnClickListener(view -> {
            isRequestminDate = false;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(maxDate)
                    .setMinDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewError = findViewById(R.id.txt_moves_error);
        textViewError.setVisibility(GONE);

        progressBar = findViewById(R.id.pb_moves);
        progressBar.setVisibility(GONE);

    }

    private void showProgress(){
        dismissError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissProgress();
        recyclerView.setAdapter(null);
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }

    private void dismissError(){
        textViewError.setVisibility(GONE);
    }


    @Override
    public void error(String message) {
        showError(message);
    }

    @Override
    public void success(String response) {
        dismissProgress();
        inventoryMovementsList = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("results");

            for (int i = 0; i < jsonArray.length(); i++) {
                InventoryMovements inventoryMovement = new InventoryMovements(jsonArray.getJSONObject(i), true);
                inventoryMovementsList.add(inventoryMovement);
            }

            if (inventoryMovementsList.isEmpty()) {
                recyclerView.setAdapter(null);
                showError("No se encontraron movimientos en las fechas consultadas.");
            } else {
                adapter = new InventoryMoveAdapter(inventoryMovementsList, this);
                adapter.setOnClickCardListener(this);
                recyclerView.setAdapter(adapter);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void navigateToTicketDetail(InventoryMovements movement){
        startActivity(new Intent(this, MovementInventoryTicketActivity.class)
            .putExtra("move", movement));
    }

    @Override
    public void onClickCard(InventoryMovements movement) {
        navigateToTicketDetail(movement);
    }
}

class InventoryMoveAdapter extends RecyclerView.Adapter<InventoryMoveAdapter.InventoryViewHolder> {

    public interface OnClickCardListener {
        void onClickCard(InventoryMovements movement);
    }

    List<InventoryMovements> inventoryMovementsList;
    Context context;
    OnClickCardListener onClickCardListener;

    public InventoryMoveAdapter(List<InventoryMovements> inventoryMovementsList, Context context) {
        this.inventoryMovementsList = inventoryMovementsList;
        this.context = context;
    }

    public void setOnClickCardListener(OnClickCardListener onClickCardListener) {
        this.onClickCardListener = onClickCardListener;
    }

    @NonNull
    @Override
    public InventoryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_inventory_movements, viewGroup, false);
        return new InventoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull InventoryViewHolder inventoryViewHolder, int i) {

        InventoryMovements movement = inventoryMovementsList.get(i);

        if (movement.getProduct() != null) {
            inventoryViewHolder.brandName.setText(movement.getProduct().getBrandName());
            inventoryViewHolder.productName.setText(movement.getProduct().getProductName());
            inventoryViewHolder.sku.setText(movement.getProduct().getProduct_key());

            if (!movement.getProduct().getSize().isEmpty()){
                inventoryViewHolder.size.setText(movement.getProduct().getSize());
                inventoryViewHolder.size.setVisibility(View.VISIBLE);
            }

            if (!movement.getProduct().getColor().isEmpty()){
                inventoryViewHolder.color.setText(movement.getProduct().getColor());
                inventoryViewHolder.color.setVisibility(View.VISIBLE);
            }
        } else {
            inventoryViewHolder.brandName.setVisibility(GONE);
            inventoryViewHolder.productName.setVisibility(GONE);
            inventoryViewHolder.sku.setVisibility(GONE);
        }

        inventoryViewHolder.date.setText(movement.getFormattedDate());
        inventoryViewHolder.description.setText(movement.getDescription());
        inventoryViewHolder.units.setText("Unidades: " + movement.getUnits());

        GradientDrawable shapeStatus = (GradientDrawable) inventoryViewHolder.status.getBackground();
        int res = movement.getTypeTAG().equals("E") ? R.color.colorBlue : R.color.colorRed;
        String statusText = movement.getTypeTAG().equals("E") ? "Entrada" : "Salida";
        shapeStatus.setColor(context.getResources().getColor(res));
        inventoryViewHolder.status.setText(statusText);

        inventoryViewHolder.cardView.setOnClickListener(view -> {
            if (onClickCardListener == null) { return; }
            onClickCardListener.onClickCard(movement);
        });

    }

    @Override
    public int getItemCount() {
        return inventoryMovementsList.size();
    }

    class InventoryViewHolder extends RecyclerView.ViewHolder {

        TextView brandName, productName, sku;
        TextView size, color, descripcion;
        TextView status, date, units, description;
        MaterialCardView cardView;

        public InventoryViewHolder(@NonNull View itemView) {
            super(itemView);


            brandName = itemView.findViewById(R.id.txt_brand);
            productName = itemView.findViewById(R.id.product_name_rules);
            sku = itemView.findViewById(R.id.sku_add_product);
            size = itemView.findViewById(R.id.size_add);
            color = itemView.findViewById(R.id.color_add);
            descripcion = itemView.findViewById(R.id.desc);
            status = itemView.findViewById(R.id.txt_status_cxc);
            date = itemView.findViewById(R.id.txt_customer_name);
            units = itemView.findViewById(R.id.txt_units);
            description = itemView.findViewById(R.id.txt_desc);
            cardView = itemView.findViewById(R.id.cardView);

        }
    }
}

