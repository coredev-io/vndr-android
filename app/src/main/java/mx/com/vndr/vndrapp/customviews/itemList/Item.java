package mx.com.vndr.vndrapp.customviews.itemList;

import android.view.View;

public class Item {
    private String title;
    private String subtitle;
    private View.OnClickListener listener;

    public Item(String title, String subtitle) {
        this.title = title;
        this.subtitle = subtitle;
    }

    public Item(String title, String subtitle, View.OnClickListener listener) {
        this.title = title;
        this.subtitle = subtitle;
        this.listener = listener;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public View.OnClickListener getListener() {
        return listener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }
}
