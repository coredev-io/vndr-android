package mx.com.vndr.vndrapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.PickDateActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.OrderDetailActivity;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodViewHolder> {

    List<String> items;
    OnPaymentSelected selected;

    public PaymentMethodAdapter(List<String> items) {
        this.items = items;
    }

    public void setSelected(OnPaymentSelected selected) {
        this.selected = selected;
    }

    @NonNull
    @Override
    public PaymentMethodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.radio_cardview, parent, false);
        return new PaymentMethodViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodViewHolder holder, final int position) {
        holder.radioButton.setText(items.get(position));
        holder.radioButton.setChecked(false);
        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){

                    /*
                    order.getCxc().setMethodPayment(items.get(position));
                    if (order.getCxc().getPayFrequency() != 1){
                        ((Activity)context).startActivityForResult(new Intent(context, PickDateActivity.class)
                                        .putExtra("type", PickDateActivity.Type.FIRST_PAY)
                                        .putExtra("order",order)
                                        .putExtra("isPaid",true)

                                ,1);
                    } else{
                        ((Activity)context).startActivityForResult(new Intent(context, OrderDetailActivity.class)
                                .putExtra("order",order),1);
                    }

                     */
                    if (selected != null)
                        selected.onPaymentSelected(position);


                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class PaymentMethodViewHolder extends RecyclerView.ViewHolder{

        RadioButton radioButton;
        public PaymentMethodViewHolder(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.rb_cardview);
        }
    }

    public interface OnPaymentSelected {
        void onPaymentSelected(int position);
    }
}
