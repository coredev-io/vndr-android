package mx.com.vndr.vndrapp.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.ShoppingOrdersActivity;
import mx.com.vndr.vndrapp.models.Product;

public class ProductInventoryActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView brandName, catalog, productName, sku;
    TextView size, color, descripcion;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    Button button;

    TextView currentStockTextView;
    TextView orderPendingIncompleteTextView;
    TextView availableStockTextView;
    TextView pendingStockInTextView;
    TextView currentCalculatedStockTextView;
    TextView configBuyBackTextView;
    TextView configOptimumTextView;
    TextView configMinimalTextView;
    TextView pendingOrderTextView;

    Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_inventory);

        product = (Product) getIntent().getSerializableExtra("product");
        setUpUI();
        fillData();
    }

    private void setUpUI(){
        brandName = findViewById(R.id.txt_brand_name);
        productName = findViewById(R.id.product_name);
        catalog = findViewById(R.id.txt_catalog_name);
        sku = findViewById(R.id.sku_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        progressBar = findViewById(R.id.pb_inventory);
        linearLayout = findViewById(R.id.ll_inventory);
        button = findViewById(R.id.btn_inventory);
        currentStockTextView = findViewById(R.id.current_stock);
        orderPendingIncompleteTextView = findViewById(R.id.order_pending_total);
        availableStockTextView = findViewById(R.id.available_stock);
        pendingStockInTextView = findViewById(R.id.pending_stock_in);
        currentCalculatedStockTextView = findViewById(R.id.current_calculated_stock);
        configBuyBackTextView = findViewById(R.id.config_buy_back);
        configOptimumTextView = findViewById(R.id.config_optimum);
        configMinimalTextView = findViewById(R.id.config_minimal);
        pendingOrderTextView = findViewById(R.id.pending_order);


        brandName.setText(product.getBrandName());
        catalog.setText(product.getCatalog().getCatalogName());
        productName.setText(product.getProductName());
        sku.setText(product.getProduct_key());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_inventory);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    private void fillData(){
        currentStockTextView.setText(String.valueOf(product.getInventory().getCurrentStock()));
        orderPendingIncompleteTextView.setText(String.valueOf(product.getInventory().getorderPendingTotal()));
        availableStockTextView.setText(String.valueOf(product.getInventory().getAvailableStock()));
        pendingStockInTextView.setText(String.valueOf(product.getInventory().getPendingStock()));
        currentCalculatedStockTextView.setText(String.valueOf(product.getInventory().getCurrentCalculatedStock()));
        configBuyBackTextView.setText(String.valueOf(product.getInventory().getBuyBackConfiguration()));
        configOptimumTextView.setText(String.valueOf(product.getInventory().getOptimalConfiguration()));
        configMinimalTextView.setText(String.valueOf(product.getInventory().getMinimunConfiguration()));
        pendingOrderTextView.setText(String.valueOf(product.getInventory().getPendingOrder()));
    }

    public void onClickBackButton(View view){
        this.finish();
    }

    public void onClickShoppingOrder(View view){
        startActivity(new Intent(this, ShoppingOrdersActivity.class));
    }
}