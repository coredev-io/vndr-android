package mx.com.vndr.vndrapp.catalogs;

import android.util.Log;

import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Catalog;

public class CatalogListPresenter implements CatalogListInteractor.OnRequestFinish {

    private CatalogListView view;
    private CatalogListInteractor interactor;
    private Brand brand;
    private CatalogActivity activity;

    public CatalogListPresenter(CatalogListView view, CatalogListInteractor interactor, Brand brand, CatalogActivity activity) {
        this.view = view;
        this.interactor = interactor;
        this.brand = brand;
        this.activity = activity;
        customUI();
    }

    public void getCatalogs(){
        view.hideEmptyCatalogs();
        view.showPorgress();
        interactor.getData(brand,this);
    }


    public void customUI(){
        if (activity.equals(CatalogActivity.CATALOG_ACTIVITY)){

        } else {
            view.hideFAB();
        }
    }

    @Override
    public void onSuccess(CatalogListAdapter adapter) {
        view.setAdapter(adapter);
        view.dismissProgress();
    }

    @Override
    public void onError(String error) {
        view.dismissProgress();
        view.showError(error);
    }

    @Override
    public void onEmptyCatalogs() {
        view.dismissProgress();
        view.showEmptyCatalogs();
    }

    @Override
    public void onItemClickListener(Catalog catalog) {
        if (activity == CatalogActivity.CATALOG_ACTIVITY)
            view.navigateToCatalogDetail(catalog);
        else
            view.navigateToProductList(catalog);
    }
}
