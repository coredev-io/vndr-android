package mx.com.vndr.vndrapp.api.analytics;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.customviews.VndrList.ProductAnalytic;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListType;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_SALES;

public class APISalesResume {

    private final static String TAG = "APISupplier";

    private RequestQueue queue;

    public APISalesResume(RequestQueue queue) {
        this.queue = queue;
    }

    public void getSalesResume(OnSalesResponse onSalesResponse, Date minDate, Date maxDate){

        String url = URL_ANALYTICS_SALES +
                "?start_date=" +
                VndrDateFormat.dateToWSFormat(minDate) +
                "&end_date=" +
                VndrDateFormat.dateToWSFormat(maxDate);

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onSalesResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200) {
                                onSalesResponse.onError(jsonObject.getString("message"));
                                return;
                            }
                            List<VndrList> data = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data").getJSONObject("sales");

                            JSONArray jsonArray = jsonObject.getJSONArray("brands");

                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    "Total",
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    jsonObject
                                                                            .getDouble("total")
                                                            )
                                            ).setBGColor(R.color.colorPrimary)
                                    )
                            );
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject brandObject = jsonArray.getJSONObject(i);
                                data.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        brandObject.getString("brand_name"),
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        brandObject
                                                                                .getDouble("amount")
                                                                )
                                                ).setBGColor(R.color.colorAccent)
                                        )
                                );


                            }

                            data.add(new VndrList("Forma de pago general"));
                            JSONObject gralObject = jsonObject.getJSONObject("general_sales");

                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    "Total",
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    gralObject
                                                                            .getDouble("total")
                                                            )
                                            ).setBGColor(R.color.colorAccent)
                                    )
                            );

                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    "Contado riguroso",
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    gralObject
                                                                            .getDouble("cash_sales")
                                                            )
                                            ).setBGColor(R.color.colorAccent)
                                    )
                            );

                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    "Venta en abonos",
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    gralObject
                                                                            .getDouble("credit_sales")
                                                            )
                                            ).setBGColor(R.color.colorAccent)
                                    )
                            );

                            data.add(new VndrList(VndrListType.SEPARATOR));

                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    "Anticipos",
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    gralObject
                                                                            .getDouble("advance_payments")
                                                            )
                                            ).setBGColor(R.color.colorAccent)
                                    )
                            );


                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    "Pago en abonos",
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    gralObject
                                                                            .getDouble("credit_payments")
                                                            )
                                            ).setBGColor(R.color.colorAccent)
                                    )
                            );

                            jsonArray = jsonObject.getJSONArray("credit_sales");
                            data.add(new VndrList("Ventas en abonos"));

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject saleObject = jsonArray.getJSONObject(i);
                                data.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        saleObject.getString("label"),
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        saleObject
                                                                                .getDouble("amount")
                                                                )
                                                ).setBGColor(R.color.colorAccent)
                                        )
                                );

                                data.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        "Anticipo",
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        saleObject
                                                                                .getDouble("advance_payment")
                                                                )
                                                ).setBGColor(R.color.colorAccent)
                                        )
                                );


                                data.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        "Pago en abonos",
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        saleObject
                                                                                .getDouble("payments")
                                                                )
                                                ).setBGColor(R.color.colorAccent)
                                        )
                                );

                                if (i < jsonArray.length()-1){
                                    data.add(new VndrList(VndrListType.SEPARATOR));
                                }

                            }

                            onSalesResponse.onSuccess(new VndrListAdapter(data));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onSalesResponse.onError("Ocurrió un error al parsear los datos.");
                        }

                    }
                });
    }

    public void getSalesResumeByBrand(String idBrand, Date minDate, Date maxDate, OnSalesResponse onSalesResponse){
        String url = URL_ANALYTICS_SALES +
                "?start_date=" +
                VndrDateFormat.dateToWSFormat(minDate) +
                "&end_date=" +
                VndrDateFormat.dateToWSFormat(maxDate) +
                "&brand=" +
                idBrand;

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onSalesResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200) {
                                onSalesResponse.onError(jsonObject.getString("message"));
                                return;
                            }
                            List<VndrList> data = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data");

                            data.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    jsonObject.getString("brand_name"),
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    jsonObject
                                                                            .getDouble("total")
                                                            )
                                            ).setBGColor(R.color.colorPrimary)
                                    )
                            );

                            JSONArray categoriesArray = jsonObject.getJSONArray("categories");

                            for (int i = 0; i < categoriesArray.length(); i++) {
                                JSONObject object = categoriesArray.getJSONObject(i);
                                data.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        object.getString("name"),
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        object
                                                                                .getDouble("total")
                                                                )
                                                ).setBGColor(R.color.textColor)
                                        )
                                );
                                JSONArray arrayProducts = object.getJSONArray("products");
                                for (int j = 0; j < arrayProducts.length(); j++) {
                                    JSONObject objectProduct = arrayProducts.getJSONObject(j);
                                    ProductAnalytic productAnalytic = new ProductAnalytic(
                                            objectProduct.getString("name"),
                                            objectProduct.getString("product_key"),
                                            String.valueOf(objectProduct.getInt("quantity")),
                                            NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("total")),
                                            NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("average_sale_price")),
                                            NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("stock_price")),
                                            String.valueOf(objectProduct.getInt("average_margin")) + "%"

                                            );
                                    data.add(new VndrList(productAnalytic));
                                }

                            }

                            onSalesResponse.onSuccess(new VndrListAdapter(data));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onSalesResponse.onError("Ocurrió un error al parsear los datos.");
                        }

                    }
                });
    }

    public interface OnSalesResponse {
        void onSuccess(VndrListAdapter adapter);
        void onError(String error);
    }

}
