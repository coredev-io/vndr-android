package mx.com.vndr.vndrapp.customviews;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.core.content.res.ResourcesCompat;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import mx.com.vndr.vndrapp.App;
import mx.com.vndr.vndrapp.R;

public class VndrPieChart {

    PieChart pieChart;
    String title;
    ArrayList<PieEntry> entries;
    PieDataSet dataSet;
    Typeface typeface;
    float textSize = 16f;

    public VndrPieChart(String title, ArrayList<PieEntry> entries) {
        this.title = title;
        this.entries = entries;
        typeface = ResourcesCompat.getFont(FirebaseApp.getInstance().getApplicationContext(), R.font.dinpro_medium);
    }

    public void setEntries(ArrayList<PieEntry> entries) {
        this.entries = entries;
    }

    public void buildPieChart(PieChart chart){
        this.pieChart = chart;
        pieChart.setUsePercentValues(true);
        pieChart.getDescription().setEnabled(false);

        pieChart.setDragDecelerationFrictionCoef(0.95f);
        pieChart.setCenterText(title);
        pieChart.setCenterTextSize(textSize);
        pieChart.setCenterTextTypeface(typeface);
        pieChart.setCenterTextColor(R.color.colorAccent);

        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleColor(Color.WHITE);

        pieChart.setTransparentCircleColor(Color.WHITE);
        pieChart.setTransparentCircleAlpha(110);

        pieChart.setHoleRadius(58f);
        pieChart.setTransparentCircleRadius(61f);

        pieChart.setDrawCenterText(true);

        pieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(true);
        pieChart.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        pieChart.animateY(1400, Easing.EaseInOutQuad);

        Legend l = pieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setEnabled(true);



        pieChart.setEntryLabelColor(Color.WHITE);
        pieChart.setEntryLabelTextSize(100f);
        pieChart.setDrawEntryLabels(false);

        dataSet = new PieDataSet(entries, title);

        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
    }

    public void setColors(int... colors){
        dataSet.setColors(colors);
    }

    public void show(){
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(pieChart));
        data.setValueTextSize(textSize);
        data.setValueTypeface(typeface);
        data.setValueTextColor(Color.WHITE);
        pieChart.setData(data);
        pieChart.highlightValues(null);
        pieChart.invalidate();
    }
}
