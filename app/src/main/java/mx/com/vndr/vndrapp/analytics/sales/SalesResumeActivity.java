package mx.com.vndr.vndrapp.analytics.sales;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.APISalesResume;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListType;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;
import mx.com.vndr.vndrapp.util.DataVndr;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static android.view.View.GONE;

public class SalesResumeActivity extends AppCompatActivity implements VndrDatePicker.OnSelectedDate, APISalesResume.OnSalesResponse {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textViewDateInit;
    TextView textViewDateEnd;
    TextView textViewError;
    ProgressBar progressBar;
    Date minDate;
    Date maxDate;
    boolean isRequestminDate;
    Query query;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_resume);
        setupUI();
        setCurrentDate();
        if (getIntent().hasExtra("query"))
            query = (Query) getIntent().getSerializableExtra("query");
        else
            query = Query.ALL;

        getData();
    }

    private void getData(){
        showProgress();
        recyclerView.setAdapter(null);
        if (query.equals(Query.BY_BRAND)){
            new APISalesResume(Volley.newRequestQueue(this))
                    .getSalesResumeByBrand(
                            getIntent().getStringExtra("brand"),
                            minDate,
                            maxDate,
                            this
                    );
        } else {
            new APISalesResume(Volley.newRequestQueue(this))
                    .getSalesResume(this, minDate, maxDate);
        }

    }


    private void setCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date());

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);

        maxDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH, 1); //Set first day of month

        minDate = calendar.getTime();

        //  Display on UI
        textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
        textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
    }

    private void checkValidDateRange(){
        //  Previene que la fecha inicial sea mayor a la fecha final
        //  En dado caso fecha final se pone igual a la inicial
        if (minDate.compareTo(maxDate) > 0){
            maxDate = minDate;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
    }

    //  UI Methods
    private void setupUI(){
        toolbar = findViewById(R.id.tb_sales_resume);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_sales_resume);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewDateInit = findViewById(R.id.txt_moves_date_init);
        textViewDateInit.setOnClickListener(view -> {
            isRequestminDate = true;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewDateEnd = findViewById(R.id.txt_moves_date_end);
        textViewDateEnd.setOnClickListener(view -> {
            isRequestminDate = false;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(maxDate)
                    .setMinDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewError = findViewById(R.id.txt_sales_resume_error);
        textViewError.setVisibility(GONE);

        progressBar = findViewById(R.id.pb_sales_resume);
        progressBar.setVisibility(GONE);
    }

    private void showProgress(){
        dismissError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissProgress();
        recyclerView.setAdapter(null);
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }

    private void dismissError(){
        textViewError.setVisibility(GONE);
    }

    @Override
    public void selectedDate(Date date) {
        if (isRequestminDate){
            minDate = date;
            textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
            checkValidDateRange();
        }
        else{
            maxDate = date;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
        getData();
    }

    @Override
    public void onSuccess(VndrListAdapter adapter) {
        recyclerView.setAdapter(adapter);
        dismissProgress();
    }

    @Override
    public void onError(String error) {
        dismissProgress();
        showError(error);
    }

    public enum Query {
        BY_BRAND,ALL;
    }
}