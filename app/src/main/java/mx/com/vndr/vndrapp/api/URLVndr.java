package mx.com.vndr.vndrapp.api;

import mx.com.vndr.vndrapp.BuildConfig;

public class URLVndr {
    private static final String HOST_NAME = BuildConfig.API_HOST_NAME;


    private static final String API = "api/v1/";
    public static final String URL_ACTIVE_ORDERS = HOST_NAME + API + "orders?status=active";
    public static final String URL_ORDERS = HOST_NAME + API + "orders";
    public static final String URL_VALIDATE_TOKEN = HOST_NAME + API + "auth/login";
    public static final String URL_DELETE_SESSION = HOST_NAME + API + "auth/signOut";
    public static final String URL_PAYMENT_METHODS = HOST_NAME + API + "paymentMethods";


    public static final String URL_CUSTOMERS = HOST_NAME + API + "clients";
    public static final String URL_ORDER_DETAIL = HOST_NAME + API + "orders?id_order=";
    public static final String URL_CATALOGS_USER = HOST_NAME + API + "catalogs?owner=self";
    public static final String URL_PRODUCTS_CATALOG = HOST_NAME + API + "products?id_catalog=";
    public static final String URL_BRANDS = HOST_NAME + API + "brands";
    public static final String URL_USER_BRANDS = HOST_NAME + API + "brands-user";
    public static final String URL_OWN_BRANDS = URL_USER_BRANDS + "?type=own";
    public static final String URL_BRANDS_TO_EDIT_CATALOGS = URL_USER_BRANDS + "?type=catalog";

    public static final String URL_COLLABORATIVE_BRANDS = URL_USER_BRANDS + "?type=collaborative";

    public static final String URL_ACTIVE_USER_BRANDS = URL_USER_BRANDS + "?type=active";

    public static final String URL_PROFILE_BRANDS = HOST_NAME + API + "profile/config-brands";

    public static final String URL_PRODUCTS_STD_CLASS = HOST_NAME + API + "products-user?standard_classification_id=";
    public static final String URL_PRODUCTS_BY_CATALOG = HOST_NAME + API + "products?id_catalog=";
    public static final String URL_PRODUCTS = HOST_NAME + API + "products";
    public static final String URL_PRODUCTS_USER = HOST_NAME + API + "products-user";
    public static final String URL_PRODUCTS_INVENTORY = HOST_NAME + API + "products-user?inventory=true";



    public static final String URL_CXC = HOST_NAME + API + "cxc";
    public static final String URL_CXC_ACTIVE = URL_CXC + "?status=active";
    public static final String URL_CXC_ACTIVE_CUSTOMER = URL_CXC_ACTIVE + "&client_id=";
    public static final String URL_CXC_COMPLETED = URL_CXC + "?status=completed";
    public static final String URL_CXC_COMPLETED_CUSTOMER = URL_CXC_COMPLETED + "&client_id=";
    public static final String URL_CXC_DETAIL = URL_CXC + "?id_cxc=";

    public static final String URL_MOVEMENT_TYPE = HOST_NAME + API + "movement-type";
    public static final String URL_MOVEMENT = HOST_NAME + API +"movements";
    public static final String URL_MOVEMENT_REVERSE = HOST_NAME + API +"movement-reverse";

    public static final String URL_NOTIFICATIONS = HOST_NAME + API + "notifications?type=";

    public static final String URL_CATALOGS = HOST_NAME + API + "catalogs";

    public static final String URL_CLASS = HOST_NAME + API + "classifications";

    public static final String URL_CXC_SINGLE = HOST_NAME + API + "cxc-single";
    public static final String URL_CXC_TERMS = HOST_NAME + API + "cxc-terms";

    public static final String URL_STANDARD_CLASSIFICATION = HOST_NAME + API + "classifications";

    public static final String URL_USER = HOST_NAME + API + "user";
    public static final String URL_GRANT_PERMISSION_USER = URL_USER + "/grant-permission-data-usage";
    public static final String URL_CHANGE_USER_DEVICE = URL_USER + "/change-device";
    public static final String URL_VALIDATE_DATA_USER = URL_USER + "/validate";
    public static final String URL_SUPPLIERS = HOST_NAME + API + "suppliers";

    public static final String URL_PURCHASES = HOST_NAME + API + "purchases/orders";
    public static final String URL_PURCHASES_CART = URL_PURCHASES + "/cart";
    public static final String URL_PURCHASES_SET_DATE = URL_PURCHASES + "/set-order-date";
    public static final String URL_PURCHASES_SET_SEND_DATE = URL_PURCHASES + "/set-sending-date";
    public static final String URL_PURCHASES_SUGGESTIONS = HOST_NAME + API +"purchases/purchase-suggestions";

    public static final String URL_ANALYTICS = HOST_NAME + API + "analytics";
    public static final String URL_ANALYTICS_SALES = URL_ANALYTICS + "/sales";
    public static final String URL_ANALYTICS_COLLECTION = URL_ANALYTICS + "/collection";
    public static final String URL_ANALYTICS_CXC = URL_ANALYTICS + "/cxc";
    public static final String URL_ANALYTICS_CXC_CURRENT = URL_ANALYTICS + "/cxc-current";
    public static final String URL_ANALYTICS_PROJECTIONS = URL_ANALYTICS + "/cxc-projections";
    public static final String URL_ANALYTICS_CXC_RESUME = URL_ANALYTICS + "/cxc-resume";
    public static final String URL_ANALYTICS_RESUME = URL_ANALYTICS + "/resume";
    public static final String URL_ANALYTICS_SHOPPING_ORDERS = URL_ANALYTICS + "/purchase-orders";
    public static final String URL_ANALYTICS_MOVEMENTS = URL_ANALYTICS + "/movements";
    public static final String URL_ANALYTICS_PURCHASE = URL_ANALYTICS + "/purchase-invoices";


    public static final String URL_PURCHASE_INVOICE = HOST_NAME + API + "purchases/invoices";
    public static final String URL_PURCHASE_INVOICE_SET_DATE = HOST_NAME + API + "purchases/invoices/update-status/";
    public static final String URL_PURCHASE_INVOICE_CART = HOST_NAME + API + "purchases/invoices/cart";

    public static final String URL_REWARDS = HOST_NAME + API + "reward-types";

    public static final String URL_PAYMENT_METHODS_INVOICE = HOST_NAME + API + "payment-methods-invoice";

    public static final String URL_FINISH_PURCHASE_INVOICE = URL_PURCHASE_INVOICE + "/save/";
    public static final String URL_DELETE_PURCHASE_INVOICE = HOST_NAME + API + "purchases/invoice";

    public static final String URL_INVENTORY = HOST_NAME + API + "inventory";
    public static final String URL_USER_CLASS = HOST_NAME + API + "classifications-user";

    public static final String URL_TICKET = HOST_NAME + API + "tickets";

    public static final String URL_E_TICKET_TYPES = HOST_NAME + API + "ticket-types?tag=E";
    public static final String URL_S_TICKET_TYPES = HOST_NAME + API + "ticket-types?tag=S";

    public static final String URL_TICKET_ENTRY = HOST_NAME + API + "tickets/entry";
    public static final String URL_TICKET_EXIT = HOST_NAME + API + "tickets/remove";
    public static final String URL_TICKET_ARQUEO = HOST_NAME + API + "tickets/adjust-inventory";

}
