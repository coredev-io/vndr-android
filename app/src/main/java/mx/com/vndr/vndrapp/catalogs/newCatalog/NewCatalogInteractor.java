package mx.com.vndr.vndrapp.catalogs.newCatalog;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.models.Catalog;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_CATALOGS;

public class NewCatalogInteractor {

    private String token;
    private RequestQueue queue;

    public NewCatalogInteractor(String token, RequestQueue queue) {
        this.token = token;
        this.queue = queue;
    }

    public void createCatalog(final Catalog catalog, final OnRequestFinish listener, final boolean isOwnerVndr){
        StringRequest request = new StringRequest(Request.Method.POST, URL_CATALOGS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                listener.onCreateSuccess();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("brand",catalog.getIdBrand());
                params.put("catalog_name",catalog.getCatalogName());
                params.put("catalog_key", catalog.getKey());
                if (!isOwnerVndr){

                    params.put("collaborative_catalog",String.valueOf(catalog.isCollaborative()));
                    params.put("is_public", String.valueOf(catalog.isPublic()));
                } else {
                    params.put("collaborative_catalog",String.valueOf(true));
                    params.put("is_public", String.valueOf(true));
                }
                params.put("validity", String.valueOf(catalog.isValid()));

                if (catalog.isValid()){
                    params.put("catalog_date_init", String.valueOf(catalog.getInitDate().getTime()));
                    params.put("catalog_date_end", String.valueOf(catalog.getEndDate().getTime()));
                }

                return params;
            }
        };

        queue.add(request);
    }

    public void updateCatalog(final Catalog catalog, final OnRequestFinish listener, final boolean isOwnerVndr){
        String url = URL_CATALOGS + "/" + catalog.getId();
        Log.e("UPDATE",url);
        StringRequest request = new StringRequest(Request.Method.PATCH, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("TAG",response);
                System.out.println("=-=-=-=-=-=-=-=-=-=-=-=");
                System.out.println(response);
                listener.onUpdateSuccess();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("catalog_name",catalog.getCatalogName());
                params.put("catalog_key", catalog.getKey());
                if (!isOwnerVndr){

                    params.put("collaborative_catalog",String.valueOf(catalog.isCollaborative()));
                    params.put("is_public", String.valueOf(catalog.isPublic()));
                } else {
                    params.put("collaborative_catalog",String.valueOf(true));
                    params.put("is_public", String.valueOf(true));
                }
                params.put("validity", String.valueOf(catalog.isValid()));

                if (catalog.isValid()){
                    params.put("catalog_date_init", String.valueOf(catalog.getInitDate().getTime()));
                    params.put("catalog_date_end", String.valueOf(catalog.getEndDate().getTime()));
                }

                System.out.println(params.toString());

                return params;
            }
        };

        queue.add(request);
    }

    interface OnRequestFinish{
        void onUpdateSuccess();
        void onCreateSuccess();
        void onError(String error);
    }
}
