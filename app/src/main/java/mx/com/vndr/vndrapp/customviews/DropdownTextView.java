package mx.com.vndr.vndrapp.customviews;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;

import mx.com.vndr.vndrapp.R;

public class DropdownTextView extends androidx.appcompat.widget.AppCompatAutoCompleteTextView {

    private boolean isDropDownVisible = false;

    public DropdownTextView(Context context) {
        super(context);
        customView();
    }

    public DropdownTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        customView();
    }

    public DropdownTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        customView();
    }

    public void setAdapter(int stringArray){
        setAdapter(new ArrayAdapter<>(
                getContext(),
                R.layout.support_simple_spinner_dropdown_item,
                getResources().getStringArray(stringArray)
                )
        );
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP){
            hideKeyboard();

            if (isDropDownVisible && !isPopupShowing()) {
                showDropDown();
            } else if (!isDropDownVisible){
                isDropDownVisible = true;
                showDropDown();
            } else {
                isDropDownVisible = false;
                dismissDropDown();
            }
        }

        return super.onTouchEvent(event);
    }

    private void customView(){
        setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_drop_down,
                0);
        setInputType(InputType.TYPE_NULL);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (!focused)
            isDropDownVisible = false;
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        isDropDownVisible = false;
    }

    @Override
    public void showDropDown() {
        super.showDropDown();
        setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_drop_up,
                0);
    }

    @Override
    public void dismissDropDown() {
        super.dismissDropDown();
        setCompoundDrawablesRelativeWithIntrinsicBounds(
                0,
                0,
                R.drawable.ic_arrow_drop_down,
                0);
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);
    }
}
