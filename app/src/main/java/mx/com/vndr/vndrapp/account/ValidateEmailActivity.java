package mx.com.vndr.vndrapp.account;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.SignInActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;

public class ValidateEmailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_email);
    }


    public void onResendVerificationEmailClick(View view){
        VndrAuth.getInstance().getCurrentUser().sendEmailVerification();
        Toast.makeText(this, "Se envió el email de verificación correctamente", Toast.LENGTH_LONG).show();
    }

    public void onClick(View view){
        onBackPressed();
    }

    private void navigatoToLogin(){
        Intent intent = new Intent(this, SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigatoToLogin();
    }
}