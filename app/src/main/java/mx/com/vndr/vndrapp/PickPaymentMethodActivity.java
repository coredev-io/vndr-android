package mx.com.vndr.vndrapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.adapters.PaymentMethodAdapter;
import mx.com.vndr.vndrapp.analytics.business.MovementAnalyticsActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PAYMENT_METHODS;

public class PickPaymentMethodActivity extends AppCompatActivity implements PaymentMethodAdapter.OnPaymentSelected {

    RecyclerView recyclerView;
    PaymentMethodAdapter adapter;
    List<String> methods;
    List<String> codes;
    Toolbar toolbar;
    boolean isAnalytcisFlow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_payment_method);

        recyclerView = findViewById(R.id.rv_payment_method);
        toolbar = findViewById(R.id.tb_pick_payment);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        methods = new ArrayList<>();
        adapter = new PaymentMethodAdapter(methods);

        adapter.setSelected(this);

        recyclerView.setAdapter(adapter);


        getPaymentMethods();
        isAnalytcisFlow = getIntent().getBooleanExtra("isAnalytics", false);


    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.notifyDataSetChanged();

    }

    public void getPaymentMethods(){
        StringRequest request = new StringRequest(Request.Method.GET, URL_PAYMENT_METHODS,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    System.out.println(response);
                    processResponse(response);
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }


    public void processResponse(String response){
        try {
            JSONArray array = new JSONArray(response);
            codes = new ArrayList<>();
            for (int i = 0; i < array.length(); i ++) {
                JSONObject jsonObject = array.getJSONObject(i);
                methods.add(jsonObject.getString("payment_method"));
                codes.add(jsonObject.getString("code"));
            }

            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }

    @Override
    public void onPaymentSelected(int position) {
        if (isAnalytcisFlow){
            MovementAnalyticsActivity.Query query = MovementAnalyticsActivity.Query.BY_CASH;
            query.setPaymentMethodCode(codes.get(position));
            navigateToMovements(query);
        } else {
            this.setResult(
                    RESULT_OK,
                    new Intent()
                            .putExtra("code",codes.get(position))
                            .putExtra("method",methods.get(position))
            );
            this.finish();
        }

    }

    public void navigateToMovements(MovementAnalyticsActivity.Query query){
        startActivity(new Intent(this, MovementAnalyticsActivity.class).putExtra("query", query));
    }
}
