package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static com.android.volley.Request.Method.GET;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.customviews.OptionList.OnClickListener;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.ShoppingOrdersAdapter;
import mx.com.vndr.vndrapp.inventory.supplier.SuppliersActivity;
import mx.com.vndr.vndrapp.orders.ActiveOrdersActivity;
import mx.com.vndr.vndrapp.orders.AllOrdersActivity;

public class ProductsPurchaseMainActivity extends AppCompatActivity implements VndrRequest.VndrResponse, PurchaseInvoceAdapter.OnPurchaseInvoceSelected {

    RecyclerView recyclerView;
    TextView textViewEmpty;
    TextView textViewAmount;
    ProgressBar progressBar;
    Toolbar toolbar;
    Supplier supplier;

    /**
     * Overrided Methods
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_purchase_main);
        setupUI();

        if (getIntent().hasExtra("supplier")){
            supplier = (Supplier) getIntent().getSerializableExtra("supplier");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        //emulateWS();
        getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.active_orders_menu,menu);
        return true;
    }

    /**
     * UI Methods
     */

    private void setupUI(){
        recyclerView = findViewById(R.id.rv_pp_main);
        textViewEmpty = findViewById(R.id.txt_pp_empty);
        textViewAmount = findViewById(R.id.txt_pp_amount);
        progressBar = findViewById(R.id.pb_pp_main);
        toolbar = findViewById(R.id.tb_pp_main);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.all){
                startActivity(new Intent(ProductsPurchaseMainActivity.this, InvoicesActivity.class).putExtra("supplier", supplier));
            }
            return false;
        });
    }

    private void showLoading(){
        recyclerView.setAdapter(null);
        textViewAmount.setText("$0.00");
        textViewEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void showData(){
        textViewEmpty.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    private void showEmptyState(){
        recyclerView.setAdapter(null);
        textViewAmount.setText("$0.00");
        textViewEmpty.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void showMessageError(String messageError){
        recyclerView.setAdapter(null);
        textViewAmount.setText("$0.00");
        textViewEmpty.setVisibility(View.VISIBLE);
        textViewEmpty.setText(messageError);
        progressBar.setVisibility(View.GONE);
    }


    public void onClickNuevaFactura(View view){
        if (supplier == null) {
            startActivityForResult(new Intent(this, SuppliersActivity.class).putExtra("pickMode", true), 0);
        } else {
            Supplier.Selected.current.setSupplier(supplier);
            startActivity(new Intent(this, CartRegisterPurchaseActivity.class));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {

            System.out.println(Supplier.Selected.current.getSupplier().getName());
            startActivity(new Intent(this, CartRegisterPurchaseActivity.class));
        }
    }

    /**
     * WS Data
     */

    private void getData(){
        showLoading();
        String url = URL_PURCHASE_INVOICE + "?";

        if (supplier != null) {
            url += "supplier=" + supplier.getId() + "&";
        }

        url += "type=pending";

        new VndrRequest(Volley.newRequestQueue(this))
                .get(url, this);
    }

    @Override
    public void error(String message) {
        showMessageError(message);
    }

    @Override
    public void success(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            JSONArray jsonArray = jsonObject.getJSONArray("invoices");

            if (jsonArray.length() == 0) {
                showEmptyState();
                return;
            }

            List<PurchaseInvoice> purchaseInvoices = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++) {
                PurchaseInvoice purchaseInvoice = new PurchaseInvoice(
                        jsonArray.getJSONObject(i)
                );
                purchaseInvoices.add(purchaseInvoice);
            }
            recyclerView.setAdapter(
                    new PurchaseInvoceAdapter(purchaseInvoices, this)
                            .setOnShoppingOrderSelected(this)
            );

            textViewAmount.setText(
                    NumberFormat.getCurrencyInstance().format(
                            jsonObject.getJSONObject("resume").getDouble("amount")
                    )
            );

            showData();

        } catch (JSONException e) {
            e.printStackTrace();
            showMessageError("Ocurrió un error al procesar la solicitud.");
        } catch (ParseException e) {
            e.printStackTrace();
            showMessageError("Ocurrió un error al procesar la solicitud.");
        }
    }


    @Override
    public void onSelected(PurchaseInvoice purchaseInvoice) {
        if (purchaseInvoice.getStatusCode().equals("I00")) {
            startActivity(
                    new Intent(this, CartRegisterPurchaseActivity.class)
                            .putExtra("purchase_invoice", purchaseInvoice)
            );
        } else {

            PurchaseInvoice.Selected.current.setPurchaseInvoice(purchaseInvoice);

            startActivity(
                    new Intent(this, PurchaseInvoiceDetailActivity.class)
            );
        }
    }
}