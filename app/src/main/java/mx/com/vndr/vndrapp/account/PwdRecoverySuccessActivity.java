package mx.com.vndr.vndrapp.account;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import mx.com.vndr.vndrapp.R;

public class PwdRecoverySuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pwd_recovery_success);
    }

    public void onFinishButton(View view){
        this.finish();
    }

    @Override
    public void onBackPressed() {
    }
}
