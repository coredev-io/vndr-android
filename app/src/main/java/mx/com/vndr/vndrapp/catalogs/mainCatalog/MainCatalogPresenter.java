package mx.com.vndr.vndrapp.catalogs.mainCatalog;

import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;

public class MainCatalogPresenter implements MainCatalogInteractor.OnItemClickListener {

    MainCatalogsView catalogsView;
    MainCatalogInteractor interactor;

    public MainCatalogPresenter(MainCatalogsView catalogsView, MainCatalogInteractor interactor) {
        this.catalogsView = catalogsView;
        this.interactor = interactor;
        this.interactor.setListener(this);
    }

    public void getData(){
        catalogsView.setAdapter(
                interactor.getAdapter()
        );
    }


    @Override
    public void onAssociateBrandClick() {
        catalogsView.navigateToAsociarMarca();
    }

    @Override
    public void onNewBrandClick() {
        catalogsView.nagivateToRegistrarMarca();
    }

    @Override
    public void onEditCatalog() {
        catalogsView.navigateToCatalog();
    }

    @Override
    public void onEditProduct() {
        catalogsView.navigateToProduct();
    }

}
