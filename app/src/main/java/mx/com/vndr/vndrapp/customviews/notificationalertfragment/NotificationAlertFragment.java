package mx.com.vndr.vndrapp.customviews.notificationalertfragment;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrSupport;

public class NotificationAlertFragment extends BottomSheetDialogFragment {

    private NotificationAlertData notificationAlertData;
    private View.OnClickListener onClickListener;

    public void setNotificationAlertData(NotificationAlertData notificationAlertData) {
        this.notificationAlertData = notificationAlertData;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(dialogInterface -> {
            BottomSheetDialog d = (BottomSheetDialog) dialogInterface;
            FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification_alert, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button button = view.findViewById(R.id.btn_notification);
        ConstraintLayout whatsapp = view.findViewById(R.id.cl_whats_alert);
        ConstraintLayout sms = view.findViewById(R.id.cl_sms_alert);
        ConstraintLayout mail = view.findViewById(R.id.cl_email_alert);
        TextView titleTextView = view.findViewById(R.id.title_alert);
        TextView subtitleTextView = view.findViewById(R.id.subtitle_alert);
        TextView detailSMSTextView = view.findViewById(R.id.txt_detail_sms);
        TextView detalMailTextView = view.findViewById(R.id.txt_detial_mail);
        TextView alertMessageTextView = view.findViewById(R.id.txt_alert_message);

        detailSMSTextView.setText(notificationAlertData.getDetailSMS());
        detalMailTextView.setText(notificationAlertData.getDetailMail());
        titleTextView.setText(notificationAlertData.getTitle());
        subtitleTextView.setText(notificationAlertData.getSubtitle());
        alertMessageTextView.setText(notificationAlertData.getAlertMessage());



        sms.setOnClickListener(view13 -> shareToSMS());
        mail.setOnClickListener(view12 -> shareToEmail());
        whatsapp.setOnClickListener(view1 -> shareToWhatsApp());
        if (this.onClickListener != null)
            button.setOnClickListener(this.onClickListener);
        else
            button.setOnClickListener(view1 -> this.dismiss());
    }

    private void shareToSMS(){
        startActivity(notificationAlertData.getSMSIntent());
    }

    private void shareToEmail(){
        try {
            startActivityForResult(notificationAlertData.getEmailIntent(),0);
        } catch (ActivityNotFoundException e) {
            Dialogs.showAlert(
                    getContext(),
                    getResources().getString(R.string.mailIntentMessageError)
            );
        }
    }

    private void shareToWhatsApp(){
        if (notificationAlertData.getWhatsAppIntent().resolveActivity(getContext().getPackageManager()) != null){
            startActivity(notificationAlertData.getWhatsAppIntent());
        } else {
            Toast.makeText(getContext(), "WhatsApp no está instalado en este dispositivo.", Toast.LENGTH_SHORT).show();
        }
    }
}


