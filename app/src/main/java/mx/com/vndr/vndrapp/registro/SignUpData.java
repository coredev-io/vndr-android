package mx.com.vndr.vndrapp.registro;


public class SignUpData{

    private static SignUpData instance;

    private String name;
    private String lastName;
    private String email;
    private String mobilePhone;
    private String zipCode;
    private String pwd;
    private boolean shareMyData;
    private boolean shareCustomerData;
    private String survey;

    public static SignUpData getInstance(){
        synchronized (SignUpData.class) {
            if(instance == null){
                instance = new SignUpData();
            }
        }
        return instance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public void setShareMyData(boolean shareMyData) {
        this.shareMyData = shareMyData;
    }

    public void setShareCustomerData(boolean shareCustomerData) {
        this.shareCustomerData = shareCustomerData;
    }

    public void setSurvey(String survey) {
        this.survey = survey;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getPwd() {
        return pwd;
    }

    public boolean isShareMyData() {
        return shareMyData;
    }

    public boolean isShareCustomerData() {
        return shareCustomerData;
    }

    public String getSurvey() {
        return survey;
    }
}