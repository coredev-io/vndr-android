package mx.com.vndr.vndrapp.brands;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.catalogs.CatalogsListActivity;
import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.models.Brand;

public class BrandListActivity extends AppCompatActivity implements BrandListView{

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView emptyBrandsTextView;

    BrandListType customType;
    BrandListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brand_list);
        setUpUI();

        if (getIntent().hasExtra("type"))
            customType = (BrandListType) getIntent().getSerializableExtra("type");
        else
            customType = BrandListType.BRANDS_TO_EDIT_CATALOGS;

        presenter = new BrandListPresenter(this, new BrandInteractor(Volley.newRequestQueue(this)), customType);
        presenter.getBrandListToEditCatalogs();
    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_brand_list);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerView = findViewById(R.id.rv_brand_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        progressBar = findViewById(R.id.pb_brand_list);
        progressBar.setVisibility(View.GONE);
        emptyBrandsTextView = findViewById(R.id.txt_empty_brand_list);
        emptyBrandsTextView.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyBrands(String message) {
        emptyBrandsTextView.setVisibility(View.VISIBLE);
        emptyBrandsTextView.setText(message);
    }

    @Override
    public void setRecyclerViewData(BrandListAdapter adapter) {
        recyclerView.setAdapter(null);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void navigateToCatalogList(Brand brand) {
        startActivity(new Intent(
                        this,
                        CatalogsListActivity.class
                ).putExtra("brand", brand)
                        .putExtra("activity", CatalogActivity.CATALOG_ACTIVITY)
        );
    }


    public enum BrandListType {
        OWN_BRANDS,
        BRANDS_TO_EDIT_CATALOGS,
        BRANDS_TO_ADD_PRODUCTS
    }
}

