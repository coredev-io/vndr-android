package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_REWARDS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_STANDARD_CLASSIFICATION;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.products.ProductActivity;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class TotalInvoiceActivity extends AppCompatActivity implements TextWatcher {

    Toolbar toolbar;
    Button button;
    TextView txtSubtotal;
    TextView txtTotal;
    TextView txtSupplier;
    EditText editTextNumber;
    EditText editTextPoints;
    EditText editTextIVA;
    AutoCompleteTextView autoCompleteRewards;
    ProgressBar progressBar;
    CheckBox checkBox;

    PurchaseInvoice purchaseInvoice;
    Supplier supplier;

    HashMap<String, String> rewardsTypeMap = new HashMap<>();
    double orderAmount = 0;
    double subtotal = 0;
    double total = 0;
    double iva = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_invoice);
        setupUI();
        getRewardType();
        purchaseInvoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();
        supplier = purchaseInvoice.getSupplier();
        orderAmount = purchaseInvoice.getOrderAmount();
        txtSupplier.setText(supplier.getName());
        setCalculo(checkBox.isChecked());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    private void setCalculo(boolean includesIVA){

        if (includesIVA) {
            subtotal = orderAmount / 1.16;
            iva = orderAmount - (orderAmount/ 1.16);
            total = orderAmount;
            txtSubtotal.setText(
                    NumberFormat.getCurrencyInstance().format(subtotal)
            );
            editTextIVA.setText(
                    NumberFormat.getCurrencyInstance().format(iva)
            );
            txtTotal.setText(NumberFormat.getCurrencyInstance().format(orderAmount));
        } else {
            subtotal = orderAmount;
            iva = orderAmount * 0.16;
            total = orderAmount * 1.16;
            txtSubtotal.setText(NumberFormat.getCurrencyInstance().format(orderAmount));
            editTextIVA.setText(
                    NumberFormat.getCurrencyInstance().format(iva)
            );
            txtTotal.setText(NumberFormat.getCurrencyInstance().format(total));
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void setupUI(){
        toolbar = findViewById(R.id.tb_total_invoice);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        button = findViewById(R.id.btn_total_invoice);
        txtSubtotal = findViewById(R.id.txt_subtotal_invoice);
        editTextIVA = findViewById(R.id.edtxt_invoice_iva);
        txtTotal = findViewById(R.id.txt_total_invoice);
        txtSupplier = findViewById(R.id.txt_invoice_supplier);

        editTextNumber = findViewById(R.id.edtxt_invoice_number);
        editTextPoints = findViewById(R.id.edtxt_invoice_points);
        autoCompleteRewards = findViewById(R.id.auto_rewards);
        progressBar = findViewById(R.id.pb_total_invoice);
        checkBox = findViewById(R.id.checkBox_total_invoice);

        autoCompleteRewards.setOnTouchListener((view, motionEvent) -> {
            if (motionEvent.getAction() == KeyEvent.ACTION_UP){
                hideKeyboard();
                if (autoCompleteRewards.isPopupShowing())
                    autoCompleteRewards.dismissDropDown();
                else
                    autoCompleteRewards.showDropDown();
            }
            return false;
        });

        editTextNumber.addTextChangedListener(this);
        editTextPoints.addTextChangedListener(this);
        autoCompleteRewards.addTextChangedListener(this);
        editTextIVA.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
        editTextIVA.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String txt = editTextIVA.getText().toString();

                if (txt.equals("$") || txt.equals(".")) {
                    editTextIVA.setText("");
                    txt = "";
                }

                txt = txt.replace("$", "");
                if (txt.contains(",")) {
                    txt = txt.replaceAll(",", "");
                }
                iva = 0;
                if (!txt.isEmpty()) { iva = Double.parseDouble(txt); }

                subtotal = checkBox.isChecked() ? orderAmount / 1.16 : orderAmount;
                total = subtotal + iva;

                txtTotal.setText(
                        NumberFormat.getCurrencyInstance().format(total)
                );
            }
        });

        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            setCalculo(b);
        });

        button.setEnabled(false);

    }

    public void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    public void dismissLoading(){
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    public  void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    public void getRewardType(){
        autoCompleteRewards.setText("Cargando...");
        new VndrRequest(Volley.newRequestQueue(this))
                .get(URL_REWARDS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        autoCompleteRewards.setText("Error");
                        Toast.makeText(TotalInvoiceActivity.this, "Ocurrió un error al obtener las tipos de recompensas", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void success(String response) {
                        List<String> rewardsType = new ArrayList<>();
                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++){
                                JSONObject object = array.getJSONObject(i);

                                rewardsTypeMap.put(
                                        object.getString("description"),
                                        object.getString("code"));
                                rewardsType.add(
                                        object.getString("description")
                                );


                            }

                            autoCompleteRewards.setText("");

                            autoCompleteRewards.setAdapter(new ArrayAdapter<>(TotalInvoiceActivity.this, R.layout.support_simple_spinner_dropdown_item, rewardsType));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void onClick(View view){
        if (iva == 0){
            Dialogs.showAlertQuestion(this, "El IVA es $0.00 ¿Deseas continuar?", (materialDialog, dialogAction) -> {
                appendInvoiceData();
            });
        } else {
            appendInvoiceData();
        }

    }

    public void appendInvoiceData(){
        showLoading();
        Map<String, String> params = new HashMap<>();

        params.put("invoice_document_number", editTextNumber.getText().toString());
        params.put("rewards_code", autoCompleteRewards.getText().toString().isEmpty() ? "" : rewardsTypeMap.get(autoCompleteRewards.getText().toString()));
        params.put("rewards_points", editTextPoints.getText().toString());
        params.put("iva", String.valueOf(iva));
        params.put("included_iva", String.valueOf(checkBox.isChecked()));
        params.put("subtotal", String.valueOf(subtotal));
        params.put("total", String.valueOf(total));

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .patch(URL_PURCHASE_INVOICE + "/" + purchaseInvoice.getPurchaseId() , new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismissLoading();
                        Dialogs.showAlert(TotalInvoiceActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismissLoading();
                        startActivityForResult(new Intent(TotalInvoiceActivity.this, InvoiceDateActivity.class),1);
                    }
                });
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        button.setEnabled(
                !editTextNumber.getText().toString().isEmpty()
        );
    }
}