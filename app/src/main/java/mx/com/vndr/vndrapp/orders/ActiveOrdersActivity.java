package mx.com.vndr.vndrapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customer.SelectCustomerActivity;
import mx.com.vndr.vndrapp.adapters.ActiveOrdersRecyclerViewAdapter;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ACTIVE_ORDERS;

public class ActiveOrdersActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<Order> orderList = new ArrayList<>();
    ActiveOrdersRecyclerViewAdapter adapter;
    TextView countIncomplete, countNoSended, countNoDelivery, amountIncomplete, amountNoSended, amountNoDelivery
            ,emptyOrdersTxt;
    ProgressBar progressBar;
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_orders);
        recyclerView = findViewById(R.id.rv_active_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new ActiveOrdersRecyclerViewAdapter(orderList, this);
        recyclerView.setAdapter(adapter);
        setUpUI();


    }

    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println(data.getBooleanExtra("shouldRefresh", false));
        if (
                requestCode == 0 &&
                        resultCode == RESULT_OK && data != null
                && data.getBooleanExtra("shouldRefresh", false)
        ){
            System.out.println("refresh");
            orderList = new ArrayList<>();
            getActiveOrders();
        }
    }
    */

    @Override
    protected void onResume() {
        super.onResume();
        orderList.clear();
        adapter.notifyDataSetChanged();
        getActiveOrders();

    }

    public void getActiveOrders(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_ACTIVE_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                processResponse(response);
                progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));
        Volley.newRequestQueue(this).add(request);

    }

    private void processResponse(String response){
        try {
            JSONObject jsonResponse = new JSONObject(response);

            if (jsonResponse.has("statusCode") && jsonResponse.getInt("statusCode") == 404){
                Log.e("TAG", jsonResponse.getString("message"));

                countIncomplete.setText("0");
                amountIncomplete.setText("$0.00");


                countNoSended.setText("0");
                amountNoSended.setText("$0.00");


                countNoDelivery.setText("0");
                amountNoDelivery.setText("$0.00");

                emptyOrdersTxt.setVisibility(View.VISIBLE);

            } else if (jsonResponse.has("orders")){

                JSONObject orders = jsonResponse.getJSONObject("orders");

                if (orders.has("incomplete")){
                    JSONArray array = orders.getJSONArray("incomplete");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }


                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }

                        orderList.add(order);
                    }
                }

                if (orders.has("not_sended")){
                    JSONArray array = orders.getJSONArray("not_sended");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }


                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }

                        orderList.add(order);
                    }
                }

                if (orders.has("not_delivery")){
                    JSONArray array = orders.getJSONArray("not_delivery");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }

                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }

                        orderList.add(order);
                    }
                }

                countIncomplete.setText(String.valueOf(jsonResponse.getJSONObject("incomplete").getInt("count")));
                amountIncomplete.setText(parseCurrency(jsonResponse.getJSONObject("incomplete").getDouble("amount")));


                countNoSended.setText(String.valueOf(jsonResponse.getJSONObject("not_sended").getInt("count")));
                amountNoSended.setText(parseCurrency(jsonResponse.getJSONObject("not_sended").getDouble("amount")));


                countNoDelivery.setText(String.valueOf(jsonResponse.getJSONObject("not_delivered").getInt("count")));
                amountNoDelivery.setText(parseCurrency(jsonResponse.getJSONObject("not_delivered").getDouble("amount")));
                emptyOrdersTxt.setVisibility(View.GONE);
                adapter.notifyDataSetChanged();



            }
        } catch (JSONException e) {
            e.printStackTrace();
            progressBar.setVisibility(View.GONE);
        }
    }


    public void onClickNewOrder(View view){
        requestForCustomer();
    }

    private void requestForCustomer(){
        startActivityForResult(
                new Intent(
                        this,
                        SelectCustomerActivity.class),
                9
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9  & resultCode == RESULT_OK){
            Customer customer = (Customer) data.getSerializableExtra("customer");
            navigateToShoppingCart(customer);
        }
    }

    private void navigateToShoppingCart(Customer customer){
        Intent intent = new Intent(this,ShoppingCartActivity.class);
        intent.putExtra("customer",customer);
        startActivity(intent);
    }

    private void setUpUI(){
        countIncomplete = findViewById(R.id.count_incomplete_orders);
        countNoSended = findViewById(R.id.count_no_sended_orders);
        countNoDelivery = findViewById(R.id.count_no_deli_orders);
        amountIncomplete = findViewById(R.id.amount_incomplete_orders);
        amountNoSended = findViewById(R.id.amount_no_sended_orders);
        amountNoDelivery = findViewById(R.id.amount_no_deliv_orders);
        progressBar = findViewById(R.id.pb_active_orders);
        toolbar = findViewById(R.id.tb_active_orders);
        emptyOrdersTxt = findViewById(R.id.txt_empty_active_orders);

        emptyOrdersTxt.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.all){
                    startActivity(new Intent(ActiveOrdersActivity.this, AllOrdersActivity.class));
                }
                return false;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.active_orders_menu,menu);
        return true;
    }

    private String parseCurrency(double i){
        return NumberFormat.getCurrencyInstance().format(i);
    }
}
