package mx.com.vndr.vndrapp.customer.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import mx.com.vndr.vndrapp.R;

public class ListItemAdapter extends RecyclerView.Adapter<ListItemAdapter.ListItemViewHolder> {

    List<String> title;
    List<String> descriptions;
    List<View.OnClickListener> listeners;

    public ListItemAdapter(List<String> title, List<String> descriptions, List<View.OnClickListener> listeners) {
        this.title = title;
        this.descriptions = descriptions;
        this.listeners = listeners;
    }

    @NonNull
    @Override
    public ListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);
        return new ListItemViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ListItemViewHolder holder, int position) {
        holder.title.setText(title.get(position));
        holder.description.setText(descriptions.get(position));
        holder.layout.setOnClickListener(listeners.get(position));
    }

    @Override
    public int getItemCount() {
        return title.size();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder{

        TextView title, description;
        ConstraintLayout layout;
        public ListItemViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_title);
            description = itemView.findViewById(R.id.txt_description);
            layout = itemView.findViewById(R.id.layout_item_list);
        }
    }
}
