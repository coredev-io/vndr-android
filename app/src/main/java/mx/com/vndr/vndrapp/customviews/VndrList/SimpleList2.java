package mx.com.vndr.vndrapp.customviews.VndrList;

public class SimpleList2 {
    private String s1;
    private String s2;
    private boolean isCustom = false;
    private int bgColorResId = android.R.color.white;

    public SimpleList2(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public boolean isCustom() {
        return isCustom;
    }

    public int getBgColorResId() {
        return bgColorResId;
    }

    public SimpleList2 setBGColor(int resId){
        isCustom = true;
        bgColorResId = resId;
        return this;
    }

    public String getS1() {
        return s1;
    }

    public void setS1(String s1) {
        this.s1 = s1;
    }

    public String getS2() {
        return s2;
    }

    public void setS2(String s2) {
        this.s2 = s2;
    }
}
