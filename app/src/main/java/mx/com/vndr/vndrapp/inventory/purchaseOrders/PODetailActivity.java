package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.OrderProductsFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.OrderDetailAdapter;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders.OnShoppingOrdersResponse;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class PODetailActivity extends AppCompatActivity implements View.OnClickListener, OnShoppingOrdersResponse, APIShoppingOrders.OnCreateShoppingOrder {

    Toolbar toolbar;
    RecyclerView recyclerView;
    EditText editTextNotes;

    OrderDetailAdapter adapter;
    List<String> valueList = new ArrayList<>();
    List<String> keysList = new ArrayList<>();
    ShoppingOrder order;
    Button buttonSend;
    Button buttonFinish;
    TextView textViewNotes;
    TextInputLayout textInputLayoutNotes;
    LinearLayout linearLayoutCompletedInfo;
    TextView textViewCompletedDate;
    TextView textViewCompletedSended;
    boolean isNotSendedView;
    boolean isCompleteView;
    String purchaseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_o_detail);
        isNotSendedView = getIntent().getBooleanExtra("isNotSended",false);
        isCompleteView = getIntent().getBooleanExtra("isCompleted", false);
        if (!getIntent().hasExtra("id"))
            purchaseId = ShoppingOrder.Selected.current.getShoppingOrder().getPurchaseId();
        else
            purchaseId = getIntent().getStringExtra("id");
        setupUI();

        if (isNotSendedView) {
            customSendedViewUI();
            getOrderDetail();
        } else if (isCompleteView){
            customCompletedViewUI();
            getOrderDetail();
        } else {
            fillData();
        }
    }

    private void getOrderDetail(){
        new APIShoppingOrders(Volley.newRequestQueue(this))
                .getShoppingOrderDetail(
                        this,
                        purchaseId
                );
    }

    private void customSendedViewUI(){
        buttonFinish.setText("Finalizar");
        buttonSend.setText("Enviar Orden");
        textInputLayoutNotes.setVisibility(View.GONE);
        textViewNotes.setVisibility(View.GONE);
    }
    private void customCompletedViewUI(){
        buttonFinish.setVisibility(View.GONE);
        buttonSend.setVisibility(View.GONE);
        textInputLayoutNotes.setVisibility(View.GONE);
        textViewNotes.setVisibility(View.GONE);
        linearLayoutCompletedInfo.setVisibility(View.VISIBLE);
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_po_detail);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_po_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        editTextNotes = findViewById(R.id.edtxt_notes_po);

        buttonSend = findViewById(R.id.btn_sec_po_detail);
        buttonFinish = findViewById(R.id.btn_po_detail);

        textInputLayoutNotes = findViewById(R.id.il_po_detail);
        textViewNotes = findViewById(R.id.textView10);

        linearLayoutCompletedInfo = findViewById(R.id.ll_completed_info);
        textViewCompletedDate = findViewById(R.id.txt_date_complete_po);
        textViewCompletedSended = findViewById(R.id.txt_send_po);
    }

    private void fillData(){
        order = ShoppingOrder.Selected.current.getShoppingOrder();

        keysList.add("Proveedor");
        valueList.add( order.getSupplier().getName());

        keysList.add("Productos");
        valueList.add(String.valueOf(order.getProductList().size()));

        keysList.add("Total mercancía");
        valueList.add(order.getTotalMerchFormat());

        keysList.add("Cargo por envío");
        valueList.add(order.getShippingCostFormat());

        keysList.add("Total de la orden");
        valueList.add(order.getTotalOrderAmount());

        keysList.add("Fecha de la orden");
        valueList.add(order.getOrderDateStr());


        adapter = new OrderDetailAdapter(valueList,keysList,this);
        recyclerView.setAdapter(adapter);
    }

    public void onClickModifyOrder(View view){
        if (isNotSendedView){
            showNotificationsMenu();
        } else {
            this.setResult(RESULT_BACK);
            this.finish();
        }
    }

    public void onClickFinishOrder(View view){
        if (isNotSendedView || isCompleteView){
            this.setResult(RESULT_BACK);
            this.finish();
        }
        else{

            new APIShoppingOrders(Volley.newRequestQueue(this))
                    .setOrderDate(this);
        }

    }


    private void showNotificationsMenu(){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setOnClickListener(view -> {
            startActivityForResult(
                    new Intent(
                            this,
                            PickOrderDateActivity.class)
                            .putExtra("isSendDate", true),
                    1);
        });
        alertFragment.setNotificationAlertData(
                ShoppingOrder.Selected.current.getShoppingOrder().getNotificationAlertData()
        );
        alertFragment.show(getSupportFragmentManager(),"notifications");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onClick(View view) {
        new OrderProductsFragment(order.getProductList()).show(getSupportFragmentManager(),"productlist");
    }

    @Override
    public void onSuccess(ShoppingOrder shoppingOrder) {
        ShoppingOrder.Selected.current.setShoppingOrder(shoppingOrder);
        fillData();
    }

    @Override
    public void onError(String messageError) {
        Dialogs.showAlert(this, messageError);
    }

    @Override
    public void onSuccess() {
        startActivityForResult(new Intent(this, POSuccessActivity.class),1);
    }
}