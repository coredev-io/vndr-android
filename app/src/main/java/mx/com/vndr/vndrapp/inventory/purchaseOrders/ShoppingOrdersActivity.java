package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.toolbox.Volley;
import java.util.List;
import java.util.Map;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.ShoppingOrdersAdapter.OnShoppingOrderSelected;
import mx.com.vndr.vndrapp.inventory.supplier.SuppliersActivity;

import static mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders.*;

public class ShoppingOrdersActivity extends AppCompatActivity implements OnShoppingOrdersResponse, OnShoppingOrderSelected {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textViewAIncompleteAmount;
    TextView textViewNoSendAmount;
    TextView textViewNoShoppingOrders;
    ProgressBar progressBar;
    Query query;
    boolean requestForSupplier;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_orders);

        if (getIntent().hasExtra("query")){
            query = (Query) getIntent().getSerializableExtra("query");
            requestForSupplier = false;
        } else {
            query = Query.ALL_SUPPLIERS;
            requestForSupplier = true;
        }
        setupUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        new APIShoppingOrders(Volley.newRequestQueue(this))
                .getShoppingOrders(this, query);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_shopping_orders);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_shopping_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewNoShoppingOrders = findViewById(R.id.txt_no_shopping_orders);
        progressBar = findViewById(R.id.pb_shopping_orders);

        textViewNoSendAmount = findViewById(R.id.txt_no_send_amount);
        textViewAIncompleteAmount = findViewById(R.id.txt_incomplete_amount);

        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.all){
                navigateToCompleteShoppingOrders();
            }
            return false;
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.active_orders_menu,menu);
        return true;
    }

    private void navigateToCompleteShoppingOrders(){
        Query newQuery = null;
        if (query.equals(Query.ALL_SUPPLIERS))
            newQuery = Query.ALL_COMPLETE;
        else if (query.equals(Query.BY_SUPPLIER)){
            newQuery = Query.COMPLETE_BY_SUPPLIERS;
            newQuery.setSupplierId(query.getSupplierId());
        }
        startActivity(new Intent(this, CompletePOActivity.class).putExtra("query",newQuery));
    }

    public void onClickNewShoppingOrder(View view){
        if (requestForSupplier)
            pickSupplier();
        else
            navigateToCart(null);
    }


    @Override
    public void onSuccess(List<ShoppingOrder> shoppingOrders, Map<String, String> amountsHeader) {
        dismissLoading();
        printHeaders(amountsHeader);
        recyclerView.setAdapter(
                new ShoppingOrdersAdapter(shoppingOrders, this)
                        .setOnShoppingOrderSelected(this)
        );
    }

    @Override
    public void onEmptyList(String message) {
        dismissLoading();
        textViewNoShoppingOrders.setVisibility(View.VISIBLE);
        textViewNoShoppingOrders.setText(message);
    }

    @Override
    public void onError(String messageError) {
        dismissLoading();
        textViewNoShoppingOrders.setVisibility(View.VISIBLE);
        textViewNoShoppingOrders.setText(messageError);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        textViewNoShoppingOrders.setVisibility(View.GONE);
        recyclerView.setAdapter(null);
        textViewAIncompleteAmount.setText("$0.00");
        textViewNoSendAmount.setText("$0.00");
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void printHeaders(Map<String, String> amountsHeader){
        textViewAIncompleteAmount.setText(amountsHeader.get("PO00"));
        textViewNoSendAmount.setText(amountsHeader.get("PO01"));
    }

    private void pickSupplier(){
        startActivityForResult(new Intent(this, SuppliersActivity.class).putExtra("pickMode", true), 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && resultCode == RESULT_OK){
            navigateToCart(null);
        }

    }

    /**
     * Go to shopping cart activity
     * @param shoppingOrder null for new shopping cart
     */
    private void navigateToCart(ShoppingOrder shoppingOrder){
        Intent shoppingCartIntent = new Intent(this, POCartActivity.class);
        if (shoppingOrder != null)
            shoppingCartIntent.putExtra("shopping_order", shoppingOrder);
        startActivity(shoppingCartIntent);
    }

    @Override
    public void onSelected(ShoppingOrder shoppingOrder) {
        ShoppingOrder.Selected.current.setShoppingOrder(shoppingOrder);
        //  Si es incompleto
        if (shoppingOrder.getStatusCode().equals("PO00")){
            navigateToCart(shoppingOrder);
        } else {
            navigateToDetail();
        }
    }

    private void navigateToDetail(){
        startActivityForResult(new Intent(this, PODetailActivity.class).putExtra("isNotSended", true),1);
    }

    public enum Query {
        BY_SUPPLIER,ALL_SUPPLIERS, COMPLETE_BY_SUPPLIERS, ALL_COMPLETE;

        private String supplierId;

        public String getSupplierId() {
            return supplierId;
        }

        public void setSupplierId(String supplierId) {
            this.supplierId = supplierId;
        }
    }
}