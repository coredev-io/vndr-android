package mx.com.vndr.vndrapp.api.inventory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Date;

import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class InventoryMovements implements Serializable {

    String formattedDate;
    int units;
    String typeTAG;
    String description;
    Product product;
    String initialStock;
    String currentStock;
    double cost;
    double iva;
    double totalCost;
    String notes;
    String ticketNumber;

    public InventoryMovements(JSONObject jsonObject, boolean parseProduct) throws JSONException {
        this.formattedDate = VndrDateFormat.dateWSFormatToAppFormat(jsonObject.getString("date"));
        this.units = jsonObject.getInt("units");
        this.typeTAG = jsonObject.getString("type_tag");
        this.description = jsonObject.getString("description");
        this.initialStock = String.valueOf(jsonObject.getInt("initial_stock"));
        this.currentStock = String.valueOf(jsonObject.getInt("current_stock"));
        this.cost = jsonObject.getDouble("cost");
        this.iva = jsonObject.getDouble("iva");
        this.totalCost = jsonObject.getDouble("total_cost");
        this.notes = jsonObject.getString("notes");
        this.ticketNumber = String.valueOf(jsonObject.getInt("ticket"));

        if (parseProduct) {
            JSONObject productJSON = jsonObject.getJSONObject("product");
            this.product = new Product(
                    productJSON.getString("_id"),
                    productJSON.getString("product_name"),
                    productJSON.getString("product_key"),
                    productJSON.getString("size"),
                    productJSON.getString("color"),
                    productJSON.getJSONObject("brand").getString("brand_name")
            );
        }


    }

    public String getFormattedDate() {
        return formattedDate;
    }

    public int getUnits() {
        return units;
    }

    public String getTypeTAG() {
        return typeTAG;
    }

    public String getDescription() {
        return description;
    }

    public Product getProduct() {
        return product;
    }

    public String getInitialStock() {
        return initialStock;
    }

    public String getCurrentStock() {
        return currentStock;
    }

    public double getCost() {
        return cost;
    }

    public double getIva() {
        return iva;
    }

    public double getTotalCost() {
        return totalCost;
    }

    public String getCurrencyCost() {
        return NumberFormat.getCurrencyInstance().format(cost);
    }

    public String getCurrencyIva() {
        return NumberFormat.getCurrencyInstance().format(iva);
    }

    public String getCurrencyTotalCost() {
        return NumberFormat.getCurrencyInstance().format(totalCost);
    }

    public String getNotes() {
        return notes;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }
}
