package mx.com.vndr.vndrapp.customviews.VndrList;

public class ProductAnalytic {
    String name, key, quantity, total, saleAvgAmount, inventoryAmount, marginAvg;
    boolean isPurchase = false;
    boolean isInvoice = false;
    String orderNumber, orderDate;

    public ProductAnalytic(String name, String key, String quantity, String total, String saleAvgAmount, String inventoryAmount, String marginAvg) {
        this.name = name;
        this.key = key;
        this.quantity = quantity;
        this.total = total;
        this.saleAvgAmount = saleAvgAmount;
        this.inventoryAmount = inventoryAmount;
        this.marginAvg = marginAvg;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getSaleAvgAmount() {
        return saleAvgAmount;
    }

    public void setSaleAvgAmount(String saleAvgAmount) {
        this.saleAvgAmount = saleAvgAmount;
    }

    public String getInventoryAmount() {
        return inventoryAmount;
    }

    public void setInventoryAmount(String inventoryAmount) {
        this.inventoryAmount = inventoryAmount;
    }

    public String getMarginAvg() {
        return marginAvg;
    }

    public void setMarginAvg(String marginAvg) {
        this.marginAvg = marginAvg;
    }

    public boolean isPurchase() {
        return isPurchase;
    }

    public void setPurchase(boolean purchase) {
        isPurchase = purchase;
    }

    public boolean isInvoice() {
        return isInvoice;
    }

    public void setInvoice(boolean invoice) {
        isInvoice = invoice;
    }
}
