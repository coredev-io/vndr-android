package mx.com.vndr.vndrapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.OrderDetailViewHolder> {


    List<String> values;
    List<String> keys;
    View.OnClickListener listener;

    public OrderDetailAdapter(List<String> values, List<String> keys, View.OnClickListener listener) {
        this.values = values;
        this.keys = keys;
        this.listener = listener;
    }

    @NonNull
    @Override
    public OrderDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_detail_item, parent, false);

        return new OrderDetailViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderDetailViewHolder holder, int position) {
        holder.key.setText(keys.get(position));
        holder.value.setText(values.get(position));
        holder.imageView.setVisibility(View.GONE);

        if (keys.get(position).equals("Productos")){
            holder.layout.setOnClickListener(listener);
            holder.imageView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return keys.size();
    }


    public class OrderDetailViewHolder extends RecyclerView.ViewHolder {

        TextView key, value;
        ConstraintLayout layout;
        ImageView imageView;
        public OrderDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            key = itemView.findViewById(R.id.key);
            value = itemView.findViewById(R.id.value);
            layout = itemView.findViewById(R.id.layout_item);
            imageView = itemView.findViewById(R.id.img_dropdown);
        }
    }
}


