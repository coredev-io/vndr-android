package mx.com.vndr.vndrapp.orders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.OrderProductsFragment;
import mx.com.vndr.vndrapp.PaymentCalendarActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.OrderDetailAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.models.Cxc;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.models.PaymentCalendar;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDER_DETAIL;

public class OrderReviewActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    OrderDetailAdapter adapter;
    List<String> valueList = new ArrayList<>();
    List<String> keysList = new ArrayList<>();
    TextView complete, send, delivery;
    Button button;
    Toolbar toolbar;
    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_review);
        setUpUI();

        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
            setStatusUI();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (order != null) {
            valueList = new ArrayList<>();
            keysList = new ArrayList<>();
            adapter = new OrderDetailAdapter(valueList,keysList, this);
            recyclerView.setAdapter(adapter);
            getOrderDetail(order.getIdOrder());
        }

    }

    public void getData(){

        keysList.add("Cliente");
        valueList.add( order.getCustomer().getCustomerFullName());

        keysList.add("Productos");
        valueList.add(String.valueOf(order.getProducts().size()));

        keysList.add( "Total mercancía");
        valueList.add(NumberFormat.getCurrencyInstance().format(order.getOrderAmount()));

        keysList.add("Cargo por envío");
        valueList.add( NumberFormat.getCurrencyInstance().format(order.getShippingAmount()));

        keysList.add("Total del pedido");
        valueList.add(NumberFormat.getCurrencyInstance().format(order.getTotalOrderAmount()));

        keysList.add("Fecha de venta");
        valueList.add(order.getParseSaleDate());

        if (order.getDelivery().getEstimateDeliveryDate() != null){
            keysList.add( "Fecha de entrega");
            valueList.add(order.getDelivery().getEstimateDeliveryDateFormatt());
        }

        if (!order.getDelivery().isPersonalDelivery()){

            if (order.getDelivery().getEstimateShippingDate() != null){
                keysList.add( "Fecha de envío");
                valueList.add(order.getDelivery().getEstimateShippingDateFormatt());
            }

            if (order.getDelivery().getMessengerService() != null){
                keysList.add( "Forma de entrega");
                valueList.add(order.getDelivery().getMessengerService());
            }
            if (order.getDelivery().getTrackingCode() != null){
                keysList.add( "Código de rastreo");
                valueList.add(order.getDelivery().getTrackingCode());
            }

        }

        if (order.getCxc().getPayFrequency() != 1){

            keysList.add("Monto anticipo");
            valueList.add( NumberFormat.getCurrencyInstance().format(order.getCxc().getAnticipo()));

            if (order.getCxc().getPayDate() != null){

                keysList.add("Fecha de pago anticipo");
                valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getPayDate()));


                keysList.add("Medio de pago");
                valueList.add(order.getCxc().getMethodPayment());
            }



            keysList.add("Saldo a pagar en abonos");
            valueList.add(NumberFormat.getCurrencyInstance().format(order.getCxc().getSaldoPlazo()));


            keysList.add("Periodicidad de pago");

            switch (order.getCxc().getPayFrequency()){
                case 7:
                    valueList.add("Semanal");
                    break;
                case 15:
                    valueList.add("Quincenal");
                    break;
                case 30:
                    valueList.add("Mensual");
                    break;
            }

            keysList.add("Número de pagos");
            valueList.add(String.valueOf(order.getCxc().getNumPagos()));


            keysList.add("Importe de cada pago");
            valueList.add(NumberFormat.getCurrencyInstance().format(order.getCxc().getMontoPago()));


            keysList.add("Fecha de primer pago");
            valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getFirstPayDate()));

        } else{

            keysList.add("Medio de pago");
            valueList.add(order.getCxc().getMethodPayment());

            if (!order.getCxc().getMethodPayment().equals("Pendiente de pago")){
                keysList.add( "Fecha de pago");
                valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getPayDate()));
                keysList.add("Saldo pendiente");
                valueList.add("$0.00");



            } else{

                if (order.getCxc().getPayDate() != null){
                    keysList.add( "Fecha esperada de pago");
                    valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getPayDate()));
                }


                keysList.add("Saldo pendiente");
                valueList.add( order.getTotalOrderAmountStr());




            }


        }

        keysList.add("Notas");
        valueList.add( order.getNotes());

        adapter = new OrderDetailAdapter(valueList, keysList, this);
        recyclerView.setAdapter(adapter);
    }


    private void getOrderDetail(String idOrder){
        //progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_ORDER_DETAIL + idOrder, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //progressBar.setVisibility(View.GONE);
                processOrderDetail(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressBar.setVisibility(View.GONE);
                System.out.println(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void processOrderDetail(String response){
        List<ShoppingCartProduct> productList = new ArrayList<>();
        try {
            JSONObject objectResponse = new JSONObject(response);
            JSONArray productsArray = objectResponse.getJSONArray("order_products");

            for (int i = 0; i < productsArray.length(); i++){

                JSONObject productObject = productsArray.getJSONObject(i).getJSONObject("product");

                Product product = new Product(
                        productObject.getString("_id"),
                        productObject.getString("sku"),
                        productObject.getString("product_name"),
                        productObject.getString("standard_classification"),
                        productObject.getString("notes"),
                        productObject.getString("sub_brand"),
                        productObject.getString("category"),
                        productObject.getString("subcategory"),
                        productObject.getString("product_key"),
                        productObject.getString("size"),
                        productObject.getString("color"),
                        productObject.getString("description"),
                        productObject.getString("url_image"),
                        productObject.getString("product_points"),
                        productObject.getString("barcode"),
                        productObject.getString("qrcode"),
                        productObject.getDouble("list_price"),
                        productObject.getBoolean("discontinued"),
                        new Catalog(
                                productObject.getJSONObject("catalog").getString("_id"),
                                productObject.getJSONObject("catalog").getString("catalog_name")
                        )
                );

                ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct(
                        product,
                        productsArray.getJSONObject(i).getDouble("sale_price"),
                        productsArray.getJSONObject(i).getDouble("adjustment"),
                        productsArray.getJSONObject(i).getDouble("subtotal"),
                        productsArray.getJSONObject(i).getInt("products_amount"),
                        productsArray.getJSONObject(i).getString("notes")
                );
                productList.add(shoppingCartProduct);
            }


            /*
                General
             */

            order.setProducts(productList);


            order.setTotalOrderAmount(objectResponse.getDouble("total_order_amount"));
            order.setShippingAmount(objectResponse.getDouble("shipping_pay"));
            order.setOrderNumber(objectResponse.getInt("order_number"));
            order.setSaleDateFormmat(objectResponse.getString("sale_date"));
            order.setOrderAmount(objectResponse.getDouble("order_amount"));
            order.setNotes(objectResponse.getString("notes"));

            if (objectResponse.has("complete_date"))
                order.setCompleteDate(objectResponse.getString("complete_date"));

            if (objectResponse.has("shipping_date"))
                order.setShippingDate(objectResponse.getString("shipping_date"));

            if (objectResponse.has("delivery_date"))
                order.setDeliveryDate(objectResponse.getString("delivery_date"));


            if (objectResponse.has("delivery")){
                Delivery delivery = new Delivery();
                JSONObject deliveryJSON = objectResponse.getJSONObject("delivery");
                delivery.setType(deliveryJSON.getString("delivery_type"));

                if (delivery.isPersonalDelivery() && deliveryJSON.has("estimate_delivery_date")){
                    delivery.setEstimateDeliveryDate(deliveryJSON.getString("estimate_delivery_date"));
                } else if (!delivery.isPersonalDelivery()){

                    if (deliveryJSON.has("estimate_delivery_date"))
                        delivery.setEstimateDeliveryDate(deliveryJSON.getString("estimate_delivery_date"));
                    if (deliveryJSON.has("messenger_service"))
                        delivery.setMessengerService(deliveryJSON.getString("messenger_service"));
                    if (deliveryJSON.has("traking_code"))
                        delivery.setTrackingCode(deliveryJSON.getString("traking_code"));
                    if (deliveryJSON.has("estimate_shipping_date"))
                        delivery.setEstimateShippingDate(deliveryJSON.getString("estimate_shipping_date"));
                }

                order.setDelivery(delivery);
            }

            if (objectResponse.has("cxc")){

                order.setCxc(new Cxc());


                order.getCxc().setPayFrequency(objectResponse.getJSONObject("cxc").getInt("ar_pmt_frequency"));
                if (objectResponse.getJSONObject("cxc").has("ar_downpayment_amount"))
                    order.getCxc().setAnticipo(objectResponse.getJSONObject("cxc").getDouble("ar_downpayment_amount"));

                if (objectResponse.getJSONObject("cxc").has("ar_payment_method")){
                    order.getCxc().setMethodPayment(objectResponse.getJSONObject("cxc").getString("ar_payment_method"));
                    order.getCxc().setPayDate(objectResponse.getJSONObject("cxc").getString("ar_pmt_due_date"));
                }
                else {
                    order.getCxc().setMethodPayment("Pendiente de pago");
                }


                if(order.getCxc().getPayFrequency() != 1){
                    order.getCxc().setAnticipo(objectResponse.getJSONObject("cxc").getDouble("ar_downpayment_amount"));
                    order.getCxc().setNumPagos(objectResponse.getJSONObject("cxc").getInt("ar_number_periods"));
                    order.getCxc().setMontoPago(objectResponse.getJSONObject("cxc").getDouble("ar_pmt_amount"));

                    order.getCxc().setDiasAviso(objectResponse.getJSONObject("cxc").getInt("ar_pmt_early_notice"));
                    order.getCxc().setDiasCortesia(objectResponse.getJSONObject("cxc").getInt("ar_grace_period"));
                    //order.getCxc().setAlertas(objectResponse.getJSONObject("cxc").getBoolean("ar_pmt_collections_notice"));

                    order.getCxc().setFirstPayDate(objectResponse.getJSONObject("cxc").getString("ar_initial_duedate"));


                }

            }

            if (objectResponse.has("shipping_date")) {
                order.setSendedDate(objectResponse.getString("shipping_date"));
                System.out.println(order.getSendedDate());

            }

            if (objectResponse.has("status")){
                order.setStatus(
                        objectResponse.getJSONObject("status").getBoolean("complete"),
                        objectResponse.getJSONObject("status").getBoolean("sended"),
                        objectResponse.getJSONObject("status").getBoolean("delivered")
                        );
            }
            if (objectResponse.has("schedule_info")){
                System.out.println(objectResponse.getJSONObject("schedule_info").toString());
                order.getCxc().setSaldoPlazo(objectResponse.getJSONObject("schedule_info").getDouble("order_amount"));

                JSONArray calendarArray = objectResponse.getJSONObject("schedule_info").getJSONArray("calendar");
                List<PaymentCalendar> paymentsCalendar = new ArrayList<>();

                for (int i = 0; i < calendarArray.length(); i++) {
                    JSONObject calendar = calendarArray.getJSONObject(i);

                    if (calendar.getInt("number") == 0) {
                        System.out.println("=_=_=_=_=_=_=_=_=_=_=_=_=_=_=_");
                        order.setDownPaymentPaid(false);
                        System.out.println(order.isDownPaymentPaid());
                        order.getCxc().setAnticipo(calendar.getDouble("amount"));
                        order.getCxc().setPayDate(calendar.getString("due_date"));
                        continue;
                    }

                    paymentsCalendar.add(new PaymentCalendar(
                            calendar.getString("title"),
                            calendar.getDouble("amount"),
                            VndrDateFormat.dateWSFormatToAppFormat(calendar.getString("due_date"))
                    ));

                    order.setScheduleInfo(paymentsCalendar);
                }


                order.setNotificationAlertData(
                        new NotificationAlertData(
                                objectResponse.getJSONObject("schedule_info").getJSONObject("notifications"),
                                objectResponse.getJSONObject("schedule_info").getJSONObject("contact_info")
                        )
                );
            }
            getData();
            setStatusUI();

            toolbar.setTitle("Pedido " + order.getOrderNumberStr());


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void setStatusUI(){

        if (order.getDelivery().isPersonalDelivery()){
            send.setVisibility(View.GONE);
        }


        switch (order.getStatus()){

            case Order.TERMINADO:
                button.setVisibility(View.GONE);

                complete.setTextColor(getResources().getColor(R.color.colorAccent));
                complete.setTypeface(null, Typeface.BOLD);
                complete.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                if (order.isCompleteDate())
                    complete.setText("Fecha de venta " + order.getParseSaleDate());


                send.setTextColor(getResources().getColor(R.color.colorAccent));
                send.setTypeface(null, Typeface.BOLD);
                send.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                if (order.isShippingDate())
                    send.setText("Enviado " + order.getShippingDate());

                delivery.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                delivery.setTextColor(getResources().getColor(R.color.colorAccent));
                delivery.setTypeface(null, Typeface.BOLD);
                if (order.isDeliveryDate())
                    delivery.setText("Entregado " + order.getDeliveryDate());

                break;
            case Order.NO_ENTREGADO:

                complete.setTextColor(getResources().getColor(R.color.colorAccent));
                complete.setTypeface(null, Typeface.BOLD);
                complete.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                if (order.isCompleteDate())
                    complete.setText("Fecha de venta " + order.getParseSaleDate());

                send.setTextColor(getResources().getColor(R.color.colorAccent));
                send.setTypeface(null, Typeface.BOLD);
                send.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                if (order.isShippingDate())
                    send.setText("Enviado " + order.getShippingDate());

                break;
            case Order.NO_ENVIADO:
                complete.setTextColor(getResources().getColor(R.color.colorAccent));
                complete.setTypeface(null, Typeface.BOLD);
                complete.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                if (order.isCompleteDate())
                    complete.setText("Fecha de venta " + order.getParseSaleDate());
                break;
        }

    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_order_review);
        recyclerView = findViewById(R.id.rv_order_review);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        complete = findViewById(R.id.txt_stat_complete);
        send = findViewById(R.id.txt_stat_send);
        delivery = findViewById(R.id.txt_stat_delivery);
        button = findViewById(R.id.btn_review_order);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.all){
                if (order.getCxc().getPayFrequency() == 1)
                    Dialogs.showAlert(this, "Este pedido no cuenta con un calendario de pagos.");
                else
                startActivity(
                        new Intent(
                                OrderReviewActivity.this,
                                PaymentCalendarActivity.class)
                                .putExtra("order", order)
                                .putExtra("isReadMode", true)
                                .putExtra("isPaid", order.isDownPaymentPaid())
                );
            }
            return false;
        });
    }

    public void onClickCompleteStatus(View view){
        startActivityForResult(new Intent(this,CompleteOrderStatusActivity.class).putExtra("order",order),0);
    }

    @Override
    public void onClick(View view) {
        new OrderProductsFragment(order.getProducts()).show(getSupportFragmentManager(),"productlist");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.active_orders_menu,menu);
        return true;
    }
}
