package mx.com.vndr.vndrapp.catalogs;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Catalog;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_CATALOGS;

public class CatalogListInteractor {

    private String token;
    private RequestQueue queue;
    private CatalogListAdapter adapter;
    private List<Catalog> catalogList;

    public CatalogListInteractor(String token, RequestQueue queue) {
        this.token = token;
        this.queue = queue;
        catalogList = new ArrayList<>();
    }

    public void getData(Brand brand, final OnRequestFinish listener){
        String url = URL_CATALOGS + "?id_brand=" + brand.getBrandId() + "&type=edit";
        Log.e("TAG",url);
        StringRequest request = new StringRequest(Request.Method.GET, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);

                try {
                    processResponse(response,listener);
                    createAdapter(listener);
                    listener.onSuccess(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                    processEmptyResponse(response,listener);
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");

                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        queue.add(request);
    }

    private void processResponse(String response, final OnRequestFinish listener) throws JSONException {
        JSONArray array = new JSONArray(response);

        catalogList = new ArrayList<>();

        for (int i = 0; i < array.length(); i++){
            JSONObject object = array.getJSONObject(i).getJSONObject("catalog");
            Catalog catalog = new Catalog(
                    object.getString("_id"),
                    object.getString("catalog_name"),
                    object.getString("catalog_key"),
                    object.getBoolean("validity")
            );

            catalog.setExpired(array.getJSONObject(i).getBoolean("active"));

            if (object.has("collaborative_catalog")){
                catalog.setCollaborative(object.getBoolean("collaborative_catalog"));
            }
            if (catalog.isValid()){
                if (object.has("catalog_date_init")){
                    catalog.setInitDate(object.getString("catalog_date_init"));
                }
                if (object.has("catalog_date_end")){
                    catalog.setEndDate(object.getString("catalog_date_end"));
                }
            }

            if (object.has("brand")){
                catalog.setIdBrand(object.getJSONObject("brand").getString("_id"));
            }
            if (object.has("is_public")){
                catalog.setPublic(object.getBoolean("is_public"));
            }

            if (object.has("brand_owner")){

                if (object.getString("brand_owner").equals(VndrAuth.getInstance().getCurrentUser().getUid()))
                    catalog.setItsMine(true);
                else if (object.getString("brand_owner").equals("adminvndr"))
                    catalog.setOwnerIsVndr(true);
                else
                    catalog.setItsMine(false);
            }

            catalogList.add(catalog);
        }

    }

    private void processEmptyResponse(String response, final OnRequestFinish listener){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getInt("statusCode") == 404){
                listener.onEmptyCatalogs();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");
        }
    }

    private void createAdapter(final OnRequestFinish listener){
        adapter = new CatalogListAdapter(catalogList, new CatalogListAdapter.OnClickCatalogListener() {
            @Override
            public void onSelect(Catalog catalog) {
                listener.onItemClickListener(catalog);
            }
        });
    }


    interface OnRequestFinish{
        void onSuccess(CatalogListAdapter adapter);
        void onError(String error);
        void onEmptyCatalogs();
        void onItemClickListener(Catalog catalog);
    }
}
