package mx.com.vndr.vndrapp.inventory.productsPurchase;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;

public class PurchaseInvoiceDetailAdapter extends RecyclerView.Adapter<PurchaseInvoiceDetailAdapter.PurchaseInvoiceDetailViewHolder> {

    List<String> values;
    List<String> keys;
    View.OnClickListener listener;

    public PurchaseInvoiceDetailAdapter(List<String> values, List<String> keys, View.OnClickListener listener) {
        this.values = values;
        this.keys = keys;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PurchaseInvoiceDetailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_detail_item, parent, false);

        return new PurchaseInvoiceDetailViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PurchaseInvoiceDetailViewHolder holder, int position) {
        holder.key.setText(keys.get(position));
        holder.value.setText(values.get(position));
        holder.imageView.setVisibility(View.GONE);

        if (keys.get(position).equals("Productos")){
            holder.layout.setOnClickListener(listener);
            holder.imageView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return keys.size();
    }


    public class PurchaseInvoiceDetailViewHolder extends RecyclerView.ViewHolder {

        TextView key, value;
        ConstraintLayout layout;
        ImageView imageView;
        public PurchaseInvoiceDetailViewHolder(@NonNull View itemView) {
            super(itemView);
            key = itemView.findViewById(R.id.key);
            value = itemView.findViewById(R.id.value);
            layout = itemView.findViewById(R.id.layout_item);
            imageView = itemView.findViewById(R.id.img_dropdown);
        }
    }
}
