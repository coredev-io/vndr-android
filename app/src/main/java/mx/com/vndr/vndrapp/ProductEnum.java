package mx.com.vndr.vndrapp;

public enum ProductEnum {
    CATALOGS_PARENT,
    QUERY_PRODUCTS_BY_BRAND,
    QUERY_PRODUCTS_BY_CLASS
}
