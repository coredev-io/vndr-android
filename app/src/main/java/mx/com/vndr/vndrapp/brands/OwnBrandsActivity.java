package mx.com.vndr.vndrapp.brands;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.catalogs.CatalogsListActivity;
import mx.com.vndr.vndrapp.catalogs.newBrand.NewBrandActivity;
import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.util.DataVndr;

public class OwnBrandsActivity extends AppCompatActivity implements OwnBrandsView{

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView txtNoOwnBrands;
    OwnBrandsPresenter presenter;
    Boolean goToCatalogs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_own_brands);

        toolbar = findViewById(R.id.tb_own_brands);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.rv_own_brands);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        progressBar = findViewById(R.id.pb_own_brands);
        txtNoOwnBrands = findViewById(R.id.txt_no_own_brands);

        BrandInteractor interactor = new BrandInteractor(Volley.newRequestQueue(this));
        presenter = new OwnBrandsPresenter(this, interactor);
        goToCatalogs = getIntent().getBooleanExtra("goToCatalogs", false);

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getOwnBrands();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyBrands() {
        txtNoOwnBrands.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyBrands() {
        txtNoOwnBrands.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showData(CheckedItemAdapter adapter) {
        recyclerView.setAdapter(null);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showBrandDetail(Brand brand) {

        if (goToCatalogs){
            startActivity(new Intent(
                    this,
                    CatalogsListActivity.class
                ).putExtra("brand", brand)
                .putExtra("activity", CatalogActivity.CATALOG_ACTIVITY)
            );

        } else {
            startActivity(
                    new Intent(
                            this, NewBrandActivity.class
                    )
                            .putExtra("brand",brand)
            );
        }
    }

    public void navigateToNewBrand(View view){
        startActivity(new Intent(
                this, NewBrandActivity.class
        ));
    }
}
