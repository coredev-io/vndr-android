package mx.com.vndr.vndrapp.catalogs.associateBrand;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public interface AssociateBrandView {
    void setAdapter(CheckedItemAdapter adapter);
    void showProgress();
    void dismissProgress();
    void showRequestError(String error);
    void showModal(Brand brand);
}
