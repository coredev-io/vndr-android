package mx.com.vndr.vndrapp.brands;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.catalogs.CatalogListAdapter;
import mx.com.vndr.vndrapp.catalogs.CatalogsListActivity;
import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.util.DataVndr;

public class CollaborativeBrandsActivity extends AppCompatActivity implements CollaborativeBrandsView {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    CollaborativePresenter presenter;
    TextView emptyBrands;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collaborative_brands);

        toolbar = findViewById(R.id.tb_coll_brands);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        emptyBrands = findViewById(R.id.txt_coll_brand);
        recyclerView = findViewById(R.id.rv_coll_brands);
        emptyBrands.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        progressBar = findViewById(R.id.pb_coll_brands);
        presenter = new CollaborativePresenter(this,
                new BrandInteractor(Volley.newRequestQueue(this)));
        presenter.getCollaborativeBrands();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyBrands() {
        emptyBrands.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyBrands() {
        emptyBrands.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showData(CheckedItemAdapter adapter) {
        recyclerView.setAdapter(null);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void navigateToCatalogEditable(Brand brand) {
        System.out.println(brand.getBrandName());
        startActivity(
                new Intent(
                        this,
                        CatalogsListActivity.class
                ).putExtra("brand",brand).putExtra("activity", CatalogActivity.PRODUCT_ACTIVITY)
        );
    }
}
