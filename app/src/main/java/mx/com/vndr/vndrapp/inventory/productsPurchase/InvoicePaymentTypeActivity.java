package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PAYMENT_METHODS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PAYMENT_METHODS_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.PaymentMethodAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class InvoicePaymentTypeActivity extends AppCompatActivity implements PaymentMethodAdapter.OnPaymentSelected  {

    RecyclerView recyclerView;
    PaymentMethodAdapter adapter;
    List<String> methods;
    List<String> codes;
    Toolbar toolbar;

    PurchaseInvoice purchaseInvoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_payment_type);

        recyclerView = findViewById(R.id.rv_payment_type);
        toolbar = findViewById(R.id.tb_invoice_type);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        methods = new ArrayList<>();
        adapter = new PaymentMethodAdapter(methods);

        adapter.setSelected(this);

        recyclerView.setAdapter(adapter);


        getPaymentMethods();
        purchaseInvoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();

    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.notifyDataSetChanged();

    }

    public void getPaymentMethods(){
        StringRequest request = new StringRequest(Request.Method.GET, URL_PAYMENT_METHODS_INVOICE,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        processResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    public void processResponse(String response){
        try {
            JSONArray array = new JSONArray(response);
            codes = new ArrayList<>();
            for (int i = 0; i < array.length(); i ++) {
                JSONObject jsonObject = array.getJSONObject(i);
                methods.add(jsonObject.getString("payment_method"));
                codes.add(jsonObject.getString("code"));
            }

            adapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onPaymentSelected(int position) {
        Map<String, String> params = new HashMap<>();
        params.put("payment_method", codes.get(position));


        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .patch(URL_PURCHASE_INVOICE + "/" + purchaseInvoice.getPurchaseId() , new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Dialogs.showAlert(InvoicePaymentTypeActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        startActivityForResult(
                                new Intent(InvoicePaymentTypeActivity.this, InvoiceReviewActivity.class),
                                1);
                    }
                });
    }

}