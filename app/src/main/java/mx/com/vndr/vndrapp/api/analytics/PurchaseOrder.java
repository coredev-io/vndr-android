package mx.com.vndr.vndrapp.api.analytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;

import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class PurchaseOrder {

    String id;
    String supplierName;
    String status;
    String purchaseDate;
    String sendDate;
    String totalAmount;
    String orderNumber;
    String statusCode;

    public PurchaseOrder(JSONObject jsonObject) throws JSONException {
        id = jsonObject.getString("_id");
        orderNumber = "#" + String.valueOf(jsonObject.getInt("purchase_number"));
        supplierName = jsonObject.getString("supplier");
        purchaseDate = VndrDateFormat.stringDateParamsFormatToAppFormat(jsonObject.getString("order_date"));
        sendDate = VndrDateFormat.stringDateParamsFormatToAppFormat(jsonObject.getString("sending_date"));
        totalAmount = NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("amount"));
        status = jsonObject.getString("label");
        statusCode = jsonObject.getString("status_code");
    }

    public String getId() {
        return id;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public boolean isCompleted(){
        return statusCode.equals("PO02");
    }

    public boolean isNotSended(){
        return statusCode.equals("PO01");
    }
}
