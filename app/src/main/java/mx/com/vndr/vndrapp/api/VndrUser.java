package mx.com.vndr.vndrapp.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.AppState;
import mx.com.vndr.vndrapp.account.UserProfile;
import mx.com.vndr.vndrapp.registro.SignUpData;
import mx.com.vndr.vndrapp.util.VndrSecure;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_CHANGE_USER_DEVICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_GRANT_PERMISSION_USER;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_USER;

public class VndrUser {

    private static final String TAG = "VndrUser";

    private FirebaseUser firebaseUser;
    private String sessionToken;
    private RequestQueue requestQueue;
    private Context context;

    public VndrUser(){
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        context = FirebaseAuth.getInstance().getApp().getApplicationContext();
        requestQueue = Volley.newRequestQueue(context);
    }

    public String getUid(){
        return firebaseUser.getUid();
    }

    public void updateFirebaseUser(){
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        getSessionTokenFromFirebase(new VndrUserCallback() {
            @Override
            public void onVndrUserCallbackSuccess() {

            }

            @Override
            public void onVndrUserCallbackError(String messageError) {

            }
        });

    }

    /**q
     * Actualiza la información del usuario
     * antes de incovar este método es necesario
     * instanciar el objeto UserProfile
     * @param callback
     */

    public void updateInfoUserWithUserProfile(VndrUserCallback callback){

        new VndrRequest(requestQueue)
                .setRequestParams(UserProfile.getInstance().getParamsBody())
                .patch(URL_USER, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.onVndrUserCallbackError(message);
                    }

                    @Override
                    public void success(String response) {
                        if (!UserProfile.getInstance().isEmailUpdated())
                            callback.onVndrUserCallbackSuccess();
                        else
                            updateEmail(UserProfile.getInstance().getEmail(), callback);

                    }
                });

    }

    private void updateEmail(String email, VndrUserCallback callback){
        firebaseUser.updateEmail(email).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                Log.d(TAG, "Email updated successfull");
                setEmail(email);
                UserProfile.getInstance().setEmailWasUpdated(true);
                callback.onVndrUserCallbackSuccess();
            }
            else{
                rollbackEmail();
                Log.e(TAG, "Failed to update email. Reason: ");
                if (task.getException().getMessage() != null)
                    Log.e(TAG, task.getException().getMessage());
                callback.onVndrUserCallbackError(task.getException().getMessage());
            }

        });
    }

    private void rollbackEmail(){
        Map<String, String> params = new HashMap<>();
        params.put("mail", getEmail());
        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .put(URL_USER, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {}

                    @Override
                    public void success(String response) {}
                });
    }

    /**
     * Realiza la validación de seguridad de los datos del usaurio.
     * Si los datos son correctos actualiza la información del
     * dispositivo de acceso en la plataforma
     * @param mail
     * @param mobilePhone
     * @param zipCode
     * @param callback
     */

    public void changeDevice(String mail, String mobilePhone, String zipCode, VndrUserCallback callback){

        Map<String, String> params = new HashMap<>();
        params.put("mail", mail);
        params.put("mobilePhone", mobilePhone);
        params.put("zipCode",zipCode);
        params.put("os","Android");
        params.put("osVersion", String.valueOf(android.os.Build.VERSION.SDK_INT));
        params.put("phoneModel", android.os.Build.MODEL);
        params.put("phoneManufacturer", Build.MANUFACTURER);
        params.put("appInstanceId", FirebaseAuth.getInstance().getUid());

        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .put(URL_CHANGE_USER_DEVICE, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.onVndrUserCallbackError(message);
                    }

                    @Override
                    public void success(String response) {
                        createSession(callback);
                    }
                });
    }

    /**
     * Actualiza los permisos de uso de datos personales y de clientes.
     * Para invocar este método es necesario primero instanciar un
     * objeto de tipo SignUpData
     * @param callback
     */

    public void grantPermissionDataUsageWithSignUpData(VndrUserCallback callback){

        if (sessionToken.isEmpty())
            callback.onVndrUserCallbackError("El usuario no cuenta con una sesióna activa.");
        Map<String, String> params = new HashMap<>();
        params.put("personalDataAuth", String.valueOf(SignUpData.getInstance().isShareMyData()));
        params.put("clientsDataAuth", String.valueOf(SignUpData.getInstance().isShareCustomerData()));
        params.put("survey", SignUpData.getInstance().getSurvey());

        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .put(URL_GRANT_PERMISSION_USER, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.onVndrUserCallbackError(message);
                    }

                    @Override
                    public void success(String response) {
                        setPersonalDataUsageAuth(SignUpData.getInstance().isShareMyData());
                        setCustomersDataUsageAuth(SignUpData.getInstance().isShareCustomerData());
                        callback.onVndrUserCallbackSuccess();
                    }
                });
    }

    /**
     * Actualiza los permisos de uso de datos personales y de clientes
     * @param personalData
     * @param clientsData
     * @param callback
     */
    public void grantPermissionDataUsage(boolean personalData, boolean clientsData, VndrUserCallback callback){

        Map<String, String> params = new HashMap<>();
        params.put("personalDataAuth", String.valueOf(personalData));
        params.put("clientsDataAuth", String.valueOf(clientsData));

        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .put(URL_GRANT_PERMISSION_USER, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.onVndrUserCallbackError(message);
                    }

                    @Override
                    public void success(String response) {
                        setPersonalDataUsageAuth(personalData);
                        setCustomersDataUsageAuth(clientsData);
                        callback.onVndrUserCallbackSuccess();
                    }
                });
    }

    /**
     * Recupera el token de sesión proporcionado por firebase
     * @param callback
     */
    public void getSessionTokenFromFirebase(VndrUser.VndrUserCallback callback){
        firebaseUser.getIdToken(true).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                sessionToken = task.getResult().getToken();
                Log.d(TAG, "Firebase token getted successfull " + sessionToken);
                callback.onVndrUserCallbackSuccess();
            } else {
                Log.e(TAG, "Failed to get firebase token. Reason: ");
                Log.e(TAG, task.getException().getMessage());
                callback.onVndrUserCallbackError(task.getException().getMessage());
            }
        });
    }

    /**
     * Crea una sesión en la plataforma vndr+
     * Si no se tiene un token de sesión lo obtiene primero
     * @param callback
     */
    public void createVndrSession(VndrUserCallback callback){
        if (sessionToken == null){
            getSessionTokenFromFirebase(new VndrUserCallback() {
                @Override
                public void onVndrUserCallbackSuccess() {
                    createSession(callback);
                }

                @Override
                public void onVndrUserCallbackError(String messageError) {
                    callback.onVndrUserCallbackError(messageError);
                }
            });
        } else {
            createSession(callback);
        }
    }

    private void createSession(VndrUserCallback callback){
        Map<String, String> params = new HashMap<>();
        VndrSecure.appendDeviceInfor(params);

        new VndrRequest(requestQueue)
                .setRequestParams(params)
                .post(URLVndr.URL_VALIDATE_TOKEN, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.onVndrUserCallbackError(message);
                    }

                    @Override
                    public void statusCodeError(int statusCode, String message) {
                        if (statusCode == 403){
                            callback.callbackErrorStatusCode(statusCode);
                        } else {
                            callback.onVndrUserCallbackError(message);
                        }
                    }

                    @Override
                    public void licenseError(String message, VndrMembership.LicenseVndrStatus status) {
                        callback.onVndrUserCallbackLicenseError(message, status);
                    }

                    @Override
                    public void success(String response) {
                        Log.e(TAG, response);

                        try {
                            JSONObject object = new JSONObject(response).getJSONObject("user");
                            UserProfile.getInstance().parseWithJSONObject(object);

                            setEmail(object.getString("mail"));
                            setPhoneNumber(object.getString("mobilePhone"));
                            String name = object.getString("firstGivenName");
                            name += " " + object.getString("firstFamilyName");
                            setName(name);
                            setCustomersDataUsageAuth(object.getBoolean("clientsDataAuth"));
                            setPersonalDataUsageAuth(object.getBoolean("personalDataAuth"));
                            //  Save License for App state
                            object = new JSONObject(response).getJSONObject("license_info");


                            if (object.getString("status").isEmpty()) {
                                VndrMembership.status = VndrMembership.LicenseVndrStatus.LC99;
                            } else {
                                VndrMembership.status = VndrMembership.LicenseVndrStatus.valueOf(object.getString("status"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            callback.onVndrUserCallbackError(e.getLocalizedMessage());
                        }
                        callback.onVndrUserCallbackSuccess();
                    }
                });
    }

    public boolean isEmailVerified(){
        Log.e(TAG, "Is email verified: " + firebaseUser.isEmailVerified());
        return firebaseUser.isEmailVerified();
    }

    /**
     * Envía un email para verificar el correo electrónico
     */
    public void sendEmailVerification(){
        firebaseUser.sendEmailVerification().addOnCompleteListener(task -> {
            if (task.isSuccessful())
                Log.d(TAG, "Email verification sended successfull");
            else{
                Log.e(TAG, "Failed to send email verification. Reason: ");
                if (task.getException().getMessage() != null)
                    Log.e(TAG, task.getException().getMessage());
            }

        });
    }

    /**
     * Cambia la contraseña del usuario en firebase.
     * @param pwd
     * @param callback
     */
    public void updatePwd(String pwd, VndrUserCallback callback){
        firebaseUser.updatePassword(pwd).addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                Log.i(TAG, "Password updated successfull");
                callback.onVndrUserCallbackSuccess();
            } else {
                Log.e(TAG, "Failed to update password. Reason: ");
                Log.e(TAG, task.getException().getMessage());
                callback.onVndrUserCallbackError(task.getException().getMessage());
            }
        });
    }


    /**
     * Actualiza el full name del usaurio en firebase.
     * @param name
     */
    public void updateUserProfile(String name){
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        firebaseUser.updateProfile(profileUpdates).addOnCompleteListener(task -> {
            if (task.isSuccessful())
                Log.i(TAG, "User ic_profile updated successfull");
            else{
                Log.e(TAG, "Failed to update user ic_profile. Reason: ");
                if (task.getException().getMessage() != null)
                    Log.e(TAG, task.getException().getMessage());
            }
        });
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setPhoneNumber(String phoneNumber){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putString("phoneNumber", phoneNumber)
                .apply();
    }

    public static String getPhoneNumber(Context context){
        return context
                .getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getString("phoneNumber", "");
    }

    public static String getEmail(Context context) {
        return context
                .getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getString("email", "");
    }

    public String getEmail(){
        return context
                .getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getString("email", "");
    }

    public static String getName(Context context) {
        return context
                .getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getString("name", "");
    }

    public void setName(String name){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putString("name", name)
                .apply();
    }

    public static void setName(String name, Context context){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putString("name", name)
                .apply();
    }

    public void setEmail(String email){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putString("email", email)
                .apply();
    }

    public static void setEmail(String email, Context context){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putString("email", email)
                .apply();
    }

    public void setPersonalDataUsageAuth(boolean personalDataUsageAuth){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putBoolean("personalDataUsageAuth", personalDataUsageAuth)
                .apply();
    }

    public boolean getPersonalDataUsageAuth(){
        return  context
                .getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getBoolean("personalDataUsageAuth", false);
    }

    public void setCustomersDataUsageAuth(boolean customersDataUsageAuth){
        context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .edit()
                .putBoolean("customersDataUsageAuth", customersDataUsageAuth)
                .apply();
    }

    public boolean getCustomersDataUsageAuth(){
        return  context
                .getSharedPreferences(TAG, Context.MODE_PRIVATE)
                .getBoolean("customersDataUsageAuth", false);
    }


    public interface VndrUserCallback {
        void onVndrUserCallbackSuccess();
        void onVndrUserCallbackError(String messageError);
        default void onVndrUserCallbackLicenseError(String message, VndrMembership.LicenseVndrStatus status){}
        default void callbackErrorStatusCode(int statusCode){}
    }

}
