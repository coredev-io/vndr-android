package mx.com.vndr.vndrapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;

public class ShoppingCartProductAdapter extends RecyclerView.Adapter<ShoppingCartProductAdapter.ProductViewHolder> {

    private List<ShoppingCartProduct> items;
    private Context context;
    private Boolean isReadMode = false;

    public ShoppingCartProductAdapter(List<ShoppingCartProduct> items, Context context) {
        this.items = items;
        this.context = context;
    }

    public ShoppingCartProductAdapter(List<ShoppingCartProduct> items, Context context, Boolean isReadMode) {
        this.items = items;
        this.context = context;
        this.isReadMode = isReadMode;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.shopping_cart_item, parent, false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int position) {
        holder.count.setText(String.valueOf(position + 1));
        holder.productName.setText(items.get(position).getProduct().getProductName());
        holder.productKey.setText(items.get(position).getProduct().getProduct_key());
        holder.subtotal.setText(items.get(position).getSubtotalString());
        if (items.get(position).getSalePrice() != null){
            holder.salePrice.setText(
                    String.valueOf(items.get(position).getProductsCount()) +
                            " piezas x " +
                            items.get(position).getSalePriceStr()
            );
        } else {
            holder.salePrice.setText(
                    String.valueOf(items.get(position).getProductsCount()) +
                            " piezas x " +
                            items.get(position).getBuyPriceStr()
            );
        }



        if (items.get(position).getProduct().getColor().isEmpty())
            holder.color.setVisibility(View.GONE);
        else
            holder.color.setText(items.get(position).getProduct().getColor());

        if (items.get(position).getProduct().getSize().isEmpty())
            holder.size.setVisibility(View.GONE);
        else
            holder.size.setText(items.get(position).getProduct().getSize());

        if (items.get(position).getNotes().isEmpty())
            holder.notes.setVisibility(View.GONE);
        else {
            holder.notes.setVisibility(View.VISIBLE);

            holder.notes.setText(items.get(position).getNotes());
        }

        if (isReadMode){
            holder.delete.setVisibility(View.GONE);
        }

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ShoppingCartActivity) context).orderWasUpdated = true;
                ((ShoppingCartActivity) context).updateTotalOrderUI(false, items.get(position).getSubtotal());
                items.remove(position);
                ShoppingCartProductAdapter.this.notifyDataSetChanged();
                ((ShoppingCartActivity) context).checkListProducts();
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        TextView count, productName, productKey, salePrice, subtotal;
        TextView size, color, notes;
        ImageButton delete;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);

            count = itemView.findViewById(R.id.count_cart_item);
            productName = itemView.findViewById(R.id.product_name_cart_item);
            productKey = itemView.findViewById(R.id.product_key_item);
            salePrice = itemView.findViewById(R.id.sale_price_cart_item);
            subtotal = itemView.findViewById(R.id.subtotal_cart_item);
            delete = itemView.findViewById(R.id.delete_cart_item);
            size = itemView.findViewById(R.id.size_item);
            color = itemView.findViewById(R.id.color_item);
            notes = itemView.findViewById(R.id.notes_item);
        }
    }
}
