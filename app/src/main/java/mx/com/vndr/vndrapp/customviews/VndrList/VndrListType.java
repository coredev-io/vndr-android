package mx.com.vndr.vndrapp.customviews.VndrList;

public enum VndrListType {
    SECTION_TITLE,
    HEADER,
    CARD_DETAIL,
    CARD_MOVEMENT,
    EMPTY_LIST,
    SIMPLE_LIST2,
    SEPARATOR,
    PRODUCT_ANALYTIC,
    CHART,
    PURCHASE_ORDER,
    INVOICE
}
