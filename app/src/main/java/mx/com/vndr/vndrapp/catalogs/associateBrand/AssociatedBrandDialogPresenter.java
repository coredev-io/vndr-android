package mx.com.vndr.vndrapp.catalogs.associateBrand;

import mx.com.vndr.vndrapp.models.Brand;

public class AssociatedBrandDialogPresenter implements AssociatedBrandDialogInteractor.OnFinishRequestListener {

    private AssociateBrandDialogView dialogView;
    private AssociatedBrandDialogInteractor interactor;

    public AssociatedBrandDialogPresenter(AssociateBrandDialogView dialogView, AssociatedBrandDialogInteractor interactor) {
        this.dialogView = dialogView;
        this.interactor = interactor;
    }


    public void associateBrand(Brand brand){
        dialogView.showProgress();
        interactor.associateBrand(this, brand);

    }

    @Override
    public void onSuccess() {
        dialogView.dismissProgress();
        dialogView.showResponse("Se actualizó la marca con éxito.");
        dialogView.dismissDialog();
    }

    @Override
    public void onError(String message) {
        dialogView.updateUIWithOriginalData();
        dialogView.dismissProgress();
        dialogView.showResponse(message);

    }
}
