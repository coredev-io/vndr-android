package mx.com.vndr.vndrapp.catalogs.associateBrand;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.android.volley.toolbox.Volley;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Brand;

public class AssociatedDialogFragment extends BottomSheetDialogFragment implements AssociateBrandDialogView, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    // TODO: Customize parameter argument names
    private static final String ARG_BRAND = "brand";
    private AssociatedBrandDialogPresenter presenter;
    private Toolbar toolbar;
    private Button button;
    private ProgressBar progressBar;
    private Brand brand;
    private Brand updateBrand;
    private Switch activeSwitch;
    private EditText registerKeyEditText, supervisorKeyEditText, supervisorNameEditText, afiliacion;
    private TextView headerPwd;
    private TextInputLayout pwdLayout;
    private Boolean showPWDLayout;

    // TODO: Customize parameters
    public static AssociatedDialogFragment newInstance(Brand brand) {
        final AssociatedDialogFragment fragment = new AssociatedDialogFragment();
        final Bundle args = new Bundle();
        args.putSerializable(ARG_BRAND, brand);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                BottomSheetDialog d = (BottomSheetDialog) dialogInterface;
                FrameLayout bottomSheet = d.findViewById(com.google.android.material.R.id.design_bottom_sheet);
                BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        });

        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        presenter = new AssociatedBrandDialogPresenter(this,
                new AssociatedBrandDialogInteractor(
                        Volley.newRequestQueue(getContext()),
                        VndrAuth.getInstance().getCurrentUser().getSessionToken()
                )
        );


        return inflater.inflate(R.layout.fragment_associated_brand_dialog, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        System.out.println("CREATEEED");
        brand = (Brand) getArguments().getSerializable(ARG_BRAND);

        updateBrand = new Brand(brand.getBrandName(),brand.getBrandId());
        toolbar = view.findViewById(R.id.tb_associate_brand_dialog);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        activeSwitch = view.findViewById(R.id.switch_active_in_user);

        activeSwitch.setOnCheckedChangeListener(this);

        button = view.findViewById(R.id.btn_continue);

        button.setOnClickListener(this);

        progressBar = view.findViewById(R.id.pb_asssociated_brand_dialog);

        toolbar.setTitle(updateBrand.getBrandName());

        registerKeyEditText = view.findViewById(R.id.register_key_edtxt);
        supervisorKeyEditText = view.findViewById(R.id.supervisorkey_edtxt);
        supervisorNameEditText = view.findViewById(R.id.supervisorname_edtxt);
        customUI(view);
        updateUIWithOriginalData();

    }

    private void customUI(View v){
        headerPwd = v.findViewById(R.id.txt_pwd_header);
        pwdLayout = v.findViewById(R.id.pwd_afiliacion_input);
        afiliacion = v.findViewById(R.id.pwd_afiliacion);

        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        if (brand.getBrandOwner().equals("adminvndr") || brand.getBrandOwner().equals(user.getUid())){
            headerPwd.setVisibility(View.GONE);
            //pwdLayout.setVisibility(View.GONE);
            brand.setAfiliacion(false);
            updateBrand.setAfiliacion(false);
            showPWDLayout = false;
        } else {
            headerPwd.setVisibility(View.VISIBLE);
            //pwdLayout.setVisibility(View.VISIBLE);
            brand.setAfiliacion(true);
            updateBrand.setAfiliacion(true);
            showPWDLayout = true;
        }
        if (activeSwitch.isChecked()){
            pwdLayout.setVisibility(View.VISIBLE);
        } else {
            pwdLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissDialog() {
        dismiss();
        ((AssociateBrandActivity)getContext()).onResume();
    }

    @Override
    public void showResponse(String response) {
        Toast.makeText(getContext(), response, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateUIWithOriginalData() {
        activeSwitch.setChecked(brand.isActiveInUser());
        registerKeyEditText.setText(brand.getClaveRegistro());
        supervisorNameEditText.setText(brand.getNombreSupervisor());
        supervisorKeyEditText.setText(brand.getClaveSupervisor());
        if (brand.isActiveInUser()){
            pwdLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {

        updateBrand.setClaveRegistro(registerKeyEditText.getText().toString());
        updateBrand.setNombreSupervisor(supervisorNameEditText.getText().toString());
        updateBrand.setClaveSupervisor(supervisorKeyEditText.getText().toString());
        updateBrand.setPwdAfiliacion(afiliacion.getText().toString());
        presenter.associateBrand(updateBrand);
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        updateBrand.setActiveInUser(b);
        if (!brand.isActiveInUser()){
            if (b && showPWDLayout){
                pwdLayout.setVisibility(View.VISIBLE);
            } else if (!b && showPWDLayout){
                pwdLayout.setVisibility(View.GONE);
            }
        }


    }
}
