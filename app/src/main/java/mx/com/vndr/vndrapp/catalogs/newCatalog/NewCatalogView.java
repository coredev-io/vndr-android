package mx.com.vndr.vndrapp.catalogs.newCatalog;

public interface NewCatalogView {
    void showProgress();
    void dismissProgress();
    void showMessage(String message);
    void finishActivity();
    void setViewMode();
}
