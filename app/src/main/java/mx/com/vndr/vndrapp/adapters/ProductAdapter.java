package mx.com.vndr.vndrapp.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.orders.AddProductActivity;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    List<Product> items;
    OnListItemClickListener listener;
    private boolean hidePrice = false;

    public ProductAdapter(List<Product> items, OnListItemClickListener listener) {
        this.items = items;
        this.listener = listener;
    }

    public void hidePrice() {
        this.hidePrice = true;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item, parent, false);

        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, final int position) {
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onProductSelected(items.get(position));
            }
        });
        holder.name.setText(items.get(position).getProductName());
        holder.catalog.setText(items.get(position).getProduct_key());
        holder.price.setText(items.get(position).getListPriceStr());
        if (!items.get(position).getSize().isEmpty()) {
            holder.size.setVisibility(View.VISIBLE);
            holder.size.setText(items.get(position).getSize());
        }
        if (!items.get(position).getColor().isEmpty()) {
            holder.color.setVisibility(View.VISIBLE);
            holder.color.setText(items.get(position).getColor());
        }
        if (!items.get(position).getDescription().isEmpty()) {
            holder.descripcion.setVisibility(View.VISIBLE);
            holder.descripcion.setText(items.get(position).getDescription());
        }

        if (hidePrice)
            holder.price.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder{

        View view;
        TextView name, catalog, price, size, color, descripcion;

        public ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view = itemView;
            name = itemView.findViewById(R.id.name_product_item);
            catalog = itemView.findViewById(R.id.catalog_product_item);
            price = itemView.findViewById(R.id.list_price_product_item);
            size = itemView.findViewById(R.id.size_product_item);
            color = itemView.findViewById(R.id.color_product_item);
            descripcion = itemView.findViewById(R.id.desc_product_item);
        }
    }

    public interface OnListItemClickListener{
        void onProductSelected(Product product);
    }
}
