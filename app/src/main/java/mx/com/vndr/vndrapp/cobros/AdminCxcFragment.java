package mx.com.vndr.vndrapp.cobros;

import android.app.Dialog;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.AccountReceivable;

import static android.graphics.Typeface.BOLD;

public class AdminCxcFragment extends BottomSheetDialogFragment {

    Switch recordatorioPagoSwitch, faltaPagoSwitch;
    TextView txtCourtesyDaysDesc, txtPreventiveReminderDesc;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_config_cxc, container, false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtCourtesyDaysDesc = view.findViewById(R.id.courtesy_days_desc);
        txtPreventiveReminderDesc = view.findViewById(R.id.preventive_reminder_desc);
        recordatorioPagoSwitch = view.findViewById(R.id.switch1);
        faltaPagoSwitch = view.findViewById(R.id.switch2);

        recordatorioPagoSwitch.setChecked(AccountReceivable.current.isPaymentReminderAlert());
        faltaPagoSwitch.setChecked(AccountReceivable.current.isPaymentEarlyNoticeAlert());

        SpannableString a = new SpannableString(
                getResources()
                        .getString(
                                R.string.preventive_reminder_desc,
                                AccountReceivable.current.getPaymentEarlyNotice()
                        )
        );

        a.setSpan(new StyleSpan(BOLD),54,56, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        a.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)),54,56,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SpannableString b = new SpannableString(getResources().getString(R.string.courtesy_days_desc, AccountReceivable.current.getGracePeriod()));

        b.setSpan(new StyleSpan(BOLD),8,10,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        b.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)),8,10,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        txtPreventiveReminderDesc.setText(a, TextView.BufferType.SPANNABLE);
        txtCourtesyDaysDesc.setText(b, TextView.BufferType.SPANNABLE);
    }
}
