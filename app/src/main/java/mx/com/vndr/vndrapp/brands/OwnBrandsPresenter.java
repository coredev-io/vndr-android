package mx.com.vndr.vndrapp.brands;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItem;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemPresenter;
import mx.com.vndr.vndrapp.models.Brand;

public class OwnBrandsPresenter implements BrandInteractor.BrandCallback {
    OwnBrandsView viewBrand;
    BrandInteractor interactor;

    public OwnBrandsPresenter(OwnBrandsView view, BrandInteractor interactor) {
        this.viewBrand = view;
        this.interactor = interactor;
    }

    public void getOwnBrands(){
        viewBrand.showProgress();
        viewBrand.hideEmptyBrands();
        interactor.getOwnBrands(this);
    }

    @Override
    public void error(String message) {
        viewBrand.hideProgress();
        viewBrand.showError(message);
    }

    @Override
    public void success(final List<Brand> brandList) {
        viewBrand.hideProgress();

        if (brandList.isEmpty()){
            viewBrand.showEmptyBrands();
        } else {

            List<CheckedItem> items = new ArrayList<>();

            for (int i = 0; i < brandList.size(); i++){
                final int finalI = i;
                items.add(
                        new CheckedItem(
                                brandList.get(i).getBrandName(),
                                true, new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                viewBrand.showBrandDetail(brandList.get(finalI));
                            }
                        }
                        )
                );
            }
            viewBrand.showData(new CheckedItemAdapter(
                    new CheckedItemPresenter(
                            items
                    )
            ));
        }
    }

    @Override
    public void emptyBrandList() {
        viewBrand.hideProgress();
        viewBrand.showEmptyBrands();
    }

}
