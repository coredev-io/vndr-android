package mx.com.vndr.vndrapp.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class VndrDateFormat {

    private static final String APP_PATTERN_DATE_FORMAT = "dd/MMM/yyyy";
    private static final String WS_PATTERN_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String WS_PARAMS_PATTERN_DATE_FORMAT = "yyyy-MM-dd";

    public static String dateToString(Date date){
        return new SimpleDateFormat(APP_PATTERN_DATE_FORMAT, Locale.getDefault()).format(date);
    }

    public static Date stringToDate(String date) throws ParseException {
        return new SimpleDateFormat(WS_PATTERN_DATE_FORMAT, Locale.getDefault()).parse(date);
    }

    public static String dateToWSFormat(Date date){
        return new SimpleDateFormat(WS_PARAMS_PATTERN_DATE_FORMAT, Locale.getDefault()).format(date);
    }

    public static String stringDateParamsFormatToAppFormat(String date){
        try {
            return new SimpleDateFormat(APP_PATTERN_DATE_FORMAT,Locale.getDefault()).format(new SimpleDateFormat(WS_PARAMS_PATTERN_DATE_FORMAT, Locale.getDefault()).parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static String dateWSFormatToAppFormat(String wsDate){
        try {
            return dateToString(stringToDate(wsDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return wsDate;
    }
}
