package mx.com.vndr.vndrapp.models;

import java.io.Serializable;
import java.text.NumberFormat;

public class PaymentCalendar implements Serializable {
    String title;
    String dueDate;
    double amount;

    public PaymentCalendar(String title, double amount, String dueDate) {
        this.title = title;
        this.amount = amount;
        this.dueDate = dueDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmountStr() {
        return NumberFormat.getInstance().format(amount);
    }

    public double getAmount() {
        return amount;
    }

    public void setAmountStr(double amount) {
        this.amount = amount;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
}
