package mx.com.vndr.vndrapp.analytics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieEntry;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.APIResume;
import mx.com.vndr.vndrapp.customviews.VndrPieChart;

public class QuickViewActivity extends AppCompatActivity implements APIResume.OnAnalyticsResumeResponse {

    PieChart chartVentas;
    PieChart chartCobranza;
    ProgressBar progressBar;
    TextView textView;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_view);
        setupUI();
        getData();
    }

    public void onClickContinue(View view){
        startActivity(new Intent(this, AnalyticsMenuActivity.class));
    }

    private void setupUI(){
        chartVentas = findViewById(R.id.pie_chart_ventas);
        chartCobranza = findViewById(R.id.pie_chart_cobranza);
        progressBar = findViewById(R.id.pb_quickview);
        textView = findViewById(R.id.txt_quickview);
        toolbar = findViewById(R.id.toolbar4);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        chartVentas.setVisibility(View.INVISIBLE);
        chartCobranza.setVisibility(View.INVISIBLE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void showCharts(){
        chartCobranza.setVisibility(View.VISIBLE);
        chartVentas.setVisibility(View.VISIBLE);
    }

    private void showError(String messageError){
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
    }

    public void getData(){
        showLoading();
        new APIResume(Volley.newRequestQueue(this))
                .getResume(this);
    }

    @Override
    public void onSuccess(List<ArrayList<PieEntry>> listPieEntries) {
        dismissLoading();
        showCharts();

        if (!listPieEntries.get(0).isEmpty()){
            VndrPieChart salesChart = new VndrPieChart("Ventas", listPieEntries.get(0));
            salesChart.buildPieChart(chartVentas);
            salesChart.setColors(
                    getResources().getColor(R.color.colorAccent),
                    getResources().getColor(R.color.colorOk)
            );
            salesChart.show();
        } else {
            chartVentas.setVisibility(View.GONE);
        }

        if (!listPieEntries.get(1).isEmpty()) {
            VndrPieChart collectionChart = new VndrPieChart("Cobranza", listPieEntries.get(1));
            collectionChart.buildPieChart(chartCobranza);
            collectionChart.setColors(
                    getResources().getColor(R.color.colorAccent),
                    getResources().getColor(R.color.colorPorVencer),
                    getResources().getColor(R.color.colorRed),
                    getResources().getColor(R.color.colorAlertLow)
            );
            collectionChart.show();
        } else {
            chartVentas.setVisibility(View.GONE);
        }


    }

    @Override
    public void onError(String error) {
        dismissLoading();
        showError(error);
    }

}