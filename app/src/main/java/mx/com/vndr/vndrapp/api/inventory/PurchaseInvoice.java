package mx.com.vndr.vndrapp.api.inventory;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class PurchaseInvoice implements Serializable {

    private String purchaseId;
    private String purchaseNumber;
    private String statusLabel;
    private String statusCode;
    private Double totalOrderAmount;
    private Double orderAmount;
    private Double ivaAmount;
    private Double subtotalAmount;
    private String rewards;
    private String rewardType;
    private String paymentMethod;

    private Date orderDate;
    private Date sendDate;
    private Supplier supplier;
    private Double shippingCost;
    private List<ShoppingCartProduct> productList = new ArrayList<>();
    private NotificationAlertData notificationAlertData;

    public  PurchaseInvoice(JSONObject jsonObject) throws JSONException, ParseException {
        if (jsonObject.has("purchase_id"))
            this.purchaseId = jsonObject.getString("purchase_id");
        else if (jsonObject.has("_id"))
            this.purchaseId = jsonObject.getString("_id");
        this.purchaseNumber = jsonObject.getString("invoice_document_number"); //info.invoice_document_number
        this.statusLabel = jsonObject.getString("reception_status_label");
        this.totalOrderAmount = jsonObject.getDouble("total_invoice_amount");
        if (jsonObject.has("invoice_date") && !jsonObject.getString("invoice_date").equals("null"))
            this.orderDate = VndrDateFormat.stringToDate(jsonObject.getString("invoice_date"));
        this.supplier = new Supplier(jsonObject.getJSONObject("supplier"));
        this.statusCode = jsonObject.getString("reception_status_code");
        this.shippingCost = jsonObject.getDouble("shipping_cost");
        this.orderAmount = jsonObject.getDouble("invoice_amount");

        if (jsonObject.has("reception_date") && !jsonObject.getString("reception_date").equals("null"))
            this.sendDate = VndrDateFormat.stringToDate(jsonObject.getString("reception_date"));

        if (jsonObject.has("rewards_points"))
            this.rewards = String.valueOf( jsonObject.getInt("rewards_points"));

        if (jsonObject.has("rewards_description"))
            this.rewardType = jsonObject.getString("rewards_description");

        if (jsonObject.has("payment_method_description"))
            this.paymentMethod = jsonObject.getString("payment_method_description");

        if (jsonObject.has("iva"))
            this.ivaAmount = jsonObject.getDouble("iva");

        if (jsonObject.has("subtotal")) {
            this.subtotalAmount = jsonObject.getDouble("subtotal");
        }

        if (jsonObject.has("products")) {
            productList = new ArrayList<>();
            JSONArray array = jsonObject.getJSONArray("products");

            for (int i = 0; i < array.length(); i++) {
                productList.add(new ShoppingCartProduct(array.getJSONObject(i)));
            }
        }



    }

    public PurchaseInvoice() {
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getPurchaseNumber() {
        return "#" + purchaseNumber;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public String getTotalOrderAmount() {
        return NumberFormat.getCurrencyInstance().format(totalOrderAmount);
    }

    public Double getTotalOrderAmountDouble() {
        return totalOrderAmount;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public String getOrderDateStr() {
        if (orderDate != null)
            return VndrDateFormat.dateToString(orderDate);
        else
            return "";
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public void setPurchaseNumber(String purchaseNumber) {
        this.purchaseNumber = purchaseNumber;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setTotalOrderAmount(Double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Double getShippingCost() {
        return shippingCost;
    }

    public String getShippingCostDecimalFormat(){
        if (shippingCost == 0)
            return "";
        else
            return new DecimalFormat("#.00").format(shippingCost);
    }

    public String getShippingCostFormat() {
        return NumberFormat.getCurrencyInstance().format(shippingCost);
    }

    public void setTotalMerch(){
        double merchTotal = 0.0;
        for (ShoppingCartProduct cartProduct: getProductList()){
            merchTotal += cartProduct.getSubtotal();
        }
        this.orderAmount = merchTotal;
    }

    public String getTotalMerchFormat(){
        return NumberFormat.getCurrencyInstance().format(orderAmount);
    }

    public void setShippingCost(Double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public List<ShoppingCartProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ShoppingCartProduct> productList) {
        this.productList = productList;
    }

    public NotificationAlertData getNotificationAlertData() {
        return notificationAlertData;
    }

    public void setNotificationAlertData(NotificationAlertData notificationAlertData) {
        this.notificationAlertData = notificationAlertData;
    }

    public Supplier getSupplier() {
        if (supplier == null)
            return new Supplier();
        else
            return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public String getSendDateStr() {
        if (sendDate != null)
            return VndrDateFormat.dateToString(sendDate);
        else
            return "";
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getIvaAmount() {
        return NumberFormat.getCurrencyInstance().format(ivaAmount);
    }

    public String getSubtotalAmount() {
        return NumberFormat.getCurrencyInstance().format(subtotalAmount);
    }

    public String getRewards() {
        return rewards;
    }

    public String getRewardType() {
        return rewardType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public byte[] getCreateUpdateOrderBody(boolean isUpdate) throws JSONException {
        JSONObject params = new JSONObject();
        JSONArray products = new JSONArray();

        for (ShoppingCartProduct product : productList) {

            JSONObject object = new JSONObject();
            object.put("product",product.getProduct().getIdProduct());
            object.put("quantity", product.getProductsCount());
            object.put("purchase_price", product.getBuyPrice());
            object.put("iva", 0);
            object.put("notes",product.getNotes());

            products.put(object);
        }

        if (isUpdate)
            params.put("purchase_invoice_id", getPurchaseId());
        params.put("supplier_id", getSupplier().getId());
        params.put("shipping_cost", getShippingCost());
        params.put("products", products);
        return params.toString().getBytes();
    }

    public Map<String, String> getUpdateOrderDateBody(){
        Map<String, String> params = new HashMap<>();
        params.put("purchase_id", getPurchaseId());
        params.put("order_date", VndrDateFormat.dateToWSFormat(getOrderDate()));
        Log.e("TAG", params.toString());
        return params;
    }

    public Map<String, String> getUpdateSendOrderDateBody(){
        Map<String, String> params = new HashMap<>();
        params.put("purchase_id", getPurchaseId());
        params.put("sending_date", VndrDateFormat.dateToWSFormat(getSendDate()));
        return params;
    }

    public enum Selected {
        current;

        private PurchaseInvoice purchaseInvoice;

        public PurchaseInvoice getPurchaseInvoice() {
            return purchaseInvoice;
        }

        public void setPurchaseInvoice(PurchaseInvoice purchaseInvoice) {
            this.purchaseInvoice = purchaseInvoice;
        }
    }
}

