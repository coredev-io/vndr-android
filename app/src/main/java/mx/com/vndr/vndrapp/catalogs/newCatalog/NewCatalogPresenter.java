package mx.com.vndr.vndrapp.catalogs.newCatalog;

import mx.com.vndr.vndrapp.models.Catalog;

public class NewCatalogPresenter implements NewCatalogInteractor.OnRequestFinish {

    private NewCatalogView view;
    private NewCatalogInteractor interactor;

    public NewCatalogPresenter(NewCatalogView view, NewCatalogInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void createCatalog(Catalog catalog, boolean isOwnerVndr){
        view.showProgress();
        interactor.createCatalog(catalog,this, isOwnerVndr);
    }

    public void updateCatalog(Catalog catalog, boolean isOwnerVndr){
        view.showProgress();
        interactor.updateCatalog(catalog,this, isOwnerVndr);
    }

    @Override
    public void onUpdateSuccess() {
        view.dismissProgress();
        view.showMessage("Se actualizó exitosamente el catálogo");
        view.finishActivity();
    }

    @Override
    public void onCreateSuccess() {
        view.dismissProgress();
        view.showMessage("Se agregó exitosamente el catálogo");
        view.finishActivity();
    }

    @Override
    public void onError(String error) {
        view.dismissProgress();
        view.showMessage(error);
    }
}
