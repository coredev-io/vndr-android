package mx.com.vndr.vndrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.anychart.core.annotations.Line;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.api.URLVndr;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.customviews.itemSingleList.ItemSingleListAdapter;
import mx.com.vndr.vndrapp.customviews.itemSingleList.OnSingleListClickItemListener;
import mx.com.vndr.vndrapp.models.Brand;

public class SelectBrandActivity extends AppCompatActivity implements VndrRequest.VndrResponse, OnSingleListClickItemListener {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textViewMessage;
    List<Brand> brands = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_brand);
        setupUI();
        getData();
    }

    private void getData(){
        showLoading();
        new VndrRequest(Volley.newRequestQueue(this))
                .get(URLVndr.URL_ACTIVE_USER_BRANDS, this);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_select_brand);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_select_brand);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        progressBar = findViewById(R.id.pb_select_brand);
        textViewMessage = findViewById(R.id.txt_select_brand);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void showMessage(String message){
        textViewMessage.setVisibility(View.VISIBLE);
        textViewMessage.setText(message);
    }

    @Override
    public void error(String message) {
        dismissLoading();
        showMessage(message);
    }

    @Override
    public void success(String response) {
        dismissLoading();
        try {
            JSONArray brandsArray = new JSONArray(response);

            List<String> items = new ArrayList<>();

            for (int i = 0; i < brandsArray.length(); i++) {
                JSONObject jsonObject = brandsArray.getJSONObject(i);

                brands.add(new Brand(
                        jsonObject.getString("brand_name"),
                        jsonObject.getString("_id")
                ));

                items.add(jsonObject.getString("brand_name"));

            }

            recyclerView.setAdapter(new ItemSingleListAdapter(items, this));
        } catch (JSONException e) {
            e.printStackTrace();
            showMessage("Ocurrió un error inesperado al procesar la solicitud");
        }
    }

    @Override
    public void onClickListener(String item, int pos) {

        Brand.Selected.current.setBrand(brands.get(pos));
        this.setResult(RESULT_OK);
        this.finish();
    }
}