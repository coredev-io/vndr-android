package mx.com.vndr.vndrapp.models;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cxc implements Serializable {

    int payFrequency;
    Date payDate, firstPayDate;
    String methodPayment,idCxc;
    Double anticipo, saldoPlazo, montoPago, pendingPay;
    int numPagos, diasAviso, diasCortesia;
    boolean paymentReminderAlert, earlyNoticeAlert;


    public Cxc() {
    }


    public int getPayFrequency() {
        return payFrequency;
    }

    public void setPayFrequency(int payFrequency) {
        this.payFrequency = payFrequency;
    }

    public String getPayDateFormatt(){
        SimpleDateFormat output = new SimpleDateFormat("dd/MMM/yyyy");
        return output.format(payDate);
    }
    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getMethodPayment() {
        return methodPayment;
    }

    public void setMethodPayment(String methodPayment) {
        this.methodPayment = methodPayment;
    }

    public String getIdCxc() {
        return idCxc;
    }

    public void setIdCxc(String idCxc) {
        this.idCxc = idCxc;
    }

    public Double getAnticipo() {
        return anticipo;
    }

    public Double getSaldoPlazo() {
        return saldoPlazo;
    }

    public Double getMontoPago() {
        return montoPago;
    }

    public int getNumPagos() {
        return numPagos;
    }

    public void setAnticipo(Double anticipo) {
        this.anticipo = anticipo;
    }

    public void setSaldoPlazo(Double saldoPlazo) {
        this.saldoPlazo = saldoPlazo;
    }

    public void setMontoPago(Double montoPago) {
        this.montoPago = montoPago;
    }

    public void setNumPagos(int numPagos) {
        this.numPagos = numPagos;
    }

    public Date getFirstPayDate() {
        return firstPayDate;
    }

    public void setFirstPayDate(Date firstPayDate) {
        this.firstPayDate = firstPayDate;
    }

    public void setPayDate(String payDate){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");

        Date d = null;
        try
        {
            d = input.parse(payDate);
            setPayDate(d);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public void setFirstPayDate(String firstPayDate){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");

        Date d = null;
        try
        {
            d = input.parse(firstPayDate);
            setFirstPayDate(d);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public int getDiasAviso() {
        return diasAviso;
    }

    public void setDiasAviso(int diasAviso) {
        this.diasAviso = diasAviso;
    }

    public int getDiasCortesia() {
        return diasCortesia;
    }

    public void setDiasCortesia(int diasCortesia) {
        this.diasCortesia = diasCortesia;
    }

    public boolean isPaymentReminderAlert() {
        return paymentReminderAlert;
    }

    public void setPaymentReminderAlert(boolean paymentReminderAlert) {
        this.paymentReminderAlert = paymentReminderAlert;
    }

    public boolean isEarlyNoticeAlert() {
        return earlyNoticeAlert;
    }

    public void setEarlyNoticeAlert(boolean earlyNoticeAlert) {
        this.earlyNoticeAlert = earlyNoticeAlert;
    }

    public Double getPendingPay() {
        return pendingPay;
    }

    public void setPendingPay(Double pendingPay) {
        this.pendingPay = pendingPay;
    }
}
