package mx.com.vndr.vndrapp.models;

import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.PhoneNumberUtils;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Locale;

public class Customer implements Serializable {
    private String idCustomer, customerFullName, customerName, customerLastName, email, phone, zipCode;
    private String gender, secondName, secondLastName;


    public Customer(String idCustomer, String customerFullName, String customerName, String customerLastName, String secondName, String secondLastName) {
        this.idCustomer = idCustomer;
        this.customerFullName = customerFullName;
        this.customerName = customerName;
        this.customerLastName = customerLastName;
        this.secondName = secondName;
        this.secondLastName = secondLastName;
    }

    public Customer(String idCustomer, String customerFullName, String customerName, String customerLastName, String email, String phone, String zipCode, String gender, String secondName, String secondLastName) {
        this.idCustomer = idCustomer;
        this.customerFullName = customerFullName;
        this.customerName = customerName;
        this.customerLastName = customerLastName;
        this.email = email;
        this.phone = phone;
        this.zipCode = zipCode;
        this.gender = gender;
        this.secondName = secondName;
        this.secondLastName = secondLastName;
    }

    public Customer(String idCustomer, String customerFullName, String customerName, String customerLastName, String email, String phone, String zipCode, String gender) {
        this.idCustomer = idCustomer;
        this.customerFullName = customerFullName;
        this.customerName = customerName;
        this.customerLastName = customerLastName;
        this.email = email;
        this.phone = phone;
        this.zipCode = zipCode;
        this.gender = gender;
    }

    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getCustomerFullName() {
        return customerFullName;
    }

    public void setCustomerFullName(String customerFullName) {
        this.customerFullName = customerFullName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerLastName() {
        return customerLastName;
    }

    public void setCustomerLastName(String customerLastName) {
        this.customerLastName = customerLastName;
    }

    public String getInitials(){
        return new StringBuilder().append(customerName.charAt(0)).append(customerLastName.charAt(0)).toString();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return PhoneNumberUtils.formatNumber(phone,"MX");
        else
            return PhoneNumberUtils.formatNumber(phone);
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }
}
