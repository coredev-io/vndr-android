package mx.com.vndr.vndrapp.api;

import android.app.Application;
import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.NonNull;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.FirebaseApp;

import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.App;
import mx.com.vndr.vndrapp.util.Dialogs;

public class VndrRequest implements Response.ErrorListener, Response.Listener<String> {

    private final String TAG = "VndrRequest";
    private RequestQueue requestQueue;
    private Map<String, String> params = null;
    private byte[] bytesParams = null;
    private Map<String, String> headers;
    private VndrResponse responseListener;

    public VndrRequest(RequestQueue requestQueue) {
        this.requestQueue = requestQueue;
        headers = new HashMap<>();
        if (VndrAuth.getInstance().getCurrentUser().getSessionToken() != null) {
            headers.put("Authorization", "Bearer " +
                    VndrAuth.getInstance().getCurrentUser().getSessionToken());
            Log.e(TAG, headers.toString());
        }

    }


    public void get(String url, VndrResponse response){
        Log.d(TAG, url);
        responseListener = response;
        newRequest(Request.Method.GET, url);
    }

    public void post(String url, VndrResponse response){
        Log.d(TAG, url);
        responseListener = response;
        newRequest(Request.Method.POST, url);
    }

    public void put(String url, VndrResponse response){
        Log.d(TAG, url);
        responseListener = response;
        newRequest(Request.Method.PUT, url);
    }

    public void patch(String url, VndrResponse response){
        Log.d(TAG, url);
        responseListener = response;
        newRequest(Request.Method.PATCH, url);
    }

    public void delete(String url, VndrResponse response){
        Log.d(TAG, url);
        responseListener = response;
        newRequest(Request.Method.DELETE, url);
    }



    private void newRequest(int method, String url){

        StringRequest request = new StringRequest(method, url, this, this){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (params == null)
                    return super.getParams();
                else
                    return params;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                if (bytesParams != null)
                    return bytesParams;
                else
                    return super.getBody();
            }


            @Override
            public String getBodyContentType() {
                if (bytesParams != null)
                    return "application/json";
                else
                    return super.getBodyContentType();
            }
        };

        requestQueue.add(request);
    }

    public VndrRequest setRequestParams(Map<String, String> params) {
        this.params = params;
        Log.d(TAG, params.toString());
        return this;
    }

    public VndrRequest setRequestByteParams(byte[] byteParams) {
        this.bytesParams = byteParams;
        return this;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, error.toString());
        if(error instanceof NoConnectionError){
            Log.e(TAG, "Instance of No connection error");
            responseListener.error("Ocurrió un problema de conexión.");
        }  else if (error.getCause() instanceof MalformedURLException) {
            responseListener.error("Ocurrió un error inesperado al acceder a los recursos de vndr+, por favor contacta a soporte");

        } else if (error instanceof AuthFailureError){

            if (error.networkResponse.data != null){
                String errorMessage = new String(error.networkResponse.data);
                Log.e(TAG, errorMessage);

                try {
                    JSONObject jsonResponse = new JSONObject(errorMessage);
                    if (jsonResponse.has("statusCode") && jsonResponse.getInt("statusCode") == 403){
                        responseListener.statusCodeError(
                                jsonResponse.getInt("statusCode"),
                                jsonResponse.getString("message")
                        );
                    } else if (jsonResponse.has("error") && jsonResponse.getString("error").equals("license_error")) {
                        VndrMembership.status = VndrMembership.LicenseVndrStatus.valueOf(jsonResponse.getString("license"));
                        responseListener.licenseError(
                                jsonResponse.getString("message"),
                                VndrMembership.status);
                    } else {
                        responseListener.error(jsonResponse.getString("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    responseListener.error("Ocurrió un error al procesar la solicitud");
                }
            } else {
                responseListener.error("Ocurrió un error inesperado");
            }




        } else if (error instanceof ServerError || error.getCause() instanceof ServerError) {
            if (error.networkResponse.data != null){
                String errorMessage = new String(error.networkResponse.data);
                Log.e(TAG, errorMessage);

                try {
                    JSONObject jsonResponse = new JSONObject(errorMessage);
                    int statusCode = jsonResponse.getInt("statusCode");

                    switch (statusCode){
                        case 401:
                            // session_error_vndr
                            responseListener.error(jsonResponse.getString("message"));

                            break;
                        default:
                            responseListener.error(jsonResponse.getString("message"));
                            break;
                    }




                } catch (JSONException e) {
                    e.printStackTrace();
                    responseListener.error("Ocurrió un error al procesar la solicitud");
                }
            } else {
                responseListener.error("Ocurrió un error inesperado");
            }

        }else if (error instanceof TimeoutError || error.getCause() instanceof SocketTimeoutException
                || error.getCause() instanceof ConnectTimeoutException
                || error.getCause() instanceof SocketException
                || (error.getCause().getMessage() != null
                && error.getCause().getMessage().contains("Connection timed out"))) {

            responseListener.error("Se excedió el tiempo de respuesta del servidor.");

        } else {
            responseListener.error("Ocurrió un error inesperado");
        }

    }

    @Override
    public void onResponse(String response) {
        Log.d(TAG, response);
        responseListener.success(response);
    }

    public interface VndrResponse{
        void error(String message);
        default void statusCodeError(int statusCode, String message){

        }
        default void licenseError(String message, VndrMembership.LicenseVndrStatus status){

        }
        void success(String response);
    }
}

