package mx.com.vndr.vndrapp.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.NumberFormat;

import mx.com.vndr.vndrapp.api.inventory.Inventory;

public class Product implements Serializable {
    String idProduct, sku, productName,standard_classification, notes, sub_brand, category, subcategory, product_key, size, color, description, url_image, product_points,barcode, qrcode, currentCalculatedStock;
    Double listPrice;
//    Integer currentCalculatedStock;
    Boolean isDiscontinued;
    Catalog catalog;
    Inventory inventory;
    String height;
    String stdClassId = "";
    String brandName = "";


    public Product(String idProduct, String sku, String productName, String standard_classification, String notes, String sub_brand, String category, String subcategory, String product_key, String size, String color, String description, String url_image, String product_points, String barcode, String qrcode, Double listPrice, Boolean isDiscontinued, Catalog catalog) {
        this.idProduct = idProduct;
        this.sku = sku;
        this.productName = productName;
        this.standard_classification = standard_classification;
        this.notes = notes;
        this.sub_brand = sub_brand;
        this.category = category;
        this.subcategory = subcategory;
        this.product_key = product_key;
        this.size = size;
        this.color = color;
        this.description = description;
        this.url_image = url_image;
        this.product_points = product_points;
        this.barcode = barcode;
        this.qrcode = qrcode;
        this.listPrice = listPrice;
        this.isDiscontinued = isDiscontinued;
        this.catalog = catalog;
    }

    public Product(String idProduct, String sku, String productName, String standard_classification, String notes, String sub_brand, String category, String subcategory, String product_key, String size, String color, String description, String url_image, String product_points, String barcode, String qrcode, Double listPrice, Boolean isDiscontinued, Catalog catalog, String currentCalculatedStock) {
        this.idProduct = idProduct;
        this.sku = sku;
        this.productName = productName;
        this.standard_classification = standard_classification;
        this.notes = notes;
        this.sub_brand = sub_brand;
        this.category = category;
        this.subcategory = subcategory;
        this.product_key = product_key;
        this.size = size;
        this.color = color;
        this.description = description;
        this.url_image = url_image;
        this.product_points = product_points;
        this.barcode = barcode;
        this.qrcode = qrcode;
        this.listPrice = listPrice;
        this.isDiscontinued = isDiscontinued;
        this.catalog = catalog;
        this.currentCalculatedStock = currentCalculatedStock;
    }

    public Product(String idProduct, String productName, String product_key, String size, String color, String brandName) {
        this.idProduct = idProduct;
        this.productName = productName;
        this.product_key = product_key;
        this.size = size;
        this.color = color;
        this.brandName = brandName;
    }

    public Product(JSONObject jsonProductObject) throws JSONException {
        this.idProduct = jsonProductObject.getString("_id");
        this.productName = jsonProductObject.getString("product_name");
        this.standard_classification = jsonProductObject.getString("standard_classification");
        this.notes = jsonProductObject.getString("notes");
        this.product_key = jsonProductObject.getString("product_key");
        this.size = jsonProductObject.getString("size");
        this.color = jsonProductObject.getString("color");
        this.description = jsonProductObject.getString("description");
        this.listPrice = jsonProductObject.getDouble("list_price");
        this.isDiscontinued = jsonProductObject.getBoolean("discontinued");
        if (!jsonProductObject.has("catalog")){
            this.catalog = new Catalog(jsonProductObject.getString("catalog_id"),
                    jsonProductObject.getString("catalog_name"));
        } else {
            this.catalog = new Catalog(jsonProductObject.getJSONObject("catalog").getString("_id"),
                    jsonProductObject.getJSONObject("catalog").getString("catalog_name"));
        }
        if (!jsonProductObject.has("inventory")){
            this.currentCalculatedStock ="0";
        } else {
            this.currentCalculatedStock = String.valueOf(jsonProductObject.getJSONObject("inventory").getInt("current_calculated_stock"));
        }
        //this.currentCalculatedStock = jsonProductObject.getJSONObject("inventory").getString("current_calculated_stock");
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public String getSku() {
        return sku;
    }

    public String getProductName() {
        return productName;
    }

    public String getStandard_classification() {
        return standard_classification;
    }

    public String getNotes() {
        return notes;
    }

    public String getSub_brand() {
        return sub_brand;
    }

    public String getCategory() {
        return category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public String getProduct_key() {
        return product_key;
    }

    public String getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getDescription() {
        return description;
    }

    public String getUrl_image() {
        return url_image;
    }

    public String getProduct_points() {
        return product_points;
    }

    public String getBarcode() {
        return barcode;
    }

    public String getQrcode() {
        return qrcode;
    }

    public Double getListPrice() {
        return listPrice;
    }

    public String getListPriceStr(){
        return NumberFormat.getCurrencyInstance().format(listPrice);
    }

    public Boolean getDiscontinued() {
        return isDiscontinued;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public String getStdClassId() {
        return stdClassId;
    }

    public void setStdClassId(String stdClassId) {
        this.stdClassId = stdClassId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCurrentCalculatedStock() {
        return this.currentCalculatedStock;
    }
}
