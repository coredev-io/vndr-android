package mx.com.vndr.vndrapp.orders;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.datepPicker.DatePickerFragment;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDERS;

public class CompleteOrderStatusActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, MaterialDialog.SingleButtonCallback {


    Toolbar toolbar;
    TextView sendDate, deliverDate;
    ProgressBar progressBar;
    CardView cardViewSend;
    Order order;
    boolean isSend = false;
    Date newDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_order_status);

        toolbar = findViewById(R.id.tb_order_status);
        sendDate = findViewById(R.id.txt_send_date_c);
        deliverDate = findViewById(R.id.txt_deliver_date_c);
        progressBar = findViewById(R.id.pb_complete_status);
        cardViewSend = findViewById(R.id.cv_complete_send);

        progressBar.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CompleteOrderStatusActivity.this.setResult(RESULT_CANCELED);
                onBackPressed();
            }
        });



        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
        }


        if (order.getDelivery().isPersonalDelivery()){
            cardViewSend.setVisibility(View.GONE);
        } else {
            //  Si es un pedido por mensajeria y no está entregado
            //  setter al afecha de enviado. si no existe.
            if (order.getStatus().equals(Order.NO_ENTREGADO)){
                sendDate.setText(order.getSendedDate());
            }

        }

    }

    public void launchCalendarSend(View view){
        isSend = true;
        new DatePickerFragment(this, order.getSaleDate().getTime(), new Date().getTime(),order.getSaleDate())
                .show(getSupportFragmentManager(), "datePicker");
    }

    public void launchCalendarDeliver(View view){
        long minDate = 0;
        Date initDate;

        if (order.getDelivery().isPersonalDelivery()){
            minDate = order.getSaleDate().getTime();
            initDate = order.getSaleDate();
        } else {
            if (order.getStatus().equals(Order.NO_ENVIADO)){
                minDate = order.getSaleDate().getTime();
                initDate = order.getSaleDate();
            } else {
                minDate = order.shippingDate().getTime();
                initDate = order.shippingDate();
            }
        }

        isSend = false;
        new DatePickerFragment(this, minDate, new Date().getTime(),initDate)
                .show(getSupportFragmentManager(), "datePicker");
    }




    public void completeOrder(final Boolean isSend, final Date date){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.PATCH, URL_ORDERS + "/status/" + order.getIdOrder(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);

                        SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                        if (isSend){
                            sendDate.setText(format.format(date));
                            order.shippingDate(date);
                            order.setStatus(true, true, false);
                        }
                        else {
                            deliverDate.setText(format.format(date));
                            order.deliveryDate(date);
                        }
                        System.out.println(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(CompleteOrderStatusActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                System.out.println(new String(error.networkResponse.data));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("update_date", String.valueOf(date.getTime()));

                if (isSend) params.put("is_sended","true");
                else params.put("is_delivered","true");

                System.out.println(params.toString());
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);



    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar c = Calendar.getInstance();
        c.set(i, i1, i2);
        newDate = c.getTime();
        confirmationDialog();
    }

    public void confirmationDialog(){
        Dialogs.showAlertQuestion(this,"Esta fecha ya no la podrás cambiar más adelante, ¿Quieres continuar?",this);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        completeOrder(isSend, newDate);
    }
}
