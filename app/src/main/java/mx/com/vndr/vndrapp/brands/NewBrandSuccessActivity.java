package mx.com.vndr.vndrapp.brands;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Brand;

public class NewBrandSuccessActivity extends AppCompatActivity {

    TextView brandCode, brandName;
    Brand brand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_brand_success);

        brandCode = findViewById(R.id.txt_brand_code);
        brandName = findViewById(R.id.txt_brand_name);
        brand = (Brand) getIntent().getSerializableExtra("brand");

        brandCode.setText(brand.getBrandCode());
        brandName.setText(brand.getBrandName());
    }


    public void onClick(View view){
        this.setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void onBackPressed() {
    }
}
