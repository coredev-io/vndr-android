package mx.com.vndr.vndrapp.customviews;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {

    DatePickerDialog.OnDateSetListener listener;
    long minDate = 0;

    public DatePickerFragment(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    public DatePickerFragment(DatePickerDialog.OnDateSetListener listener, long minDate) {
        this.listener = listener;
        this.minDate = minDate;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), listener, year, month, day);
        if (minDate != 0)
            dialog.getDatePicker().setMinDate(minDate);
        return dialog;
    }

}