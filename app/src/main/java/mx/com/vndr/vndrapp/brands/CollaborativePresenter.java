package mx.com.vndr.vndrapp.brands;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItem;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemPresenter;
import mx.com.vndr.vndrapp.models.Brand;

public class CollaborativePresenter implements BrandInteractor.BrandCallback {

    CollaborativeBrandsView viewC;
    BrandInteractor interactor;

    public CollaborativePresenter(CollaborativeBrandsView view, BrandInteractor interactor) {
        this.viewC = view;
        this.interactor = interactor;
    }

    public void getCollaborativeBrands(){
        viewC.showProgress();
        interactor.getCollaborativeBrands(this);
    }

    @Override
    public void error(String message) {
        viewC.hideProgress();
        viewC.showError(message);

    }

    @Override
    public void success(final List<Brand> brandList) {
        viewC.hideProgress();
        if (brandList.isEmpty()){
            viewC.showEmptyBrands();
        } else {

            List<CheckedItem> items = new ArrayList<>();

            for (int i = 0; i < brandList.size(); i++){
                final int finalI = i;
                items.add(
                        new CheckedItem(
                                brandList.get(i).getBrandName(),
                                true, new View.OnClickListener() {
                            @Override

                            public void onClick(View view) {
                                viewC.navigateToCatalogEditable(brandList.get(finalI));
                            }
                        }
                        )
                );
            }
            viewC.showData(new CheckedItemAdapter(
                    new CheckedItemPresenter(
                            items
                    )
            ));
        }

    }

    @Override
    public void emptyBrandList() {
        viewC.hideProgress();
        viewC.showEmptyBrands();
    }
}
