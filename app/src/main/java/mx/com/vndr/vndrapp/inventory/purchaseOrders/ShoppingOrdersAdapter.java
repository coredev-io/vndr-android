package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;



public class ShoppingOrdersAdapter extends RecyclerView.Adapter<ShoppingOrdersAdapter.ShoppingOrderViewHolder> {

    public interface OnShoppingOrderSelected {
        void onSelected(ShoppingOrder shoppingOrder);
    }

    private List<ShoppingOrder> items;
    private Context context;
    private OnShoppingOrderSelected onShoppingOrderSelected;

    public ShoppingOrdersAdapter(List<ShoppingOrder> shoppingOrders, Context context) {
        this.items = shoppingOrders;
        this.context = context;
    }

    public ShoppingOrdersAdapter setOnShoppingOrderSelected(OnShoppingOrderSelected onShoppingOrderSelected) {
        this.onShoppingOrderSelected = onShoppingOrderSelected;
        return this;
    }

    @NonNull
    @Override
    public ShoppingOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_card, parent, false);
        return new ShoppingOrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingOrderViewHolder holder, int position) {
        ShoppingOrder shoppingOrder = items.get(position);
        holder.name.setText(shoppingOrder.getSupplier().getName());
        holder.initials.setText(shoppingOrder.getSupplier().getInitials());
        holder.order.setText(shoppingOrder.getPurchaseNumber());
        holder.balance.setText(shoppingOrder.getTotalOrderAmount());
        holder.saleDate.setText(shoppingOrder.getOrderDateStr());
        holder.status.setText(shoppingOrder.getStatusLabel());
        setColor(holder.status.getBackground(), shoppingOrder.getStatusCode());
        if (onShoppingOrderSelected != null)
            holder.cardView.setOnClickListener(view -> onShoppingOrderSelected.onSelected(shoppingOrder));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setColor(Drawable background, String status){

        int color = 0;

        switch (status){
            case "PO01":
                color = ContextCompat.getColor(context,R.color.colorYellow);
                break;
            case "PO02":
                color = ContextCompat.getColor(context,R.color.colorComplete);
                break;
            default:
                color = ContextCompat.getColor(context,R.color.colorRed);
                break;
        }

        if (background instanceof ShapeDrawable) {
            // cast to 'ShapeDrawable'
            ShapeDrawable shapeDrawable = (ShapeDrawable) background;
            shapeDrawable.getPaint().setColor(color);
        } else if (background instanceof GradientDrawable) {
            // cast to 'GradientDrawable'
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setColor(color);
        } else if (background instanceof ColorDrawable) {
            // alpha value may need to be set again after this call
            ColorDrawable colorDrawable = (ColorDrawable) background;
            colorDrawable.setColor(color);
        }
    }

    public class ShoppingOrderViewHolder extends RecyclerView.ViewHolder{
        TextView status, name, initials, order, saleDate, balance;
        CardView cardView;
        public ShoppingOrderViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status_order_item);
            name = itemView.findViewById(R.id.fullname_order_item);
            initials = itemView.findViewById(R.id.txt_initial_name_order);
            order = itemView.findViewById(R.id.order_number_order_item);
            saleDate = itemView.findViewById(R.id.sale_date_order_item);
            balance = itemView.findViewById(R.id.balance_order_item);
            cardView = itemView.findViewById(R.id.cardview_order_item);
        }
    }
}