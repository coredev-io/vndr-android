package mx.com.vndr.vndrapp.util;

public class Constants {

    public static final int TIME_OUT = 10000;
    public static final int RETRY_COUNT = 1;
    public static final int RESULT_BACK = 2;

}
