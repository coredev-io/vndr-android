package mx.com.vndr.vndrapp.analytics;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.analytics.business.MyBussinesMenuActivity;
import mx.com.vndr.vndrapp.analytics.collections.CollectionMenuActivity;
import mx.com.vndr.vndrapp.analytics.inventory.PurchasesInventoryMenuActivity;
import mx.com.vndr.vndrapp.analytics.sales.SalesMenuActivity;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class AnalyticsMenuActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analytics_menu);
        setupUI();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_analytics_menu);
        recyclerView = findViewById(R.id.rv_analytics_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuAnalyticsOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToSalesMenu));

        options = Arrays.asList(getResources().getStringArray(R.array.menuAnalyticsOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToCollectionMenu));

        options = Arrays.asList(getResources().getStringArray(R.array.menuAnalyticsOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToPurchaseMenu));


        options = Arrays.asList(getResources().getStringArray(R.array.menuAnalyticsOption4));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToMyBussinessMenu));

        /*
        options = Arrays.asList(getResources().getStringArray(R.array.menuAnalyticsOption5));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));
         */

        return optionList;
    }

    public void navigateToSalesMenu(){
        startActivity(new Intent(this, SalesMenuActivity.class));
    }

    public void navigateToCollectionMenu(){
        startActivity(new Intent(this, CollectionMenuActivity.class));
    }

    public void navigateToPurchaseMenu(){
        startActivity(new Intent(this, PurchasesInventoryMenuActivity.class));
    }

    public void navigateToMyBussinessMenu(){
        startActivity(new Intent(this, MyBussinesMenuActivity.class));
    }

    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }
}