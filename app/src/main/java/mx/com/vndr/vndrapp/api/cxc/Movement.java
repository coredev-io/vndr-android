package mx.com.vndr.vndrapp.api.cxc;

import java.text.NumberFormat;

public enum Movement {
    current;

    double sumAbonos;
    double sumCargos;

    public void setSumAbonos(double sumAbonos) {
        this.sumAbonos = sumAbonos;
    }

    public void setSumCargos(double sumCargos) {
        this.sumCargos = sumCargos;
    }


    public String getCurrencySumAbonos() {
        return NumberFormat.getCurrencyInstance().format(sumAbonos);
    }

    public String getCurrencySumCargos() {
        return NumberFormat.getCurrencyInstance().format(sumCargos);
    }
}
