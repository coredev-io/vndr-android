package mx.com.vndr.vndrapp.registro;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.textfield.TextInputLayout;

import mx.com.vndr.vndrapp.AppState;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.EditTextPhoneNumber;
import mx.com.vndr.vndrapp.util.Dialogs;

public class SignUpActivity extends AppCompatActivity implements TextWatcher, VndrAuth.CreateUserResult {

    Toolbar toolbar;
    Button button;
    TextInputLayout textInputLayoutName;
    TextInputLayout textInputLayoutLastName;
    TextInputLayout textInputLayoutEmail;
    TextInputLayout textInputLayoutPhone;
    TextInputLayout textInputLayoutZipCode;
    TextInputLayout textInputLayoutPwd;
    TextInputLayout textInputLayoutPwdRepeat;
    EditText editTextName;
    EditText editTextLastName;
    EditText editTextEmail;
    EditTextPhoneNumber editTextPhone;
    EditText editTextZipCode;
    EditText editTextPwd;
    EditText editTextPwdRepeat;
    ProgressBar progressBar;

    private SignUpData data;

    private final int REQUEST_TYC = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setupUI();

        data = SignUpData.getInstance();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_signup);

        button = findViewById(R.id.btn_next_signup);

        textInputLayoutName = findViewById(R.id.il_name);
        textInputLayoutLastName = findViewById(R.id.il_last_name);
        textInputLayoutEmail = findViewById(R.id.il_email);
        textInputLayoutPhone = findViewById(R.id.il_phone_number);
        textInputLayoutZipCode = findViewById(R.id.il_zipcode);
        textInputLayoutPwd = findViewById(R.id.il_pwd);
        textInputLayoutPwdRepeat = findViewById(R.id.il_pwd_repeat);

        editTextName = findViewById(R.id.et_name);
        editTextLastName = findViewById(R.id.et_last_name);
        editTextEmail = findViewById(R.id.et_email);
        editTextPhone = findViewById(R.id.et_phone);
        editTextZipCode = findViewById(R.id.et_zipcode);
        editTextPwd = findViewById(R.id.et_pwd);
        editTextPwdRepeat = findViewById(R.id.et_pwd_repeat);

        progressBar = findViewById(R.id.pb_signup);
        progressBar.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        button.setEnabled(false);

        editTextName.addTextChangedListener(this);
        editTextLastName.addTextChangedListener(this);
        editTextEmail.addTextChangedListener(this);
        editTextPhone.addTextChangedListener(this);
        editTextZipCode.addTextChangedListener(this);
        editTextPwd.addTextChangedListener(this);
        editTextPwdRepeat.addTextChangedListener(this);
    }

    public void onClickNext(View view){

        data.setName(editTextName.getText().toString());
        data.setLastName(editTextLastName.getText().toString());
        data.setEmail(editTextEmail.getText().toString());
        data.setMobilePhone(editTextPhone.getRawPhoneNumber());
        data.setZipCode(editTextZipCode.getText().toString());
        data.setPwd(editTextPwd.getText().toString());

        navigateToTyCActivity();
    }

    private void navigateToTyCActivity(){
        startActivityForResult(
                new Intent(this, PrivacyAndOpActivity.class),
                REQUEST_TYC);
    }

    private void navigateToDataUsageAuthActivity(){
        startActivityForResult(
                new Intent(this, DataUsageAuthActivity.class),
                REQUEST_TYC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode ==  REQUEST_TYC && resultCode == RESULT_OK){
            createAccount();
        } else if (requestCode == REQUEST_TYC && resultCode == RESULT_CANCELED){
            Dialogs.showAlert(this, "Para crear una cuenta es necesario aceptar los términos y condiciones.");
        }

    }

    private void createAccount(){
        showLoader();
        VndrAuth.getInstance().createUserWithSignUpData(this);
    }

    @Override
    public void onUserCreatedSuccessfull() {
        data.setPwd("");
        dismissLoader();
        AppState.setAppState(this, AppState.NOT_LICENSED);
        AppEventsLogger.newLogger(this).logEvent("signupSuccess");
        navigateToDataUsageAuthActivity();
        this.setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void onFailureCreateUser(String messageError) {
        dismissLoader();
        Dialogs.showAlert(this, messageError);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        TextInputLayout currentTextInputLayout = getFocusedTextInputLayout();

        if (charSequence.toString().isEmpty()){
            currentTextInputLayout.setError("Este campo es requerido");
        } else {
            currentTextInputLayout.setError("");
        }

        if (editTextEmail.isFocused()){
            if (!charSequence.toString().contains("@")){
                textInputLayoutEmail.setError("Ingresa un email válido");
            } else {
                textInputLayoutEmail.setError("");
            }
        }

        if (editTextPwdRepeat.isFocused()){
            if (!editTextPwdRepeat.getText().toString().equals(editTextPwd.getText().toString())){
                textInputLayoutPwdRepeat.setError("Las contraseñas deben coincidir");
            } else {
                textInputLayoutPwdRepeat.setError("");
            }
        }

        if (!inputFieldsAreEmpty() && isEmailValid() && pwdAreEquals())
            button.setEnabled(true);
        else
            button.setEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    /**
     * Si los campos no están vacios se habilita el botón
     * de acción
     */
    private boolean inputFieldsAreEmpty(){
        if (editTextName.getText().toString().isEmpty())
            return true;
        else if (editTextLastName.getText().toString().isEmpty())
            return true;
        else if (editTextEmail.getText().toString().isEmpty())
            return true;
        else if (editTextPhone.getText().toString().isEmpty())
            return true;
        else if (editTextZipCode.getText().toString().isEmpty())
            return true;
        else if (editTextPwd.getText().toString().isEmpty())
            return true;
        else if (editTextPwdRepeat.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private boolean isEmailValid(){
        if (editTextEmail.getText().toString().contains("@"))
            return true;
        else
            return false;
    }

    private boolean pwdAreEquals(){
        if (editTextPwd.getText().toString().equals(editTextPwdRepeat.getText().toString()))
            return true;
        else
            return false;
    }

    private TextInputLayout getFocusedTextInputLayout(){
        if (editTextName.isFocused()){
            return textInputLayoutName;
        } else if (editTextLastName.isFocused()){
            return textInputLayoutLastName;
        } else if (editTextEmail.isFocused()){
            return textInputLayoutEmail;
        } else if (editTextPhone.isFocused()){
            return textInputLayoutPhone;
        } else if (editTextZipCode.isFocused()){
            return textInputLayoutZipCode;
        } else if (editTextPwd.isFocused()){
            return textInputLayoutPwd;
        } else {
            return textInputLayoutPwdRepeat;
        }
    }

    private void showLoader(){
        button.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        textInputLayoutName.setEnabled(false);
        textInputLayoutLastName.setEnabled(false);
        textInputLayoutEmail.setEnabled(false);
        textInputLayoutPhone.setEnabled(false);
        textInputLayoutZipCode.setEnabled(false);
        textInputLayoutPwd.setEnabled(false);
        textInputLayoutPwdRepeat.setEnabled(false);
    }

    private void dismissLoader(){
        button.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        textInputLayoutName.setEnabled(true);
        textInputLayoutLastName.setEnabled(true);
        textInputLayoutEmail.setEnabled(true);
        textInputLayoutPhone.setEnabled(true);
        textInputLayoutZipCode.setEnabled(true);
        textInputLayoutPwd.setEnabled(true);
        textInputLayoutPwdRepeat.setEnabled(true);

    }


}
