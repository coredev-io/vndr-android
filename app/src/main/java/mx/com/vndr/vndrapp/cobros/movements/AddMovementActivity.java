package mx.com.vndr.vndrapp.cobros.movements;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;

import mx.com.vndr.vndrapp.PickPaymentMethodActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.TicketMovementActivity;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class AddMovementActivity extends AppCompatActivity implements View.OnClickListener, TextWatcher, CalendarView.OnDateChangeListener, APIVndrCxc.MovementResponse {

    Toolbar toolbar;
    EditText editTextAmount;
    EditText editTextComments;
    Button button;
    CalendarView calendarView;
    TextInputLayout textInputLayoutAmount;
    TextView textViewNewBalance,
            textViewCustomerName,
            textViewOrder,
            textViewCurrentBalance,
            textViewExigible,
            textViewNewExigible;
    TextView textViewUnpaidAmountDue;
    TextView textViewPmtAmount;
    LinearLayout linearLayoutExigible;
    LinearLayout linearLayoutNewExigible;
    LinearLayout linearLayoutBalance, linearLayoutNewBalance, linearLayoutPmtAmount, linearLayoutUnPaidAmountDue;
    ProgressDialog progressDialog;

    private double currentBalance;
    private double newBalance;
    private double currentExigible;
    private double newExigible;
    private CxCMovement movement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_movement);

        setupUI();


        currentBalance = NewMovement.me.getSaldoActual();
        newBalance = NewMovement.me.getSaldoActual();
        newExigible = NewMovement.me.getExigible();
        currentExigible = NewMovement.me.getExigible();
        movement = NewMovement.me.getMovement();
        movement.setDate(new Date(calendarView.getDate()));

        displayData();

        customUI();

    }

    private void customUI(){

        if (movement.getType().getMovementType() == MovementType.Query.C){
            getSupportActionBar().setTitle("Cargo a cuenta");
        } else if (movement.getType().getCode().equals("M004") || movement.getType().getCode().equals("M005")){
            getSupportActionBar().setTitle("Devolución de mercancía");
        } else {
            getSupportActionBar().setTitle("Cobro o abono a cuenta");
        }


        if (movement.getType().getCode().equals("M007")) {
            editTextAmount.setEnabled(false);
            editTextAmount.setText(
                    String.valueOf(currentBalance)
            );
        }

        if (NewMovement.me.isUniquePaymentCxc()){
            linearLayoutUnPaidAmountDue.setVisibility(View.GONE);
            linearLayoutPmtAmount.setVisibility(View.GONE);
            linearLayoutNewBalance.setVisibility(View.GONE);
            linearLayoutBalance.setVisibility(View.GONE);
        }
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_add_move);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener((view -> onBackPressed()));

        button = findViewById(R.id.btn_add_move);
        button.setOnClickListener(this);
        button.setEnabled(false);

        calendarView = findViewById(R.id.cv_add_move);
        calendarView.setMinDate(NewMovement.me.getSaleDate().getTime());
        calendarView.setOnDateChangeListener(this);

        textViewNewBalance = findViewById(R.id.txt_new_balance_add_move);
        textViewCustomerName = findViewById(R.id.txt_name_add_move);
        textViewOrder = findViewById(R.id.txt_order_add_move);
        textViewCurrentBalance = findViewById(R.id.txt_actual_balance_add_move);
        textViewExigible = findViewById(R.id.txt_exigible_add_move);
        textViewNewExigible = findViewById(R.id.txt_new_exigible_add_move);
        textViewPmtAmount = findViewById(R.id.txt_pmt_amount);
        textViewUnpaidAmountDue = findViewById(R.id.txt_unpaid_amount_due);

        editTextComments = findViewById(R.id.edtxt_comments_add_move);
        editTextComments.addTextChangedListener(this);

        editTextAmount = findViewById(R.id.edtxt_amount_add_move);
        editTextAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
        editTextAmount.addTextChangedListener(this);

        textInputLayoutAmount = findViewById(R.id.til_amount_add_move);

        linearLayoutExigible = findViewById(R.id.ll_exigible_add_move);
        linearLayoutNewExigible = findViewById(R.id.ll_newexigible_add_move);

        linearLayoutBalance = findViewById(R.id.ll_balance);
        linearLayoutNewBalance = findViewById(R.id.ll_new_balance);
        linearLayoutPmtAmount = findViewById(R.id.ll_pmt_amount);
        linearLayoutUnPaidAmountDue = findViewById(R.id.ll_unpaid_amount_due);


        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getResources().getString(R.string.app_name));
        progressDialog.setMessage("Agregando movimiento, por favor espera...");
        progressDialog.setCancelable(false);
    }

    private void displayData(){
        textViewCustomerName.setText(
                NewMovement.me.getCustomerName()
        );

        textViewOrder.setText(
                NewMovement.me.getOrderId()
        );

        textViewCurrentBalance.setText(
                NumberFormat.getCurrencyInstance().format(currentBalance)
        );

        textViewNewBalance.setText(
                NumberFormat.getCurrencyInstance().format(newBalance)
        );

        textViewExigible.setText(
                NumberFormat.getCurrencyInstance().format(
                        NewMovement.me.getExigible()
                )
        );

        textViewNewExigible.setText(
                NumberFormat.getCurrencyInstance().format(
                        NewMovement.me.getExigible()
                )
        );

        textViewPmtAmount.setText(
                NewMovement.me.getCurrencyPmtAmount()
        );

        textViewUnpaidAmountDue.setText(
                NewMovement.me.getCurrencyUnpaidAmountDue()
        );
    }


    private double getEditTextRawAmount(){
        if (editTextAmount.getText().toString().isEmpty()
        || editTextAmount.getText().toString().equals("."))
            return 0;
        else
            return Double.valueOf(editTextAmount.getText().toString());
    }

    private void calculateNewBalance(){
        if (movement.getType().getMovementType() == MovementType.Query.A) {
            newBalance = currentBalance - getEditTextRawAmount();
            newExigible = currentExigible - getEditTextRawAmount();

            if (newExigible < 0){
                newExigible = 0;
            }
        }
        else{
            newBalance = currentBalance + getEditTextRawAmount();
            if (movement.getType().getCode().equals("M012"))
                newExigible = currentExigible + getEditTextRawAmount();
        }

        textViewNewBalance.setText(
                NumberFormat.getCurrencyInstance().format(newBalance)
        );

        textViewNewExigible.setText(
                NumberFormat.getCurrencyInstance().format(newExigible)
        );

        movement.setAmount(getEditTextRawAmount());
    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        calculateNewBalance();
        if (editTextAmount.getText().toString().isEmpty()){
            textInputLayoutAmount.setError("Debes ingresar el importe.");
            button.setEnabled(false);
        } else if (getEditTextRawAmount() <= 0){
            textInputLayoutAmount.setError("El importe debe ser mayor a cero.");
            button.setEnabled(false);
        }

        else if (getEditTextRawAmount() > NewMovement.me.getMaxMovementAmount() && NewMovement.me.getMovement().getType().getMovementType() == MovementType.Query.A) {
            textInputLayoutAmount.setError("El importe no puede ser mayor al saldo actual.");
            button.setEnabled(false);
        }

        else{
            textInputLayoutAmount.setError("");
            button.setEnabled(true);
        }
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(i, i1,i2);
        movement.setDate(calendar.getTime());
    }

    @Override
    public void onClick(View view) {

        if (movement.getType().isRequestPayment())
            askForPaymentMethod();
        else
            addMovement();
    }

    private void addMovement(){
        progressDialog.show();
        new APIVndrCxc(Volley.newRequestQueue(this))
                .addMovement(movement, this);
    }

    private void askForPaymentMethod(){
        startActivityForResult(
                new Intent(
                        this,
                        PickPaymentMethodActivity.class
                ), 9
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9 && resultCode == RESULT_OK){
            movement.setPaymentMethod(data.getStringExtra("code"));
            addMovement();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        movement.setComment(editTextComments.getText().toString());
    }


    @Override
    public void addMovementSuccess(CxCMovement movement) {
        progressDialog.dismiss();
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
        this.finish();
    }

    @Override
    public void movementError(String messageError) {
        progressDialog.dismiss();
        Dialogs.showAlert(this, messageError);
    }
}
