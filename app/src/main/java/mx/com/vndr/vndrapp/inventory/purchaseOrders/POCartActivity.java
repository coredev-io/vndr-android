package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders.OnShoppingOrdersResponse;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class POCartActivity extends AppCompatActivity implements APIShoppingOrders.OnCreateShoppingOrder {

    Toolbar toolbar;
    RecyclerView recyclerView;
    Button button;
    EditText editTextShippingAmount;
    TextView textViewNoItems;
    TextView textViewTotalAmount;
    TextView textViewSupplierName;
    ProgressBar progressBar;

    private double shoppingOrderAmount = 0;
    private double shippingAmountAux = 0;
    private List<ShoppingCartProduct> productList = new ArrayList<>();
    ShoppingCartProductAdapter adapter;
    boolean orderWasUpdated = false;
    Supplier supplier;
    ShoppingOrder shoppingOrder;
    boolean updateMode = false;
    boolean continueLater = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_o_cart);
        setupUI();

        supplier = Supplier.Selected.current.getSupplier();

        if (getIntent().hasExtra("shopping_order")){
            updateMode = true;
            shoppingOrder = (ShoppingOrder) getIntent().getSerializableExtra("shopping_order");
            supplier = shoppingOrder.getSupplier();
            getShoppingOrderDetail(shoppingOrder.getPurchaseId());
        }

        textViewSupplierName.setText(supplier.getName());

        checkListProducts();

    }

    private void getShoppingOrderDetail(String idOrder){
        showLoading();
        new APIShoppingOrders(Volley.newRequestQueue(this))
                .getShoppingOrderDetail(new OnShoppingOrdersResponse() {

                    @Override
                    public void onSuccess(ShoppingOrder shoppingOrder) {
                        dismiss();
                        POCartActivity.this.shoppingOrder = shoppingOrder;
                        productList = shoppingOrder.getProductList();
                        fillData();
                        checkListProducts();
                    }

                    @Override
                    public void onError(String messageError) {
                        dismiss();
                        Dialogs.showAlert(POCartActivity.this, messageError, (dialog, which) -> {
                            POCartActivity.this.finish();
                        });

                    }
                }, idOrder);
    }

    private void fillData(){
        textViewTotalAmount.setText(shoppingOrder.getTotalOrderAmount());
        if (shoppingOrder.getShippingCost() != null)
            editTextShippingAmount.setText(shoppingOrder.getShippingCostDecimalFormat());
        adapter = new ShoppingCartProductAdapter(productList, this);
        recyclerView.setAdapter(adapter);
    }


    public void onClickAddProduct(View view){
        navigateToSuggestions();
    }

    public void onClickContinuar(View view){
        onClickSaveAndContinueLater();
    }

    private void buildShoppingOrder(){
        if (shoppingOrder == null){
            shoppingOrder = new ShoppingOrder();
            shoppingOrder.setSupplier(supplier);
        }
        shoppingOrder.setShippingCost(getCleanDoubleValue(editTextShippingAmount.getText().toString()));
        shoppingOrder.setProductList(productList);
        shoppingOrder.setTotalOrderAmount(shoppingOrderAmount);
        shoppingOrder.setTotalMerch();
        ShoppingOrder.Selected.current.setShoppingOrder(shoppingOrder);
    }

    private void navigateToSuggestions(){
        startActivityForResult(new Intent(this, OrderSuggestionsActivity.class),0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            ShoppingCartProduct product = (ShoppingCartProduct) data.getSerializableExtra("product");
            productList.add(product);
            adapter.notifyDataSetChanged();
            orderWasUpdated = true;
            updateTotalOrderUI(true, product.getSubtotal());
            checkListProducts();
        }

        if (requestCode == 1 && resultCode == RESULT_OK){
            this.finish();
        }

        if (requestCode == 1 && resultCode != RESULT_OK){
            shoppingOrder = ShoppingOrder.Selected.current.getShoppingOrder();
            updateMode = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shopping_cart_menu,menu);
        return true;
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_po_cart);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.menu_delete_order){
                Dialogs.showAlertQuestion(
                        this,
                        "¿Estás seguro que ya no requieres este carrito?",
                        (dialog, which) -> onClickDeleteShoppingCart());
            }
            return false;
        });

        recyclerView = findViewById(R.id.rv_po_cart);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewSupplierName = findViewById(R.id.txt_po_cart_supplier_name);
        progressBar = findViewById(R.id.pb_po_cart);

        button = findViewById(R.id.btn_po_cart);
        editTextShippingAmount = findViewById(R.id.et_po_shipping_order);
        editTextShippingAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
        editTextShippingAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                orderWasUpdated = true;
                if (shoppingOrderAmount != 0)
                    shoppingOrderAmount = shoppingOrderAmount - shippingAmountAux;
                shippingAmountAux = getCleanDoubleValue(editable.toString());
                updateTotalOrderUI(true, shippingAmountAux);

            }
        });

        textViewNoItems = findViewById(R.id.txt_po_no_items);
        textViewTotalAmount = findViewById(R.id.txt_po_total);

        adapter = new ShoppingCartProductAdapter(productList, this);
        recyclerView.setAdapter(adapter);
    }

    private void onClickDeleteShoppingCart() {
        if (shoppingOrder == null || shoppingOrder.getPurchaseId() == null)
            this.finish();
        else{
            new APIShoppingOrders(Volley.newRequestQueue(this))
                    .deleteShoppingOrder(this, shoppingOrder.getPurchaseId());

        }
    }

    @Override
    public void onSuccessDelete() {
        this.finish();
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    private void dismiss(){
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    public void navigateToPickDate(){
        startActivityForResult(new Intent(this, PickOrderDateActivity.class),1);
    }


    public void updateTotalOrderUI(boolean isAdd, Double value){
        if (isAdd)
            shoppingOrderAmount = shoppingOrderAmount + value;
        else if (!isAdd && shoppingOrderAmount != 0.00)
            shoppingOrderAmount = shoppingOrderAmount - value;

        textViewTotalAmount.setText(NumberFormat.getCurrencyInstance().format(shoppingOrderAmount));
    }

    public void checkListProducts(){
        if (productList.isEmpty()){
            button.setEnabled(false);
            textViewNoItems.setVisibility(View.VISIBLE);
        } else {
            button.setEnabled(true);
            textViewNoItems.setVisibility(View.GONE);

        }
    }

    private double getCleanDoubleValue(String value){
        if (value.isEmpty() || value.equals("."))
            return 0;
        else
            return Double.parseDouble(value);
    }

    @Override
    public void onBackPressed() {
        if (orderWasUpdated){
            Dialogs.showAlertQuestion(
                    this,
                    "¿Deseas guardar la orden y continuar después?",
                    ((dialog, which) -> {onClickSaveAndContinueLater(); continueLater = true; }),
                    (dialog, which) -> POCartActivity.this.finish()
            );
        } else this.finish();
    }

    private void onClickSaveAndContinueLater() {
        //  Build shoppingOrder
        buildShoppingOrder();

        new APIShoppingOrders(Volley.newRequestQueue(this))
                .createUpdateShoppingOrder(shoppingOrder, this, updateMode);
        showLoading();
    }

    @Override
    public void onSuccess() {
        dismiss();
        if (continueLater) this.finish();
        else navigateToPickDate();
    }

    @Override
    public void onError(String messageError) {
        dismiss();
        Dialogs.showAlert(this, messageError);
    }

    public class ShoppingCartProductAdapter extends RecyclerView.Adapter<ShoppingCartProductAdapter.ProductViewHolder> {

        private List<ShoppingCartProduct> items;
        private Context context;

        public ShoppingCartProductAdapter(List<ShoppingCartProduct> items, Context context) {
            this.items = items;
            this.context = context;
        }

        @NonNull
        @Override
        public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shopping_cart_item, parent, false);
            return new ProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ProductViewHolder holder, final int position) {
            holder.count.setText(String.valueOf(position + 1));
            holder.productName.setText(items.get(position).getProduct().getProductName());
            holder.productKey.setText(items.get(position).getProduct().getProduct_key());
            holder.subtotal.setText(items.get(position).getSubtotalString());
            holder.salePrice.setText(
                    items.get(position).getProductsCount() +
                    " piezas x " +
                    items.get(position).getBuyPriceStr()
            );


            if (items.get(position).getProduct().getColor().isEmpty())
                holder.color.setVisibility(View.GONE);
            else
                holder.color.setText(items.get(position).getProduct().getColor());

            if (items.get(position).getProduct().getSize().isEmpty())
                holder.size.setVisibility(View.GONE);
            else
                holder.size.setText(items.get(position).getProduct().getSize());

            if (items.get(position).getNotes().isEmpty())
                holder.notes.setVisibility(View.GONE);
            else {
                holder.notes.setVisibility(View.VISIBLE);

                holder.notes.setText(items.get(position).getNotes());
            }

            holder.delete.setOnClickListener(view -> {

                ((POCartActivity) context).orderWasUpdated = true;
                ((POCartActivity) context).updateTotalOrderUI(false, items.get(position).getSubtotal());
                items.remove(position);
                ShoppingCartProductAdapter.this.notifyDataSetChanged();
                ((POCartActivity) context).checkListProducts();
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class ProductViewHolder extends RecyclerView.ViewHolder {

            TextView count, productName, productKey, salePrice, subtotal;
            TextView size, color, notes;
            ImageButton delete;

            public ProductViewHolder(@NonNull View itemView) {
                super(itemView);

                count = itemView.findViewById(R.id.count_cart_item);
                productName = itemView.findViewById(R.id.product_name_cart_item);
                productKey = itemView.findViewById(R.id.product_key_item);
                salePrice = itemView.findViewById(R.id.sale_price_cart_item);
                subtotal = itemView.findViewById(R.id.subtotal_cart_item);
                delete = itemView.findViewById(R.id.delete_cart_item);
                size = itemView.findViewById(R.id.size_item);
                color = itemView.findViewById(R.id.color_item);
                notes = itemView.findViewById(R.id.notes_item);
            }
        }
    }

}