package mx.com.vndr.vndrapp.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.EditTextPhoneNumber;
import mx.com.vndr.vndrapp.models.Customer;

import static android.view.View.GONE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CUSTOMERS;

public class AddCustomerActivity extends AppCompatActivity {

    Toolbar toolbar;
    RadioGroup genderRG;
    EditText nameET, lastNameET, emailET, zipCodeET;
    EditTextPhoneNumber phoneET;
    EditText secondLastNameEt, secondNameEt;
    TextInputLayout nameIL, lastNameIL, emailIL, phoneIL, zipCodeIL;
    Button button;
    ProgressBar progressBar;
    RadioButton maleRB, femaleRB;

    String genderSelected;
    Boolean isEditMode = false;
    Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_customer);

        if (getIntent().hasExtra("customer")){
            customer = (Customer) getIntent().getSerializableExtra("customer");
            isEditMode = true;
        }
        setUpUI();
    }

    public void addCustomer(){
        showProgress(true);
        StringRequest request = new StringRequest(Request.Method.POST, URL_CUSTOMERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        Toast.makeText(AddCustomerActivity.this, "Cliente agregado con éxito", Toast.LENGTH_LONG).show();
                        showProgress(false);
                        AddCustomerActivity.this.finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                Toast.makeText(AddCustomerActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return createBody();
            }
        };

        Volley.newRequestQueue(this).add(request);
    }

    public void updateCustomer(){
        showProgress(true);
        StringRequest request = new StringRequest(Request.Method.PUT, URL_CUSTOMERS + "/" + customer.getIdCustomer(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println(response);
                        Toast.makeText(AddCustomerActivity.this, "Cliente actualizado con éxito", Toast.LENGTH_LONG).show();
                        showProgress(false);
                        AddCustomerActivity.this.setResult(RESULT_OK,new Intent().putExtra("customer",
                                new Customer(
                                        customer.getIdCustomer(),
                                        nameET.getText().toString() + " " + secondNameEt.getText().toString() + " " +  lastNameET.getText().toString() + " " + secondLastNameEt.getText().toString(),
                                        nameET.getText().toString(),
                                        lastNameET.getText().toString(),
                                        emailET.getText().toString(),
                                        phoneET.getRawPhoneNumber(),
                                        zipCodeET.getText().toString(),
                                        genderSelected,
                                        secondNameEt.getText().toString(),
                                        secondLastNameEt.getText().toString()
                                )
                                ));
                        AddCustomerActivity.this.finish();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                Toast.makeText(AddCustomerActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                showProgress(false);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return createBody();
            }
        };

        Volley.newRequestQueue(this).add(request);
    }


    private Map<String, String> createBody(){
        Map<String, String> body = new HashMap<>();

        body.put("given_name",nameET.getText().toString());
        body.put("family_name", lastNameET.getText().toString());
        body.put("email",emailET.getText().toString());
        body.put("mobile_phone",phoneET.getRawPhoneNumber());
        body.put("zipcode",zipCodeET.getText().toString());

        if (!secondNameEt.getText().toString().isEmpty()){
            body.put("second_given_name",secondNameEt.getText().toString());
        }

        if (!secondLastNameEt.getText().toString().isEmpty()){
            body.put("second_family_name",secondLastNameEt.getText().toString());
        }
        Log.e("TAG",secondLastNameEt.getText().toString());

        if (genderSelected != null){
            body.put("gender",genderSelected);
        }

        return body;
    }

    private void showProgress(Boolean show){
        if (show){
            button.setVisibility(GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            button.setVisibility(View.VISIBLE);
            progressBar.setVisibility(GONE);
        }
    }

    public void onClickCreate(View view){
        if (isEditMode){
            //  Activar campos
            nameET.setEnabled(true);
            lastNameET.setEnabled(true);
            zipCodeET.setEnabled(true);
            emailET.setEnabled(true);
            phoneET.setEnabled(true);
            genderRG.setEnabled(true);
            button.setText("Actualizar");
            isEditMode = false;
            secondLastNameEt.setEnabled(true);
            secondNameEt.setEnabled(true);

            maleRB.setEnabled(true);
            femaleRB.setEnabled(true);
        } else{
            if (button.getText().toString().equals("Actualizar"))
                updateCustomer();
            else
                addCustomer();
        }
    }

    private void checkEmpty(){
        if (
                nameET.getText().toString().isEmpty() ||
                        lastNameET.getText().toString().isEmpty() ||
                        emailET.getText().toString().isEmpty() ||
                        phoneET.getText().toString().isEmpty() ||
                        zipCodeET.getText().toString().isEmpty()
        ){
            button.setEnabled(false);
        } else {
            button.setEnabled(true);
        }
    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_add_customer);
        genderRG = findViewById(R.id.rg_gender);
        nameET = findViewById(R.id.edtxt_name);
        lastNameET = findViewById(R.id.edtxt_last_name);
        emailET = findViewById(R.id.edtxt_email);
        phoneET = findViewById(R.id.edtxt_phone);
        zipCodeET = findViewById(R.id.edtxt_zipcode);
        button = findViewById(R.id.btn_add_customer);
        nameIL = findViewById(R.id.il_name);
        lastNameIL = findViewById(R.id.il_last_name);
        zipCodeIL = findViewById(R.id.il_zipcode);
        phoneIL = findViewById(R.id.il_phone);
        emailIL = findViewById(R.id.il_email);
        progressBar = findViewById(R.id.pb_add_customer);
        progressBar.setVisibility(GONE);
        secondNameEt = findViewById(R.id.edtxt_second_name);
        secondLastNameEt = findViewById(R.id.edtxt_second_last_name);
        maleRB = findViewById(R.id.rb_male);
        femaleRB = findViewById(R.id.rb_female);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        genderRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (i == R.id.rb_female){
                    genderSelected = "femenino";
                } else {
                    genderSelected = "masculino";
                }
            }
        });

        setUPListeners();

        if (isEditMode){

            //  Llenar datos por default
            nameET.setText(customer.getCustomerName());
            lastNameET.setText(customer.getCustomerLastName());
            zipCodeET.setText(customer.getZipCode());
            emailET.setText(customer.getEmail());
            phoneET.setText(customer.getPhone());
            secondNameEt.setText(customer.getSecondName());
            secondLastNameEt.setText(customer.getSecondLastName());
            //button.setVisibility(GONE);
            genderSelected = customer.getGender();
            if (genderSelected.equals("femenino")){
                genderRG.check(R.id.rb_female);
            } else if (genderSelected.equals("masculino")){
                genderRG.check(R.id.rb_male);
            } else {
                genderSelected = null;
            }

            //  Desactivar campos
            nameET.setEnabled(false);
            lastNameET.setEnabled(false);
            zipCodeET.setEnabled(false);
            emailET.setEnabled(false);
            phoneET.setEnabled(false);
            genderRG.setEnabled(false);
            button.setText("Editar");
            button.setEnabled(true);
            secondLastNameEt.setEnabled(false);
            secondNameEt.setEnabled(false);
            maleRB.setEnabled(false);
            femaleRB.setEnabled(false);


        }
    }

    private void setUPListeners(){
        nameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (nameET.getText().toString().isEmpty()){
                    nameIL.setError("Debes ingresar un nombre");
                } else{
                    nameIL.setError("");
                }
                checkEmpty();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        lastNameET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (lastNameET.getText().toString().isEmpty()){
                    lastNameIL.setError("Debes ingresar los apellidos");
                }else{
                    lastNameIL.setError("");
                }
                checkEmpty();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        emailET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (emailET.getText().toString().isEmpty()){
                    emailIL.setError("Debes ingresar un email");
                }else{
                    emailIL.setError("");
                }
                checkEmpty();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        phoneET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (phoneET.getText().toString().isEmpty()){
                    phoneIL.setError("Debes ingresar un teléfono");
                }else{
                    phoneIL.setError("");
                }
                checkEmpty();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        zipCodeET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (zipCodeET.getText().toString().isEmpty()){
                    zipCodeIL.setError("Debes ingresar el código postal");
                }else{
                    zipCodeIL.setError("");
                }
                checkEmpty();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }
}
