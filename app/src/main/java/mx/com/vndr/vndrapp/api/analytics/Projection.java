package mx.com.vndr.vndrapp.api.analytics;

public class Projection {
    String month;
    String amountStr;
    int percent;

    public Projection(String month, String amountStr, int percent) {
        this.month = month;
        this.amountStr = amountStr;
        this.percent = percent;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAmountStr() {
        return amountStr;
    }

    public void setAmountStr(String amountStr) {
        this.amountStr = amountStr;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }
}