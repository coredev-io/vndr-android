package mx.com.vndr.vndrapp.orders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.customer.adapters.ListItemAdapter;

public class HeaderOrderAdapter extends RecyclerView.Adapter<HeaderOrderAdapter.HeaderOrderViewHolder> {

    List<List<String>> items;

    public HeaderOrderAdapter(List<List<String>> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public HeaderOrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_header_orders, parent, false);
        return new HeaderOrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HeaderOrderViewHolder holder, int position) {

        holder.title.setText(items.get(position).get(0));
        holder.count.setText(items.get(position).get(1));
        holder.amount.setText(items.get(position).get(2));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class HeaderOrderViewHolder extends RecyclerView.ViewHolder{
        TextView title, count, amount;
        public HeaderOrderViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.header_title);
            count = itemView.findViewById(R.id.header_count);
            amount = itemView.findViewById(R.id.header_amount);
        }
    }
}
