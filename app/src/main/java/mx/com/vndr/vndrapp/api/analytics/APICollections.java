package mx.com.vndr.vndrapp.api.analytics;

import com.android.volley.RequestQueue;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.TreeDataEntry;
import com.anychart.data.Tree;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.analytics.collections.ProjectionActivity;
import mx.com.vndr.vndrapp.analytics.collections.StatusCollectionActivity;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList3;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListType;
import mx.com.vndr.vndrapp.customviews.VndrPieChart;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_COLLECTION;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_CXC;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_CXC_CURRENT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_CXC_RESUME;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_PROJECTIONS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_SALES;

public class APICollections {

    private final static String TAG = "APICollections";

    private RequestQueue queue;

    public APICollections(RequestQueue queue) {
        this.queue = queue;
    }

    public void getCollection(OnCollectionResponse onCollectionResponse, StatusCollectionActivity.Query query){
        String url;
        String chartTitle;
        if (query == StatusCollectionActivity.Query.CURRENT){
            url = URL_ANALYTICS_COLLECTION;
            chartTitle = "Cobranza";
        } else {
            url = URL_ANALYTICS_CXC_CURRENT;
            chartTitle = "Por cobrar";
        }
        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onCollectionResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200) {
                                onCollectionResponse.onError(jsonObject.getString("message"));
                                return;
                            }
                            List<VndrList> data = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data");

                            data.add(new VndrList(new SimpleList2(jsonObject.getString("title"),"").setBGColor(R.color.colorAccent)));

                            jsonObject = jsonObject.getJSONObject("collection");


                            // chart
                            ArrayList<PieEntry> entries = new ArrayList<>();
                            entries.add(new PieEntry(
                                    jsonObject.getJSONObject("collected").getInt("amount_percent"),
                                    jsonObject.getJSONObject("collected").getString("label")
                            ));
                            entries.add(new PieEntry(
                                    jsonObject.getJSONObject("discounts").getInt("amount_percent"),
                                    jsonObject.getJSONObject("discounts").getString("label")
                            ));
                            entries.add(new PieEntry(
                                    jsonObject.getJSONObject("overdue").getInt("amount_percent"),
                                    jsonObject.getJSONObject("overdue").getString("label")
                            ));
                            entries.add(new PieEntry(
                                    jsonObject.getJSONObject("pending").getInt("amount_percent"),
                                    jsonObject.getJSONObject("pending").getString("label")
                            ));
                            data.add(new VndrList(new VndrPieChart(chartTitle, entries)));


                            data.add(new VndrList(new SimpleList3(
                                    "Total",
                                    "100%",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("total"))
                            )));

                            data.add(new VndrList(VndrListType.SEPARATOR));

                            data.add(new VndrList(new SimpleList3(
                                    jsonObject.getJSONObject("collected").getString("label"),
                                    String.valueOf(jsonObject.getJSONObject("collected").getDouble("amount_percent")) + "%",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getJSONObject("collected").getDouble("amount"))
                            )));


                            data.add(new VndrList(new SimpleList3(
                                    jsonObject.getJSONObject("discounts").getString("label"),
                                    String.valueOf(jsonObject.getJSONObject("discounts").getDouble("amount_percent")) + "%",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getJSONObject("discounts").getDouble("amount"))
                            )));


                            data.add(new VndrList(new SimpleList3(
                                    jsonObject.getJSONObject("overdue").getString("label"),
                                    String.valueOf(jsonObject.getJSONObject("overdue").getDouble("amount_percent"))+ "%",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getJSONObject("overdue").getDouble("amount"))
                            )));

                            data.add(new VndrList(new SimpleList3(
                                    jsonObject.getJSONObject("pending").getString("label"),
                                    String.valueOf(jsonObject.getJSONObject("pending").getDouble("amount_percent"))+ "%",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getJSONObject("pending").getDouble("amount"))
                            )));



                            onCollectionResponse.onSuccess(new VndrListAdapter(data));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onCollectionResponse.onError("Ocurrió un error al parsear los datos.");
                        }

                    }
                });
    }

    public void getStatusCxc(OnCollectionResponse onCollectionResponse){
        new VndrRequest(queue)
                .get(URL_ANALYTICS_CXC, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onCollectionResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {


                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200) {
                                onCollectionResponse.onError(jsonObject.getString("message"));
                                return;
                            }
                            List<VndrList> data = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data").getJSONObject("cxc");
                            JSONObject object = jsonObject.getJSONObject("outstanding");
                            data.add(new VndrList(new SimpleList2(object.getString("title"),"").setBGColor(R.color.colorAlertHigh)));
                            JSONArray array = object.getJSONArray("concepts");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                data.add(new VndrList(new SimpleList3(
                                        object1.getString("title"),
                                        String.valueOf(object1.getInt("quantity")),
                                        NumberFormat.getCurrencyInstance().format(object1.getDouble("amount"))
                                )));
                            }

                            data.add(new VndrList(VndrListType.SEPARATOR));

                            data.add(new VndrList(new SimpleList3(
                                    "Total",
                                    String.valueOf(object.getInt("total_quantity")),
                                    NumberFormat.getCurrencyInstance().format(object.getDouble("total"))
                            )));

                            object = jsonObject.getJSONObject("due_to_expire");
                            data.add(new VndrList(new SimpleList2(object.getString("title"),"").setBGColor(R.color.colorPorVencer)));
                            array = object.getJSONArray("concepts");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                data.add(new VndrList(new SimpleList3(
                                        object1.getString("title"),
                                        String.valueOf(object1.getInt("quantity")),
                                        NumberFormat.getCurrencyInstance().format(object1.getDouble("amount"))
                                )));
                            }

                            data.add(new VndrList(VndrListType.SEPARATOR));

                            data.add(new VndrList(new SimpleList3(
                                    "Total",
                                    String.valueOf(object.getInt("total_quantity")),
                                    NumberFormat.getCurrencyInstance().format(object.getDouble("total"))
                            )));

                            object = jsonObject.getJSONObject("overdue");
                            data.add(new VndrList(new SimpleList2(object.getString("title"),"").setBGColor(R.color.colorAlertHigh)));
                            array = object.getJSONArray("concepts");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object1 = array.getJSONObject(i);
                                data.add(new VndrList(new SimpleList3(
                                        object1.getString("title"),
                                        String.valueOf(object1.getInt("quantity")),
                                        NumberFormat.getCurrencyInstance().format(object1.getDouble("amount"))
                                )));
                            }

                            data.add(new VndrList(VndrListType.SEPARATOR));

                            data.add(new VndrList(new SimpleList3(
                                    "Total",
                                    String.valueOf(object.getInt("total_quantity")),
                                    NumberFormat.getCurrencyInstance().format(object.getDouble("total"))
                            )));



                            onCollectionResponse.onSuccess(new VndrListAdapter(data));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            onCollectionResponse.onError("Ocurrió un error al parsear los datos.");
                        }


                    }
                });
    }

    public void getProjection(OnProjectionResponse onProjectionResponse){
        new VndrRequest(queue)
                .get(URL_ANALYTICS_PROJECTIONS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onProjectionResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200) {
                                onProjectionResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            JSONArray array = jsonObject.getJSONObject("data").getJSONArray("projections");
                            List<Projections> projectionsList = new ArrayList<>();
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                String title = object.getString("year");
                                JSONArray arrayPs = object.getJSONArray("projections");
                                List<Projection> projectionList = new ArrayList<>();

                                for (int j = 0; j < arrayPs.length(); j++) {
                                    JSONObject objectP = arrayPs.getJSONObject(j);
                                    projectionList.add(
                                            new Projection(
                                                    objectP.getString("label"),
                                                    NumberFormat.getCurrencyInstance().format(objectP.getDouble("amount")),
                                                    objectP.getInt("percent")
                                            )
                                    );
                                }
                                projectionsList.add(new Projections(title,projectionList));
                            }

                            onProjectionResponse.onSuccess(
                                    projectionsList,
                                    NumberFormat.getCurrencyInstance().format(
                                            jsonObject.getJSONObject("data").getDouble("total_projection")
                                    )
                                    );



                        } catch (JSONException e) {
                            e.printStackTrace();
                            onProjectionResponse.onError("Ocurrió un error al procesar la solicitud.");
                        }

                    }
                });
    }

    public void getResumeCxc(OnCollectionResponse onCollectionResponse, Date minDate, Date maxDate){


        String url = URL_ANALYTICS_CXC_RESUME +
                "?start_date=" +
                VndrDateFormat.dateToWSFormat(minDate) +
                "&end_date=" +
                VndrDateFormat.dateToWSFormat(maxDate);

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onCollectionResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {

                        JSONObject jsonObject = null;
                        try {
                            jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200) {
                                onCollectionResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            jsonObject = jsonObject.getJSONObject("data");

                            List<VndrList> data = new ArrayList<>();

                            data.add(new VndrList(new SimpleList2(
                                    "Abonos",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("total_payments"))
                            ).setBGColor(R.color.colorPrimary)));

                            JSONArray array = jsonObject.getJSONArray("payments");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                data.add(new VndrList(new SimpleList2(
                                        object.getString("title"),
                                        NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                ).setBGColor(R.color.colorAccent)));
                            }

                            data.add(new VndrList(new SimpleList2(
                                    "Cargos",
                                    NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("total_charges"))
                            ).setBGColor(R.color.colorPrimary)));

                            array = jsonObject.getJSONArray("charges");

                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                data.add(new VndrList(new SimpleList2(
                                        object.getString("title"),
                                        NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                ).setBGColor(R.color.colorAccent)));
                            }



                            onCollectionResponse.onSuccess(new VndrListAdapter(data));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onCollectionResponse.onError("Ocurrió un error al procesar la solicitud.");
                        }

                    }
                });
    }

    public interface OnProjectionResponse {
        void onSuccess(List<Projections> projections, String amount);
        void onError(String error);
    }

    public interface OnCollectionResponse {
        void onSuccess(VndrListAdapter adapter);
        void onError(String error);
    }

}
