package mx.com.vndr.vndrapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Order;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDER_DETAIL;

public class SuccesOrderActivity extends AppCompatActivity {

    TextView orderNum;
    ProgressBar progressBar;
    Button button;
    public Order order;
    //public String sms, email, title, subject, subtitle, detailSMS, detailMail;
    private NotificationAlertData notificationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_succes_order);

        orderNum = findViewById(R.id.order_number_success);
        progressBar = findViewById(R.id.progressBarSuccess);
        button = findViewById(R.id.button6Success);
        progressBar.setVisibility(View.GONE);

        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
            orderNum.setText(String.valueOf(order.getOrderNumber()));
        }


    }


    public void onClickFinish(View view){
        //onBackPressed();
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
        getSharedData(order.getIdOrder());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        System.out.println(resultCode);
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_OK);
        this.finish();
    }

    private void navigateToNotificationFragment(){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setNotificationAlertData(notificationData);
        alertFragment.setOnClickListener(view -> onBackPressed());


        alertFragment.show(getSupportFragmentManager(),"alert");
    }

    private void getSharedData(String idOrder){

        StringRequest request = new StringRequest(Request.Method.GET, URL_ORDER_DETAIL + idOrder + "&messages=true", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    button.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject(response);
                    notificationData = new NotificationAlertData(jsonObject.getJSONObject("notifications"),jsonObject.getJSONObject("contact_info"));
                    navigateToNotificationFragment();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(SuccesOrderActivity.this, "Ocurrió un error inesperado, inéntalo de nuevo.", Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                    button.setVisibility(View.VISIBLE);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                button.setVisibility(View.VISIBLE);
                Toast.makeText(SuccesOrderActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                System.out.println(new String(error.networkResponse.data));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        Volley.newRequestQueue(this).add(request);
    }
}
