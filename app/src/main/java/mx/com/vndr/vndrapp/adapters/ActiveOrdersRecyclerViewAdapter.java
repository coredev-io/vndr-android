package mx.com.vndr.vndrapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.ActiveOrdersActivity;
import mx.com.vndr.vndrapp.orders.AllOrdersActivity;
import mx.com.vndr.vndrapp.orders.OrderDetailActivity;
import mx.com.vndr.vndrapp.orders.OrderReviewActivity;
import mx.com.vndr.vndrapp.orders.OrdersByClientActivity;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;

import static mx.com.vndr.vndrapp.models.Order.INCOMPLETO;


public class ActiveOrdersRecyclerViewAdapter extends RecyclerView.Adapter<ActiveOrdersRecyclerViewAdapter.OrderViewHolder> {

    private List<Order> items;
    private Context context;

    public ActiveOrdersRecyclerViewAdapter(List<Order> orderList, Context c) {
        this.items = orderList;
        this.context = c;
    }

    public void setColor(Drawable background, String status){


        int color = 0;
        switch (status){
            case Order.NO_ENTREGADO:
                color = ContextCompat.getColor(context,R.color.colorPrimary);
                break;
            case Order.NO_ENVIADO:
                color = ContextCompat.getColor(context,R.color.colorYellow);
                break;
            case Order.TERMINADO:
                color = ContextCompat.getColor(context,R.color.colorAccent);
                break;
            case INCOMPLETO:
                color = ContextCompat.getColor(context,R.color.colorRed);
            default:
                color = ContextCompat.getColor(context,R.color.colorRed);
                break;
        }

        if (background instanceof ShapeDrawable) {
            // cast to 'ShapeDrawable'
            ShapeDrawable shapeDrawable = (ShapeDrawable) background;
            shapeDrawable.getPaint().setColor(color);
        } else if (background instanceof GradientDrawable) {
            // cast to 'GradientDrawable'
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setColor(color);
        } else if (background instanceof ColorDrawable) {
            // alpha value may need to be set again after this call
            ColorDrawable colorDrawable = (ColorDrawable) background;
            colorDrawable.setColor(color);
        }
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_card, parent, false);
        return new OrderViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {
        holder.name.setText(items.get(position).getCustomer().getCustomerFullName());
        holder.initials.setText(items.get(position).getCustomer().getInitials());
        holder.order.setText(items.get(position).getOrderNumberStr());
        holder.balance.setText(items.get(position).getTotalOrderAmountStr());
        holder.saleDate.setText(items.get(position).getParseSaleDate());
        holder.status.setText(items.get(position).getStatus());
        setColor(holder.status.getBackground(), items.get(position).getStatus());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                if (items.get(position).getStatus().equals(INCOMPLETO)){
                    Intent intent = new Intent(context, ShoppingCartActivity.class);
                    intent.putExtra("order",items.get(position));

                    intent.putExtra("isNewOrder", false);
                    try{
                        ((ActiveOrdersActivity)context).startActivityForResult(intent,0);
                    } catch (Exception e){
                        try{
                            ((AllOrdersActivity)context).startActivityForResult(intent,0);
                        } catch (Exception e1){
                            ((OrdersByClientActivity)context).startActivityForResult(intent,0);
                        }
                    }
                } else{

                    Intent intent = new Intent(context, OrderReviewActivity.class);
                    intent.putExtra("order",items.get(position));
                    intent.putExtra("isComplete", true);

                    try{
                        ((ActiveOrdersActivity)context).startActivityForResult(intent,0);
                    } catch (Exception e){
                        try{
                            ((AllOrdersActivity)context).startActivityForResult(intent,0);
                        } catch (Exception e1){
                            ((OrdersByClientActivity)context).startActivityForResult(intent,0);
                        }
                    }


                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return items.size();
    }


    public class OrderViewHolder extends RecyclerView.ViewHolder{

        //  Ligar a ui

        TextView status, name, initials, order, saleDate, balance;
        CardView cardView;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status_order_item);
            name = itemView.findViewById(R.id.fullname_order_item);
            initials = itemView.findViewById(R.id.txt_initial_name_order);
            order = itemView.findViewById(R.id.order_number_order_item);
            saleDate = itemView.findViewById(R.id.sale_date_order_item);
            balance = itemView.findViewById(R.id.balance_order_item);
            cardView = itemView.findViewById(R.id.cardview_order_item);
        }
    }
}
