package mx.com.vndr.vndrapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import mx.com.vndr.vndrapp.analytics.QuickViewActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrMembership;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.catalogs.mainCatalog.MainCatalogsActivity;
import mx.com.vndr.vndrapp.cobros.MainCobrosActivity;
import mx.com.vndr.vndrapp.cobros.notifications.NotificationsActivity;
import mx.com.vndr.vndrapp.customer.MainCustomerActivity;
import mx.com.vndr.vndrapp.customer.SelectCustomerActivity;
import mx.com.vndr.vndrapp.inventory.InventoryMenuActivity;
import mx.com.vndr.vndrapp.membership.MembershipActivity;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.orders.ActiveOrdersActivity;
import mx.com.vndr.vndrapp.settings.SettingsActivity;
import mx.com.vndr.vndrapp.util.Dialogs;
import zendesk.android.Zendesk;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_DELETE_SESSION;

public class MainActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener {

    Toolbar toolbar;
    TextView textViewName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI();

        textViewName.setText(VndrUser.getName(this));
        validateLicenseStatus();

    }

    private void validateLicenseStatus(){
        if (VndrMembership.status == null) {
            Log.e("TAG", "Error al validar la licencia");
            Toast.makeText(this, "Error al validar la licencia", Toast.LENGTH_LONG).show();
            this.onBackPressed();
            return;
        }

        //  LC06: Alerta de pago no procesado
        if (VndrMembership.status.equals(VndrMembership.LicenseVndrStatus.LC06)){
            Log.i("TAG", "License type: " + VndrMembership.status.toString());

            Dialogs.showCustomAlert(this)
                    .content(getString(R.string.lc06))
                    .positiveText("Administrar suscripción")
                    .onPositive((dialog, which) -> {
                        String url = String.format(
                                "https://play.google.com/store/account/subscriptions?sku=%s&package=%s",
                                BuildConfig.SKU_PROFESSIONAL_LICENSE,
                                BuildConfig.APPLICATION_ID
                        );

                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    })
                    .negativeText("Después")
                    .show();
        }
        AppState.setAppState(this, AppState.ENROLLED);
    }

    private void setupUI(){

        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(this);

        textViewName = findViewById(R.id.txt_name);

    }

    public void onClickAnalytics(View view){
        startActivity(new Intent(this, QuickViewActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.config_menu, menu);
        return true;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.menu_settings){
            navigateToSettingsMenu();
        }
        return false;
    }

    private void navigateToSettingsMenu(){
        startActivity(
                new Intent(this, SettingsActivity.class)
        );
    }

    private void displayMemberships(){
        startActivityForResult(new Intent(this, MembershipActivity.class),1);
    }

    public void onClickPedidos(View view){
        startActivity(new Intent(this, ActiveOrdersActivity.class));
    }

    public void onClickCustomers(View view){
        startActivityForResult(
                new Intent(this, SelectCustomerActivity.class),
                9
        );
    }

    public void onClickNotifications(View view){
        startActivity(new Intent(this, NotificationsActivity.class));
    }

    private void goToCustomerMainMenu(Customer customer){
        startActivity(
                new Intent(
                        this,
                        MainCustomerActivity.class
                ).putExtra("customer", customer)
        );
    }

    public void goToInventoryMenu(View view){
        startActivity(
                new Intent(
                        this,
                        InventoryMenuActivity.class
                )
        );
    }

    public void onClickCobros(View view){
        startActivity(
                new Intent(
                        this,
                        MainCobrosActivity.class
                )
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 9  && resultCode == RESULT_OK){
            Customer customer = (Customer) data.getSerializableExtra("customer");
            goToCustomerMainMenu(customer);
        }
        else if (requestCode == 1 && resultCode == RESULT_CANCELED){
            Dialogs.showAlert(
                    this,
                    "Iniciar el periodo de prueba o suscribirte a uno de nuestros planes te dará acceso a la aplicación.",
                    (dialog, which) -> onBackPressed());
        }
    }

    public void onClickCatalogs(View view){
        startActivity(
                new Intent(this, MainCatalogsActivity.class)
        );
    }



    @Override
    public void onBackPressed() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Cerrando sesión...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        VndrAuth.getInstance().logout(new VndrAuth.VndrAuthResult() {
            @Override
            public void onVndrAuthError(String messageError) {
                progressDialog.dismiss();
                MainActivity.this.finish();
            }

            @Override
            public void onVndrAuthSuccess() {
                progressDialog.dismiss();
                MainActivity.this.finish();
            }
        });
    }
}
