package mx.com.vndr.vndrapp.inventory.movements;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.api.inventory.InventoryMovements;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;

public class MovementInventoryTicketActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    Button button;
    InventoryMovements movement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_inventory_ticket);
        toolbar = findViewById(R.id.tb_ticket_mov);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener((view -> onBackPressed()));

        recyclerView = findViewById(R.id.rv_ticket_mov);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        button = findViewById(R.id.btn_continuar_ticket_mov);
        getData();
    }

    private void getData(){
        movement = (InventoryMovements) getIntent().getSerializableExtra("move");
        List<VndrList> vndrLists = new ArrayList<>();
        SimpleList2 list3;


        list3 = new SimpleList2(
                "Ticket",
                movement.getTicketNumber());
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Tipo de movimiento",
                movement.getDescription()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Producto",
                movement.getProduct().getProductName()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Clave de producto",
                movement.getProduct().getProduct_key()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Fecha",
                movement.getFormattedDate()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Existencia inicial",
                movement.getInitialStock()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Movimiento",
                String.valueOf(movement.getUnits())
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Existencia final",
                movement.getCurrentStock()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Costo unitario",
                movement.getCurrencyCost()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "IVA",
                movement.getCurrencyIva()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Total del movimiento",
                movement.getCurrencyTotalCost()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Notas",
                movement.getNotes()
        );
        vndrLists.add(new VndrList(list3));

        recyclerView.setAdapter(new VndrListAdapter(vndrLists));
    }

    public void onClick(View view){
        this.finish();
    }
}