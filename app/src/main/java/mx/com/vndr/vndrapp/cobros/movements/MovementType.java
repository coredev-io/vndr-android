package mx.com.vndr.vndrapp.cobros.movements;

import java.io.Serializable;
import java.util.List;

public class MovementType implements Serializable {

    private String code;
    private String type;
    private String description;
    private String typeTag;
    private Query movementType;
    private boolean requestPayment;


    public MovementType(String code, String type, String description, boolean requestPayment) {
        this.code = code;
        this.type = type;
        this.description = description;
        this.movementType = Query.valueOf(type);
        this.requestPayment = requestPayment;
    }

    public MovementType() {
    }

    public String getCode() {
        return code;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public Query getMovementType() {
        return movementType;
    }

    public String getTypeTag() {
        return typeTag;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTypeTag(String typeTag) {
        this.typeTag = typeTag;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isRequestPayment() {
        return requestPayment;
    }

    public enum Query {
        A,  // Abono
        C,  //  Cargo
        ALL,
        current;

        private Query selected;

        public Query getSelected() {
            return selected;
        }

        public void setSelected(Query selected) {
            this.selected = selected;
        }
    }
}
