package mx.com.vndr.vndrapp.analytics.inventory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class PurchasesInventoryMenuActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchases_inventory_menu);
        setupUI();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_pi_menu);
        recyclerView = findViewById(R.id.rv_pi_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuPurchaseInvOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToPurchaseOrderMenu));

        options = Arrays.asList(getResources().getStringArray(R.array.menuPurchaseInvOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToPurchasesMenu));
        /*
        options = Arrays.asList(getResources().getStringArray(R.array.menuPurchaseInvOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));
         */

        return optionList;
    }

    private void navigateToPurchaseOrderMenu(){
        startActivity(new Intent(this, PurchaseOrderMenuActivity.class));
    }

    private void navigateToPurchasesMenu(){
        startActivity(new Intent(this, PurchasesOptionsMenuActivity.class));
    }

    /*
    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }
     */
}