package mx.com.vndr.vndrapp.api;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.android.billingclient.api.AcknowledgePurchaseParams;
import com.android.billingclient.api.AcknowledgePurchaseResponseListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.ConsumeParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.BuildConfig;

public class VndrMembership implements
        PurchasesUpdatedListener,
        BillingClientStateListener,
        SkuDetailsResponseListener {

    private static final String TAG = "VndrMembership";
    public static LicenseVndrStatus status;

    private static VndrMembership instance;

    private BillingClient billingClient;
    private Context context;
    private PurchaseResult result;

    private VndrMembership(){
        billingClient = BillingClient.newBuilder(
                FirebaseAuth.getInstance().getApp().getApplicationContext()
        ).setListener(this).enablePendingPurchases().build();
        startMembershipConnection();
    }

    public static VndrMembership getInstance(){
        synchronized (VndrAuth.class) {
            if(instance == null){
                instance = new VndrMembership();
            }
        }
        return instance;
    }

    public void purchaseLicense(Context context, PurchaseResult result){
        this.result = result;
        this.context = context;
        if (billingClient.isReady()){
            queryProducts();
        } else {
            Log.e("TAG", "Billing Client is not ready cannot purchase");
            this.startMembershipConnection();
            this.result.onPurchaseLicenseFail("Ocurrió un error inesperado al conectarse con Google Play Store, por favor inténtalo de nuevo.");
        }
    }

    public void startMembershipConnection(){
        if (!billingClient.isReady()){
            Log.i(TAG, "Billing client is not ready. Starting connection");
            billingClient.startConnection(this);
        } else {
            Log.e(TAG, "Billing client is already connected");
        }
    }

    public void endMembershipConnection(){
        instance = null;
        billingClient.endConnection();
    }

    @Override
    public void onBillingSetupFinished(BillingResult billingResult) {

        if (billingResult.getResponseCode() !=  BillingClient.BillingResponseCode.OK) {
            Log.e(TAG, billingResult.getDebugMessage());
            return;
        }

        Log.d(TAG, "Billing service connected");
    }

    @Override
    public void onBillingServiceDisconnected() {
        Log.d(TAG, "Billing service disconnected");
    }

    private void queryProducts(){
        List<String> skuList = new ArrayList<>();
        skuList.add(BuildConfig.SKU_PROFESSIONAL_LICENSE);
        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        billingClient.querySkuDetailsAsync(params.build(), this);
    }

    @Override
    public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> skuDetailsList) {
        if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK && skuDetailsList == null) {
            Log.e(TAG, billingResult.getDebugMessage());
            result.onPurchaseLicenseFail(billingResult.getDebugMessage());
            return;
        }

        for (SkuDetails skuDetails : skuDetailsList) {
            Log.d(TAG, skuDetails.getTitle());
            Log.d(TAG, skuDetails.getDescription());
            Log.d(TAG, skuDetails.getPrice() + " " + skuDetails.getPriceCurrencyCode());
            Log.d(TAG, skuDetails.getOriginalJson());
        }

        purchase(skuDetailsList.get(0));
    }

    private void purchase(SkuDetails skuDetails){
        BillingResult featureSupportedResult = billingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS);
        if (featureSupportedResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
            Log.e(TAG, featureSupportedResult.getDebugMessage());
            result.onPurchaseLicenseFail(featureSupportedResult.getDebugMessage());
            return;
        }
        Log.e(TAG, VndrAuth.getInstance().getCurrentUser().getUid());
        BillingFlowParams flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(skuDetails)
                .setObfuscatedAccountId(VndrAuth.getInstance().getCurrentUser().getUid())
                .build();
        billingClient.launchBillingFlow((Activity) context, flowParams);
    }

    @Override
    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> purchases) {

        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED) {
            // Handle an error caused by a user cancelling the purchase flow.
            Log.d(TAG, billingResult.getDebugMessage());
            return;
        }

        if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK
                && purchases == null) {
            Log.e(TAG, billingResult.getDebugMessage());
            result.onPurchaseLicenseFail(billingResult.getDebugMessage());
            return;
        }


        for (Purchase purchase : purchases) {
            handlePurchase(purchase);
        }
    }

    private void handlePurchase(Purchase purchase) {
        if (purchase.getPurchaseState() == Purchase.PurchaseState.PURCHASED) {
            if (!purchase.isAcknowledged()) {
                AcknowledgePurchaseParams acknowledgePurchaseParams =
                        AcknowledgePurchaseParams.newBuilder()

                                .setPurchaseToken(purchase.getPurchaseToken())
                                .build();

                billingClient.acknowledgePurchase(acknowledgePurchaseParams, billingResult -> {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                        Log.i(TAG, "Purchase is Acknowledged");
                        this.result.onPurchaseLicenseSuccess();
                    } else {
                        Log.e(TAG, "purchase not Acknowledged, " + billingResult.getDebugMessage());
                    }
                });
            } else {
                Log.i(TAG, "Purchase is Acknowledged");
                this.result.onPurchaseLicenseSuccess();
            }
        } else {
            Log.e(TAG, "Purchase is not purchased " + purchase.getPurchaseState());
        }
    }

    public interface PurchaseResult {
        void onPurchaseLicenseSuccess();
        void onPurchaseLicenseFail(String messageError);
    }

    public enum LicenseVndrStatus {
        LC00,   // Licencia de Administrador
        LC01,   // Licencia de desarrollo-testing
        LC02,   // Licencia de prueba gratuita
        LC03,   // Licencia activa
        LC04,   // Licencia vencida
        LC05,   // Licencia cancelada
        LC06,   // Licencia en período de gracia
        LC07,   // Licencia suspendida
        LC08,   // Licencia pausada
        LC99    // No hay información de licencia
    }
}


