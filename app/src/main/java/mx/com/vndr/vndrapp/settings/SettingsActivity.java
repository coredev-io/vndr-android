package mx.com.vndr.vndrapp.settings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.account.ChangePwdActivity;
import mx.com.vndr.vndrapp.account.UserProfileActivity;
import mx.com.vndr.vndrapp.customviews.OptionList.OnClickListener;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.membership.MembershipActivity;
import mx.com.vndr.vndrapp.membership.ProfesionalMembershipActivity;
import mx.com.vndr.vndrapp.registro.DataUsageAuthActivity;
import mx.com.vndr.vndrapp.registro.PrivacyAndOpActivity;
import mx.com.vndr.vndrapp.registro.TyCActivity;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrSupport;
import zendesk.android.Zendesk;

public class SettingsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setupUI();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_settings_menu);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        recyclerView = findViewById(R.id.rv_settings_menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(createOptionListAdapter());

    }

    private OptionListAdapter createOptionListAdapter(){
        List<Option> optionList = new ArrayList<>();

        optionList.add(
                new Option("Información de contacto", "Actualiza tus datos personales", this::navigateToUserProfile)
        );

        optionList.add(
                new Option("Suscripción y pago", "Revisa el plan de pagos", this::navigateToMembership)
        );

        optionList.add(
                new Option("Seguridad de acceso a vndr+", "Cambia tu contraseña", this::navigateToChangePwd)
        );

        optionList.add(
                new Option("Privacidad y Operación", "Consulta cómo opera vndr+", this::navigateToTyC)
        );

        optionList.add(
                new Option("Datos Personales", "Autoriza el alcance de uso por vndr+", this::navigateToDataUsage)
        );

        optionList.add(
                new Option("Asistencia al usuario", "Comunícate directamente con nosotros", this::showHelpDialog)
        );

        return new OptionListAdapter(optionList);
    }

    private void navigateToUserProfile(){
        startActivity(
                new Intent(
                        this,
                        UserProfileActivity.class
                )
        );
    }

    private void navigateToMembership(){
        startActivity(
                new Intent(
                        this,
                        ProfesionalMembershipActivity.class
                ).putExtra("closeAction", true)
        );
    }

    private void navigateToChangePwd(){
        startActivity(
                new Intent(
                        this,
                        ChangePwdActivity.class
                )
        );
    }

    private void navigateToTyC(){
        startActivity(
                new Intent(
                        this,
                        PrivacyAndOpActivity.class
                ).putExtra("readMode", true)
        );
    }

    private void navigateToDataUsage(){
        startActivity(
                new Intent(
                        this,
                        DataUsageAuthActivity.class
                ).putExtra("updateMode", true)
        );
    }



    private void showHelpDialog(){
        Zendesk.getInstance().getMessaging().showMessaging(this);
        /*
        Dialogs.showCustomAlert(this)
                .content("Nuestro horario de asistencia es de Lunes a Viernes de 10:00 AM a 06:00 PM Hora Central de México")
                .positiveText("Enviar mensaje")
                .cancelable(false)
                .negativeText("Cancelar")
                .onPositive((dialog, which) -> {
                    startActivity(VndrSupport.getEmailSupportIntent());
                })
                .build()
                .show();
*/
    }
}
