package mx.com.vndr.vndrapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;

public class PaymentCalendarAdapter extends RecyclerView.Adapter<PaymentCalendarAdapter.PaymentCalendarViewHolder> {

    List<String> items;
    String amount;

    public PaymentCalendarAdapter(List<String> items, String amount) {
        this.items = items;
        this.amount = amount;
    }

    @NonNull
    @Override
    public PaymentCalendarViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_calendar_item, parent, false);

        return new PaymentCalendarAdapter.PaymentCalendarViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentCalendarViewHolder holder, int position) {
        holder.date.setText(items.get(position));
        holder.amount.setText(amount);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class PaymentCalendarViewHolder extends RecyclerView.ViewHolder{

        TextView date, amount;
        public PaymentCalendarViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.txt_date);
            amount = itemView.findViewById(R.id.txt_amount);
        }
    }
}
