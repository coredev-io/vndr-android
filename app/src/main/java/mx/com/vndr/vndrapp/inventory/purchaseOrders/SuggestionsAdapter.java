package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.SuggestionProducts;

public class SuggestionsAdapter extends RecyclerView.Adapter<SuggestionsAdapter.SuggestionsViewHolder> {

    public interface OnSuggestionSelected {
        void onSuggestionSelected(SuggestionProducts product);
    }
    List<SuggestionProducts> items;
    OnSuggestionSelected onSuggestionSelected;

    public SuggestionsAdapter(List<SuggestionProducts> items) {
        this.items = items;
    }

    public SuggestionsAdapter setOnSuggestionSelected(OnSuggestionSelected onSuggestionSelected) {
        this.onSuggestionSelected = onSuggestionSelected;
        return this;
    }

    @NonNull
    @Override
    public SuggestionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_order_suggestion, parent, false);
        return new SuggestionsViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SuggestionsViewHolder holder, int position) {
        if (onSuggestionSelected != null)
            holder.view.setOnClickListener(view -> onSuggestionSelected.onSuggestionSelected(items.get(position)));
        holder.name.setText(items.get(position).getProduct().getProductName());
        holder.catalog.setText(items.get(position).getProduct().getProduct_key());
        if (!items.get(position).getProduct().getSize().isEmpty()) {
            holder.size.setVisibility(View.VISIBLE);
            holder.size.setText(items.get(position).getProduct().getSize());
        } else {
            holder.size.setVisibility(View.GONE);
            holder.size.setText("");
        }
        if (!items.get(position).getProduct().getColor().isEmpty()) {
            holder.color.setVisibility(View.VISIBLE);
            holder.color.setText(items.get(position).getProduct().getColor());
        } else {
            holder.color.setVisibility(View.GONE);
            holder.color.setText("");
        }
        if (!items.get(position).getProduct().getDescription().isEmpty()) {
            holder.descripcion.setVisibility(View.VISIBLE);
            holder.descripcion.setText(items.get(position).getProduct().getDescription());
        } else {
            holder.descripcion.setVisibility(View.GONE);
            holder.descripcion.setText("");
        }

        holder.faltante.setText("Faltante: " + items.get(position).getCountMissing());

        holder.available_stock.setText("Disponible: " + items.get(position).getProduct().getInventory().getAvailableStock());
        holder.pending_stock_in.setText("Por recibir: " + items.get(position).getProduct().getInventory().getPendingStock());
        holder.current_stock.setText("Nivel actual: " + items.get(position).getProduct().getInventory().getCurrentCalculatedStock());
        holder.config_optimum.setText("Nivel óptimo: " + items.get(position).getProduct().getInventory().getOptimalConfiguration());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class SuggestionsViewHolder extends RecyclerView.ViewHolder{

        View view;
        TextView name, catalog, size, color, descripcion, faltante;
        TextView available_stock, pending_stock_in, current_stock, config_optimum;

        public SuggestionsViewHolder(@NonNull View itemView) {
            super(itemView);
            this.view = itemView;
            name = itemView.findViewById(R.id.name_product_item_po);
            catalog = itemView.findViewById(R.id.catalog_product_item_po);
            size = itemView.findViewById(R.id.size_product_item_po);
            color = itemView.findViewById(R.id.color_product_item_po);
            descripcion = itemView.findViewById(R.id.desc_product_item_po);
            faltante = itemView.findViewById(R.id.suggestion_item_po);
            available_stock = itemView.findViewById(R.id.available_stock);
            pending_stock_in = itemView.findViewById(R.id.pending_stock_in);
            current_stock = itemView.findViewById(R.id.current_stock);
            config_optimum = itemView.findViewById(R.id.config_optimum);
        }
    }
}