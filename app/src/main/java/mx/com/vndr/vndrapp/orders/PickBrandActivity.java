package mx.com.vndr.vndrapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.CustomExpandableListAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.catalogs.newBrand.NewBrandActivity;
import mx.com.vndr.vndrapp.inventory.PickInventoryProductActivity;
import mx.com.vndr.vndrapp.inventory.SelectStdClassActivity;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.StandardClassification;
import mx.com.vndr.vndrapp.products.PickProductActivity;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ACTIVE_USER_BRANDS;

public class PickBrandActivity extends AppCompatActivity {


    ExpandableListView expandableListView;
    ProgressBar progressBar;
    List<String> expandableListTitle;
    HashMap<String, List<String>> expandableListDetail = new HashMap<>();
    HashMap<String, List<StandardClassification>> classificationHashMap = new HashMap<>();
    HashMap<String, String> brandList = new HashMap<>();

    CustomExpandableListAdapter adapter;
    Toolbar toolbar;
    TextView empty;

    String idCatalog = "";

    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_brand);
        expandableListView = findViewById(R.id.exp_lv_brands);
        progressBar = findViewById(R.id.pb_pick_brand);
        empty = findViewById(R.id.txt_empty_brands);

        toolbar = findViewById(R.id.tb_pick_brand);
        fab = findViewById(R.id.floatingActionButton2);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        expandableListTitle = new ArrayList<>(expandableListDetail.keySet());



        expandableListView.setOnChildClickListener((expandableListView, view, i, i1, l) -> {

            Intent intent = new Intent(PickBrandActivity.this, PickProductActivity.class);

            if (getIntent().getBooleanExtra("pickProductInventory", false)) {
                intent = new Intent(PickBrandActivity.this, PickInventoryProductActivity.class);
            }

            if (getIntent().getBooleanExtra("shoppingOrder", false))
                intent.putExtra("shoppingOrder", true);
            if (getIntent().getBooleanExtra("pickProduct", false))
                intent.putExtra("pickProduct", true);
            if (getIntent().getBooleanExtra("inventoryRules", false))
                intent.putExtra("inventoryRules", true);
            String brandName = expandableListTitle.get(i);
            String idBrand = brandList.get(brandName);
            String idClass = classificationHashMap.get(brandName).get(i1).getId();

            intent.putExtra("idClass", idClass);
            intent.putExtra("idBrand", idBrand);

            if (getIntent().hasExtra("activity")){
                intent.putExtra("activity", ProductEnum.QUERY_PRODUCTS_BY_BRAND);
            }
            startActivityForResult(intent,0);

            return false;
        });

        getData();
        if (!getIntent().getBooleanExtra("shoppingOrder", false)){
            fab.setVisibility(View.GONE);
        }


    }

    public void getData(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_ACTIVE_USER_BRANDS , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(View.GONE);
                processResponse(response);
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            System.out.println(new String(error.networkResponse.data));
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);

    }

    private void processResponse(String response){
        try {
            JSONArray arrayResponse = new JSONArray(response);

            if (arrayResponse.length() == 0){
                empty.setVisibility(View.VISIBLE);
            }

            //  Recorrido por el arreglo de la respuesta
            for (int i = 0; i < arrayResponse.length(); i++){

                //  Obtiene el primer objeto del arreglo.
                JSONObject object = arrayResponse.getJSONObject(i);
                //  Obtiene el arreglo de clasificacion standard de la marca


                //  IF HASSS =-=-==-=-=
                if (object.has("standard_classification")){
                    JSONArray classificationArray = object.getJSONArray("standard_classification");

                    //  Si la marca no tiene clasificación standard no se agrega a la lista
                    if (classificationArray.length() != 0){
                        //  Obtiene el  nombre de la marca
                        String brandName = object.getString("brand_name");
                        String idBrand = object.getString("_id");
                        Brand.Selected.current.setBrand(new Brand(brandName, idBrand));

                        List<String> classificationStd = new ArrayList<>();

                        if (expandableListDetail.get(brandName) != null){
                            classificationStd = expandableListDetail.get(brandName);
                        }

                        //  Recorre el arreglo classsificacion standard
                        //  Para obtener una lista de las clasificaciones
                        List<StandardClassification> list = new ArrayList<>();
                        for (int j = 0; j < classificationArray.length();j++){
                            //  Se agrega la clasificacion a la lista
                            classificationStd.add(classificationArray.getJSONObject(j).getString("name"));
                            //  Se asocia la clasificacion con la marca en una lista nueva.
                            list.add( new StandardClassification(
                                    classificationArray.getJSONObject(j).getString("id"),
                                    classificationArray.getJSONObject(j).getString("name")
                            ));
                        }
                        classificationHashMap.put(brandName,list);
                        //  Se agrega a una lista la marca con todas las clasificaciones.
                        expandableListDetail.put(brandName,classificationStd);

                        if (brandList.get(brandName) == null){
                            brandList.put(brandName,idBrand);
                        }


                    }
                }



            }
            expandableListTitle = new ArrayList<>(expandableListDetail.keySet());
            Collections.sort(expandableListTitle);
            adapter = new CustomExpandableListAdapter(this,expandableListTitle,expandableListDetail);
            expandableListView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onClickNewBrand(View view){
        startActivity(
                new Intent(
                        this,
                        NewBrandActivity.class
                )
        );
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            this.setResult(resultCode, data);
            this.finish();
        }
    }

}
