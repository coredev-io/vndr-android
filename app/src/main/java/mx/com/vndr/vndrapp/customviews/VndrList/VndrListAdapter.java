package mx.com.vndr.vndrapp.customviews.VndrList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.PieChart;
import com.google.android.material.card.MaterialCardView;

import java.text.NumberFormat;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.Invoice;
import mx.com.vndr.vndrapp.api.analytics.PurchaseOrder;
import mx.com.vndr.vndrapp.api.cxc.CxCCard;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.api.cxc.CxCStatus;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.customviews.VndrPieChart;
import mx.com.vndr.vndrapp.models.Cxc;

public class VndrListAdapter extends RecyclerView.Adapter {

    private final static int SECTION_TITLE = 0;
    private final static int HEADER = 1;
    private final static int CARD = 2;
    private final static int MOVE = 3;
    private final static int EMPTY_LIST = 4;
    private final static int SIMPLE_LIST2 = 5;
    private final static int SEPARATOR = 6;
    private final static int PRODUCT_ANALYTIC = 7;
    private final static int CHART = 8;
    private final static int PURCHASE_ORDER = 9;
    private final static int INVOICE = 10;

    private List<VndrList> data;
    private Context context;
    private OnCardItemSelected listener;


    public VndrListAdapter(List<VndrList> data) {
        this.data = data;
    }

    public void setListener(OnCardItemSelected listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();

        switch (viewType){
            case HEADER:
                return new SimpleList3ViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.simple_list_item_3, parent, false)
                );
            case CARD:
                return new ItemCxcViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_cxc, parent, false)
                );
            case MOVE:
                return new ItemCxcMoveViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_cxc_movement, parent, false)
                );
            case EMPTY_LIST:
                return new SectionViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_empty_list, parent, false)
                );
            case SIMPLE_LIST2:
                return new SimpleList2ViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.simple_list_item2, parent, false)
                );
            case SEPARATOR:
                return new SeparatorViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_separator, parent, false)
                );
            case PRODUCT_ANALYTIC:
                return new ItemProductAnalyticsViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.card_product_analytics, parent, false)
                );

            case CHART:
                return new ChartViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_chart, parent, false)
                );

            case PURCHASE_ORDER:
                return new PurchaseOrderViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_analytics_purchase, parent, false)
                );

            case INVOICE:
                return new InvoiceViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.item_analytics_invoice, parent, false)
                );

            default: // SECITON_TITLE
                return new SectionViewHolder(
                        LayoutInflater.from(parent.getContext())
                                .inflate(R.layout.simple_list_item_1, parent, false)
                );
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        VndrList vndrList = data.get(position);

        switch (vndrList.getType()){

            case SECTION_TITLE:
                SectionViewHolder sectionHolder = (SectionViewHolder) holder;
                sectionHolder.textView1.setText(vndrList.getSectionTitle());

                break;
            case HEADER:
                SimpleList3ViewHolder holder3 = (SimpleList3ViewHolder) holder;
                SimpleList3 list3 = vndrList.getSimpleList3();
                holder3.textView1.setText(list3.getS1());
                holder3.textView2.setText(list3.getS2());
                holder3.textView3.setText(list3.getS3());
                break;
            case CARD_DETAIL:
                ItemCxcViewHolder cxcHolder = (ItemCxcViewHolder) holder;
                CxCCard cxc = vndrList.getCxc();

                cxcHolder.name.setText(cxc.getCustomerName());
                cxcHolder.status.setText(cxc.getStatusDesc());
                GradientDrawable shapeStatus = (GradientDrawable) cxcHolder.status.getBackground();
                shapeStatus.setColor(context.getResources().getColor(cxc.getStatus().getColorResId()));
                cxcHolder.orderNumber.setText(String.valueOf(cxc.getOrderNumber()));
                cxcHolder.totalAmount.setText(cxc.getCurrencyTotalAmount());
                cxcHolder.dueAmount.setText(cxc.getCurrencyBalanceOverdue());

                if (cxc.getDueDate() != null)
                    cxcHolder.dueDate.setText(cxc.getDueDateFormatted());
                else{
                    cxcHolder.dueDate.setVisibility(View.GONE);
                    cxcHolder.dueDateDesc.setVisibility(View.GONE);
                }

                if (listener != null)
                    cxcHolder.cardView.setOnClickListener((view -> listener.onCxCSelected(cxc)));

                cxcHolder.dateAmount.setText(cxc.getCurrencyBalanceDue());
                if (cxc.getBillingDate() != null)
                    cxcHolder.billingDate.setText(cxc.getBillingFormattedDate());
                else {
                    cxcHolder.billingDate.setVisibility(View.GONE);
                    cxcHolder.billingDateDesc.setVisibility(View.GONE);
                }
                cxcHolder.pmtAmount.setText(cxc.getCurrencyPmtAmount());

                if (cxc.getStatus().equals(CxCStatus.VENCIDO)){
                    cxcHolder.fechaLimiteTableRow.setVisibility(View.GONE);
                    cxcHolder.pagoPeriodoTableRow.setVisibility(View.GONE);
                    cxcHolder.adeudoTableRow.setVisibility(View.GONE);
                } else if (cxc.getStatus().equals(CxCStatus.NO_COBRADO)){
                    cxcHolder.vencidoTableRow.setVisibility(View.GONE);
                    cxcHolder.fechaLimiteTableRow.setVisibility(View.GONE);
                    cxcHolder.pagoPeriodoTableRow.setVisibility(View.GONE);
                    cxcHolder.adeudoTableRow.setVisibility(View.GONE);
                }else {
                    cxcHolder.fechaLimiteTableRow.setVisibility(View.VISIBLE);
                    cxcHolder.pagoPeriodoTableRow.setVisibility(View.VISIBLE);
                    cxcHolder.adeudoTableRow.setVisibility(View.VISIBLE);
                    cxcHolder.vencidoTableRow.setVisibility(View.VISIBLE);
                }


                break;
            case CARD_MOVEMENT:
                ItemCxcMoveViewHolder moveHolder = (ItemCxcMoveViewHolder) holder;
                CxCMovement movement = vndrList.getMovement();

                moveHolder.amount.setText(movement.getCurrencyAmount());
                moveHolder.title.setText(movement.getDescription());
                moveHolder.desc.setText(movement.getDateFormatted());
                moveHolder.createDate.setText(movement.getCreateDateFormatted());

                moveHolder.type.setText(movement.getType().getTypeTag());
                GradientDrawable shapeType = (GradientDrawable) moveHolder.type.getBackground();
                int res;
                if (movement.getType().getType().equals(MovementType.Query.A.toString()))
                    res = R.color.colorBlue;
                else
                    res = R.color.colorRed;
                shapeType.setColor(context.getResources().getColor(res));

                if (listener != null)
                    moveHolder.cardView.setOnClickListener((view -> listener.onMoveSelected(movement)));

                break;
            case EMPTY_LIST:
                SectionViewHolder sectionViewHolder = (SectionViewHolder) holder;
                sectionViewHolder.textView1.setText(vndrList.getEmptyMessage());
                break;
            case SIMPLE_LIST2:
                SimpleList2ViewHolder holder2 = (SimpleList2ViewHolder) holder;
                SimpleList2 list2 = vndrList.getSimpleList2();
                holder2.textView1.setText(list2.getS1());
                holder2.textView2.setText(list2.getS2());
                holder2.textView1.setBackgroundResource(list2.getBgColorResId());
                holder2.textView2.setBackgroundResource(list2.getBgColorResId());
                if (list2.isCustom()){
                    holder2.textView1.setTextColor(Color.WHITE);
                    holder2.textView2.setTextColor(Color.WHITE);
                } else {
                    holder2.textView1.setTextColor(context.getColor(R.color.colorAccent));
                    holder2.textView2.setTextColor(context.getColor(R.color.colorAccent));
                }
                if (holder2.textView2.getText().toString().isEmpty()){
                    holder2.textView2.setVisibility(View.GONE);
                }
                break;
            case PRODUCT_ANALYTIC:
                ItemProductAnalyticsViewHolder holder4 = (ItemProductAnalyticsViewHolder) holder;
                ProductAnalytic productAnalytic = vndrList.getProductAnalytic();
                holder4.name.setText(productAnalytic.getName());
                holder4.key.setText(productAnalytic.getKey());
                holder4.total.setText(productAnalytic.getTotal());
                holder4.quantity.setText(productAnalytic.getQuantity());
                holder4.saleAmountProm.setText(productAnalytic.getSaleAvgAmount());
                holder4.saleInventory.setText(productAnalytic.getInventoryAmount());
                holder4.average.setText(productAnalytic.getMarginAvg());
                if (productAnalytic.isPurchase){
                    holder4.textView4.setText("Precio compra prom.");
                    holder4.textView5.setText("Precio catálogo");
                    holder4.textView6.setText("Descuento");

                    if (productAnalytic.isInvoice){
                        holder4.textView7.setText("Número de factura");
                        holder4.textView8.setText("Fecha de factura");
                    } else {
                        holder4.textView7.setText("Número de orden");
                        holder4.textView8.setText("Fecha de orden");
                    }

                    if (productAnalytic.getOrderNumber() != null){
                        holder4.linearLayout3.setVisibility(View.VISIBLE);
                        holder4.orderNumber.setText(productAnalytic.getOrderNumber());
                    }
                    if (productAnalytic.getOrderDate() != null){
                        holder4.linearLayout4.setVisibility(View.VISIBLE);
                        holder4.orderDate.setText(productAnalytic.getOrderDate());
                    }
                    holder4.linearLayout1.setVisibility(View.GONE);
                    holder4.linearLayout2.setVisibility(View.GONE);
                } else {
                    holder4.linearLayout1.setVisibility(View.GONE);
                    holder4.linearLayout2.setVisibility(View.GONE);
                }
                break;
            case CHART:
                ChartViewHolder chartHolder = (ChartViewHolder) holder;
                VndrPieChart vndrPieChart = vndrList.getChart();
                vndrPieChart.buildPieChart(chartHolder.chart);
                vndrPieChart.setColors(
                        context.getResources().getColor(R.color.colorAccent),
                        context.getResources().getColor(R.color.colorRed),
                        context.getResources().getColor(R.color.colorAlertLow),
                        context.getResources().getColor(R.color.colorPorVencer)
                );
                vndrPieChart.show();
                break;
            case PURCHASE_ORDER:
                PurchaseOrderViewHolder purchaseViewHolder = (PurchaseOrderViewHolder) holder;
                PurchaseOrder purchaseOrder = vndrList.getPurchaseOrder();

                purchaseViewHolder.status.setText(purchaseOrder.getStatus());
                purchaseViewHolder.name.setText(purchaseOrder.getSupplierName());
                purchaseViewHolder.purchaseNumber.setText(purchaseOrder.getOrderNumber());
                purchaseViewHolder.purchaseDate.setText(purchaseOrder.getPurchaseDate());
                purchaseViewHolder.sendDate.setText(purchaseOrder.getSendDate());
                purchaseViewHolder.totalAmount.setText(purchaseOrder.getTotalAmount());
                purchaseViewHolder.cardView.setOnClickListener(view -> {
                  if (listener != null)
                      listener.onPurchaseOrderSelected(purchaseOrder);
                });

                GradientDrawable shapeType2 = (GradientDrawable) purchaseViewHolder.status.getBackground();
                int res2;
                if (purchaseOrder.isNotSended())
                    res2 = R.color.colorAlertLow;
                else
                    res2 = R.color.colorPorVencer;

                shapeType2.setColor(context.getResources().getColor(res2));

                break;
            case INVOICE:
                InvoiceViewHolder invoiceViewHolder = (InvoiceViewHolder) holder;
                Invoice invoice = vndrList.getInvoice();

                invoiceViewHolder.status.setText(invoice.getStatus());
                invoiceViewHolder.name.setText(invoice.getSupplierName());
                invoiceViewHolder.invoiceNumber.setText(invoice.getInvoiceNumber());
                invoiceViewHolder.invoiceDate.setText(invoice.getInvoiceDate());
                invoiceViewHolder.receptionDate.setText(invoice.getReceptionDate());
                invoiceViewHolder.totalAmount.setText(invoice.getAmount());
                invoiceViewHolder.cardView.setOnClickListener(view -> {
                    if (listener != null)
                        listener.onInvoiceSelected(invoice);
                });

                GradientDrawable shapeType3 = (GradientDrawable) invoiceViewHolder.status.getBackground();
                int res3;
                if (invoice.getStatusCode().equals("I01"))
                    res3 = R.color.colorAlertLow;
                else
                    res3 = R.color.colorComplete;

                shapeType3.setColor(context.getResources().getColor(res3));

                break;
        }

    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        VndrList vndrList = data.get(position);

        switch (vndrList.getType()){
            case HEADER:
                return HEADER;
            case CARD_DETAIL:
                return CARD;
            case CARD_MOVEMENT:
                return MOVE;
            case EMPTY_LIST:
                return EMPTY_LIST;
            case SIMPLE_LIST2:
                return SIMPLE_LIST2;
            case SEPARATOR:
                return SEPARATOR;
            case PRODUCT_ANALYTIC:
                return PRODUCT_ANALYTIC;
            case CHART:
                return CHART;
            case PURCHASE_ORDER:
                return PURCHASE_ORDER;
            case INVOICE:
                return INVOICE;
            default:
                return SECTION_TITLE;
        }
    }

    class SeparatorViewHolder extends RecyclerView.ViewHolder{

        SeparatorViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    class SimpleList3ViewHolder extends RecyclerView.ViewHolder{

        TextView textView1, textView2, textView3;

        SimpleList3ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.text1);
            textView2 = itemView.findViewById(R.id.text2);
            textView3 = itemView.findViewById(R.id.text3);
        }
    }

    class SimpleList2ViewHolder extends RecyclerView.ViewHolder{

        TextView textView1, textView2;

        SimpleList2ViewHolder(@NonNull View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.text1);
            textView2 = itemView.findViewById(R.id.text2);
        }
    }

    class ItemCxcViewHolder extends RecyclerView.ViewHolder{

        TextView status, name, orderNumber, dueDate, billingDateDesc, totalAmount, dueAmount, dateAmount, pmtAmount, billingDate, dueDateDesc;
        TableRow adeudoTableRow;
        TableRow pagoPeriodoTableRow;
        TableRow fechaLimiteTableRow;
        TableRow vencidoTableRow;
        MaterialCardView cardView;
        ItemCxcViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.txt_status_cxc);
            name = itemView.findViewById(R.id.txt_customer_name);
            orderNumber = itemView.findViewById(R.id.txt_order_num);
            dueDate = itemView.findViewById(R.id.txt_due_pay_date);
            totalAmount = itemView.findViewById(R.id.txt_total_amount);
            dueAmount = itemView.findViewById(R.id.txt_due_amount);
            dateAmount = itemView.findViewById(R.id.txt_amount_date);
            billingDateDesc = itemView.findViewById(R.id.txt_billing_date_desc);
            cardView = itemView.findViewById(R.id.card_item_cxc);
            pmtAmount = itemView.findViewById(R.id.txt_pmt_amount);
            billingDate = itemView.findViewById(R.id.txt_billing_date);
            dueDateDesc = itemView.findViewById(R.id.txt_due_pay_date_desc);
            adeudoTableRow = itemView.findViewById(R.id.tr_adeudo);
            pagoPeriodoTableRow = itemView.findViewById(R.id.tr_pago_periodo);
            fechaLimiteTableRow = itemView.findViewById(R.id.tr_fecha_limite_pago);
            vencidoTableRow = itemView.findViewById(R.id.tr_vencido);
        }
    }

    class ItemCxcMoveViewHolder extends RecyclerView.ViewHolder{

        TextView title, desc, amount, type, createDate;
        MaterialCardView cardView;
        ItemCxcMoveViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_mov_title);
            desc = itemView.findViewById(R.id.txt_mov_date);
            amount = itemView.findViewById(R.id.txt_mov_amount);
            cardView = itemView.findViewById(R.id.card_move);
            type = itemView.findViewById(R.id.txt_mov_type);
            createDate = itemView.findViewById(R.id.txt_create_date_mov);
        }
    }

    class ItemProductAnalyticsViewHolder extends RecyclerView.ViewHolder{

        TextView name, key, quantity, total, saleAmountProm, saleInventory, average;
        TextView textView1, textView2, textView3, textView4, textView5, textView6, textView7, textView8;
        TextView orderNumber, orderDate;
        LinearLayout linearLayout1, linearLayout2, linearLayout3, linearLayout4;

        ItemProductAnalyticsViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            key = itemView.findViewById(R.id.key);
            quantity = itemView.findViewById(R.id.quantity);
            total = itemView.findViewById(R.id.total);
            saleAmountProm = itemView.findViewById(R.id.sale_prom_amount);
            saleInventory = itemView.findViewById(R.id.inv_amount);
            average = itemView.findViewById(R.id.prom);
            textView1 = itemView.findViewById(R.id.textView1);
            textView2 = itemView.findViewById(R.id.textView2);
            textView3 = itemView.findViewById(R.id.textView3);
            textView4 = itemView.findViewById(R.id.textView4);
            textView5 = itemView.findViewById(R.id.textView5);
            textView6 = itemView.findViewById(R.id.textView6);
            textView7 = itemView.findViewById(R.id.textView7);
            textView8 = itemView.findViewById(R.id.textView8);
            linearLayout1 = itemView.findViewById(R.id.ll_price);
            linearLayout2 = itemView.findViewById(R.id.ll_margin);
            linearLayout3 = itemView.findViewById(R.id.ll_order_number);
            linearLayout4 = itemView.findViewById(R.id.ll_order_date);
            orderNumber = itemView.findViewById(R.id.order_number);
            orderDate = itemView.findViewById(R.id.order_date);
        }
    }

    class ChartViewHolder extends RecyclerView.ViewHolder{

        PieChart chart;

        ChartViewHolder(@NonNull View itemView) {
            super(itemView);
            chart = itemView.findViewById(R.id.chart);
        }
    }

    class SectionViewHolder extends RecyclerView.ViewHolder {
        TextView textView1;
        SectionViewHolder(@NonNull View itemView) {
            super(itemView);
            textView1 = itemView.findViewById(R.id.text1);
        }
    }

    class PurchaseOrderViewHolder extends RecyclerView.ViewHolder{

        TextView status, name, purchaseNumber, purchaseDate, sendDate, totalAmount;
        MaterialCardView cardView;
        PurchaseOrderViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            name = itemView.findViewById(R.id.supplier_name);
            purchaseNumber = itemView.findViewById(R.id.purchase_number);
            purchaseDate = itemView.findViewById(R.id.purchase_date);
            sendDate = itemView.findViewById(R.id.send_date);
            totalAmount = itemView.findViewById(R.id.total_purchase);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }

    class InvoiceViewHolder extends RecyclerView.ViewHolder{

        TextView status, name, invoiceNumber, invoiceDate, receptionDate, totalAmount;
        MaterialCardView cardView;
        InvoiceViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status);
            name = itemView.findViewById(R.id.supplier_name);
            invoiceNumber = itemView.findViewById(R.id.invoice_number);
            invoiceDate = itemView.findViewById(R.id.invoice_date);
            receptionDate = itemView.findViewById(R.id.reception_date);
            totalAmount = itemView.findViewById(R.id.total_invoice);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }

    public interface OnCardItemSelected {

        default void onCxCSelected(CxCCard cxc){

        }

        default void onMoveSelected(CxCMovement movement) {

        }

        default void onPurchaseOrderSelected(PurchaseOrder purchaseOrder) {

        }

        default void onInvoiceSelected(Invoice invoice) {

        }
    }
}
