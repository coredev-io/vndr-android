package mx.com.vndr.vndrapp.customviews.itemList;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import mx.com.vndr.vndrapp.R;

public class ItemViewHolder extends RecyclerView.ViewHolder implements ItemRowView {

    TextView titleTextView;
    TextView descriptionTextView;
    ConstraintLayout constraintLayout;

    public ItemViewHolder(@NonNull View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.txt_title);
        descriptionTextView = itemView.findViewById(R.id.txt_description);
        constraintLayout = itemView.findViewById(R.id.layout_item_list);
    }


    @Override
    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void setDescription(String description) {
        descriptionTextView.setText(description);
    }

    @Override
    public void setOnClickListener(View.OnClickListener listener) {
        constraintLayout.setOnClickListener(listener);
    }
}
