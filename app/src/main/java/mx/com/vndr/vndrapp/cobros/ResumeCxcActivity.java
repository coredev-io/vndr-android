package mx.com.vndr.vndrapp.cobros;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import java.util.List;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCCard;
import mx.com.vndr.vndrapp.api.cxc.CxCQuery;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

import static android.view.View.GONE;

public class ResumeCxcActivity extends AppCompatActivity implements APIVndrCxc.CXCResponse, VndrListAdapter.OnCardItemSelected {

    private CxCQuery query;

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textView;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_cxc);
        setupUI();
        query = (CxCQuery) getIntent().getSerializableExtra("query");
    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(null);
        getData();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_resume_cxc);
        recyclerView = findViewById(R.id.rv_resume_cxc);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.all)
                goToCompletedCxcList();
            return false;
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textView = findViewById(R.id.txt_mssg_resume_cxc);
        textView.setVisibility(GONE);

        progressBar = findViewById(R.id.pb_resume_cxc);
        progressBar.setVisibility(GONE);

    }

    private void getData(){
        showProgress();
        APIVndrCxc apiCxc = new APIVndrCxc(Volley.newRequestQueue(this));

        switch (query){
            case GENERAL:
                apiCxc.getActiveCxcList(this);
                break;
            case CUSTOMER:
                apiCxc.getCustomerActiveCxcList(this, query.getQueryParam());
                break;
        }
    }

    private void goToCompletedCxcList(){
        startActivity(
                new Intent(
                        this,
                        CompletedCxcActivity.class
                ).putExtra("query", query)
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.active_orders_menu,menu);
        return true;
    }

    @Override
    public void cxcSuccess(List<VndrList> list) {
        dismissProgress();
        VndrListAdapter adapter = new VndrListAdapter(list);
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void cxcError(String messageError) {
        dismissProgress();
        showError(messageError);
    }

    @Override
    public void onCxCSelected(CxCCard cxc) {
        startActivity(
                new Intent(
                        this,
                        DetailCxcActivity.class
                ).putExtra("id_cxc", cxc.getId())
        );
    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(GONE);
    }

    private void showError(String messageError){
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
    }

    private void dismissError(){
        textView.setVisibility(GONE);
    }
}
