package mx.com.vndr.vndrapp.catalogs.mainCatalog;

import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.itemList.ItemListAdapter;

public interface MainCatalogsView {
    void setAdapter(ItemListAdapter adapter);
    void navigateToAsociarMarca();
    void nagivateToRegistrarMarca();
    void navigateToCatalog();
    void navigateToProduct();
}
