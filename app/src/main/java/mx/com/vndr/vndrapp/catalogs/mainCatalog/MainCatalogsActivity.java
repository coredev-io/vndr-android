package mx.com.vndr.vndrapp.catalogs.mainCatalog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.brands.BrandListActivity;
import mx.com.vndr.vndrapp.brands.CollaborativeBrandsActivity;
import mx.com.vndr.vndrapp.brands.OwnBrandsActivity;
import mx.com.vndr.vndrapp.brands.OwnBrandsView;
import mx.com.vndr.vndrapp.catalogs.activeUserBrand.ActiveUserBrandsActivity;
import mx.com.vndr.vndrapp.catalogs.associateBrand.AssociateBrandActivity;
import mx.com.vndr.vndrapp.catalogs.newBrand.NewBrandActivity;
import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.itemList.ItemListAdapter;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.products.ProductClassificationActivity;

public class MainCatalogsActivity extends AppCompatActivity implements MainCatalogsView{

    Toolbar toolbar;
    RecyclerView recyclerView;

    private MainCatalogPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_catalogs);
        setUpUI();

        presenter = new MainCatalogPresenter(this, new MainCatalogInteractor());
        new Thread(new Runnable() {
            @Override
            public void run() {
                presenter.getData();
            }
        }).start();
    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_main_catalogs);
        recyclerView = findViewById(R.id.rv_main_catalogs);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

    }


    @Override
    public void setAdapter(ItemListAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void navigateToAsociarMarca() {
        startActivity(
                new Intent(
                        this,
                        AssociateBrandActivity.class
                )
        );
    }

    @Override
    public void nagivateToRegistrarMarca() {
        startActivity(
                new Intent(
                        this,
                        OwnBrandsActivity.class
                )
        );
    }

    @Override
    public void navigateToCatalog() {
        /*
        startActivity(
                new Intent(
                        this,
                        OwnBrandsActivity.class
                ).putExtra("goToCatalogs",true)
        );

         */
        startActivity(
                new Intent(
                        this,
                        BrandListActivity.class
                ).putExtra("type", BrandListActivity.BrandListType.BRANDS_TO_EDIT_CATALOGS)
        );
    }

    @Override
    public void navigateToProduct() {
        startActivity(
                new Intent(
                        this,
                        CollaborativeBrandsActivity.class
                )
        );
    }


}
