package mx.com.vndr.vndrapp.customviews.checkItemList;

import android.view.View;

public class CheckedItem {
    private String title;
    private Boolean isChecked;
    private View.OnClickListener listener;


    public CheckedItem(String title, Boolean isChecked, View.OnClickListener listener) {
        this.title = title;
        this.isChecked = isChecked;
        this.listener = listener;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public View.OnClickListener getListener() {
        return listener;
    }

    public void setListener(View.OnClickListener listener) {
        this.listener = listener;
    }
}
