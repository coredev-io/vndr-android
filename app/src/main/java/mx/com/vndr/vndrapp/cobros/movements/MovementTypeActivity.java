package mx.com.vndr.vndrapp.cobros.movements;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.Movement;
import mx.com.vndr.vndrapp.customviews.RadioListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class MovementTypeActivity extends AppCompatActivity implements APIVndrCxc.MovementResponse, RadioListAdapter.OnSelectedItemListener {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textView;
    MovementType movementTypeSelected;

    private List<MovementType> types;
    private MovementType.Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_type);

        toolbar = findViewById(R.id.tb_move_type);
        recyclerView = findViewById(R.id.rv_move_type);
        progressBar = findViewById(R.id.pb_move_type);
        textView = findViewById(R.id.txt_move_type);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener((view -> onBackPressed()));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textView.setVisibility(View.GONE);

        query = (MovementType.Query) getIntent().getSerializableExtra("query");

        if (query == MovementType.Query.A)
            getSupportActionBar().setTitle("Cobro o abono a cuenta");
        else
            getSupportActionBar().setTitle("Cargo adicional a cuenta");

        showProgress();
        new APIVndrCxc(Volley.newRequestQueue(this))
                .getMovementTypeList(NewMovement.me.getMovement().getIdCxc(),this);
    }

    @Override
    public void movementTypeList(List<MovementType> types) {
        dismissProgress();
        this.types = new ArrayList<>();
        List<String> radioList = new ArrayList<>();

        for (MovementType type : types) {
            if (type.getMovementType().equals(query)) {
                this.types.add(type);
                radioList.add(type.getDescription());
            }
        }
        RadioListAdapter adapter = new RadioListAdapter(radioList);
        adapter.setOnSelectedItemListener(this);
        recyclerView.setAdapter(null); //   Para eliminar los datos anteriores
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void movementError(String messageError) {
        dismissProgress();
        showError(messageError);
    }


    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        textView.setVisibility(View.VISIBLE);
        textView.setText(error);
    }

    private void cancelSelection(){
        this.setResult(RESULT_CANCELED);
        this.finish();
    }
    private void confirmSelection(){
        this.setResult(
                RESULT_OK,
                new Intent().putExtra(
                        "movementType",
                        movementTypeSelected
                ));
        this.finish();
    }

    @Override
    public void onSelectedItem(int itemPosition) {
        movementTypeSelected = types.get(itemPosition);
        if (types.get(itemPosition).getCode().equals("M009") || types.get(itemPosition).getCode().equals("M010")){
            showAlert("Esta operación cambiará el pago periódico del Cliente y no es reversable. ¿Deseas continuar?");
        } else if (types.get(itemPosition).getCode().equals("M007")){
            showAlert("Este movimiento cancela el total adeudado por irrecuperable y no es reversible. ¿Deseas continuar?");
        }else {
            confirmSelection();
        }
    }

    public void showAlert(String message){
        Dialogs.showAlertQuestion(
                this,
                message,
                (dialog, which) -> {
                    confirmSelection();
                }, (dialog, which) -> {
                    // Negative
                    cancelSelection();
                });
    }
}
