package mx.com.vndr.vndrapp.analytics.business;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.TicketMovementActivity;
import mx.com.vndr.vndrapp.api.cxc.APIMovements;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.api.cxc.Movement;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.cobros.movements.MovementsAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter.OnCardItemSelected;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static android.view.View.GONE;
import static mx.com.vndr.vndrapp.cobros.movements.MovementsAdapter.*;

public class MovementAnalyticsActivity extends AppCompatActivity implements
        APIMovements.OnMovementResponse,
        VndrDatePicker.OnSelectedDate,
        BaseOnTabSelectedListener,
        OnMoveSelected,
        OnCardItemSelected {

    RecyclerView recyclerView;
    Toolbar toolbar;
    TabLayout tabLayout;
    TextView textViewDateInit,
            textViewDateEnd,
            textViewError;
    ProgressBar progressBar;
    boolean isRequestminDate;
    Date minDate, maxDate;
    Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_analytics);
        setupUI();

        //  Sett max and min Date of current month
        setCurrentDate();

        query = (Query) getIntent().getSerializableExtra("query");
        query.setMovementType(MovementType.ALL);
        customUI();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    private void customUI(){
        switch (query){
            case BY_INCOME:
                toolbar.setTitle("Ingresos");
                break;
            case BY_EXPENSES:
                toolbar.setTitle("Egresos");
                break;
            case BY_CASH:
                if (query.getPaymentMethodCode() == null)
                    toolbar.setTitle("Resumen General");
                else
                    toolbar.setTitle("Medio de Pago");
                tabLayout.getTabAt(1).setText("Depósito");
                tabLayout.getTabAt(2).setText("Retiros");
                break;
        }
    }

    private void getData(){
        showProgress();
        if (!query.equals(Query.BY_CASH)){
            new APIMovements(Volley.newRequestQueue(this))
                    .getAnalyticsMovements(this, minDate, maxDate, query);
        } else {
            new APIMovements(Volley.newRequestQueue(this))
                    .getCashMovements(this, minDate, maxDate, query);
        }
    }

    @Override
    public void selectedDate(Date date) {
        if (isRequestminDate){
            minDate = date;
            textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
            checkValidDateRange();
        }
        else{
            maxDate = date;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
        getData();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {


        if (tab.getPosition() == 1)
           query.setMovementType(MovementType.A);
        else if (tab.getPosition() == 2)
            query.setMovementType(MovementType.C);
        else
            query.setMovementType(MovementType.ALL);

        getData();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        //  Clear layout on tab unselected
        recyclerView.setAdapter(null);
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //  Por ahora no hacer nada
    }

    @Override
    public void moveSelected(CxCMovement movement) {
        navigateToMovementTicket(movement);
    }

    // Logic methods

    private void setCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date());

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);

        maxDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH, 1); //Set first day of month

        minDate = calendar.getTime();

        //  Display on UI
        textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
        textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
    }

    private void checkValidDateRange(){
        //  Previene que la fecha inicial sea mayor a la fecha final
        //  En dado caso fecha final se pone igual a la inicial
        if (minDate.compareTo(maxDate) > 0){
            maxDate = minDate;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
    }

    //  UI Methods
    private void setupUI(){
        toolbar = findViewById(R.id.tb_moves);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        tabLayout = findViewById(R.id.tabl_moves);
        tabLayout.addOnTabSelectedListener(this);

        recyclerView = findViewById(R.id.rv_moves);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewDateInit = findViewById(R.id.txt_moves_date_init);
        textViewDateInit.setOnClickListener(view -> {
            isRequestminDate = true;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewDateEnd = findViewById(R.id.txt_moves_date_end);
        textViewDateEnd.setOnClickListener(view -> {
            isRequestminDate = false;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(maxDate)
                    .setMinDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewError = findViewById(R.id.txt_moves_error);
        textViewError.setVisibility(GONE);

        progressBar = findViewById(R.id.pb_moves);
        progressBar.setVisibility(GONE);
    }

    private void showProgress(){
        dismissError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissProgress();
        recyclerView.setAdapter(null);
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }

    private void dismissError(){
        textViewError.setVisibility(GONE);
    }

    private void navigateToMovementTicket(CxCMovement movement){
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
    }

    @Override
    public void onSuccess(VndrListAdapter adapter) {
        recyclerView.setAdapter(null);
        dismissProgress();
        adapter.setListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onMoveSelected(CxCMovement movement) {
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
    }

    @Override
    public void onError(String error) {
        showError(error);
    }

    public enum Query {
        BY_INCOME, BY_EXPENSES, BY_CASH;

        private MovementType movementType;
        private String paymentMethodCode;

        public String getPaymentMethodCode() {
            return paymentMethodCode;
        }

        public void setPaymentMethodCode(String paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
        }

        public MovementType getMovementType() {
            return movementType;
        }

        public void setMovementType(MovementType movementType) {
            this.movementType = movementType;
        }
    }

    public enum MovementType {
        ALL, A, C
    }

}
