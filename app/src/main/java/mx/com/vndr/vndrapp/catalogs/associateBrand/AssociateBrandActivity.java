package mx.com.vndr.vndrapp.catalogs.associateBrand;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public class AssociateBrandActivity extends AppCompatActivity implements AssociateBrandView{

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    AssociateBrandPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_associate_brand);
        setUpUI();
        presenter = new AssociateBrandPresenter(new AssociateBrandInteractor(Volley.newRequestQueue(this), VndrAuth.getInstance().getCurrentUser().getSessionToken()),this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("TAG","ON RESUME =-=-=-");
        presenter.getData();
    }

    public void setUpUI(){
        toolbar = findViewById(R.id.tb_associated_brands);
        recyclerView = findViewById(R.id.rv_associated_brands);
        progressBar = findViewById(R.id.pb_associated_brand);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void setAdapter(CheckedItemAdapter adapter) {
        if (recyclerView.getAdapter() == null){
            recyclerView.setAdapter(adapter);
        } else{
            recyclerView.setAdapter(null);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showRequestError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showModal(Brand brand) {
        AssociatedDialogFragment.newInstance(brand).show(getSupportFragmentManager(), "dialog");
    }

}
