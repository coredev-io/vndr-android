package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import me.abhinay.input.CurrencyEditText;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.SuggestionProducts;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.Dialogs;

public class ProductQuantityActivity extends AppCompatActivity {



    Toolbar toolbar;
    TextView catalog, sku, total;
    TextView size, color, descripcion;
    TextView listPrice;
    ImageButton add, rm;
    EditText count, notes;
    TextView textViewPending;
    TextView textViewPendingOptimal;
    int pendingToBuy = 0;
    int optimalToBuy = 0;
    CurrencyEditText buyPrice;
    LinearLayout linearLayoutMissing;

    Product product;
    SuggestionProducts suggestionProduct;
    boolean isSuggestion = false;

    int c = 1; //   Products

    TextView pending_stock_in, current_stock, config_optimum, current_calculated_stock;
    TextView config_buy_back, config_minimal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_quantity);

        if (getIntent().hasExtra("product"))
            product = (Product) getIntent().getSerializableExtra("product");

        if (getIntent().hasExtra("productSuggestion")){
            suggestionProduct = (SuggestionProducts) getIntent().getSerializableExtra("productSuggestion");
            product = suggestionProduct.getProduct();
            isSuggestion = true;
        }

        setUpUI();

        if (isSuggestion){
            linearLayoutMissing.setVisibility(View.VISIBLE);
            TextView incomplete = findViewById(R.id.txt_incomplete);
            TextView pendingSend = findViewById(R.id.txt_pending_send);
            TextView pendingDelivery = findViewById(R.id.txt_pending_delivery);
            TextView totalMissing = findViewById(R.id.txt_total_missing);
            incomplete.setText(suggestionProduct.getCountIncomplete());
            pendingSend.setText(suggestionProduct.getCountPendingShipping());
            pendingDelivery.setText(suggestionProduct.getCountPendingDelivery());
            totalMissing.setText(String.valueOf(product.getInventory().getAvailableStock()));
            textViewPending.setVisibility(View.VISIBLE);
            textViewPendingOptimal.setVisibility(View.VISIBLE);

            pendingToBuy = product.getInventory().getCurrentCalculatedStock() + c;
            optimalToBuy = product.getInventory().getOptimalConfiguration() - pendingToBuy;

            textViewPending.setText("Nuevo nivel actual: " + pendingToBuy);
            textViewPendingOptimal.setText("Faltante para óptimo: " + optimalToBuy);

            current_stock.setText(String.valueOf(product.getInventory().getCurrentStock()));
            pending_stock_in.setText(String.valueOf(product.getInventory().getPendingStock()));
            current_calculated_stock.setText(String.valueOf(product.getInventory().getCurrentCalculatedStock()));
            config_buy_back.setText(String.valueOf(product.getInventory().getBuyBackConfiguration()));
            config_optimum.setText(String.valueOf(product.getInventory().getOptimalConfiguration()));
            config_minimal.setText(String.valueOf(product.getInventory().getMinimunConfiguration()));

        }
    }

    private void updatePendingToBuy(){
        pendingToBuy = product.getInventory().getCurrentCalculatedStock() + c;
        optimalToBuy = product.getInventory().getOptimalConfiguration() - pendingToBuy;

        if (optimalToBuy < 0) {
            optimalToBuy = 0;
        }

        textViewPending.setText("Nuevo nivel actual: " + pendingToBuy);
        textViewPendingOptimal.setText("Faltante para óptimo: " + optimalToBuy);
    }

    private void setUpUI(){
        catalog = findViewById(R.id.catalog_add_product);
        sku = findViewById(R.id.sku_add_product);
        total = findViewById(R.id.total_add_product);
        add = findViewById(R.id.btn_add);
        rm = findViewById(R.id.btn_rm);
        count = findViewById(R.id.count_add_product);
        buyPrice = findViewById(R.id.sale_price_add_product);
        notes = findViewById(R.id.notes_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        listPrice = findViewById(R.id.list_price);
        linearLayoutMissing = findViewById(R.id.ll_missing_po);
        linearLayoutMissing.setVisibility(View.GONE);
        textViewPending = findViewById(R.id.txt_pending_buy);
        textViewPending.setVisibility(View.GONE);
        textViewPendingOptimal = findViewById(R.id.txt_pending_optimal);
        textViewPendingOptimal.setVisibility(View.GONE);

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);

        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);

        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        listPrice.setText("Precio en catálogo: " + product.getListPriceStr());
        toolbar = findViewById(R.id.tb_add_product);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        buyPrice.setCurrency("$");
        buyPrice.setDelimiter(false);
        buyPrice.setSpacing(true);
        buyPrice.setDecimals(true);

        buyPrice.setOnKeyListener((view, i, keyEvent) -> {
            buyPrice.setSelection(buyPrice.getText().toString().length());
            return false;
        });

        buyPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateBalance();
            }
        });


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle(product.getProductName());
            catalog.setText(product.getCatalog().getCatalogName());
            sku.setText(product.getProduct_key());
        } else {
            catalog.setText(product.getProductName());
            sku.setText(product.getCatalog().getCatalogName());
        }

        getSupportActionBar();

        pending_stock_in = findViewById(R.id.pending_stock_in);
        current_stock = findViewById(R.id.current_stock);
        config_optimum = findViewById(R.id.config_optimum);
        current_calculated_stock = findViewById(R.id.current_calculated_stock);
        config_buy_back = findViewById(R.id.config_buy_back);
        config_minimal = findViewById(R.id.config_minimal);


    }

    public void onClickAddProduct(View view){

        if (buyPrice.getCleanDoubleValue() == 0)
            Dialogs.showAlertQuestion(
                    this,
                    "El precio de compra es $0.00, ¿Estás seguro?",
                    (dialog, which) -> passDataAndFinish());
        else if (buyPrice.getCleanDoubleValue() > product.getListPrice())
            Dialogs.showAlertQuestion(
                    this,
                    "El precio de compra es mayor al precio de catálogo, ¿Estás seguro?",
                    (dialog, which) -> passDataAndFinish());
        else
            passDataAndFinish();

    }

    private void passDataAndFinish(){
        this.setResult(
                RESULT_OK,
                new Intent()
                        .putExtra(
                                "product",
                                new ShoppingCartProduct(
                                        product,
                                        c,
                                        notes.getText().toString(),
                                        buyPrice.getCleanDoubleValue(),
                                        buyPrice.getCleanDoubleValue()*c
                                )
                        )
        );
        this.finish();
    }

    public void addProduct(View view){
        c += 1;
        count.setText(String.valueOf(c));
        updateBalance();
        if (isSuggestion)
            updatePendingToBuy();
    }

    public void removeProduct(View view){
        if (c != 1){
            c -= 1;
            count.setText(String.valueOf(c));
            updateBalance();
            if (isSuggestion)
                updatePendingToBuy();
        }
    }

    private void updateBalance(){
        total.setText(NumberFormat.getCurrencyInstance().format(
                buyPrice.getCleanDoubleValue() * c
        ));
    }
}