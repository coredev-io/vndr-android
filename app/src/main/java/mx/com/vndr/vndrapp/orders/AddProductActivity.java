package mx.com.vndr.vndrapp.orders;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import me.abhinay.input.CurrencyEditText;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;

public class AddProductActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView catalog, sku, list_price, subtotal, adjust, total, stock;
    TextView size, color, descripcion;
    Product product;
    ImageButton add, rm;
    EditText count, notes;
    CurrencyEditText salePrice;

    int c = 1; //   Products
    double diff = 0;
    double totalProd = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        product = (Product) getIntent().getSerializableExtra("product");

        setUpUI();
    }

    public void onClickAddProduct(View view){

        //product.setNotes(notes.getText().toString());
        Log.e("TAG",notes.getText().toString());

        this.setResult(
                RESULT_OK,
                new Intent()
                    .putExtra(
                            "product",
                            new ShoppingCartProduct(
                                    product,
                                    salePrice.getCleanDoubleValue(),
                                    diff,
                                    totalProd,
                                    c,
                                    notes.getText().toString()
                            )
                        )
            );
        this.finish();
    }

    private void setUpUI(){
        catalog = findViewById(R.id.catalog_add_product);
        list_price = findViewById(R.id.list_price_add_product);
        sku = findViewById(R.id.sku_add_product);
        subtotal = findViewById(R.id.subtotal_add_product);
        adjust = findViewById(R.id.adjust_add_product);
        total = findViewById(R.id.total_add_product);
        add = findViewById(R.id.btn_add);
        rm = findViewById(R.id.btn_rm);
        count = findViewById(R.id.count_add_product);
        salePrice = findViewById(R.id.sale_price_add_product);
        notes = findViewById(R.id.notes_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        stock = findViewById(R.id.current_calculated_stock);

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);

        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);

        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_add_product);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        salePrice.setCurrency("$");
        salePrice.setDelimiter(false);
        salePrice.setSpacing(true);
        salePrice.setDecimals(true);

        salePrice.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                salePrice.setSelection(salePrice.getText().toString().length());
                return false;
            }
        });

        salePrice.setText(product.getListPriceStr());

        salePrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                updateBalance();
            }
        });




        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setTitle(product.getProductName());
            catalog.setText(product.getCatalog().getCatalogName());
            sku.setText(product.getProduct_key());
        } else {
            catalog.setText(product.getProductName());
            sku.setText(product.getCatalog().getCatalogName());
        }

        list_price.setText(product.getListPriceStr());
        subtotal.setText(product.getListPriceStr());
        total.setText(product.getListPriceStr());
        totalProd = product.getListPrice();
        stock.setText(product.getCurrentCalculatedStock());


        getSupportActionBar();
    }

    public void addProduct(View view){
        c += 1;
        System.out.println(c);
        count.setText(String.valueOf(c));
        updateBalance();
    }

    public void removeProduct(View view){
        if (c != 1){
            System.out.println(c);
            c -= 1;
            count.setText(String.valueOf(c));
            updateBalance();
        }
    }

    private void updateBalance(){


        double sub = product.getListPrice() * c;
        diff = (salePrice.getCleanDoubleValue() - product.getListPrice())*c;
        totalProd = salePrice.getCleanDoubleValue() * c;
        //double percent = (diff*100) / product.getListPrice();
        double percent = ((totalProd / sub) - 1) * 100;
        //diff = salePrice.getCleanDoubleValue() - product.getListPrice();

        subtotal.setText(NumberFormat.getCurrencyInstance().format(sub));

        if (list_price.getText().toString().equals("$ 0.00")){
            //totalProd = c * product.getListPrice();
            total.setText(NumberFormat.getCurrencyInstance().format(
                    totalProd
            ));
        } else {
            //totalProd = c * salePrice.getCleanDoubleValue();
            total.setText(NumberFormat.getCurrencyInstance().format(
                    totalProd
            ));
        }




        //diff = totalProd - (c*product.getListPrice());
        //double percent = (diff*100) / product.getListPrice();

        if (diff > 0){
            adjust.setText(
                    "(+" +  new DecimalFormat("#.##").format(percent) + "%) +" +
                            NumberFormat.getCurrencyInstance().format(diff)
            );
        } else {
            adjust.setText(
                    "(" +  new DecimalFormat("#.##").format(percent) + "%) " +
                            NumberFormat.getCurrencyInstance().format(diff)
            );
        }




    }
}
