package mx.com.vndr.vndrapp.inventory;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.inventory.movements.MenuMovesInventoryActivity;
import mx.com.vndr.vndrapp.inventory.rules.InventoryRulesActivity;
import mx.com.vndr.vndrapp.inventory.supplier.SuppliersActivity;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class InventoryMenuActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_menu);
        setupUI();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_inventory_menu);
        recyclerView = findViewById(R.id.rv_inventory_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();
        List<String> headers = Arrays.asList(getResources().getStringArray(R.array.menuInventariosHeaderTitle));

        optionList.add(new Option(headers.get(0), true));

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuComprasOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToSupplierList));

        options = Arrays.asList(getResources().getStringArray(R.array.menuComprasOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToPurchasesMenu));

        /*
        options = Arrays.asList(getResources().getStringArray(R.array.menuComprasOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));
         */

        optionList.add(new Option(headers.get(1), true));

        options = Arrays.asList(getResources().getStringArray(R.array.menuInventarioOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToInventoryRules));


        options = Arrays.asList(getResources().getStringArray(R.array.menuInventarioOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::pickQueryInventory));


        options = Arrays.asList(getResources().getStringArray(R.array.menuInventarioOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToMovements));

        return optionList;
    }

    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }

    private void navigateToSupplierList(){
        startActivity(new Intent(this, SuppliersActivity.class));
    }

    private void navigateToPurchasesMenu(){
        startActivity(new Intent(this, PurchasesMenuActivity.class));
    }

    private void navigateToInventoryRules(){
        startActivityForResult(new Intent(this, PickBrandActivity.class)
                .putExtra("inventoryRules", true),0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, InventoryRulesActivity.class)
                    .putExtra("product", product)
            );
        }

        if (requestCode == 1  && resultCode == RESULT_OK){
            //  Add product to list
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, ProductInventoryActivity.class)
                .putExtra("product", product));
        }
    }

    private void pickQueryInventory(){
        new MaterialDialog.Builder(this)
                .title("Consulta de inventario")
                .items(R.array.items_query_inventory)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    if (which == 0) {
                        startActivityForResult(new Intent(this, PickBrandActivity.class)
                                .putExtra("pickProductInventory", true)
                                .putExtra("shoppingOrder", true),1);
                    } else {
                        startActivity(new Intent(this, SelectStdClassActivity.class));
                    }
                    return false;
                })
                .show();
    }

    private void navigateToMovements(){
        startActivity(new Intent(this, MenuMovesInventoryActivity.class));
    }

}
