package mx.com.vndr.vndrapp.cobros.movements;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.tabs.TabLayout;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.TicketMovementActivity;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.api.cxc.Movement;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static android.view.View.GONE;

public class MovementsActivity extends AppCompatActivity implements APIVndrCxc.MovementResponse, VndrDatePicker.OnSelectedDate, TabLayout.BaseOnTabSelectedListener, MovementsAdapter.OnMoveSelected {

    RecyclerView recyclerView;
    Toolbar toolbar;
    TabLayout tabLayout;
    TextView textViewDateInit,
             textViewDateEnd,
             textViewError,
             textViewSumCargos,
             textViewSumAbonos;
    ProgressBar progressBar;
    boolean isRequestminDate;
    Date minDate, maxDate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movements);
        setupUI();

        //  Sett max and min Date of current month
        setCurrentDate();

        MovementType.Query.current.setSelected(MovementType.Query.ALL);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerView.setAdapter(null);
        textViewSumAbonos.setText("$0.00");
        textViewSumCargos.setText("$0.00");
        getMovements();
    }

    private void getMovements(){
        showProgress();

        new APIVndrCxc(Volley.newRequestQueue(this))
                .getMovemnts(
                        this,
                        VndrDateFormat.dateToWSFormat(minDate),
                        VndrDateFormat.dateToWSFormat(maxDate)
                );
    }

    //  Callbak and override methods

    @Override
    public void movementError(String messageError) {
        showError(messageError);
    }

    @Override
    public void movementList(List<CxCMovement> movements) {
        dismissProgress();
        MovementsAdapter adapter = new MovementsAdapter(movements);
        adapter.setOnMoveSelected(this);
        recyclerView.setAdapter(adapter);
        textViewSumCargos.setText(Movement.current.getCurrencySumCargos());
        textViewSumAbonos.setText(Movement.current.getCurrencySumAbonos());
    }

    @Override
    public void selectedDate(Date date) {
        if (isRequestminDate){
            minDate = date;
            textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
            checkValidDateRange();
        }
        else{
            maxDate = date;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
        getMovements();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        if (tab.getPosition() == 1)
            MovementType.Query.current.setSelected(MovementType.Query.A);
        else if (tab.getPosition() == 2)
            MovementType.Query.current.setSelected(MovementType.Query.C);
        else
            MovementType.Query.current.setSelected(MovementType.Query.ALL);

        getMovements();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        //  Clear layout on tab unselected
        recyclerView.setAdapter(null);
        textViewSumAbonos.setText("$0.00");
        textViewSumCargos.setText("$0.00");
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        //  Por ahora no hacer nada
    }

    @Override
    public void moveSelected(CxCMovement movement) {
        navigateToMovementTicket(movement);
    }

    // Logic methods

    private void setCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date());

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);

        maxDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH, 1); //Set first day of month

        minDate = calendar.getTime();

        //  Display on UI
        textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
        textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
    }

    private void checkValidDateRange(){
        //  Previene que la fecha inicial sea mayor a la fecha final
        //  En dado caso fecha final se pone igual a la inicial
        if (minDate.compareTo(maxDate) > 0){
            maxDate = minDate;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
    }

    //  UI Methods
    private void setupUI(){
        toolbar = findViewById(R.id.tb_moves);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        tabLayout = findViewById(R.id.tabl_moves);
        tabLayout.addOnTabSelectedListener(this);

        recyclerView = findViewById(R.id.rv_moves);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewDateInit = findViewById(R.id.txt_moves_date_init);
        textViewDateInit.setOnClickListener(view -> {
            isRequestminDate = true;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewDateEnd = findViewById(R.id.txt_moves_date_end);
        textViewDateEnd.setOnClickListener(view -> {
            isRequestminDate = false;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(maxDate)
                    .setMinDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewError = findViewById(R.id.txt_moves_error);
        textViewError.setVisibility(GONE);

        progressBar = findViewById(R.id.pb_moves);
        progressBar.setVisibility(GONE);

        textViewSumAbonos = findViewById(R.id.txt_abono);
        textViewSumCargos = findViewById(R.id.txt_cargo);
    }

    private void showProgress(){
        dismissError();
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissProgress();
        recyclerView.setAdapter(null);
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
        textViewSumAbonos.setText("$0.00");
        textViewSumCargos.setText("$0.00");
    }

    private void dismissError(){
        textViewError.setVisibility(GONE);
    }

    private void navigateToMovementTicket(CxCMovement movement){
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
    }
}
