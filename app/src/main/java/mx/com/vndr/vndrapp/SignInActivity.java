package mx.com.vndr.vndrapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.textfield.TextInputLayout;

import mx.com.vndr.vndrapp.account.AccountRecoveryActivity;
import mx.com.vndr.vndrapp.account.PasswordRecoveryActivity;
import mx.com.vndr.vndrapp.account.ValidateEmailActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrMembership;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.membership.ProfesionalMembershipActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

import static android.view.View.GONE;


public class SignInActivity extends AppCompatActivity implements VndrAuth.LoginResult {

    TextInputLayout inputLayout, inputEmail;
    EditText editText, emailText;
    Button button;
    Button buttonRecoverPwd;
    ProgressBar progressBar;
    TextView textViewName;
    VndrAuth vndrAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        setUpUI();
        vndrAuth = VndrAuth.getInstance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!AppState.getAppState(this).equals(AppState.NEW) &&
                !VndrUser.getEmail(this).isEmpty() &&
                !VndrUser.getName(this).isEmpty()) {

            inputEmail.setVisibility(GONE);
            textViewName.setVisibility(View.VISIBLE);
            textViewName.setText(VndrUser.getName(this));
            emailText.setText(VndrUser.getEmail(this));
        } else {
            inputEmail.setVisibility(View.VISIBLE);
            textViewName.setVisibility(GONE);
        }
    }

    public void onSignIn(View view){
        loading(true);
        String email = emailText.getText().toString();
        String pwd = editText.getText().toString();

        if (email.isEmpty() || pwd.isEmpty()){
            loading(false);
            AppState.setAppState(this, AppState.NEW);
            VndrUser.setEmail("", this);
            VndrUser.setName("", this);
            onResume();
            Dialogs.showAlert(this, "El email o la contraseña no son válidos, por favor inténtalo de nuevo. Asegúrate que ambos sean correctos.");
        } else {
            vndrAuth.login(email, pwd,this);
        }
    }

    public void onPasswordRecoveryClick(View view){
        startActivity(new Intent(this, PasswordRecoveryActivity.class));
    }


    private void loading(boolean show){
        if (show){
            progressBar.setVisibility(View.VISIBLE);
            button.setVisibility(GONE);
            button.setEnabled(false);
            buttonRecoverPwd.setEnabled(false);
        } else{
            progressBar.setVisibility(GONE);
            button.setVisibility(View.VISIBLE);
            button.setEnabled(true);
            buttonRecoverPwd.setEnabled(true);
            //  borrar pwdq
            editText.setText("");
            inputLayout.setError("");
            emailText.setText("");
            inputEmail.setError("");
        }
    }

    private void setUpUI(){
        inputLayout = findViewById(R.id.input_pwd_signin);
        editText = findViewById(R.id.pwd_signin);
        button = findViewById(R.id.btn_signin);
        progressBar = findViewById(R.id.pb_signin);
        emailText = findViewById(R.id.email);
        inputEmail = findViewById(R.id.input_email);
        textViewName = findViewById(R.id.txt_saludo);
        buttonRecoverPwd = findViewById(R.id.btn_recover_pdw);

        button.setEnabled(false);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0){
                    inputLayout.setError("Debes ingresar tu contraseña.");
                    button.setEnabled(false);
                } else{
                    inputLayout.setError("");
                    button.setEnabled(true);
                }

                if (emailText.getText().toString().isEmpty()){
                    button.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        emailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 0){
                    inputEmail.setError("Debes ingresar tu correo.");
                    button.setEnabled(false);
                } else{
                    inputEmail.setError("");
                    button.setEnabled(true);
                }

                if (editText.getText().toString().isEmpty()){
                    button.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        editText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_DONE && !editText.getText().toString().isEmpty()){
                onSignIn(textView);
            } else if (i == EditorInfo.IME_ACTION_DONE && editText.getText().toString().isEmpty()){
                inputLayout.setError("Debes ingresar tu contraseña.");
                button.setEnabled(false);
            }
            return false;
        });

    }


    @Override
    public void loginSuccess() {
        loading(false);
        AppEventsLogger.newLogger(this).logEvent("loginSuccess");
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void loginError(String message) {
        loading(false);
        Dialogs.showAlert(this, message);
        onResume();
    }

    @Override
    public void loginLicenseError(String message, VndrMembership.LicenseVndrStatus status) {

        loading(false);

        //  LC04, LC05, LC99. Invitar a comprar: niega acceso.

        if (
                status.equals(VndrMembership.LicenseVndrStatus.LC04) ||
                status.equals(VndrMembership.LicenseVndrStatus.LC05) ||
                status.equals(VndrMembership.LicenseVndrStatus.LC99)
        ) {
            Log.i("TAG", "Acceso denegado. \n License type: " + VndrMembership.status.toString());
            //  Lanzar pantalla de compra
            Intent i = new Intent(this, ProfesionalMembershipActivity.class);
            i.putExtra("buyAndCloseAction", true);
            startActivity(i);
            return;
        }

        //  LC07: Invitar a pagar suscripción: niega acceso.
        Log.i("TAG", "Acceso denegado. \n License type: " + VndrMembership.status.toString());

        Dialogs.showCustomAlert(this)
                .content(message)
                .positiveText("Administrar suscripción")
                .onPositive((dialog, which) -> {
                    String url = String.format(
                            "https://play.google.com/store/account/subscriptions?sku=%s&package=%s",
                            BuildConfig.SKU_PROFESSIONAL_LICENSE,
                            BuildConfig.APPLICATION_ID
                    );
                    System.out.println(url);

                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);
                })
                .negativeText("Cancelar")
                .cancelable(false)
                .show();
    }

    @Override
    public void loginBlocked() {
        loading(false);
        startActivity(new Intent(this, AccountRecoveryActivity.class));
    }

    @Override
    public void emailNotVerified() {
        loading(false);
        startActivity(new Intent(this, ValidateEmailActivity.class));
    }
}
