package mx.com.vndr.vndrapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.PaymentFrequencyActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.datepPicker.DatePickerFragment;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDERS;

public class DeliveryActivity extends AppCompatActivity {

    //TextView select_date, deliveryDateTxt;
    Toolbar toolbar;
    RadioButton personal, delivery;
    //ImageButton imageButton;
    CardView cardViewForm;
    Date selectedDeliveryDate, selectShippingDate;
    Button button;
    EditText shippingDate, service, trackCode, deliveryDate, shippingAmount;
    EditText edtxt_shipping_date;
    TextInputLayout delivery_date;
    Boolean isPersonalChecked = false;
    Boolean isDeliveryChecked = false;
    Order order;
    ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery);

        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
        }
        setupUI();
    }


    public void onClickPickDate() {

        if (selectShippingDate != null){
            new DatePickerFragment(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    Calendar c = Calendar.getInstance();
                    c.set(i, i1, i2);
                    Date d = c.getTime();
                    selectedDeliveryDate = d;
                    SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                    edtxt_shipping_date.setText(format.format(d));
                    deliveryDate.setText(format.format(d));

                }
            },selectShippingDate.getTime(), order.getSaleDate()).show(getSupportFragmentManager(), "datePicker");
        } else{
            new DatePickerFragment(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    Calendar c = Calendar.getInstance();
                    c.set(i, i1, i2);
                    Date d = c.getTime();
                    selectedDeliveryDate = d;
                    SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                    edtxt_shipping_date.setText(format.format(d));
                    deliveryDate.setText(format.format(d));

                }
            },order.getSaleDate().getTime(),order.getSaleDate()).show(getSupportFragmentManager(), "datePicker");
        }

    }


    public void showUI(Boolean personal, Boolean delivery){

        if (personal){
            //imageButton.setVisibility(View.VISIBLE);
            //select_date.setVisibility(View.VISIBLE);
            //deliveryDateTxt.setVisibility(View.VISIBLE);
            delivery_date.setVisibility(View.VISIBLE);
            cardViewForm.setVisibility(View.GONE);

            // Clear
            deliveryDate.setText("");
            selectedDeliveryDate = null;
            shippingDate.setText("");
            selectShippingDate = null;
            service.setText("");
            trackCode.setText("");
            shippingDate.setText("");

        }

        if (delivery){
            //imageButton.setVisibility(View.GONE);
            //select_date.setVisibility(View.GONE);
            //deliveryDateTxt.setVisibility(View.GONE);
            delivery_date.setVisibility(View.GONE);
            cardViewForm.setVisibility(View.VISIBLE);

            //  Clear last
            edtxt_shipping_date.setText("");
            selectedDeliveryDate = null;
        }
    }

    public void onClickDeliveryButton(View view){
        showProgress(true);
        updateOrder();
    }


    private void updateOrder(){
        StringRequest request = new StringRequest(Request.Method.PUT, URL_ORDERS  + "/" + order.getIdOrder(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                showProgress(false);
                System.out.println(response);

                Delivery delivery = new Delivery();

                if (isPersonalChecked){
                    delivery.setType("personal");
                    if (!edtxt_shipping_date.getText().toString().isEmpty())
                        delivery.setEstimateDeliveryDate(selectedDeliveryDate);
                } else if (isDeliveryChecked){
                    delivery.setType("shipping");

                    if (!shippingDate.getText().toString().isEmpty())
                        delivery.setEstimateShippingDate(selectShippingDate);
                    if (!service.getText().toString().isEmpty())
                        delivery.setMessengerService(service.getText().toString());
                    if (!trackCode.getText().toString().isEmpty())
                        delivery.setTrackingCode(trackCode.getText().toString());
                    if (!deliveryDate.getText().toString().isEmpty())
                        delivery.setEstimateDeliveryDate(selectedDeliveryDate);

                }

                order.setDelivery(delivery);

                startActivityForResult(new Intent(DeliveryActivity.this, PaymentFrequencyActivity.class)
                                .putExtra("order",order)
                        ,1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                Toast.makeText(DeliveryActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return createBody();
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private HashMap<String, String> createBody(){

        HashMap<String, String> map = new HashMap<>();


        JSONObject delivery = new JSONObject();

        try {

            if (selectedDeliveryDate != null)
                delivery.put("estimate_delivery_date",selectedDeliveryDate.getTime());

            if (isPersonalChecked){
                delivery.put("delivery_type","personal");

            } else if (isDeliveryChecked){
                delivery.put("delivery_type","shipping");

                if (selectShippingDate != null)
                    delivery.put("estimate_shipping_date",selectShippingDate.getTime());
                if (!service.getText().toString().isEmpty())
                    delivery.put("messenger_service",service.getText());
                if (!trackCode.getText().toString().isEmpty())
                    delivery.put("traking_code",trackCode.getText());
            }

            map.put("sale_date", String.valueOf(order.getSaleDate().getTime()));
            map.put("delivery", delivery.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return map;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("TAG",String.valueOf(resultCode));
        if (requestCode == 1 && resultCode != RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }

    private void setupUI(){

        //select_date = findViewById(R.id.select_date_picker);
        personal = findViewById(R.id.rb_personal);
        delivery = findViewById(R.id.rb_mensajeria);
        //deliveryDateTxt = findViewById(R.id.txt_entrega);
        //imageButton = findViewById(R.id.ib_date_picker);
        edtxt_shipping_date = findViewById(R.id.edtxt_shipping_date);
        cardViewForm = findViewById(R.id.cv_form_delivery);
        button = findViewById(R.id.btn_delivery);
        shippingDate = findViewById(R.id.editText_shipping_date);
        delivery_date = findViewById(R.id.ip_date);
        service = findViewById(R.id.service_delivery);
        trackCode = findViewById(R.id.track_code);
        deliveryDate = findViewById(R.id.edtxt_delivery_date);
        toolbar = findViewById(R.id.tb_delivery);
        shippingAmount = findViewById(R.id.shipping_order);
        progressBar = findViewById(R.id.pb_delivery);

        progressBar.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        deliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickPickDate();
            }
        });

        shippingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerFragment(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        Calendar c = Calendar.getInstance();
                        c.set(i, i1, i2);
                        Date d = c.getTime();


                        selectShippingDate = d;
                        SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                        shippingDate.setText(format.format(d));
                    }
                },order.getSaleDate().getTime(),order.getSaleDate()).show(getSupportFragmentManager(),"datePicker");
            }
        });

        edtxt_shipping_date.setOnClickListener(view -> {
            onClickPickDate();
        });

        delivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (isPersonalChecked)
                    personal.setChecked(false);
                if (!isDeliveryChecked)
                    delivery.setChecked(true);

                isPersonalChecked = false;
                isDeliveryChecked = true;

                showUI(isPersonalChecked, isDeliveryChecked);

            }
        });

        personal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (isDeliveryChecked)
                    delivery.setChecked(false);
                if (!isPersonalChecked)
                    personal.setChecked(true);

                isPersonalChecked = true;
                isDeliveryChecked = false;
                showUI(isPersonalChecked, isDeliveryChecked);

            }
        });


        shippingAmount.setEnabled(false);

        shippingAmount.setText(NumberFormat.getCurrencyInstance().format(order.getShippingAmount()));


        /*
            Modificar lo de abajo para cuando ya trae datos.
         */

        if (order.getDelivery() != null){
            Delivery d = order.getDelivery();

            if (d.isPersonalDelivery()){
                personal.setChecked(true);
                if (d.getEstimateDeliveryDate() != null){
                    edtxt_shipping_date.setText(d.getEstimateDeliveryDateFormatt());
                    selectedDeliveryDate = d.getEstimateDeliveryDate();
                }

            } else {
                delivery.setChecked(true);

                if (d.getEstimateShippingDate() != null){
                    shippingDate.setText(d.getEstimateShippingDateFormatt());
                    selectShippingDate = d.getEstimateShippingDate();
                }
                if (d.getMessengerService() != null)
                    service.setText(d.getMessengerService());
                if (d.getTrackingCode() != null)
                    trackCode.setText(d.getTrackingCode());
                if (d.getEstimateDeliveryDate() != null){
                    deliveryDate.setText(d.getEstimateDeliveryDateFormatt());
                    selectedDeliveryDate = d.getEstimateDeliveryDate();
                }
            }
        } else personal.setChecked(true);




    }

    private void showProgress(Boolean show){
        if (show){
            progressBar.setVisibility(View.VISIBLE);
            button.setVisibility(View.GONE);
        } else {
            progressBar.setVisibility(View.GONE);
            button.setVisibility(View.VISIBLE);
        }
    }
}
