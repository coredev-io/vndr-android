package mx.com.vndr.vndrapp.cobros.movements;

import java.text.NumberFormat;
import java.util.Date;

import mx.com.vndr.vndrapp.api.cxc.CxCMovement;

public enum NewMovement {
    me();

    private CxCMovement movement;
    private double saldoActual;
    private String customerName;
    private String orderId;
    private double exigible;
    private Date saleDate;
    private double maxMovementAmount;
    private double pmtAmount = 0;
    private double unpaidAmountDue = 0;
    private boolean isUniquePaymentCxc = false;

    NewMovement() {
        this.movement = new CxCMovement();
    }

    public CxCMovement getMovement() {
        return movement;
    }

    public void setMovement(CxCMovement movement) {
        this.movement = movement;
    }

    public double getSaldoActual() {
        return saldoActual;
    }

    public void setSaldoActual(double saldoActual) {
        this.saldoActual = saldoActual;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getExigible() {
        return exigible;
    }

    public void setExigible(double exigible) {
        this.exigible = exigible;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public double getMaxMovementAmount() {
        return maxMovementAmount;
    }

    public void setMaxMovementAmount(double maxMovementAmount) {
        this.maxMovementAmount = maxMovementAmount;
    }

    public void setPmtAmount(double pmtAmount) {
        this.pmtAmount = pmtAmount;
    }

    public void setUnpaidAmountDue(double unpaidAmountDue) {
        this.unpaidAmountDue = unpaidAmountDue;
    }

    public String getCurrencyPmtAmount(){
        return NumberFormat.getCurrencyInstance().format(pmtAmount);
    }

    public String getCurrencyUnpaidAmountDue(){
        return NumberFormat.getCurrencyInstance().format(unpaidAmountDue);
    }

    public boolean isUniquePaymentCxc() {
        return isUniquePaymentCxc;
    }

    public void setUniquePaymentCxc(boolean uniquePaymentCxc) {
        isUniquePaymentCxc = uniquePaymentCxc;
    }
}
