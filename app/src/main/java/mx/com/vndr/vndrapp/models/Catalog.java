package mx.com.vndr.vndrapp.models;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Catalog implements Serializable {
    String id,catalogName;
    String key;
    boolean isValid, isCollaborative, isPublic, itsMine;
    boolean isExpired;
    boolean ownerIsVndr = false;
    Date initDate, endDate;
    String idBrand;



    public Catalog() {
    }

    public Catalog(String id, String catalogName) {
        this.id = id;
        this.catalogName = catalogName;
    }

    public Catalog(String id, String catalogName, String key, boolean isValid) {
        this.id = id;
        this.catalogName = catalogName;
        this.key = key;
        this.isValid = isValid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isValid() {
        return isValid;
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }

    public boolean isCollaborative() {
        return isCollaborative;
    }

    public void setCollaborative(boolean collaborative) {
        isCollaborative = collaborative;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getIdBrand() {
        return idBrand;
    }

    public void setIdBrand(String idBrand) {
        this.idBrand = idBrand;
    }

    public boolean isExpired() {
        return isExpired;
    }

    public void setExpired(boolean expired) {
        isExpired = expired;
    }

    public boolean isOwnerIsVndr() {
        return ownerIsVndr;
    }

    public void setOwnerIsVndr(boolean ownerIsVndr) {
        this.ownerIsVndr = ownerIsVndr;
    }

    public boolean isItsMine() {
        return itsMine;
    }

    public void setItsMine(boolean itsMine) {
        this.itsMine = itsMine;
    }

    public void setInitDate(String initDate){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date d = null;
        try
        {
            d = input.parse(initDate);
            this.initDate = d;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            this.initDate = new Date();
        }
    }

    public void setEndDate(String endDate){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date d = null;
        try
        {
            d = input.parse(endDate);
            this.endDate = d;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            this.endDate = new Date();
        }
    }

    public String getStringInitDate(){
        return new SimpleDateFormat("dd/MMM/yyyy").format(initDate);
    }


    public String getStringEndDate(){
        return new SimpleDateFormat("dd/MMM/yyyy").format(endDate);
    }
}
