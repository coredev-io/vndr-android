package mx.com.vndr.vndrapp.customviews.checkItemList;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mx.com.vndr.vndrapp.R;

public class CheckedItemAdapter extends RecyclerView.Adapter<CheckedItemViewHolder> {


    CheckedItemPresenter presenter;

    public CheckedItemAdapter(CheckedItemPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CheckedItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_checked_list,parent,false)
        );
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }

    @Override
    public void onBindViewHolder(@NonNull mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemViewHolder holder, int position) {
        presenter.onBindCheckedItemRowViewAtPosition(holder,position);
    }
}
