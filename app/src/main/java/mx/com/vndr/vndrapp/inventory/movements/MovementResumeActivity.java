package mx.com.vndr.vndrapp.inventory.movements;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_INVENTORY;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_TICKET;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.InventoryMovements;
import mx.com.vndr.vndrapp.api.inventory.InventoryRules;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListType;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;
import mx.com.vndr.vndrapp.inventory.rules.InventoryRulesActivity;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class MovementResumeActivity extends AppCompatActivity implements VndrDatePicker.OnSelectedDate, InventoryMoveAdapter.OnClickCardListener {

    Toolbar toolbar;
    TextView brandName, catalog, productName, sku;
    TextView size, color, descripcion;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    TextView textViewDateInit,
            textViewDateEnd;
    TextView textViewError;

    boolean isRequestminDate;
    boolean isQueryByTicket = false;
    Date minDate, maxDate;
    Product product;

    List<VndrList> vndrLists = new ArrayList<>();
    List<InventoryMovements> inventoryMovementsList;
    InventoryMoveAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movement_resume);
        product = (Product) getIntent().getSerializableExtra("product");
        isQueryByTicket = getIntent().getBooleanExtra("isQueryByTicket", false);
        setUpUI();
        setCurrentDate();
    }


    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    private void setUpUI(){
        brandName = findViewById(R.id.txt_brand_name_rules);
        productName = findViewById(R.id.product_name_rules);
        catalog = findViewById(R.id.txt_catalog_name_rules);
        sku = findViewById(R.id.sku_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        progressBar = findViewById(R.id.pb_inventory_rules);
        textViewError = findViewById(R.id.txt_error);

        brandName.setText(product.getBrandName());
        catalog.setText(product.getCatalog().getCatalogName());
        productName.setText(product.getProductName());
        sku.setText(product.getProduct_key());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_inventory_rules);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_moves);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewDateInit = findViewById(R.id.txt_moves_date_init);
        textViewDateInit.setOnClickListener(view -> {
            isRequestminDate = true;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(minDate)
                    .show(getSupportFragmentManager());
        });

        textViewDateEnd = findViewById(R.id.txt_moves_date_end);
        textViewDateEnd.setOnClickListener(view -> {
            isRequestminDate = false;
            new VndrDatePicker()
                    .setOnSelectedDateListener(this)
                    .setcurrentDate(maxDate)
                    .setMinDate(minDate)
                    .show(getSupportFragmentManager());
        });
        progressBar = findViewById(R.id.pb_inventory_rules);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        textViewError.setVisibility(View.GONE);
        recyclerView.setAdapter(null);
    }

    private void dismisLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void getData(){
        showLoading();

        String url = URL_TICKET + "?product=" + product.getIdProduct() + "&initial_date=" + VndrDateFormat.dateToWSFormat(minDate) + "&final_date=" + VndrDateFormat.dateToWSFormat(maxDate);
        new VndrRequest(Volley.newRequestQueue(this))
                .get( url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismisLoading();
                        Dialogs.showAlert(MovementResumeActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismisLoading();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject resumeObject = jsonObject.getJSONObject("resume");

                            if (isQueryByTicket){
                                JSONArray jsonArray = jsonObject.getJSONArray("results");
                                inventoryMovementsList = new ArrayList<>();
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    InventoryMovements inventoryMovement = new InventoryMovements(jsonArray.getJSONObject(i), true);
                                    inventoryMovementsList.add(inventoryMovement);
                                }

                                if (inventoryMovementsList.isEmpty()) {
                                    recyclerView.setAdapter(null);
                                    textViewError.setText("No se encontraron datos en el periodo seleccionado.");
                                    textViewError.setVisibility(View.VISIBLE);
                                } else {
                                    adapter = new InventoryMoveAdapter(inventoryMovementsList, MovementResumeActivity.this);
                                    adapter.setOnClickCardListener(MovementResumeActivity.this);
                                    recyclerView.setAdapter(adapter);
                                }
                                return;
                            }


                            if (!resumeObject.keys().hasNext()){
                                recyclerView.setAdapter(null);
                                textViewError.setText("No se encontraron datos en el periodo seleccionado.");
                                textViewError.setVisibility(View.VISIBLE);
                            }

                            vndrLists = new ArrayList<>();

//                            vndrLists.add(new VndrList(
//                                    new SimpleList2(
//                                            "Existencia inicial",
//                                            resumeObject.getJSONObject("movements").getString("initial_stock")
//                                    ).setBGColor(R.color.colorPrimary)
//                            ));


                            JSONArray incomingArray = resumeObject.getJSONObject("movements").getJSONArray("incoming");

                            for (int i = 0; i < incomingArray.length(); i++) {
                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                incomingArray.getJSONObject(i).getString("description"),
                                                String.valueOf(incomingArray.getJSONObject(i).getInt("quantity"))
                                        )
                                ));
                            }

                            vndrLists.add(new VndrList(
                                    new SimpleList2(
                                            "Total Entradas",
                                            String.valueOf(resumeObject.getJSONObject("movements").getDouble("total_incoming"))
                                    ).setBGColor(R.color.colorPrimary)
                            ));

                            JSONArray outgoingArray = resumeObject.getJSONObject("movements").getJSONArray("outgoing");

                            for (int i = 0; i < outgoingArray.length(); i++) {
                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                outgoingArray.getJSONObject(i).getString("description"),
                                                String.valueOf(outgoingArray.getJSONObject(i).getInt("quantity"))
                                        )
                                ));
                            }

                            vndrLists.add(new VndrList(
                                    new SimpleList2(
                                            "Total Salidas",
                                            String.valueOf(resumeObject.getJSONObject("movements").getDouble("total_outgoing"))
                                    ).setBGColor(R.color.colorPrimary)
                            ));

                            vndrLists.add(new VndrList(
                                    new SimpleList2(
                                            "Movimiento neto",
                                            String.valueOf(resumeObject.getJSONObject("movements").getInt("final_stock"))
                                    ).setBGColor(R.color.colorPrimary)
                            ));



                            recyclerView.setAdapter(new VndrListAdapter(vndrLists));

                            fillData();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Dialogs.showAlert(MovementResumeActivity.this, "Ocurrió un problema al procesar la solicitud");
                        }
                    }
                });
    }

    private void fillData(){

    }

    @Override
    public void selectedDate(Date date) {
        if (isRequestminDate){
            minDate = date;
            textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
            checkValidDateRange();
        }
        else{
            maxDate = date;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
        getData();
    }

    // Logic methods

    private void setCurrentDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( new Date());

        int lastDayOfMonth = calendar.getActualMaximum(Calendar.DATE);

        calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);

        maxDate = calendar.getTime();

        calendar.set(Calendar.DAY_OF_MONTH, 1); //Set first day of month

        minDate = calendar.getTime();

        //  Display on UI
        textViewDateInit.setText(VndrDateFormat.dateToString(minDate));
        textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
    }

    private void checkValidDateRange(){
        //  Previene que la fecha inicial sea mayor a la fecha final
        //  En dado caso fecha final se pone igual a la inicial
        if (minDate.compareTo(maxDate) > 0){
            maxDate = minDate;
            textViewDateEnd.setText(VndrDateFormat.dateToString(maxDate));
        }
    }

    @Override
    public void onClickCard(InventoryMovements movement) {
        navigateToTicketDetail(movement);
    }

    private void navigateToTicketDetail(InventoryMovements movement){
        startActivity(new Intent(this, MovementInventoryTicketActivity.class)
                .putExtra("move", movement));
    }
}