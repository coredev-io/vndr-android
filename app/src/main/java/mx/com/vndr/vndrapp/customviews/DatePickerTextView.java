package mx.com.vndr.vndrapp.customviews;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.fragment.app.FragmentManager;

import java.text.DateFormat;
import java.util.Date;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.customviews.datepPicker.DatePickerFragment;
import mx.com.vndr.vndrapp.customviews.datepPicker.VndrDatePicker;

public class DatePickerTextView extends AppCompatAutoCompleteTextView implements VndrDatePicker.OnSelectedDate {

    private FragmentManager fragmentManager;
    private VndrDatePicker vndrDatePicker;

    public DatePickerTextView(Context context) {
        super(context);
        customComponentView();
    }

    public DatePickerTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        customComponentView();
    }

    public DatePickerTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        customComponentView();
    }

    private void customComponentView(){
        setInputType(InputType.TYPE_NULL);
        vndrDatePicker = new VndrDatePicker();
        vndrDatePicker.setOnSelectedDateListener(this);
    }

    public VndrDatePicker getVndrDatePicker() {
        return vndrDatePicker;
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP){
            hideKeyboard();
            if (fragmentManager != null)
                vndrDatePicker.show(fragmentManager);
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void selectedDate(Date date) {
        setText(DateFormat.getDateInstance().format(date));
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindowToken(), 0);
    }


}
