package mx.com.vndr.vndrapp.api.cxc;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class CxCCard {
    private String id;
    private CxCStatus status;
    private String statusDesc;
    private String customerName;
    private int orderNumber;
    private Date billingDate;
    private Date dueDate;
    private double totalAmount;
    private double balanceOverdue;
    private double balanceDue;
    private double pmtAmount;


    public CxCCard(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public CxCStatus getStatus() {
        return status;
    }

    public void setStatus(String status) {

        this.statusDesc = status;
        if (status.equals("Al corriente"))
            this.status = CxCStatus.AL_CORRIENTE;
        else if (status.equals("Retraso 1"))
            this.status = CxCStatus.RETRASO1;
        else if (status.equals("Retraso 2"))
            this.status = CxCStatus.RETRASO2;
        else if (status.equals("Retraso 3"))
            this.status = CxCStatus.RETRASO3;
        else if (status.equals("Retraso grave"))
            this.status = CxCStatus.RETRASO_GRAVE;
        else if (status.equals("Saldo a favor"))
            this.status = CxCStatus.SALDO_FAVOR;
        else if (status.equals("Liquidada"))
            this.status = CxCStatus.LIQUIDADA;
        else if (status.equals("No cobrado"))
            this.status = CxCStatus.NO_COBRADO;
        else if (status.equals("Vencido"))
            this.status = CxCStatus.VENCIDO;
        else if (status.equals("Pagada"))
            this.status = CxCStatus.PAGADA;
        else if (status.equals("Quebranto"))
            this.status = CxCStatus.QUEBRANTO;
        else
            this.status = CxCStatus.POR_VENCER;

    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getBillingDate() {
        return billingDate;
    }

    public String getBillingFormattedDate() {
        return VndrDateFormat.dateToString(billingDate);
    }

    public void setBillingDate(String billingDate) throws ParseException {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        this.billingDate = format.parse(billingDate);
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public String getCurrencyTotalAmount() {
        return NumberFormat.getCurrencyInstance().format(totalAmount);
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public double getBalanceOverdue() {
        return balanceOverdue;
    }

    public String getCurrencyBalanceOverdue() {
        return NumberFormat.getCurrencyInstance().format(balanceOverdue);
    }

    public void setBalanceOverdue(double balanceOverdue) {
        this.balanceOverdue = balanceOverdue;
    }

    public double getBalanceDue() {
        return balanceDue;
    }

    public String getCurrencyBalanceDue() {
        return NumberFormat.getCurrencyInstance().format(balanceDue);
    }

    public void setBalanceDue(double balanceDue) {
        this.balanceDue = balanceDue;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public double getPmtAmount() {
        return pmtAmount;
    }

    public String getCurrencyPmtAmount(){
        return NumberFormat.getCurrencyInstance().format(pmtAmount);
    }

    public void setPmtAmount(double pmtAmount) {
        this.pmtAmount = pmtAmount;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public String getDueDateFormatted() {
        return VndrDateFormat.dateToString(dueDate);
    }

    public void setDueDate(String dueDate) {
        try {
            this.dueDate = VndrDateFormat.stringToDate(dueDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
