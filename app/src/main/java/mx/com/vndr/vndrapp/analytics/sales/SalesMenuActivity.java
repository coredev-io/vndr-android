package mx.com.vndr.vndrapp.analytics.sales;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.SelectBrandActivity;
import mx.com.vndr.vndrapp.brands.BrandListActivity;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.analytics.sales.SalesResumeActivity.Query.BY_BRAND;
import static mx.com.vndr.vndrapp.brands.BrandListActivity.BrandListType.OWN_BRANDS;

public class SalesMenuActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_menu);
        setupUI();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_sales_menu);
        recyclerView = findViewById(R.id.rv_sales_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuSalesOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToSalesResume));

        options = Arrays.asList(getResources().getStringArray(R.array.menuSalesOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::pickBrand));
        /*
        options = Arrays.asList(getResources().getStringArray(R.array.menuSalesOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::displayAlertUnavailable));
        */

        return optionList;
    }

    public void navigateToSalesResume(){
        startActivity(new Intent(this, SalesResumeActivity.class));
    }

    public void pickBrand(){
        startActivityForResult(new Intent(this, SelectBrandActivity.class),0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK){
            navigateToBrandSalesResume(Brand.Selected.current.getBrand().getBrandId());
        }
    }

    public void navigateToBrandSalesResume(String idBrand){
        startActivity(new Intent(this, SalesResumeActivity.class)
                .putExtra("query", BY_BRAND)
                .putExtra("brand", idBrand)
        );
    }

    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }
}