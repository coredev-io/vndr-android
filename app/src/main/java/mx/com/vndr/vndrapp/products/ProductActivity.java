package mx.com.vndr.vndrapp.products;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.api.VndrRequest;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PRODUCTS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_STANDARD_CLASSIFICATION;

public class ProductActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    Button button;
    EditText name,key, listPrice, size, descripcion, notes;
    TextInputLayout inputClassification;
    AutoCompleteTextView sizeAutoComplete, colorAutoComplete, classificationAutoComplete;
    String catalogId = "";
    Switch vigente;
    Product product;
    boolean updateMode;
    boolean readerMode = false;
    Map<String, String> classificationListId;
    List<String> classificationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        setUpUI();
        catalogId =  getIntent().getStringExtra("catalogId");

        getClassification();

        if (getIntent().hasExtra("product")){
            updateMode = true;
            readerMode = true;
            product = (Product) getIntent().getSerializableExtra("product");
            fillData();
            button.setText("Editar");
            setReaderMode(readerMode);
        }

        if (!readerMode){
            button.setEnabled(false);
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    private void setUpUI(){
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        button = findViewById(R.id.btn_product);
        button.setOnClickListener(this);

        name = findViewById(R.id.edtxt_product_name);
        key = findViewById(R.id.edtxt_product_key);
        size = findViewById(R.id.edtxt_size);
        vigente = findViewById(R.id.switch4);
        descripcion = findViewById(R.id.edtxt_product_desc);
        notes = findViewById(R.id.edtxt_product_notes);

        classificationAutoComplete = findViewById(R.id.filled_exposed_dropdown_class);
        listPrice = findViewById(R.id.edtxt_product_price);
        inputClassification = findViewById(R.id.input_classification);
        sizeAutoComplete = findViewById(R.id.filled_exposed_dropdown);
        colorAutoComplete = findViewById(R.id.filled_exposed_dropdown_color);

        colorAutoComplete.setAdapter(new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.colors)));
        sizeAutoComplete.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, getResources().getStringArray(R.array.size)));

        classificationAutoComplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP){
                    hideKeyboard();
                    if (classificationAutoComplete.isPopupShowing())
                        classificationAutoComplete.dismissDropDown();
                    else
                        classificationAutoComplete.showDropDown();
                }
                return false;
            }
        });
        colorAutoComplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP){
                    //hideKeyboard();
                    if (colorAutoComplete.isPopupShowing())
                        colorAutoComplete.dismissDropDown();
                    else
                        colorAutoComplete.showDropDown();
                }
                return false;
            }
        });
        sizeAutoComplete.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == KeyEvent.ACTION_UP){
                    hideKeyboard();
                    if (sizeAutoComplete.isPopupShowing())
                        sizeAutoComplete.dismissDropDown();
                    else
                        sizeAutoComplete.showDropDown();
                }
                return false;
            }
        });

        TextWatcher watcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!name.getText().toString().isEmpty()
                    && !key.getText().toString().isEmpty()
                    && !classificationAutoComplete.getText().toString().isEmpty()
                        && !listPrice.getText().toString().isEmpty()
                    && getCleanDoubleValue(listPrice.getText().toString()) != 0){
                    button.setEnabled(true);
                } else {
                    button.setEnabled(false);
                }
            }
        };
        name.addTextChangedListener(watcher);
        key.addTextChangedListener(watcher);
        listPrice.addTextChangedListener(watcher);
        listPrice.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
        classificationAutoComplete.addTextChangedListener(watcher);

    }

    private double getCleanDoubleValue(String value){
        if (value.isEmpty() || value.equals("."))
            return 0;
        else
            return Double.parseDouble(value);
    }

    @Override
    public void onClick(View view) {

        if (classificationList.isEmpty()){
            Toast.makeText(this, "No se pudieron obtener las clasificaciones de producto", Toast.LENGTH_LONG).show();
        } else{
            if (!updateMode)
                addProduct();
            else if (readerMode){
                readerMode = false;
                setReaderMode(readerMode);
                //button.setEnabled(false);
            } else {
                //updateProduct();
                askForChange();
            }
        }

    }

    public void addProduct(){
        Map<String, String> params = new HashMap<>();
        params.put("catalog",catalogId);
        params.put("product_key",key.getText().toString());
        params.put("product_name",name.getText().toString());
        params.put("list_price",listPrice.getText().toString());
        params.put("standard_classification_id", classificationListId.get(classificationAutoComplete.getText().toString()));
        params.put("height",sizeAutoComplete.getText().toString());
        params.put("color", colorAutoComplete.getText().toString());
        params.put("size",size.getText().toString());
        params.put("active",String.valueOf(vigente.isChecked()));
        params.put("description", descripcion.getText().toString());
        params.put("notes",notes.getText().toString());

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .post(URL_PRODUCTS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Toast.makeText(ProductActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void success(String response) {
                        Toast.makeText(ProductActivity.this, "Producto agregado con éxito", Toast.LENGTH_LONG).show();
                        ProductActivity.this.finish();
                    }
                });

    }

    public void updateProduct(){
        Map<String, String> params = new HashMap<>();
        params.put("product_key",key.getText().toString());
        params.put("product_name",name.getText().toString());
        params.put("list_price",listPrice.getText().toString());
        params.put("standard_classification_id", classificationListId.get(classificationAutoComplete.getText().toString()));
        params.put("height",sizeAutoComplete.getText().toString());
        params.put("color", colorAutoComplete.getText().toString());
        params.put("size",size.getText().toString());
        params.put("active",String.valueOf(vigente.isChecked()));
        params.put("description", descripcion.getText().toString());
        params.put("notes",notes.getText().toString());

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .patch(URL_PRODUCTS + "/" +product.getIdProduct() , new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Toast.makeText(ProductActivity.this, message, Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void success(String response) {
                        Toast.makeText(ProductActivity.this, "Producto actualizado con éxito", Toast.LENGTH_LONG).show();
                        ProductActivity.this.finish();
                    }
                });
    }

    public void getClassification(){
        classificationAutoComplete.setText("Cargando...");
        new VndrRequest(Volley.newRequestQueue(this))
                .get(URL_STANDARD_CLASSIFICATION, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Toast.makeText(ProductActivity.this, "Ocurrió un error al obtener las clasificaciones", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void success(String response) {
                        classificationListId = new HashMap<>();
                        classificationList = new ArrayList<>();
                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++){
                                JSONObject object = array.getJSONObject(i);

                                classificationListId.put(
                                        object.getString("standard_classification"),
                                        object.getString("_id"));
                                classificationList.add(
                                        object.getString("standard_classification")
                                );


                            }
                            if (product == null)
                                classificationAutoComplete.setText("");
                            else
                                classificationAutoComplete.setText(product.getStandard_classification(), false);

                            classificationAutoComplete.setAdapter(new ArrayAdapter<>(ProductActivity.this, R.layout.support_simple_spinner_dropdown_item, classificationList));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public void setReaderMode(boolean readerMode){
        if (readerMode){
            name.setEnabled(false);
            key.setEnabled(false);
            sizeAutoComplete.setEnabled(false);
            size.setEnabled(false);
            colorAutoComplete.setEnabled(false);
            descripcion.setEnabled(false);
            listPrice.setEnabled(false);
            classificationAutoComplete.setEnabled(false);
            notes.setEnabled(false);
            vigente.setEnabled(false);
        } else {
            name.setEnabled(true);
            key.setEnabled(true);
            sizeAutoComplete.setEnabled(true);
            size.setEnabled(true);
            colorAutoComplete.setEnabled(true);
            descripcion.setEnabled(true);
            listPrice.setEnabled(true);
            classificationAutoComplete.setEnabled(true);
            notes.setEnabled(true);
            vigente.setEnabled(true);
            button.setText("Actualizar");
        }

    }

    public void fillData(){
        name.setText(product.getProductName());
        key.setText(product.getProduct_key());
        size.setText(product.getSize());
        listPrice.setText(round2Decimals(product.getListPrice()));
        notes.setText(product.getNotes());

        classificationAutoComplete.setText(product.getStandard_classification(), false);
        colorAutoComplete.setText(product.getColor(), false);
        sizeAutoComplete.setText(product.getHeight(),false);

        vigente.setChecked(product.getDiscontinued());
        descripcion.setText(product.getDescription());

    }

    public  void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void askForChange(){
        Dialogs.showAlertQuestion(this, "Actualizar los datos de este producto aplica para todos los usuarios de la marca, ¿Deseas continuar?",
                new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        updateProduct();
                    }
                });
    }

    private String round2Decimals(Double doubleValue){
        return String.format(java.util.Locale.getDefault(),"%.2f", doubleValue);
    }

}
