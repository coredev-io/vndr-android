package mx.com.vndr.vndrapp.catalogs.associateBrand;

import mx.com.vndr.vndrapp.models.Brand;

public interface AssociateBrandDialogView {
    void showProgress();
    void dismissProgress();
    void dismissDialog();
    void showResponse(String response);
    void updateUIWithOriginalData();
}
