package mx.com.vndr.vndrapp.customviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

import mx.com.vndr.vndrapp.R;

public class RadioListAdapter extends RecyclerView.Adapter<RadioListAdapter.RadioListViewHolder> {

    private List<String> data;
    private OnSelectedItemListener onSelectedItemListener;

    public RadioListAdapter(List<String> data) {
        this.data = data;
    }

    public void setOnSelectedItemListener(OnSelectedItemListener onSelectedItemListener) {
        this.onSelectedItemListener = onSelectedItemListener;
    }

    @NonNull
    @Override
    public RadioListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RadioListViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.radio_cardview, parent, false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RadioListViewHolder holder, int position) {
        holder.radioButton.setText(data.get(position));
        holder.radioButton.setOnClickListener(view -> {
            if (onSelectedItemListener != null)
                onSelectedItemListener.onSelectedItem(position);
        });
    }


    @Override
    public int getItemCount() {
        return data.size();
    }

    class RadioListViewHolder extends RecyclerView.ViewHolder{

        RadioButton radioButton;
        public RadioListViewHolder(@NonNull View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.rb_cardview);
        }
    }

    public interface OnSelectedItemListener{
        void onSelectedItem(int itemPosition);
    }
}
