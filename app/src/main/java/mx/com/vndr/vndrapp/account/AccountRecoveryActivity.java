package mx.com.vndr.vndrapp.account;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputLayout;

import mx.com.vndr.vndrapp.MainActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.customviews.EditTextPhoneNumber;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrSupport;

public class AccountRecoveryActivity extends AppCompatActivity implements TextWatcher, VndrUser.VndrUserCallback {

    Toolbar toolbar;
    TextInputLayout textInputLayoutEmail;
    TextInputLayout textInputLayoutPhone;
    TextInputLayout textInputLayoutZipCode;
    EditText editTextEmail;
    EditTextPhoneNumber editTextPhone;
    EditText editTextZipCode;
    Button buttonChangeDevice;
    Button buttonAssistance;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_recovery);
        setupUI();

    }

    public void onClickChangeDevice(View view){
        loadingMode(true);
        VndrAuth.getInstance()
                .getCurrentUser()
                .changeDevice(editTextEmail.getText().toString(), editTextPhone.getRawPhoneNumber(), editTextZipCode.getText().toString(), this);
    }

    @Override
    public void onVndrUserCallbackSuccess() {
        loadingMode(false);
        startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }

    @Override
    public void onVndrUserCallbackError(String messageError) {
        loadingMode(false);
        Dialogs.showAlert(this, messageError);
    }

    public void onClickGetAssistance(View view){
        try {
            startActivity(VndrSupport.getEmailSupportIntent());
        } catch (ActivityNotFoundException e) {
            Dialogs.showAlert(
                    this,
                    getResources().getString(R.string.mailIntentMessageError)
            );
        }
    }

    private void loadingMode(boolean show){
        if (show){
            progressBar.setVisibility(View.VISIBLE);
            buttonChangeDevice.setVisibility(View.GONE);
            buttonAssistance.setEnabled(false);
            textInputLayoutEmail.setEnabled(false);
            textInputLayoutPhone.setEnabled(false);
            textInputLayoutZipCode.setEnabled(false);
        } else {
            progressBar.setVisibility(View.GONE);
            buttonChangeDevice.setVisibility(View.VISIBLE);
            buttonAssistance.setEnabled(true);
            textInputLayoutEmail.setEnabled(true);
            textInputLayoutPhone.setEnabled(true);
            textInputLayoutZipCode.setEnabled(true);
        }
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_acc_recovery);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        buttonAssistance = findViewById(R.id.btn_assistance);
        buttonChangeDevice = findViewById(R.id.btn_change_device);
        buttonChangeDevice.setEnabled(false);

        textInputLayoutEmail = findViewById(R.id.il_email_acc_recovery);
        textInputLayoutPhone = findViewById(R.id.il_phone_acc_recovery);
        textInputLayoutZipCode = findViewById(R.id.il_zipcode_acc_recovery);

        editTextEmail = findViewById(R.id.et_email_acc_recovery);
        editTextPhone = findViewById(R.id.et_phone_acc_recovery);
        editTextZipCode = findViewById(R.id.et_zipcode_acc_recovery);

        editTextEmail.addTextChangedListener(this);
        editTextPhone.addTextChangedListener(this);
        editTextZipCode.addTextChangedListener(this);

        progressBar = findViewById(R.id.pb_acc_recovery);
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if (editTextEmail.isFocused()){
            if (charSequence.toString().isEmpty()){
                textInputLayoutEmail.setError("Este campo es requerido");
            } else if (!charSequence.toString().contains("@")){
                textInputLayoutEmail.setError("Debes ingresar un email válido");
            } else {
                textInputLayoutEmail.setError("");
            }
        }

        if (editTextPhone.isFocused()) {
            if (charSequence.toString().isEmpty()){
                textInputLayoutPhone.setError("Este campo es requerido");
            } else {
                textInputLayoutPhone.setError("");
            }
        }

        if (editTextZipCode.isFocused()) {
            if (charSequence.toString().isEmpty()){
                textInputLayoutZipCode.setError("Este campo es requerido");
            } else {
                textInputLayoutZipCode.setError("");
            }
        }

        if (!fieldsAreEmpty() && emailIsValid())
            buttonChangeDevice.setEnabled(true);
        else
            buttonChangeDevice.setEnabled(false);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private boolean fieldsAreEmpty(){
        if (editTextEmail.getText().toString().isEmpty()){
            return true;
        } else if (editTextPhone.getRawPhoneNumber().isEmpty()) {
            return true;
        } else if (editTextZipCode.getText().toString().isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    private boolean emailIsValid(){
        if (editTextEmail.getText().toString().contains("@"))
            return true;
        else
            return false;
    }
}
