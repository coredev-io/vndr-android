package mx.com.vndr.vndrapp.brands;

import android.util.Log;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.api.VndrRequest;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_BRANDS_TO_EDIT_CATALOGS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_COLLABORATIVE_BRANDS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_OWN_BRANDS;

public class BrandInteractor {

    private RequestQueue queue;

    public BrandInteractor(RequestQueue queue) {
        this.queue = queue;
    }

    public void getOwnBrands(final BrandCallback callback){

        new VndrRequest(queue)
                .get(URL_OWN_BRANDS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.error(message);
                    }

                    @Override
                    public void success(String response) {
                        try {

                            List<Brand> ownBrands = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject object = jsonArray.getJSONObject(i);

                                Brand ownBrand = new Brand(
                                        object.getString("brand_name"),
                                        object.getString("_id")
                                );

                                ownBrand.setCompany(object.getString("company"));
                                ownBrand.setBusinessLine(object.getString("business_line"));
                                ownBrand.setWeb(object.getString("web"));
                                ownBrand.setFacebook(object.getString("facebook"));
                                ownBrand.setContact_name(object.getString("contact_name"));
                                ownBrand.setEmail(object.getString("email"));
                                ownBrand.setPhone(object.getString("phone"));
                                ownBrand.setAddress(object.getString("address"));
                                ownBrand.setPublic(object.getBoolean("is_public"));

                                if (object.has("private_brand")){
                                    ownBrand.setBrandCode(
                                            object
                                                    .getJSONObject("private_brand")
                                                    .getString("brand_code")
                                    );
                                }

                                ownBrands.add(ownBrand);
                            }

                            callback.success(ownBrands);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getInt("statusCode") == 404){
                                    callback.emptyBrandList();
                                }
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                callback.error("Ocurrió un error al procesar la solicitud.");
                            }
                        }
                    }
                });
    }

    public void getBrandsToEditCatalogs(final BrandCallback callback){
        new VndrRequest(queue)
                .get(URL_BRANDS_TO_EDIT_CATALOGS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.error(message);
                    }

                    @Override
                    public void success(String response) {
                        System.out.println(response);
                        try{
                            List<Brand> brandList = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response);

                            if (jsonArray.length() == 0 ){
                                callback.emptyBrandList();
                                return;
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);

                                Brand brand = new Brand(
                                        object.getString("brand_name"),
                                        object.getString("_id")
                                );

                                Log.e("TAG","=-=-==");
                                if (object.getString("brand_owner").equals("adminvndr")){
                                    brand.setOwnerVndr(true);
                                    System.out.println("Brand owner is vndr");
                                }
                                brandList.add(brand);
                            }
                            callback.success(brandList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getInt("statusCode") == 404){
                                    callback.emptyBrandList();
                                }
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                callback.error("Ocurrió un error al procesar la solicitud.");
                            }
                        }

                    }
                });
    }


    public void getCollaborativeBrands(final BrandCallback callback){

        new VndrRequest(queue)
                .get(URL_COLLABORATIVE_BRANDS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        callback.error(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            List<Brand> ownBrands = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0; i < jsonArray.length(); i++){
                                JSONObject object = jsonArray.getJSONObject(i);

                                Brand ownBrand = new Brand(
                                        object.getString("brand_name"),
                                        object.getString("_id")
                                );

                                if (object.has("company"))
                                    ownBrand.setCompany(object.getString("company"));
                                if (object.has("business_line"))
                                    ownBrand.setBusinessLine(object.getString("business_line"));
                                if (object.has("web"))
                                    ownBrand.setWeb(object.getString("web"));
                                if (object.has("facebook"))
                                    ownBrand.setFacebook(object.getString("facebook"));
                                if (object.has("contact_name"))
                                    ownBrand.setContact_name(object.getString("contact_name"));
                                if (object.has("email"))
                                    ownBrand.setEmail(object.getString("email"));
                                if (object.has("phone"))
                                    ownBrand.setPhone(object.getString("phone"));
                                if (object.has("address"))
                                    ownBrand.setAddress(object.getString("address"));
                                if (object.has("is_public"))
                                    ownBrand.setPublic(object.getBoolean("is_public"));


                                ownBrands.add(ownBrand);
                            }

                            callback.success(ownBrands);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getInt("statusCode") == 404){
                                    callback.emptyBrandList();
                                }
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                                callback.error("Ocurrió un error al procesar la solicitud.");
                            }
                        }
                    }
                });
    }

    interface BrandCallback{
        void error(String message);
        void success(List<Brand> brandList);
        void emptyBrandList();
    }


}
