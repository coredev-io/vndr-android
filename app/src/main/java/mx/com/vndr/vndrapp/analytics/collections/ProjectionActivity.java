package mx.com.vndr.vndrapp.analytics.collections;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.core.cartesian.series.Column;
import com.anychart.enums.Anchor;
import com.anychart.enums.HoverMode;
import com.anychart.enums.Position;
import com.anychart.enums.TooltipPositionMode;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.ProjectionsAdapter;
import mx.com.vndr.vndrapp.api.analytics.APICollections;
import mx.com.vndr.vndrapp.api.analytics.Projections;

public class ProjectionActivity extends AppCompatActivity implements APICollections.OnProjectionResponse {

    Toolbar toolbar;
    ProgressBar progressBar;
    TextView textViewError;
    RecyclerView recyclerView;
    TextView textViewAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_projection);

        setupUI();
        getData();
    }


    private void showLoader(){
        textViewError.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoader(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissLoader();
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }

    public void getData(){
        showLoader();
        new APICollections(Volley.newRequestQueue(this))
                .getProjection(this);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_projection);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        textViewError = findViewById(R.id.txt_error_projection);
        progressBar = findViewById(R.id.pb_projection);
        progressBar.setVisibility(View.GONE);
        textViewError.setVisibility(View.GONE);
        recyclerView = findViewById(R.id.rv_projection);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textViewAmount = findViewById(R.id.txt_projection_amnt);
    }

    @Override
    public void onSuccess(List<Projections> projections, String amount) {
        dismissLoader();
        recyclerView.setAdapter(new ProjectionsAdapter(projections, this));
        textViewAmount.setText(amount);
    }

    @Override
    public void onError(String error) {
        showError(error);
        recyclerView.setAdapter(null);
        textViewAmount.setText("$0.00");
    }


}