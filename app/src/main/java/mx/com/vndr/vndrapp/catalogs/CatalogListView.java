package mx.com.vndr.vndrapp.catalogs;

import mx.com.vndr.vndrapp.models.Catalog;

public interface CatalogListView {
    void setAdapter(CatalogListAdapter adapter);
    void hideFAB();
    void hideEmptyCatalogs();
    void showPorgress();
    void dismissProgress();
    void showError(String error);
    void showEmptyCatalogs();
    void navigateToCatalogDetail(Catalog catalog);
    void navigateToProductList(Catalog catalog);
}
