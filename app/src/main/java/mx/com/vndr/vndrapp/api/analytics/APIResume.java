package mx.com.vndr.vndrapp.api.analytics;

import com.android.volley.RequestQueue;
import com.github.mikephil.charting.data.PieEntry;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.api.VndrRequest;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_RESUME;

public class APIResume {

    private final static String TAG = "APICollections";

    private RequestQueue queue;

    public APIResume(RequestQueue queue) {
        this.queue = queue;
    }


    public void getResume(OnAnalyticsResumeResponse onAnalyticsResumeResponse){
        new VndrRequest(queue)
                .get(URL_ANALYTICS_RESUME, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onAnalyticsResumeResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") != 200){
                                onAnalyticsResumeResponse.onError(jsonObject.getString("message"));
                                return;
                            }


                            boolean salesEmpty = false;
                            boolean collectionEmpty = false;


                            jsonObject = jsonObject.getJSONObject("data");

                            List<ArrayList<PieEntry>> listPieEntries = new ArrayList<>();

                            //  Ventas

                            if (jsonObject.getJSONObject("sales").getInt("total") != 0 ){

                                JSONObject object = jsonObject.getJSONObject("sales").getJSONObject("cash_sales");

                                ArrayList<PieEntry> entries = new ArrayList<>();
                                entries.add(new PieEntry(object.getInt("amount_percent"), object.getString("label")));

                                object = jsonObject.getJSONObject("sales").getJSONObject("credit_sales");
                                entries.add(new PieEntry(object.getInt("amount_percent"), object.getString("label")));


                                listPieEntries.add(entries);
                            } else {
                                salesEmpty = true;
                                listPieEntries.add(new ArrayList<>());
                            }


                            if (jsonObject.getJSONObject("collection").getInt("total") != 0 ){

                                ArrayList<PieEntry> entries = new ArrayList<>();

                                JSONObject object = jsonObject.getJSONObject("collection").getJSONObject("collected");

                                entries.add(new PieEntry(object.getInt("amount_percent"), object.getString("label")));

                                object = jsonObject.getJSONObject("collection").getJSONObject("pending");
                                entries.add(new PieEntry(object.getInt("amount_percent"), object.getString("label")));


                                object = jsonObject.getJSONObject("collection").getJSONObject("discounts");
                                entries.add(new PieEntry(object.getInt("amount_percent"), object.getString("label")));


                                object = jsonObject.getJSONObject("collection").getJSONObject("overdue");
                                entries.add(new PieEntry(object.getInt("amount_percent"), object.getString("label")));

                                listPieEntries.add(entries);
                            } else {
                                collectionEmpty = true;
                                listPieEntries.add(new ArrayList<>());
                            }

                            if (salesEmpty && collectionEmpty){
                                onAnalyticsResumeResponse.onError("Por el momento no cuentas con suficiente información. Agrega cuentas por cobrar.");
                            } else {
                                onAnalyticsResumeResponse.onSuccess(listPieEntries);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onAnalyticsResumeResponse.onError("Ocurrió un error inesperado al procesar la solicitud. Por favor inténtalo de nuevo.");
                        }


                    }
                });
    }

    public interface OnAnalyticsResumeResponse {
        void onSuccess(List<ArrayList<PieEntry>> listPieEntries);
        void onError(String error);
    }


}
