package mx.com.vndr.vndrapp.account;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;

import mx.com.vndr.vndrapp.AppState;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.SignInActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.util.Dialogs;

public class ChangePwdActivity extends AppCompatActivity implements TextWatcher, VndrUser.VndrUserCallback {

    Toolbar toolbar;
    ConstraintLayout constraintLayoutVerificationEmail;
    LinearLayout linearLayoutChangePwd;
    TextInputLayout textInputLayoutNewPwd;
    TextInputLayout textInputLayoutRepeatNewPwd;
    EditText editTextNewPwd;
    EditText editTextRepeatNewPwd;
    Button button;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pwd);
        setupUI();

    }

    public void onResendVerificationEmailClick(View view){
        VndrAuth.getInstance().getCurrentUser().sendEmailVerification();
        Toast.makeText(this, "Se envió el email de verificación correctamente", Toast.LENGTH_LONG).show();
    }

    public void onChangePwdClick(View view){
        VndrAuth
                .getInstance()
                .getCurrentUser()
                .updatePwd(editTextNewPwd.getText().toString(), this);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_change_pwd);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        constraintLayoutVerificationEmail = findViewById(R.id.cl_verification_email);
        linearLayoutChangePwd = findViewById(R.id.ll_change_pwd);

        textInputLayoutNewPwd = findViewById(R.id.il_new_pwd);
        textInputLayoutRepeatNewPwd = findViewById(R.id.il_repeat_new_pwd);
        editTextNewPwd = findViewById(R.id.et_new_pwd);
        editTextRepeatNewPwd = findViewById(R.id.et_repeat_new_pwd);
        editTextNewPwd.addTextChangedListener(this);
        editTextRepeatNewPwd.addTextChangedListener(this);

        button = findViewById(R.id.btn_change_pwd);
        button.setEnabled(false);

    }



    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        if (editTextNewPwd.isFocused() && charSequence.toString().isEmpty()){
            textInputLayoutNewPwd.setError("Este campo no puede estar vacío");
        } else if (editTextNewPwd.isFocused() && !charSequence.toString().isEmpty()){
            textInputLayoutNewPwd.setError("");
        } else if (editTextRepeatNewPwd.isFocused() && charSequence.toString().isEmpty()){
            textInputLayoutRepeatNewPwd.setError("Este campo no puede estar vacío");
        } else if (editTextRepeatNewPwd.isFocused() && !charSequence.toString().isEmpty()){
            textInputLayoutRepeatNewPwd.setError("");
        }

        if (editTextNewPwd.getText().toString().equals(editTextRepeatNewPwd.getText().toString())){
            textInputLayoutRepeatNewPwd.setError("");
        } else if (!editTextNewPwd.getText().toString().equals(editTextRepeatNewPwd.getText().toString()) && editTextRepeatNewPwd.isFocused()){
            textInputLayoutRepeatNewPwd.setError("Las contraseñas deben coincidir");
        }


        if (!pwdsAreEmpty() && pwdsAreEqual())
            button.setEnabled(true);
        else
            button.setEnabled(false);

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private boolean pwdsAreEmpty(){
        if (editTextNewPwd.getText().toString().isEmpty())
            return true;
        else if (editTextRepeatNewPwd.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private boolean pwdsAreEqual(){
        if (editTextNewPwd.getText().toString().equals(editTextRepeatNewPwd.getText().toString()))
            return true;
        else
            return false;
    }

    public void clearPwds(){
        editTextNewPwd.setText("");
        editTextRepeatNewPwd.setText("");
    }

    private void logout(){
        VndrAuth.getInstance().logout(null);
        AppState.setAppState(this, AppState.NEW);
        VndrUser.setEmail("",this);
        VndrUser.setName("", this);
        Intent intent = new Intent(this, SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onVndrUserCallbackSuccess() {
        clearPwds();
        Dialogs.showAlert(this, "Se actualizó la contraseña con éxito. Por seguridad deberás iniciar sesión de nuevo.", (dialog, which) -> VndrAuth.getInstance().logout(new VndrAuth.VndrAuthResult() {
            @Override
            public void onVndrAuthError(String messageError) {
                logout();
            }

            @Override
            public void onVndrAuthSuccess() {
                logout();
            }
        }));
    }

    @Override
    public void onVndrUserCallbackError(String messageError) {
        clearPwds();
        Dialogs.showAlert(this, messageError);
    }
}
