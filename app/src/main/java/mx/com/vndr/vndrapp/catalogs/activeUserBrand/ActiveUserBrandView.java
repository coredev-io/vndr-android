package mx.com.vndr.vndrapp.catalogs.activeUserBrand;

import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public interface ActiveUserBrandView {
    void setAdapter(CheckedItemAdapter adapter);
    void showProgress();
    void dismissProgress();
    void showEmptyBrands();
    void showError(String error);
    void setToolbarTitle(String title);
    void navigateToCatalogList(Brand brand, CatalogActivity activity);
}
