package mx.com.vndr.vndrapp.catalogs.associateBrand;

import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItem;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemPresenter;
import mx.com.vndr.vndrapp.models.Brand;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_USER_BRANDS;

public class AssociateBrandInteractor {

    private List<CheckedItem> items;
    private CheckedItemAdapter adapter;
    private List<Brand> brandList;
    private RequestQueue requestQueue;
    private String token;

    public AssociateBrandInteractor(RequestQueue requestQueue, String token) {
        this.requestQueue = requestQueue;
        this.token = token;
        items = new ArrayList<>();
        brandList = new ArrayList<>();
    }

    public void getBrands(final OnGetDataFinishListener listener){

        StringRequest request = new StringRequest(Request.Method.GET, URL_USER_BRANDS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {


                processResponse(response, listener);
                createAdapter();
                listener.onSuccess(adapter);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, por favor inténtalo de nuevo");
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }
        };

        requestQueue.add(request);

    }

    private void processResponse(String response, final OnGetDataFinishListener listener){
        try {
            JSONArray array = new JSONArray(response);
            items = new ArrayList<>();
            brandList = new ArrayList<>();

            for (int i = 0; i < array.length(); i++){
                JSONObject object = array.getJSONObject(i);


                System.out.println(object.toString());
                final Brand brand = new Brand(object.getString("brand_name"), object.getString("_id"));

                CheckedItem item = new CheckedItem(object.getString("brand_name"), false, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onCheckedItemClick(brand);
                    }
                });

                if (object.has("active_in_user")){
                    item.setChecked(object.getBoolean("active_in_user"));
                    brand.setActiveInUser(object.getBoolean("active_in_user"));
                }

                if (object.has("register_key")){
                    brand.setClaveRegistro(object.getString("register_key"));
                }
                if (object.has("supervisor_key")){
                    brand.setClaveSupervisor(object.getString("supervisor_key"));
                }
                if (object.has("supervisor_name")){
                    brand.setNombreSupervisor(object.getString("supervisor_name"));
                }
                if (object.has("brand_owner")){
                    brand.setBrandOwner(object.getString("brand_owner"));
                }

                items.add(item);
                brandList.add(brand);

            }




        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createAdapter(){
        adapter = new CheckedItemAdapter(
                new CheckedItemPresenter(
                        items
                )
        );
    }

    public CheckedItemAdapter getAdapter() {
        return adapter;
    }

    interface OnGetDataFinishListener{
        void onError(String error);
        void onSuccess(CheckedItemAdapter adapter);
        void onCheckedItemClick(Brand brand);
    }

}
