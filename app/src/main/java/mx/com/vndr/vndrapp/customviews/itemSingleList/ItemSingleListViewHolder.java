package mx.com.vndr.vndrapp.customviews.itemSingleList;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mx.com.vndr.vndrapp.R;

class ItemSingleListViewHolder extends RecyclerView.ViewHolder {

    TextView textView;

    public ItemSingleListViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.txt_title_single_list);
    }
}
