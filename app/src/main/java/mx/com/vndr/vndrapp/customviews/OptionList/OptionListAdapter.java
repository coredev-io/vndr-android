package mx.com.vndr.vndrapp.customviews.OptionList;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;

public class OptionListAdapter extends RecyclerView.Adapter<OptionListAdapter.OptionListViewHolder> {

    private static final int HEADER = 0;
    private static final int ROW = 1;

    private List<Option> optionList;

    public OptionListAdapter(List<Option> optionList) {
        this.optionList = optionList;
    }

    @NonNull
    @Override
    public OptionListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        if (viewType == HEADER){
            return new OptionListViewHolder(
                    LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.simple_list_item_1, parent, false)
            );
        } else {
            return new OptionListViewHolder(
                    LayoutInflater.from(parent.getContext())
                            .inflate(R.layout.item_list, parent, false)
            );
        }


    }

    @Override
    public void onBindViewHolder(@NonNull OptionListViewHolder holder, int position) {
        Option option = optionList.get(position);
        if (option.isHeader()){
            TextView textView = holder.itemView.findViewById(R.id.text1);
            textView.setText(option.getHeaderTitle() == null ? "" : option.getHeaderTitle());
            textView.setBackgroundColor(holder.itemView.getResources().getColor(R.color.colorPrimary));
            textView.setTextColor(Color.WHITE);
            textView.setTypeface(ResourcesCompat.getFont(holder.itemView.getContext(), R.font.dinpro_medium));

        } else {
            holder.title.setText(option.getTitle());
            holder.subtitle.setText(option.getSubtitle() == null ? "" : option.getSubtitle());
            holder.layout.setOnClickListener(
                    view -> {
                        if (option.getListener() != null)
                            option.getListener().onClick();
                    });
        }

    }

    @Override
    public int getItemViewType(int position) {
        super.getItemViewType(position);
        Option option = optionList.get(position);

        if (option.isHeader())
            return HEADER;
        else
            return ROW;
    }

    @Override
    public int getItemCount() {
        return optionList.size();
    }

    class OptionListViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView subtitle;
        ConstraintLayout layout;

        OptionListViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_title);
            subtitle = itemView.findViewById(R.id.txt_description);
            layout = itemView.findViewById(R.id.layout_item_list);
        }
    }
}
