package mx.com.vndr.vndrapp.customer;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.CxCQuery;
import mx.com.vndr.vndrapp.cobros.ResumeCxcActivity;
import mx.com.vndr.vndrapp.customer.adapters.ListItemAdapter;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.orders.OrdersByClientActivity;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;

public class MainCustomerActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    List<String> title = new ArrayList<>();
    List<String> description = new ArrayList<>();
    List<View.OnClickListener> listeners = new ArrayList<>();

    ListItemAdapter adapter;
    Customer customer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_customer);
        customer = (Customer) getIntent().getSerializableExtra("customer");
        setUPUI();

        adapter = new ListItemAdapter(title,description, listeners);


        title.add("Ver / Actualizar perfil");
        description.add("Manten la información de tu cliente al día");
        listeners.add(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(
                        new Intent(MainCustomerActivity.this,AddCustomerActivity.class)
                        .putExtra("customer",customer), 1
                );
            }
        });

        title.add("Levantar un pedido nuevo");
        description.add("");
        listeners.add(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainCustomerActivity.this, ShoppingCartActivity.class);
                intent.putExtra("customer",customer);
                startActivity(intent);
            }
        });


        title.add("Ver pedidos");
        description.add("Revisa pedidos anteriores");
        listeners.add(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainCustomerActivity.this, OrdersByClientActivity.class);
                intent.putExtra("customer",customer);
                startActivity(intent);
            }
        });

        title.add("Registrar cobros y cargos");
        description.add("Anota cobros recibidos y otros movimientos de cobranza");
        listeners.add(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CxCQuery query = CxCQuery.CUSTOMER;
                query.setQueryParam(customer.getIdCustomer());
                startActivity(
                        new Intent(
                                MainCustomerActivity.this,
                                ResumeCxcActivity.class
                        ).putExtra("query", query)
                );
            }
        });

        title.add("Ver cuentas por cobrar");
        description.add("Revisa pedidos a plazo no pagados");
        listeners.add(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CxCQuery query = CxCQuery.CUSTOMER;
                query.setQueryParam(customer.getIdCustomer());
                startActivity(
                        new Intent(
                                MainCustomerActivity.this,
                                ResumeCxcActivity.class
                        ).putExtra("query", query)
                );
            }
        });

        title.add("Ver estados de cuenta");
        description.add("Revisa la información a la fecha de corte");
        listeners.add(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CxCQuery query = CxCQuery.CUSTOMER;
                query.setQueryParam(customer.getIdCustomer());
                startActivity(
                        new Intent(
                                MainCustomerActivity.this,
                                ResumeCxcActivity.class
                        ).putExtra("query", query)
                );
            }
        });


        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), LinearLayoutManager.VERTICAL));


    }



    private void setUPUI(){
        toolbar = findViewById(R.id.tb_customer_detail);
        recyclerView = findViewById(R.id.rv_customer_detail);


        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        toolbar.setTitle(customer.getCustomerFullName());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK){
            customer = (Customer)data.getSerializableExtra("customer");
            toolbar.setTitle(customer.getCustomerFullName());
        }
    }
}
