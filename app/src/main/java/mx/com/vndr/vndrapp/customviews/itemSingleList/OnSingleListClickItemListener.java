package mx.com.vndr.vndrapp.customviews.itemSingleList;

public interface OnSingleListClickItemListener {
    void onClickListener(String item, int pos);
}
