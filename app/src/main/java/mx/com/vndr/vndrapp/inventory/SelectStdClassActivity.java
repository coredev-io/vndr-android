package mx.com.vndr.vndrapp.inventory;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_USER_CLASS;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.SuggestionProducts;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.SuggestionsAdapter;
import mx.com.vndr.vndrapp.products.PickProductActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class SelectStdClassActivity extends AppCompatActivity implements VndrRequest.VndrResponse, StdClassAdapter.OnStdClassSelected {

    Toolbar toolbar;
    ProgressBar progressBar;
    RecyclerView recyclerView;

    StdClassAdapter adapter;
    List<StdClass> classList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_std_class);
        setupUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_std_class);
        progressBar = findViewById(R.id.pb_std_class);
        recyclerView = findViewById(R.id.rv_std_class);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    public void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }

    public void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void getData(){
        showLoading();
        new VndrRequest(Volley.newRequestQueue(this))
                .get(URL_USER_CLASS, this);
    }

    @Override
    public void error(String message) {
        dismissLoading();
        Dialogs.showAlert(this, message);
    }

    @Override
    public void success(String response) {
        dismissLoading();
        try {
            JSONArray jsonArray = new JSONArray(response);
            classList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                classList.add(new StdClass(jsonArray.getJSONObject(i)));
            }
            adapter = new StdClassAdapter(classList);
            adapter.setOnStdClassSelected(this);
            recyclerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud");
        }

    }

    @Override
    public void onStdClassSelected(StdClass stdClass) {
        startActivity(new Intent(this, PickInventoryProductActivity.class)
            .putExtra("idClass", stdClass.id)
        );
    }
}

class StdClass {
    String id;
    String name;

    public StdClass(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.getString("id");
        this.name = jsonObject.getString("name");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

class StdClassAdapter extends RecyclerView.Adapter<StdClassAdapter.StdClassViewHolder>{

    public interface OnStdClassSelected {
        void onStdClassSelected(StdClass stdClass);
    }

    List<StdClass> items;
    OnStdClassSelected onStdClassSelected;


    public StdClassAdapter(List<StdClass> items) {
        this.items = items;
    }

    public void setOnStdClassSelected(OnStdClassSelected onStdClassSelected) {
        this.onStdClassSelected = onStdClassSelected;
    }

    @NonNull
    @Override
    public StdClassViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_single_list, viewGroup, false);
        return new StdClassViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StdClassViewHolder stdClassViewHolder, int i) {
        stdClassViewHolder.textView.setText(items.get(i).name);
        stdClassViewHolder.textView.setOnClickListener(view -> {
            onStdClassSelected.onStdClassSelected(items.get(i));
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class StdClassViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public StdClassViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt_title_single_list);
        }
    }
}