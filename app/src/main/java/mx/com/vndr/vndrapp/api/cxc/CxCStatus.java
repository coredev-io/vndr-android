package mx.com.vndr.vndrapp.api.cxc;

import android.graphics.Color;

import androidx.core.content.ContextCompat;

import mx.com.vndr.vndrapp.R;

public enum CxCStatus {
    RETRASO1(R.color.colorAlertLow),
    RETRASO2(R.color.colorAlertMedium),
    RETRASO3(R.color.colorAlertHigh),
    RETRASO_GRAVE(R.color.colorAlertMax),
    NO_COBRADO(R.color.colorAlertMax),
    POR_VENCER(R.color.colorPorVencer),
    SALDO_FAVOR(android.R.color.black),
    AL_CORRIENTE(R.color.colorOk),
    LIQUIDADA(R.color.colorComplete),
    PAGADA(R.color.colorComplete),
    QUEBRANTO(R.color.colorAlertMax),
    VENCIDO(R.color.colorNoFacturado)
    ;




    private int colorResId;
    private String raw;

    CxCStatus(int colorResId) {
        this.colorResId = colorResId;
    }

    public int getColorResId() {
        return colorResId;
    }

    public String getRaw() {
        return raw;
    }

    public CxCStatus setRaw(String raw) {
        this.raw = raw;
        return this;
    }
}
