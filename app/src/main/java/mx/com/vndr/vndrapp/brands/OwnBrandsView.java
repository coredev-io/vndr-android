package mx.com.vndr.vndrapp.brands;

import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public interface OwnBrandsView {
    void showProgress();
    void hideProgress();
    void showEmptyBrands();
    void hideEmptyBrands();
    void showError(String error);
    void showData(CheckedItemAdapter adapter);
    void showBrandDetail(Brand brand);
}
