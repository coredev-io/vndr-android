package mx.com.vndr.vndrapp.models;

import java.io.Serializable;

public class Brand implements Serializable {

    String brandName;
    String brandId, claveRegistro, claveSupervisor, nombreSupervisor;
    boolean isActiveInUser;
    boolean ownerVndr = false;

    String company, businessLine, web, facebook, contact_name, email, phone, address;
    boolean isPublic, isAfiliacion;
    String brandOwner, pwdAfiliacion, brandCode;


    public Brand(String brandName, String brandId) {
        this.brandName = brandName;
        this.brandId = brandId;
        isActiveInUser = false;
        claveRegistro = "";
        claveSupervisor = "";
        nombreSupervisor = "";
        brandOwner = "";
        isAfiliacion = false;
        pwdAfiliacion = "";
    }

    public Brand(String brandName, String company, String businessLine, String web, String facebook, String contact_name, String email, String phone, String address, boolean isPublic) {
        this.brandName = brandName;
        this.company = company;
        this.businessLine = businessLine;
        this.web = web;
        this.facebook = facebook;
        this.contact_name = contact_name;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.isPublic = isPublic;
        isActiveInUser = false;
        claveRegistro = "";
        claveSupervisor = "";
        nombreSupervisor = "";
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getClaveRegistro() {
        return claveRegistro;
    }

    public void setClaveRegistro(String claveRegistro) {
        this.claveRegistro = claveRegistro;
    }

    public String getClaveSupervisor() {
        return claveSupervisor;
    }

    public void setClaveSupervisor(String claveSupervisor) {
        this.claveSupervisor = claveSupervisor;
    }

    public String getNombreSupervisor() {
        return nombreSupervisor;
    }

    public void setNombreSupervisor(String nombreSupervisor) {
        this.nombreSupervisor = nombreSupervisor;
    }

    public boolean isActiveInUser() {
        return isActiveInUser;
    }

    public void setActiveInUser(boolean activeInUser) {
        isActiveInUser = activeInUser;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBusinessLine() {
        return businessLine;
    }

    public void setBusinessLine(String businessLine) {
        this.businessLine = businessLine;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public String getBrandOwner() {
        return brandOwner;
    }

    public void setBrandOwner(String brandOwner) {
        this.brandOwner = brandOwner;
    }

    public String getPwdAfiliacion() {
        return pwdAfiliacion;
    }

    public void setPwdAfiliacion(String pwdAfiliacion) {
        this.pwdAfiliacion = pwdAfiliacion;
    }

    public boolean isAfiliacion() {
        return isAfiliacion;
    }

    public void setAfiliacion(boolean afiliacion) {
        isAfiliacion = afiliacion;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public boolean isOwnerVndr() {
        return ownerVndr;
    }

    public void setOwnerVndr(boolean ownerVndr) {
        this.ownerVndr = ownerVndr;
    }

    public enum Selected{
        current;

        private Brand brand;

        public Brand getBrand() {
            return brand;
        }

        public void setBrand(Brand brand) {
            this.brand = brand;
        }
    }
}
