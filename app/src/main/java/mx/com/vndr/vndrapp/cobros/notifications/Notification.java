package mx.com.vndr.vndrapp.cobros.notifications;

import android.content.Intent;

import java.text.NumberFormat;

import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class Notification {

    public static String totalNotificationsNoticesAmount = "$0.00";
    public static String totalNotificationsRequirementsAmount = "$0.00";
    private NotificationType type;
    private String descriptionType;
    private String description;
    private String paymentDate;
    private String dueDate;
    private String amount;
    private String idCxc;
    private String orderNumber;
    private String customerName;
    private String paymentPeriod;
    private String unpaidAmount;
    private String balanceAmount;
    private String statusCount;
    private NotificationAlertData notificationAlertData;
    private boolean isCxCUniquePayment = false;
    private boolean isRedAlert = false;

    public Notification() {

    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public String getDescriptionType() {
        return descriptionType;
    }

    public void setDescriptionType(String descriptionType) {
        this.descriptionType = descriptionType;
    }

    public String getPaymentDate() {
        return VndrDateFormat.dateWSFormatToAppFormat(paymentDate);
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getAmount() {
        return NumberFormat.getCurrencyInstance().format(Double.valueOf(amount));
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIdCxc() {
        return idCxc;
    }

    public void setIdCxc(String idCxc) {
        this.idCxc = idCxc;
    }

    public NotificationAlertData getNotificationAlertData() {
        return notificationAlertData;
    }

    public void setNotificationAlertData(NotificationAlertData notificationAlertData) {
        this.notificationAlertData = notificationAlertData;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDueDate() {
        return VndrDateFormat.dateWSFormatToAppFormat(dueDate);
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(String paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public String getUnpaidAmount() {
        return unpaidAmount;
    }

    public void setUnpaidAmount(String unpaidAmount) {
        this.unpaidAmount = unpaidAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getStatusCount() {
        return statusCount;
    }

    public void setStatusCount(String statusCount) {
        this.statusCount = statusCount;
        if (Integer.parseInt(statusCount) > 0 && this.type.equals(NotificationType.N001)) {
            this.isRedAlert = true;
        }
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isCxCUniquePayment() {
        return isCxCUniquePayment;
    }

    public void setCxCUniquePayment(boolean cxCUniquePayment) {
        isCxCUniquePayment = cxCUniquePayment;
    }

    public boolean isRedAlert() {
        return isRedAlert;
    }

    enum Selected {
        current;

        Notification notificationSelected;

        public Notification getNotificationSelected() {
            return notificationSelected;
        }

        public void setNotificationSelected(Notification notificationSelected) {
            this.notificationSelected = notificationSelected;
        }
    }
}
