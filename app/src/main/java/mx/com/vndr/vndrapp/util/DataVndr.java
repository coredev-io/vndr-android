package mx.com.vndr.vndrapp.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class DataVndr {

    private static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences("vndr",MODE_PRIVATE);
    }

    private static SharedPreferences.Editor getEditor(Context context){
        return getSharedPreferences(context).edit();
    }

    public static void setEmail(Context context, String email){
        SharedPreferences.Editor editor = getEditor(context);
        editor.putString("email",email);
        editor.apply();
    }

    public static String getEmail(Context context){
        return getSharedPreferences(context).getString("email","");
        //return "desarrollo@vndr.com.mx";
    }
}