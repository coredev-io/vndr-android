package mx.com.vndr.vndrapp.customviews.checkItemList;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mx.com.vndr.vndrapp.R;

public class CheckedItemViewHolder extends RecyclerView.ViewHolder implements CheckedItemRowView{

    TextView titleTextView;

    public CheckedItemViewHolder(@NonNull View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.txt_title_checked);
    }


    @Override
    public void setTitle(String title) {
        titleTextView.setText(title);
    }

    @Override
    public void setChecked(Boolean isChecked) {
        if (!isChecked){
            titleTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_loyalty_empty,0,0,0);
        } else {
            titleTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_loyalty,0,0,0);
        }
    }

    @Override
    public void setClickListener(View.OnClickListener listener) {
        titleTextView.setOnClickListener(listener);
    }
}
