package mx.com.vndr.vndrapp.customviews;


import android.content.Context;
import android.os.Build;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

public class EditTextPhoneNumber extends AppCompatEditText {
    public EditTextPhoneNumber(Context context) {
        super(context);
        customView();
    }

    public EditTextPhoneNumber(Context context, AttributeSet attrs) {
        super(context, attrs);
        customView();
    }

    public EditTextPhoneNumber(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        customView();
    }

    private void customView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            addTextChangedListener(new PhoneNumberFormattingTextWatcher("MX"));
        } else {
            addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        }
    }

    public String getRawPhoneNumber(){
        return getText().toString().replaceAll("[ -()]","");
    }
}
