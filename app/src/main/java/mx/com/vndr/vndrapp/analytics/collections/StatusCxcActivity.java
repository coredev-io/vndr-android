package mx.com.vndr.vndrapp.analytics.collections;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.APICollections;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;

public class StatusCxcActivity extends AppCompatActivity implements APICollections.OnCollectionResponse{

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textViewError;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_cxc);
        setupUI();
        getData();
    }

    public void getData(){
        showLoader();
        new APICollections(Volley.newRequestQueue(this))
                .getStatusCxc(this);
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_status_cxc);
        recyclerView = findViewById(R.id.rv_status_cxc);
        textViewError = findViewById(R.id.txt_error_status_cxc);
        progressBar = findViewById(R.id.pb_status_cxc);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onSuccess(VndrListAdapter adapter) {
        dismissLoader();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {
        showError(error);
    }

    private void showLoader(){
        textViewError.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoader(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissLoader();
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }
}