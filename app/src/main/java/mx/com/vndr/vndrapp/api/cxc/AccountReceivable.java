package mx.com.vndr.vndrapp.api.cxc;

public enum AccountReceivable {
    current;


    boolean paymentReminderAlert;
    boolean paymentEarlyNoticeAlert;
    int gracePeriod;
    int paymentEarlyNotice;

    public boolean isPaymentReminderAlert() {
        return paymentReminderAlert;
    }

    public void setPaymentReminderAlert(boolean paymentReminderAlert) {
        this.paymentReminderAlert = paymentReminderAlert;
    }

    public boolean isPaymentEarlyNoticeAlert() {
        return paymentEarlyNoticeAlert;
    }

    public void setPaymentEarlyNoticeAlert(boolean paymentEarlyNoticeAlert) {
        this.paymentEarlyNoticeAlert = paymentEarlyNoticeAlert;
    }

    public int getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(int gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public int getPaymentEarlyNotice() {
        return paymentEarlyNotice;
    }

    public void setPaymentEarlyNotice(int paymentEarlyNotice) {
        this.paymentEarlyNotice = paymentEarlyNotice;
    }
}
