package mx.com.vndr.vndrapp.products;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.ProductAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.inventory.Inventory;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.ProductQuantityActivity;
import mx.com.vndr.vndrapp.inventory.rules.InventoryRulesActivity;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.orders.AddProductActivity;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PRODUCTS_BY_CATALOG;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PRODUCTS_USER;

public class PickProductActivity extends AppCompatActivity implements ProductAdapter.OnListItemClickListener {

    RecyclerView recyclerView;
    ProductAdapter adapter;
    ProgressBar progressBar;
    List<Product> products = new ArrayList<>();
    String idClass = "";
    String idBrand = "";
    String idCatalog = "";
    Toolbar toolbar;
    ProductEnum activity;
    FloatingActionButton fab;
    TextView emptyProducts;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_product);

        if (getIntent().hasExtra("idBrand") && getIntent().hasExtra("idBrand")) {
            idClass = getIntent().getStringExtra("idClass");
            idBrand = getIntent().getStringExtra("idBrand");
        }
        if (getIntent().hasExtra("idCatalog")){
            idCatalog = getIntent().getStringExtra("idCatalog");

        }


        recyclerView = findViewById(R.id.rv_pick_products);
        progressBar = findViewById(R.id.pb_pick_product);

        toolbar = findViewById(R.id.tb_pick_product);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        adapter = new ProductAdapter(products, this);

        recyclerView.setAdapter(adapter);
        fab = findViewById(R.id.fab_product);
        emptyProducts = findViewById(R.id.empty_products);
        emptyProducts.setVisibility(View.GONE);
        fab.setVisibility(View.GONE);


    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().hasExtra("activity")){
            activity = (ProductEnum) getIntent().getSerializableExtra("activity");
            if (activity.equals(ProductEnum.CATALOGS_PARENT)){
                getProductsByCatalogID();
                fab.setVisibility(View.VISIBLE);
            }else{
                getProducts();
            }
        } else{
            getProducts();
        }
    }

    private void getProducts(){
        progressBar.setVisibility(View.VISIBLE);
        String url  = URL_PRODUCTS_USER + "?id_brand=" + idBrand + "&standard_classification_id=" + idClass;
        StringRequest request = new StringRequest(Request.Method.GET, url , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(View.GONE);
                processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                System.out.println(new String(error.networkResponse.data));
//                Log.e("TAG", String.valueOf(error.networkResponse.statusCode));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                headers.put("Connection", "close");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void getProductsByCatalogID(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_PRODUCTS_BY_CATALOG + idCatalog, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(View.GONE);
                processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                System.out.println(error);
//                Log.e("TAG", String.valueOf(error.networkResponse.statusCode));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                headers.put("Connection", "close");
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void processResponse(String response){

        try {
            products.clear();
            //adapter.notifyDataSetChanged();
            recyclerView.setAdapter(null);
            emptyProducts.setVisibility(View.GONE);
            JSONArray array = new JSONArray(response);
            System.out.println(response);
            for (int i = 0; i < array.length(); i++){

                JSONObject productObject = array.getJSONObject(i);

                Product product = new Product(
                        productObject.getString("_id"),
                        productObject.getString("sku"),
                        productObject.getString("product_name"),
                        productObject.getString("standard_classification"),
                        productObject.getString("notes"),
                        productObject.getString("sub_brand"),
                        productObject.getString("category"),
                        productObject.getString("subcategory"),
                        productObject.getString("product_key"),
                        productObject.getString("size"),
                        productObject.getString("color"),
                        productObject.getString("description"),
                        productObject.getString("url_image"),
                        productObject.getString("product_points"),
                        productObject.getString("barcode"),
                        productObject.getString("qrcode"),
                        productObject.getDouble("list_price"),
                        productObject.getBoolean("active"),
                        new Catalog(
                                productObject.getJSONObject("catalog").getString("_id"),
                                productObject.getJSONObject("catalog").getString("catalog_name")

                                ),
                        String.valueOf(productObject.getJSONObject("inventory").getInt("current_calculated_stock")));
                product.setHeight(productObject.getString("height"));
                product.setStdClassId(idClass);

                if (productObject.has("inventory")) {
                    product.setInventory(new Inventory(productObject.getJSONObject("inventory")));
                }

                if (productObject.has("brand_name")) {
                    product.setBrandName(productObject.getString("brand_name"));
                }

                products.add(product);
            }
            adapter = new ProductAdapter(products, this);
            if (getIntent().getBooleanExtra("shoppingOrder", false))
                adapter.hidePrice();
            if (getIntent().getBooleanExtra("inventoryRules", false))
                adapter.hidePrice();
            recyclerView.setAdapter(adapter);





        } catch (JSONException e) {
            e.printStackTrace();
            emptyProducts.setVisibility(View.VISIBLE);
        }

    }

    public void onClickFAB(View view){
        startActivity(
                new Intent(
                        this,
                        ProductActivity.class
                ).putExtra("catalogId",idCatalog)
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            this.setResult(resultCode, data);
            this.finish();
        }
    }

    @Override
    public void onProductSelected(Product product) {
        if (activity != null && activity.equals(ProductEnum.CATALOGS_PARENT)){
            //getProductsByCatalogID();
            Log.e("TAG","=-=-==");
            startActivity(
                    new Intent(
                            this,
                            ProductActivity.class
                    ).putExtra("product",product)
            );

        } else if (activity != null && activity.equals(ProductEnum.QUERY_PRODUCTS_BY_BRAND)){

        } else if (activity != null && activity.equals(ProductEnum.QUERY_PRODUCTS_BY_CLASS)){

        }else{
            if (getIntent().getBooleanExtra("pickProduct", false)) {
                this.setResult(
                        RESULT_OK,
                        new Intent()
                                .putExtra("product", product));
                this.finish();
                return;
            }

            if (getIntent().getBooleanExtra("inventoryRules", false)){
                startActivity(new Intent(this, InventoryRulesActivity.class)
                        .putExtra("product", product)
                );
                return;
            }

            if (!getIntent().getBooleanExtra("shoppingOrder", false))
                startActivityForResult(new Intent(this, AddProductActivity.class).putExtra("product", product),0);
            else
                startActivityForResult(new Intent(this, ProductQuantityActivity.class).putExtra("product", product),0);
        }
    }
}
