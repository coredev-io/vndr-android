package mx.com.vndr.vndrapp.models;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;

public class Order implements Serializable {

    public static final String INCOMPLETO = "Incompleto";
    public static final String NO_ENVIADO = "No enviado";
    public static final String NO_ENTREGADO = "No entregado";
    public static final String TERMINADO = "Terminado";


    private String idOrder;
    private Customer customer;
    private Boolean isComplete, isSended, isDelivered;
    private Double orderAmount;
    private Double totalOrderAmount;
    private int orderNumber;
    //private String saleDate;
    private Date saleDate;
    private String saleDateFormmat = "";
    private List<ShoppingCartProduct> products;
    private Double shippingAmount;
    private Delivery delivery;
    private Cxc cxc;
    private Date sendedDate;
    private String notes;
    private Date completeDate, deliveryDate, shippingDate;
    private NotificationAlertData notificationAlertData;
    private List<PaymentCalendar> scheduleInfo;
    private boolean isDownPaymentPaid = true;


    public Order() {
        //this.saleDate = "";
        products = new ArrayList<>();
    }


    public Order(String idOrder, Customer customer, Boolean isComplete, Boolean isSended, Boolean isDelivered, Double orderAmount, Double totalOrderAmount, int orderNumber) {
        this.idOrder = idOrder;
        this.customer = customer;
        this.isComplete = isComplete;
        this.isSended = isSended;
        this.isDelivered = isDelivered;
        this.orderAmount = orderAmount;
        this.totalOrderAmount = totalOrderAmount;
        this.orderNumber = orderNumber;
        //this.saleDate = "";
        products = new ArrayList<>();
    }

    public void setComplete(Boolean complete) {
        isComplete = complete;
    }

    public void setSended(Boolean sended) {
        isSended = sended;
    }

    public void setDelivered(Boolean delivered) {
        isDelivered = delivered;
    }

    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Double getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(Double orderAmount) {
        this.orderAmount = orderAmount;
    }

    public Double getTotalOrderAmount() {
        return totalOrderAmount;
    }

    public String getTotalOrderAmountStr(){
        return NumberFormat.getCurrencyInstance().format(totalOrderAmount);
    }

    public void setTotalOrderAmount(Double totalOrderAmount) {
        this.totalOrderAmount = totalOrderAmount;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public String getOrderNumberStr() {
        return "#" + orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }




    public List<ShoppingCartProduct> getProducts() {
        return products;
    }

    public void setProducts(List<ShoppingCartProduct> products) {
        this.products = products;
    }

    public Double getShippingAmount() {
        return shippingAmount;
    }

    public void setShippingAmount(Double shippingAmount) {
        this.shippingAmount = shippingAmount;
    }


    public String getParseSaleDate(){

        if (!saleDateFormmat.isEmpty()){
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            SimpleDateFormat output = new SimpleDateFormat("dd/MMM/yyyy");

            Date d = null;
            try
            {
                d = input.parse(saleDateFormmat);
                saleDate = d;
            }
            catch (ParseException e)
            {
                e.printStackTrace();
            }
            return output.format(d);
        } else{
            return "";
        }

    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public Date getSaleDate(){
        return saleDate;
    }

    public String getSaleDateFormmat() {
        return saleDateFormmat;
    }

    public void setSaleDateFormmat(String saleDateFormmat) {
        this.saleDateFormmat = saleDateFormmat;
        try {
            setSaleDate(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(saleDateFormmat));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public Cxc getCxc() {
        return cxc;
    }

    public void setCxc(Cxc cxc) {
        this.cxc = cxc;
    }

    public String getSendedDate() {
        return new SimpleDateFormat("dd/MMM/yyyy").format(sendedDate);
    }

    public void setSendedDate(String sendedDate) {
        try {

            this.sendedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(sendedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setStatus(Boolean isComplete, Boolean isSended, Boolean isDelivered){
        this.isComplete = isComplete;
        this.isSended = isSended;
        this.isDelivered = isDelivered;
    }

    public String getStatus(){


        if (!isComplete && !isSended && !isDelivered){
            return INCOMPLETO;
        } else if (isComplete && !isSended && !isDelivered && getDelivery() != null && getDelivery().getType().equals("shipping")){
            return NO_ENVIADO;
        } else if (isComplete && !isSended && !isDelivered && getDelivery() != null && getDelivery().getType().equals("personal")){
            return NO_ENTREGADO;
        } else if (isComplete && isSended && !isDelivered){
            return NO_ENTREGADO;
        }  else if (isComplete && isSended && isDelivered){
            return TERMINADO;
        } else {
            return "No definido";
        }
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


    public String getCompleteDate() {

        return new SimpleDateFormat("dd/MMM/yyyy").format(completeDate);
    }

    public void setCompleteDate(String completeDate) {
        try
        {
            this.completeDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(completeDate);;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            this.completeDate = null;
        }
    }

    public String getDeliveryDate() {

        return new SimpleDateFormat("dd/MMM/yyyy").format(deliveryDate);
    }

    public void setDeliveryDate(String deliveryDate) {
        try
        {
            this.deliveryDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(deliveryDate);;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            this.deliveryDate = null;
        }
    }

    public String getShippingDate() {
        return new SimpleDateFormat("dd/MMM/yyyy").format(shippingDate);
    }

    public Date shippingDate(){
        return shippingDate;
    }

    public void shippingDate(Date date){
        this.shippingDate = date;
    }

    public Date deliveryDate(){
        return deliveryDate;
    }

    public void deliveryDate(Date date){
        this.deliveryDate = date;
    }

    public void setShippingDate(String shippingDate) {
        try
        {
            this.shippingDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(shippingDate);;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            this.shippingDate = null;
        }
    }

    public boolean isCompleteDate(){
        return completeDate != null;
    }

    public boolean isShippingDate(){
        return shippingDate != null;
    }

    public boolean isDeliveryDate(){
        return deliveryDate != null;
    }

    public NotificationAlertData getNotificationAlertData() {
        return notificationAlertData;
    }

    public void setNotificationAlertData(NotificationAlertData notificationAlertData) {
        this.notificationAlertData = notificationAlertData;
    }

    public List<PaymentCalendar> getScheduleInfo() {
        return scheduleInfo;
    }

    public void setScheduleInfo(List<PaymentCalendar> scheduleInfo) {
        this.scheduleInfo = scheduleInfo;
    }

    public boolean isDownPaymentPaid() {
        return isDownPaymentPaid;
    }

    public void setDownPaymentPaid(boolean downPaymentPaid) {
        isDownPaymentPaid = downPaymentPaid;
    }
}
