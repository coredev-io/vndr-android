package mx.com.vndr.vndrapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import java.util.Date;

import mx.com.vndr.vndrapp.models.Cxc;
import mx.com.vndr.vndrapp.models.Order;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class PaymentFrequencyActivity extends AppCompatActivity {

    Toolbar toolbar;
    Order order;
    Cxc cxc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_frequency);

        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
        }

        toolbar = findViewById(R.id.tb_payment_freq);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        cxc = new Cxc();
    }


    public void onUniquePaymentClick(View view){



        cxc.setPayFrequency(1);
        order.setCxc(cxc);
        startActivityForResult(new Intent(
                this,
                PickDateActivity.class)
                        .putExtra("type",PickDateActivity.Type.PAY_DATE)
                        .putExtra("order",order)
                ,1);
    }

    public void onSemanal(View view){

        /*
        order = new Order();
        order.setTotalOrderAmount(2000.00);
        order.setCxc(new Cxc());
        order.setSaleDate(new Date(System.currentTimeMillis()));

         */
        cxc.setPayFrequency(7);
        order.setCxc(cxc);
        startActivityForResult(
                new Intent(this, ConfigPaymentsActivity.class)
                .putExtra("order", order)
                ,1
        );
    }


    public void onQuincenal(View view){

        /*
        order = new Order();
        order.setTotalOrderAmount(2000.00);
        order.setCxc(new Cxc());
        order.setSaleDate(new Date(System.currentTimeMillis()));

         */

        cxc.setPayFrequency(15);
        order.setCxc(cxc);
        startActivityForResult(
                new Intent(this, ConfigPaymentsActivity.class)
                        .putExtra("order", order)
                ,1
        );
    }


    public void onMensual(View view){
/*
        order = new Order();
        order.setTotalOrderAmount(2000.00);
        order.setCxc(new Cxc());
        order.setSaleDate(new Date(System.currentTimeMillis()));

 */

        cxc.setPayFrequency(30);
        order.setCxc(cxc);
        startActivityForResult(
                new Intent(this, ConfigPaymentsActivity.class)
                        .putExtra("order", order)
                ,1
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }
}
