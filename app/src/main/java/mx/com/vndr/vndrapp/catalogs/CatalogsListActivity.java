package mx.com.vndr.vndrapp.catalogs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.catalogs.newCatalog.NewCatalogActivity;
import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.products.PickProductActivity;

public class CatalogsListActivity extends AppCompatActivity implements View.OnClickListener, CatalogListView {

    Toolbar toolbar;
    FloatingActionButton fab;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    CatalogActivity activity;
    Brand brand;
    TextView textView;
    private CatalogListPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalogs_list);
        setUpUI();
        activity = (CatalogActivity) getIntent().getSerializableExtra("activity");
        brand = (Brand) getIntent().getSerializableExtra("brand");

        presenter = new CatalogListPresenter(this,
                new CatalogListInteractor(
                        VndrAuth.getInstance().getCurrentUser().getSessionToken(),
                        Volley.newRequestQueue(this)
                ),
                brand,
                activity);

    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getCatalogs();

    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_catalog_list);
        fab = findViewById(R.id.fab_catalog_list);
        fab.setOnClickListener(this);
        recyclerView = findViewById(R.id.rv_catalog_list);
        progressBar = findViewById(R.id.pb_catalog_list);
        textView = findViewById(R.id.txt_empty_catalogs);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration( new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    @Override
    public void onClick(View view) {
        //  FABBBBBB
        startActivity(new Intent(
                this,
                NewCatalogActivity.class
        ).putExtra("createMode",true)
        .putExtra("brand",brand).putExtra("isOwner",brand.isOwnerVndr())) ;
    }

    @Override
    public void setAdapter(CatalogListAdapter adapter) {
        if (recyclerView.getAdapter() == null)
            recyclerView.setAdapter(adapter);
        else{
            recyclerView.setAdapter(null);
            recyclerView.setAdapter(adapter);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void hideFAB() {
        fab.setVisibility(View.GONE);
    }

    @Override
    public void hideEmptyCatalogs() {
        textView.setVisibility(View.GONE);
    }

    @Override
    public void showPorgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showEmptyCatalogs() {
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void navigateToCatalogDetail(Catalog catalog) {
        startActivity(new Intent(
                this,
                NewCatalogActivity.class
        ).putExtra("catalog",catalog).putExtra("isOwner",brand.isOwnerVndr())) ;
    }

    @Override
    public void navigateToProductList(Catalog catalog) {
        startActivity(new Intent(
                this,
                PickProductActivity.class
        ).putExtra("idCatalog",catalog.getId())
        .putExtra("activity", ProductEnum.CATALOGS_PARENT)
        ) ;
    }
}
