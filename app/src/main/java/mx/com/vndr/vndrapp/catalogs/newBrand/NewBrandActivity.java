package mx.com.vndr.vndrapp.catalogs.newBrand;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.textfield.TextInputLayout;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.brands.NewBrandSuccessActivity;
import mx.com.vndr.vndrapp.models.Brand;

public class NewBrandActivity extends AppCompatActivity implements NewBrandView, View.OnClickListener {

    Toolbar toolbar;
    ProgressBar progressBar;
    Button button;
    TextInputLayout brandCodeLayout;
    EditText brandName, company, businessLine, web, facebook, contactName,
        email, phone, address, brandCode;
    CheckBox brandVndr;
    Brand brand;
    MODE mode;

    private NewBrandPresenter presenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_brand);
        brand = (Brand) getIntent().getSerializableExtra("brand");

        if (brand == null){
            mode = MODE.create;
        } else {
            mode = MODE.edit;
        }
        setUpUI();
        presenter = new NewBrandPresenter(new NewBrandInteractor(VndrAuth.getInstance().getCurrentUser().getSessionToken(), Volley.newRequestQueue(this)),this);
    }

    private void setUpUI() {
        toolbar = findViewById(R.id.tb_new_brand);
        progressBar = findViewById(R.id.pb_new_brand);
        button = findViewById(R.id.btn_new_brand);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        button.setOnClickListener(this);

        brandName = findViewById(R.id.brand_name);
        brandCodeLayout = findViewById(R.id.brand_code_lo);
        brandCode = findViewById(R.id.brand_code);
        brandCode.setEnabled(false);
        company = findViewById(R.id.company);
        businessLine = findViewById(R.id.business_line);
        web = findViewById(R.id.web);
        facebook = findViewById(R.id.facebook);
        contactName = findViewById(R.id.contact_name);
        contactName.setText(
                VndrUser.getName(this)
        );
        contactName.setEnabled(false);

        email = findViewById(R.id.email);
        email.setText(VndrUser.getEmail(this));
        email.setEnabled(false);

        phone = findViewById(R.id.phone);
        phone.setText(VndrUser.getPhoneNumber(this));
        phone.setEnabled(false);

        address = findViewById(R.id.address);
        brandVndr = findViewById(R.id.brand_vndr);
        //collaborativeCatalog = findViewById(R.id.share_catalog_brand);
        brandName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (brandName.getText().toString().isEmpty()){
                    button.setEnabled(false);
                } else {
                    button.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        button.setEnabled(false);

        if (mode == MODE.edit){
            setUPEditMode();
        }

    }

    private void setUPEditMode(){
        toolbar.setTitle("Editar marca");
        brandName.setText(brand.getBrandName());
        brandName.setEnabled(false);

        company.setText(brand.getCompany());
        company.setEnabled(false);

        businessLine.setText(brand.getBusinessLine());
        businessLine.setEnabled(false);

        web.setText(brand.getWeb());
        web.setEnabled(false);

        facebook.setText(brand.getFacebook());
        facebook.setEnabled(false);

        /*
        contactName.setText(brand.getContact_name());
        contactName.setEnabled(false);

        email.setText(brand.getEmail());
        email.setEnabled(false);

        phone.setText(brand.getPhone());
        phone.setEnabled(false);

         */
        if (brand.isPublic()) {
            brandCodeLayout.setVisibility(View.VISIBLE);
            brandCode.setText(brand.getBrandCode());
        }

        address.setText(brand.getAddress());
        address.setEnabled(false);

        brandVndr.setChecked(brand.isPublic());
        brandVndr.setEnabled(false);

        //collaborativeCatalog.setChecked(brand.isCreateCollaborativeCatalog());
        //collaborativeCatalog.setEnabled(false);

        button.setEnabled(true);
        button.setText("Editar marca");
    }

    private void setupUpdateMode(){
        button.setText("Actualizar marca");
        button.setEnabled(true);
        brandName.setEnabled(true);
        company.setEnabled(true);
        businessLine.setEnabled(true);
        web.setEnabled(true);
        facebook.setEnabled(true);
        /*
        contactName.setEnabled(true);
        email.setEnabled(true);
        phone.setEnabled(true);

         */
        brandCodeLayout.setVisibility(View.GONE);
        address.setEnabled(true);
        brandVndr.setEnabled(true);
        //collaborativeCatalog.setEnabled(true);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void navigateToSuccess(Brand brand) {

        Bundle bundle = new Bundle();
        bundle.putString("brandName", brand.getBrandName());
        AppEventsLogger.newLogger(this).logEvent("newBrandAdded", bundle);

        if (brand.isPublic()){
            startActivityForResult(
                    new Intent(
                            this,
                            NewBrandSuccessActivity.class
                    ).putExtra("brand", brand), 0
            );
        } else {
            Toast.makeText(this, "Marca agregada con éxito", Toast.LENGTH_LONG).show();
            finishActivity();
        }

    }

    @Override
    public void finishActivity() {
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            this.finish();
        }
    }

    @Override
    public void onClick(View view) {

        switch (mode){
            case edit:
                mode = MODE.update;
                setupUpdateMode();
                break;
            case create:
                Brand newBrand = new Brand(
                        brandName.getText().toString(),
                        company.getText().toString(),
                        businessLine.getText().toString(),
                        web.getText().toString(),
                        facebook.getText().toString(),
                        contactName.getText().toString(),
                        email.getText().toString(),
                        phone.getText().toString(),
                        address.getText().toString(),
                        brandVndr.isChecked()
                );
                presenter.createNewBrand(newBrand);
                break;
            case update:
                Brand updateBrand = new Brand(
                        brandName.getText().toString(),
                        company.getText().toString(),
                        businessLine.getText().toString(),
                        web.getText().toString(),
                        facebook.getText().toString(),
                        contactName.getText().toString(),
                        email.getText().toString(),
                        phone.getText().toString(),
                        address.getText().toString(),
                        brandVndr.isChecked()
                );
                updateBrand.setBrandId(brand.getBrandId());
                presenter.updateBrand(updateBrand);
                break;
        }


    }

    enum MODE{
        edit, create, update
    }
}
