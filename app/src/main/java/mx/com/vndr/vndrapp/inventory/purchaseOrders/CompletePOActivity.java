package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders.OnShoppingOrdersResponse;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.orders.ActiveOrdersActivity;
import mx.com.vndr.vndrapp.orders.AllOrdersActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.inventory.purchaseOrders.ShoppingOrdersAdapter.*;

public class CompletePOActivity extends AppCompatActivity implements OnShoppingOrdersResponse, OnShoppingOrderSelected {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textView;
    ShoppingOrdersActivity.Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_p_o);
        setupUI();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (getIntent().hasExtra("query")){
            query = (ShoppingOrdersActivity.Query) getIntent().getSerializableExtra("query");
        }
        showLoader();
        new APIShoppingOrders(Volley.newRequestQueue(this))
                .getShoppingOrders(this, query);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_complete_po);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_po);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        textView = findViewById(R.id.txt_empty_complete_po);
        progressBar = findViewById(R.id.pb_complete_po);
    }

    private void showLoader(){
        progressBar.setVisibility(View.VISIBLE);
        textView.setVisibility(View.GONE);
        recyclerView.setAdapter(null);
    }

    private void dismissLoader(){
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(List<ShoppingOrder> shoppingOrders, Map<String, String> amountsHeader) {
        dismissLoader();
        recyclerView.setAdapter(
                new ShoppingOrdersAdapter(shoppingOrders, this)
                        .setOnShoppingOrderSelected(this)
        );
    }

    @Override
    public void onError(String messageError) {
        dismissLoader();
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
    }

    @Override
    public void onSelected(ShoppingOrder shoppingOrder) {
        ShoppingOrder.Selected.current.setShoppingOrder(shoppingOrder);
        navigateToDetail();
    }

    private void navigateToDetail(){
        startActivityForResult(new Intent(this, PODetailActivity.class).putExtra("isCompleted", true),1);
    }
}