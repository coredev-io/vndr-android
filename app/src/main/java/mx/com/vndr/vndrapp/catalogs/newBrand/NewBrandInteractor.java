package mx.com.vndr.vndrapp.catalogs.newBrand;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.api.URLVndr;

public class NewBrandInteractor {

    private String token;
    private RequestQueue queue;

    public NewBrandInteractor(String token, RequestQueue queue) {
        this.token = token;
        this.queue = queue;
    }

    public void createNewBrand(final Brand brand, final OnFinishRequest listener){
        StringRequest request = new StringRequest(Request.Method.POST, URLVndr.URL_BRANDS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.e("TAG",response);
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.has("statusCode")){
                        listener.onError(jsonObject.getString("message"));
                    } else {
                        brand.setBrandCode(jsonObject.getJSONObject("private").getString("brand_code"));
                        listener.onSuccess(brand);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error al tratar de procesar la solicitud, por favor inténtalo de nuevo.");
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");

                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("brand_name",brand.getBrandName());
                params.put("company",brand.getCompany());
                params.put("web",brand.getWeb());
                params.put("facebook",brand.getFacebook());
                params.put("business_line",brand.getBusinessLine());
                params.put("contact_name",brand.getContact_name());
                params.put("address",brand.getAddress());
                params.put("phone",brand.getPhone());
                params.put("email",brand.getEmail());
                params.put("is_public",String.valueOf(brand.isPublic()));
//                params.put("collaborative_catalog",String.valueOf(brand.isCreateCollaborativeCatalog()));

                return params;
            }
        };

        queue.add(request);
    }

    public void updateBrand(final Brand brand, final OnFinishRequest listener){
        StringRequest request = new StringRequest(Request.Method.PATCH, URLVndr.URL_BRANDS + "/" + brand.getBrandId(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                listener.onUpdateSuccess();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, inténtalo de nuevo");

                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("brand_name",brand.getBrandName());
                params.put("company",brand.getCompany());
                params.put("web",brand.getWeb());
                params.put("facebook",brand.getFacebook());
                params.put("business_line",brand.getBusinessLine());
                params.put("contact_name",brand.getContact_name());
                params.put("address",brand.getAddress());
                params.put("phone",brand.getPhone());
                params.put("email",brand.getEmail());
                params.put("is_public",String.valueOf(brand.isPublic()));
                //params.put("collaborative_catalog",String.valueOf(brand.isCreateCollaborativeCatalog()));



                return params;
            }
        };

        queue.add(request);
    }

    interface OnFinishRequest{
        void onSuccess(Brand brand);
        void onUpdateSuccess();
        void onError(String error);
    }
}
