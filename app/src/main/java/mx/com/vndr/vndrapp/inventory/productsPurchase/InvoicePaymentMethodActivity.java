package mx.com.vndr.vndrapp.inventory.productsPurchase;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import mx.com.vndr.vndrapp.PickDateActivity;
import mx.com.vndr.vndrapp.R;

public class InvoicePaymentMethodActivity extends AppCompatActivity {

    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_payment_method);

        toolbar = findViewById(R.id.tb_invoice_payment);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    public void onClick(View view){
        startActivityForResult(
                new Intent(this, InvoiceDateActivity.class)
                        .putExtra("type", InvoiceDateActivity.InvoiceDateType.payment),
                1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }
}