package mx.com.vndr.vndrapp.api.cxc;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.analytics.business.MovementAnalyticsActivity.MovementType;
import mx.com.vndr.vndrapp.analytics.business.MovementAnalyticsActivity.Query;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.analytics.Invoice;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListType;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_MOVEMENTS;

public class APIMovements {

    private final static String TAG = "APIMovements";

    private RequestQueue queue;

    public APIMovements(RequestQueue queue) {
        this.queue = queue;
    }

    public void getAnalyticsMovements(OnMovementResponse onMovementResponse, Date minDate, Date maxDate, Query query){
        String url  = URL_ANALYTICS_MOVEMENTS;
        url += "?analytics=";

        switch (query){
            case BY_INCOME:
                url += "income";
                break;
            case BY_EXPENSES:
                url += "expenses";
                break;
        }

        url += "&initial_date=" + VndrDateFormat.dateToWSFormat(minDate);
        url += "&final_date=" + VndrDateFormat.dateToWSFormat(maxDate);
        if (!query.getMovementType().equals(MovementType.ALL)){
            url += "&type=" + query.getMovementType().toString();
        }
        url += "&pagination=true&the_page=1&per_page=100";

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onMovementResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("results");
                            if (jsonArray.length() == 0){
                                onMovementResponse.onError("No se encontraron movimientos con los criterios seleccionados");
                                return;
                            }

                            List<VndrList> vndrLists = new ArrayList<>();

                            if (!query.getMovementType().equals(MovementType.ALL)) {
                                JSONObject info = jsonObject.getJSONObject("info");
                                System.out.println(info.toString());
                                double totalAmount = 0;
                                if (query.getMovementType().equals(MovementType.A))
                                    totalAmount = info.getDouble("total_a");
                                if (query.getMovementType().equals(MovementType.C))
                                    totalAmount = info.getDouble("total_c");

                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                info.getString("label"),
                                                NumberFormat.getCurrencyInstance().format(totalAmount)
                                        ).setBGColor(R.color.colorAccent)
                                ));

                                JSONObject clientsObject = info.getJSONObject("clients_operations");

                                if (clientsObject.has("title")){
                                    vndrLists.add(new VndrList(clientsObject.getString("title")));
                                } else {
                                    vndrLists.add(new VndrList("Operaciones con clientes"));
                                }

                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                clientsObject.getString("label"),
                                                NumberFormat.getCurrencyInstance().format(clientsObject.getDouble("total"))
                                        ).setBGColor(R.color.colorAccent)
                                ));

                                vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                JSONArray concepts = clientsObject.getJSONArray("concepts");

                                for (int i = 0; i < concepts.length(); i++) {
                                    JSONObject object = concepts.getJSONObject(i);
                                    vndrLists.add(new VndrList(
                                            new SimpleList2(
                                                    object.getString("label"),
                                                    NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                            ).setBGColor(R.color.colorAccent)
                                    ));
                                }

                                JSONObject supplierObject = info.getJSONObject("suppliers_operations");

                                if (supplierObject.has("title")){
                                    vndrLists.add(new VndrList(supplierObject.getString("title")));
                                } else {
                                    vndrLists.add(new VndrList("Operaciones con proveedores"));
                                }

                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                supplierObject.getString("label"),
                                                NumberFormat.getCurrencyInstance().format(supplierObject.getDouble("total"))
                                        ).setBGColor(R.color.colorAccent)
                                ));

                                vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                concepts = supplierObject.getJSONArray("concepts");

                                for (int i = 0; i < concepts.length(); i++) {
                                    JSONObject object = concepts.getJSONObject(i);
                                    vndrLists.add(new VndrList(
                                            new SimpleList2(
                                                    object.getString("label"),
                                                    NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                            ).setBGColor(R.color.colorAccent)
                                    ));
                                }

                            } else {
                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                "Total abonos",
                                                NumberFormat.getCurrencyInstance().format(jsonObject.getJSONObject("info").getDouble("total_a"))
                                        ).setBGColor(R.color.colorAccent)
                                ));
                                vndrLists.add(new VndrList(
                                        new SimpleList2(
                                                "Total cargos",
                                                NumberFormat.getCurrencyInstance().format(jsonObject.getJSONObject("info").getDouble("total_c"))
                                        ).setBGColor(R.color.colorAccent)
                                ));
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject resumeObject = jsonArray.getJSONObject(i).getJSONObject("resume");
                                JSONObject ticketObject = jsonArray.getJSONObject(i).getJSONObject("ticket");

                                CxCMovement movement = new CxCMovement(ticketObject.getString("_id"));
                                movement.setDescription(resumeObject.getString("description"));
                                movement.setPaymentMethod(resumeObject.getString("payment_method_name"));
                                movement.setCreateDate(resumeObject.getString("create_date"));
                                movement.setAmount(resumeObject.getDouble("amount"));
                                movement.setStringDate(resumeObject.getString("date"));

                                if (resumeObject.getString("mv_type").equals("client")) {
                                    movement.setNotificationAlertData(
                                            new NotificationAlertData(
                                                    ticketObject.getJSONObject("mv_notifications"),
                                                    resumeObject.getJSONObject("contact_info")
                                            )
                                    );
                                    movement.setCustomerName(ticketObject.getString("customer_name"));
                                    movement.setOrderNumber(String.valueOf(ticketObject.getInt("order_number")));
                                    movement.setOldBalance(ticketObject.getDouble("ar_initial_balance_ticket"));
                                    movement.setNewBalance(ticketObject.getDouble("ar_ending_balance_ticket"));
                                    movement.setNewExigible(ticketObject.getDouble("ar_ending_amount_due_ticket"));
                                    movement.setOldExigible(ticketObject.getDouble("ar_initial_amount_due_ticket"));
                                    movement.setReversable(ticketObject.getBoolean("mt_reversible"));
                                    movement.setComment(ticketObject.getString("mv_comments"));
                                } else {
                                    movement.setIsInvoice(true);
                                    movement.setInvoice(new Invoice(ticketObject, true));
                                }



                                mx.com.vndr.vndrapp.cobros.movements.MovementType type = new mx.com.vndr.vndrapp.cobros.movements.MovementType();
                                type.setCode(resumeObject.getString("code_type"));
                                type.setTypeTag(resumeObject.getString("tag_description"));
                                type.setType(resumeObject.getString("tag_type"));
                                movement.setType(type);


                                vndrLists.add(new VndrList(movement));
                            }

                            onMovementResponse.onSuccess(new VndrListAdapter(vndrLists));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onMovementResponse.onError("Ocurrió un error inesperado al procesar la solicitud");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            onMovementResponse.onError("Ocurrió un error inesperado al procesar la solicitud");
                        }
                    }
                });
    }

    public void getCashMovements(OnMovementResponse onMovementResponse, Date minDate, Date maxDate, Query query){
        String url  = URL_ANALYTICS_MOVEMENTS;
        url += "?analytics=cash";


        url += "&initial_date=" + VndrDateFormat.dateToWSFormat(minDate);
        url += "&final_date=" + VndrDateFormat.dateToWSFormat(maxDate);
        if (!query.getMovementType().equals(MovementType.ALL)){
            url += "&type=" + query.getMovementType().toString();
        }
        if (query.getPaymentMethodCode() != null)
            url += "&payment_method=" + query.getPaymentMethodCode();

        url += "&pagination=true&the_page=1&per_page=100";

        /*
        if (query.getMovementType().equals(MovementType.C)){
            onMovementResponse.onError("Esta opción estará disponible muy pronto. ¡Espérala!");
            return;
        }
         */

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onMovementResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray jsonArray = jsonObject.getJSONArray("results");
                            if (jsonArray.length() == 0){
                                onMovementResponse.onError("No se encontraron movimientos con los criterios seleccionados");
                                return;
                            }

                            List<VndrList> vndrLists = new ArrayList<>();

                            //  Query for payment methods
                            if (query.getPaymentMethodCode() != null){
                                JSONObject info = jsonObject.getJSONObject("info");

                                switch (query.getMovementType()){

                                    case ALL:
                                        vndrLists.add(new VndrList(
                                                new SimpleList2(
                                                        "Total depósitos",
                                                        NumberFormat.getCurrencyInstance().format(info.getJSONObject("cash_deposits").getDouble("total"))
                                                ).setBGColor(R.color.colorAccent)
                                        ));

                                        vndrLists.add(new VndrList(
                                                new SimpleList2(
                                                        "Total retiros",
                                                        NumberFormat.getCurrencyInstance().format(info.getJSONObject("cash_charges").getDouble("total"))
                                                ).setBGColor(R.color.colorAccent)
                                        ));
                                        break;
                                    case A:
                                        vndrLists.add(new VndrList(
                                                new SimpleList2(
                                                        "Total depósitos",
                                                        NumberFormat.getCurrencyInstance().format(info.getJSONObject("cash_deposits").getDouble("total"))
                                                ).setBGColor(R.color.colorAccent)
                                        ));
                                        break;
                                    case C:
                                        vndrLists.add(new VndrList(
                                                new SimpleList2(
                                                        "Total retiros",
                                                        NumberFormat.getCurrencyInstance().format(info.getJSONObject("cash_charges").getDouble("total"))
                                                ).setBGColor(R.color.colorAccent)
                                        ));
                                        break;
                                }
                            } else {

                                if (query.getMovementType() != MovementType.C) {
                                    JSONObject cashDeposits = jsonObject.getJSONObject("info").getJSONObject("cash_deposits");

                                    vndrLists.add(new VndrList(cashDeposits.getString("title")));



                                    vndrLists.add(new VndrList(
                                            new SimpleList2(
                                                    cashDeposits.getString("label"),
                                                    NumberFormat.getCurrencyInstance().format(cashDeposits.getDouble("total"))
                                            ).setBGColor(R.color.colorAccent)
                                    ));

                                    vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                    JSONArray concepts = cashDeposits.getJSONArray("concepts");

                                    for (int i = 0; i < concepts.length(); i++) {
                                        JSONObject object = concepts.getJSONObject(i);
                                        vndrLists.add(new VndrList(
                                                new SimpleList2(
                                                        object.getString("label"),
                                                        NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                                ).setBGColor(R.color.colorAccent)
                                        ));
                                    }
                                }

                                if (query.getMovementType() != MovementType.A) {
                                    JSONObject cashPayments = jsonObject.getJSONObject("info").getJSONObject("cash_payments");

                                    vndrLists.add(new VndrList(cashPayments.getString("title")));



                                    vndrLists.add(new VndrList(
                                            new SimpleList2(
                                                    cashPayments.getString("label"),
                                                    NumberFormat.getCurrencyInstance().format(cashPayments.getDouble("total"))
                                            ).setBGColor(R.color.colorAccent)
                                    ));

                                    vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                    JSONArray concepts = cashPayments.getJSONArray("concepts");

                                    for (int i = 0; i < concepts.length(); i++) {
                                        JSONObject object = concepts.getJSONObject(i);
                                        vndrLists.add(new VndrList(
                                                new SimpleList2(
                                                        object.getString("label"),
                                                        NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                                ).setBGColor(R.color.colorAccent)
                                        ));
                                    }
                                }

                            }



                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject resumeObject = jsonArray.getJSONObject(i).getJSONObject("resume");
                                JSONObject ticketObject = jsonArray.getJSONObject(i).getJSONObject("ticket");

                                CxCMovement movement = new CxCMovement(ticketObject.getString("_id"));
                                movement.setDescription(resumeObject.getString("description"));
                                movement.setPaymentMethod(resumeObject.getString("payment_method_name"));
                                movement.setCreateDate(resumeObject.getString("create_date"));
                                movement.setAmount(resumeObject.getDouble("amount"));
                                movement.setStringDate(resumeObject.getString("date"));

                                if (resumeObject.getString("mv_type").equals("client")) {
                                    movement.setNotificationAlertData(
                                            new NotificationAlertData(
                                                    ticketObject.getJSONObject("mv_notifications"),
                                                    resumeObject.getJSONObject("contact_info")
                                            )
                                    );
                                    movement.setCustomerName(ticketObject.getString("customer_name"));
                                    movement.setOrderNumber(String.valueOf(ticketObject.getInt("order_number")));
                                    movement.setOldBalance(ticketObject.getDouble("ar_initial_balance_ticket"));
                                    movement.setNewBalance(ticketObject.getDouble("ar_ending_balance_ticket"));
                                    movement.setNewExigible(ticketObject.getDouble("ar_ending_amount_due_ticket"));
                                    movement.setOldExigible(ticketObject.getDouble("ar_initial_amount_due_ticket"));
                                    movement.setReversable(ticketObject.getBoolean("mt_reversible"));
                                    movement.setComment(ticketObject.getString("mv_comments"));
                                } else {
                                    movement.setIsInvoice(true);
                                    movement.setInvoice(new Invoice(ticketObject, true));
                                }



                                mx.com.vndr.vndrapp.cobros.movements.MovementType type = new mx.com.vndr.vndrapp.cobros.movements.MovementType();
                                type.setCode(resumeObject.getString("code_type"));
                                type.setTypeTag(resumeObject.getString("tag_description"));
                                type.setType(resumeObject.getString("tag_type"));
                                movement.setType(type);


                                vndrLists.add(new VndrList(movement));
                            }

                            onMovementResponse.onSuccess(new VndrListAdapter(vndrLists));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onMovementResponse.onError("Ocurrió un error inesperado al procesar la solicitud");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            onMovementResponse.onError("Ocurrió un error inesperado al procesar la solicitud");
                        }
                    }
                });
    }


    public interface OnMovementResponse {
        void onSuccess(VndrListAdapter adapter);
        void onError(String error);
    }
}
