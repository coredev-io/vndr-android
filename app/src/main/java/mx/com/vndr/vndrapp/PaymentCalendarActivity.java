package mx.com.vndr.vndrapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.adapters.PaymentCalendarAdapter;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.OrderDetailActivity;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

import static android.view.View.GONE;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class PaymentCalendarActivity extends AppCompatActivity implements Toolbar.OnMenuItemClickListener{

    Toolbar toolbar;
    TextView alert, payment, numPayments, debt, dateAnticipo, amountAnticipo, plan;
    ConstraintLayout anticipoLayout;
    RecyclerView recyclerView;
    List<String> programmedPayments = new ArrayList<>();
    PaymentCalendarAdapter adapter;
    Button buttonContinue;
    Button buttonModify;

    Order order;
    boolean isPaid;
    boolean isReadMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_calendar);
        order = (Order) getIntent().getSerializableExtra("order");
        isPaid = getIntent().getBooleanExtra("isPaid", false);
        isReadMode = getIntent().getBooleanExtra("isReadMode", false);

        setupUI();

        if (isReadMode){
            buttonContinue.setVisibility(GONE);
            buttonModify.setVisibility(GONE);
            invalidateOptionsMenu();

            for (int i = 0; i < order.getScheduleInfo().size(); i++) {
                programmedPayments.add(order.getScheduleInfo().get(i).getDueDate());
                order.getCxc().setMontoPago(order.getScheduleInfo().get(i).getAmount());
            }
        }

        setupData();

    }


    private void setupData(){
        if (!isPaid){
            alert.setVisibility(View.VISIBLE);
            anticipoLayout.setVisibility(View.VISIBLE);
            dateAnticipo.setText(order.getCxc().getPayDateFormatt());
            amountAnticipo.setText(NumberFormat.getCurrencyInstance().format(order.getCxc().getAnticipo()));
        }
        System.out.println(order.getCxc().getMontoPago());
        payment.setText(NumberFormat.getCurrencyInstance().format(order.getCxc().getMontoPago()));
        debt.setText(NumberFormat.getCurrencyInstance().format(order.getCxc().getSaldoPlazo()));
        numPayments.setText(String.valueOf(order.getCxc().getNumPagos()));

        adapter = new PaymentCalendarAdapter(programmedPayments, NumberFormat.getCurrencyInstance().format(order.getCxc().getMontoPago()));

        if (!isReadMode)
            generaCalendario();

        recyclerView.setAdapter(adapter);
    }

    private void generaCalendario(){
        Date firstDate = order.getCxc().getFirstPayDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(firstDate);

        programmedPayments.add(new SimpleDateFormat("dd/MMM/yyyy").format(firstDate));

        for (int i =1; i < order.getCxc().getNumPagos();i++){


            switch (order.getCxc().getPayFrequency()){
                case 7:

                    calendar.add(Calendar.WEEK_OF_MONTH,1);
                    programmedPayments.add(new SimpleDateFormat("dd/MMM/yyyy").format(calendar.getTime()));
                    break;
                case 15:
                    if (calendar.get(Calendar.DAY_OF_MONTH) > 15){
                        calendar.set(Calendar.DAY_OF_MONTH,1);
                        calendar.add(Calendar.MONTH,1);
                    } else {
                        calendar.set(Calendar.DAY_OF_MONTH, 16);
                    }
                    programmedPayments.add(new SimpleDateFormat("dd/MMM/yyyy").format(calendar.getTime()));

                    break;
                case 30:

                    calendar.add(Calendar.MONTH,1);
                    programmedPayments.add(new SimpleDateFormat("dd/MMM/yyyy").format(calendar.getTime()));
                    break;
            }


        }

        adapter.notifyDataSetChanged();


    }
    private void setupUI(){
        toolbar = findViewById(R.id.tb_payment_cal);
        alert = findViewById(R.id.alert_payment_cal);
        anticipoLayout = findViewById(R.id.anticipo_payment_cal);
        payment = findViewById(R.id.pay_payment_cal);
        numPayments = findViewById(R.id.num_pay_payment_cal);
        debt = findViewById(R.id.plazo_payment_cal);
        dateAnticipo = findViewById(R.id.txt_date_anticipo);
        amountAnticipo = findViewById(R.id.txt_anticipo);
        plan = findViewById(R.id.txt_plan);
        recyclerView = findViewById(R.id.rv_payment_cal);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        alert.setVisibility(View.GONE);
        anticipoLayout.setVisibility(View.GONE);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        toolbar.setOnMenuItemClickListener(this);

        switch (order.getCxc().getPayFrequency()){
            case 7:
                plan.setText("Plan de pagos semanales");
                break;
            case 15:
                plan.setText("Plan de pagos quincenales");
                break;
            case 30:
                plan.setText("Plan de pagos mensuales");
                break;
        }
        buttonContinue = findViewById(R.id.btn_continue_payments);
        buttonModify = findViewById(R.id.btn_modify_payments);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }


    public void onContinue(View view){

        //  Validar que no sea un pedido histórico

        String lastDate = programmedPayments.get(programmedPayments.size() -1);

        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new SimpleDateFormat("dd/MMM/yyyy").parse(lastDate));
            String stringDateNow = VndrDateFormat.dateToString(new Date());
            Log.e("TAG", stringDateNow);
            Calendar calendarNow = Calendar.getInstance();
            calendarNow.setTime(new SimpleDateFormat("dd/MMM/yyyy").parse(stringDateNow));

            if (calendar.before(calendarNow)){
                String message = "Por el momento sólo se permite registrar pedidos totalmente pagados o con plan de pagos vigente. Si ya está liquidado, sugerimos cambiar la forma de pago a \"Pago Único\" por el importe total del pedido.";

                Dialogs.showAlert(this, message);
            } else {
                Log.e("TAG","AFTER");
                startActivityForResult(
                        new Intent(this, AdminCXCActivity.class)
                                .putExtra("order", order),
                        1
                );
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isReadMode)
            getMenuInflater().inflate(R.menu.share_menu, menu);
        return true;
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.menu_share){
            navigateToShareInformation();
        }
        return false;
    }

    private void navigateToShareInformation(){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setNotificationAlertData(order.getNotificationAlertData());

        alertFragment.show(getSupportFragmentManager(),"alert");
    }

    public void onModificar(View view){
        onBackPressed();
    }
}
