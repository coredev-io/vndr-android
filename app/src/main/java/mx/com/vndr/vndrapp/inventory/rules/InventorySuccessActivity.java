package mx.com.vndr.vndrapp.inventory.rules;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;

public class InventorySuccessActivity extends AppCompatActivity {

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_success);


        textView = findViewById(R.id.txt_inventory_success);

        textView.setText(String.valueOf(getIntent().getIntExtra("ticket", 0)));
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_OK);
        this.finish();
    }

    public void onClickFinish(View view){
        this.setResult(RESULT_OK);
        this.finish();
    }
}