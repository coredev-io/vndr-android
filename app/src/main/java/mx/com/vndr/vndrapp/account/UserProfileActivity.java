package mx.com.vndr.vndrapp.account;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;

import java.util.Date;

import mx.com.vndr.vndrapp.AppState;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.SignInActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.customviews.DatePickerTextView;
import mx.com.vndr.vndrapp.customviews.DropdownTextView;
import mx.com.vndr.vndrapp.customviews.EditTextPhoneNumber;
import mx.com.vndr.vndrapp.util.Dialogs;

public class UserProfileActivity extends AppCompatActivity implements TextWatcher, VndrUser.VndrUserCallback {

    TextInputLayout textInputLayoutFirstName;
    TextInputLayout textInputLayoutLastName;
    TextInputLayout textInputLayoutEmail;
    TextInputLayout textInputLayoutCellPhoneNumber;
    TextInputLayout textInputLayoutZipCode;
    EditText editTextFirstName;
    EditText editTextSecondName;
    EditText editTextFirstLastName;
    EditText editTextSecondLastName;
    EditText editTextEmail;
    EditTextPhoneNumber editTextPhoneNumber;
    EditTextPhoneNumber editTextCellPhoneNumber;
    EditText editTextZipCode;
    EditText editTextColonia;
    EditText editTextStreet;
    DropdownTextView autoCompleteTextViewSexo;
    DatePickerTextView datePickerTextView;

    Button button;
    Toolbar toolbar;

    boolean isReadMode;
    boolean isEditMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setupUI();
        displayCurrentData();
        setReadMode();
    }

    public void onButtonClick(View view){
        if (isReadMode){
            setEditMode();
        } else if (isEditMode) {
            UserProfile profile = UserProfile.getInstance();
            profile.setFirstName(editTextFirstName.getText().toString());
            profile.setFirstLastName(editTextFirstLastName.getText().toString());
            profile.setSecondName(editTextSecondName.getText().toString());
            profile.setSecondLastName(editTextSecondLastName.getText().toString());
            profile.setGender(autoCompleteTextViewSexo.getText().toString());
            profile.setBirthDay(datePickerTextView.getText().toString());
            profile.setEmail(editTextEmail.getText().toString());
            profile.setPhoneNumber(editTextPhoneNumber.getRawPhoneNumber());
            profile.setCellPhoneNumber(editTextCellPhoneNumber.getRawPhoneNumber());
            profile.setZipCode(editTextZipCode.getText().toString());
            profile.setColonia(editTextColonia.getText().toString());
            profile.setStreetAddress(editTextStreet.getText().toString());

            VndrAuth.getInstance().getCurrentUser().updateInfoUserWithUserProfile(this);
        }
    }

    private void setReadMode(){
        isReadMode = true;
        isEditMode = false;
        editTextFirstName.setEnabled(false);
        editTextSecondName.setEnabled(false);
        editTextFirstLastName.setEnabled(false);
        editTextSecondLastName.setEnabled(false);
        editTextEmail.setEnabled(false);
        editTextPhoneNumber.setEnabled(false);
        editTextCellPhoneNumber.setEnabled(false);
        editTextZipCode.setEnabled(false);
        editTextColonia.setEnabled(false);
        editTextStreet.setEnabled(false);
        autoCompleteTextViewSexo.setEnabled(false);
        datePickerTextView.setEnabled(false);
        button.setText("Editar información");
        button.setEnabled(true);
    }

    private void setEditMode(){
        isEditMode = true;
        isReadMode = false;
        editTextFirstName.setEnabled(true);
        editTextSecondName.setEnabled(true);
        editTextFirstLastName.setEnabled(true);
        editTextSecondLastName.setEnabled(true);
        editTextEmail.setEnabled(true);
        editTextPhoneNumber.setEnabled(true);
        editTextCellPhoneNumber.setEnabled(true);
        editTextZipCode.setEnabled(true);
        editTextColonia.setEnabled(true);
        editTextStreet.setEnabled(true);
        autoCompleteTextViewSexo.setEnabled(true);
        datePickerTextView.setEnabled(true);
        button.setText("Actualizar");
        button.setEnabled(true);
    }

    private void displayCurrentData(){
        UserProfile profile = UserProfile.getInstance();
        editTextFirstName.setText(profile.getFirstName());
        editTextSecondName.setText(profile.getSecondName());
        editTextFirstLastName.setText(profile.getFirstLastName());
        editTextSecondLastName.setText(profile.getSecondLastName());
        editTextEmail.setText(profile.getEmail());
        editTextPhoneNumber.setText(profile.getPhoneNumber());
        editTextCellPhoneNumber.setText(profile.getCellPhoneNumber());
        editTextZipCode.setText(profile.getZipCode());
        editTextColonia.setText(profile.getColonia());
        editTextStreet.setText(profile.getStreetAddress());
        autoCompleteTextViewSexo.setText(profile.getGender());
        datePickerTextView.setText(profile.getBirthDay());
    }

    private void setupUI(){

        toolbar = findViewById(R.id.tb_user_profile);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        textInputLayoutFirstName = findViewById(R.id.il_first_name);
        textInputLayoutLastName = findViewById(R.id.il_first_last_name);
        textInputLayoutEmail = findViewById(R.id.il_user_email);
        textInputLayoutCellPhoneNumber = findViewById(R.id.il_user_cell_phone);
        textInputLayoutZipCode = findViewById(R.id.il_user_zipcode);
        editTextFirstName = findViewById(R.id.et_first_name);
        editTextSecondName = findViewById(R.id.et_second_name);
        editTextFirstLastName = findViewById(R.id.et_first_last_name);
        editTextSecondLastName = findViewById(R.id.et_second_last_name);
        editTextEmail = findViewById(R.id.et_user_email);
        editTextPhoneNumber = findViewById(R.id.et_phone_number);
        editTextCellPhoneNumber = findViewById(R.id.et_cell_phone_number);
        editTextZipCode = findViewById(R.id.et_zipcode);
        editTextColonia = findViewById(R.id.et_colonia);
        editTextStreet = findViewById(R.id.et_street);
        autoCompleteTextViewSexo = findViewById(R.id.actv_sex);
        datePickerTextView = findViewById(R.id.dptv_birth_date);
        button = findViewById(R.id.btn_profile_user);
        button.setEnabled(false);
        autoCompleteTextViewSexo.setAdapter(R.array.sexo_profile_array);

        datePickerTextView.setFragmentManager(getSupportFragmentManager());
        datePickerTextView.getVndrDatePicker().setMaxDate(new Date());

        editTextFirstName.addTextChangedListener(this);
        editTextFirstLastName.addTextChangedListener(this);
        editTextEmail.addTextChangedListener(this);
        editTextCellPhoneNumber.addTextChangedListener(this);
        editTextZipCode.addTextChangedListener(this);

    }

    private TextInputLayout getCurrentTextInputLayout(){
        if (editTextFirstName.isFocused())
            return textInputLayoutFirstName;
        else if (editTextFirstLastName.isFocused())
            return textInputLayoutLastName;
        else if (editTextEmail.isFocused())
            return textInputLayoutEmail;
        else if (editTextCellPhoneNumber.isFocused())
            return textInputLayoutCellPhoneNumber;
        else if (editTextZipCode.isFocused())
            return textInputLayoutZipCode;
        else
            return new TextInputLayout(this);
    }

    private boolean areEmptyRequiredFields(){
        if (editTextFirstName.getText().toString().isEmpty())
            return true;
        else if (editTextFirstLastName.getText().toString().isEmpty())
            return true;
        else if (editTextEmail.getText().toString().isEmpty())
            return true;
        else if (editTextCellPhoneNumber.getText().toString().isEmpty())
            return true;
        else if (editTextZipCode.getText().toString().isEmpty())
            return true;
        else
            return false;
    }

    private boolean isValidaEmail(){
        if (editTextEmail.getText().toString().contains("@"))
            return true;
        else
            return false;
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.toString().isEmpty()){
            getCurrentTextInputLayout().setError("Este campo no puede estar vacío");
        } else
            getCurrentTextInputLayout().setError("");

        if (getCurrentTextInputLayout() == textInputLayoutEmail && !isValidaEmail()){
            textInputLayoutEmail.setError("Debes ingresar un email válido.");
        }


        if (areEmptyRequiredFields() || !isValidaEmail()){
            button.setEnabled(false);
        } else
            button.setEnabled(true);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onVndrUserCallbackSuccess() {
        setReadMode();
        if (!UserProfile.getInstance().emailWasUpdated())
            Dialogs.showAlert(this, "Los datos fueron actualizados con éxito.", (dialog, which) -> {
                onBackPressed();
            });
        else {
            Dialogs.showAlert(this, "Los datos fueron actualizados con éxito. Por seguridad es necesario que inicies sesión de nuevo.", (dialog, which) -> {
                logout();
            });
        }
    }

    private void logout(){
        VndrAuth.getInstance().logout(null);
        AppState.setAppState(this, AppState.NEW);
        VndrUser.setEmail("",this);
        VndrUser.setName("", this);
        Intent intent = new Intent(this, SignInActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onVndrUserCallbackError(String messageError) {
        Dialogs.showAlert(this, messageError);
    }
}
