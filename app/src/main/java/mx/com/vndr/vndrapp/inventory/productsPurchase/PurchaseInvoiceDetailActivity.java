package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_FINISH_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.OrderProductsFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.util.Dialogs;

public class PurchaseInvoiceDetailActivity extends AppCompatActivity implements VndrRequest.VndrResponse, View.OnClickListener  {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    Button button;
    TextView invoiceDate;
    TextView receptionDate;

    PurchaseInvoice purchaseInvoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_invoice_detail);
        setupUI();
        purchaseInvoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();
    }


    @Override
    protected void onStart() {
        super.onStart();
        getDetail();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_invoice_detail);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_invoice_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar = findViewById(R.id.pb_invoice_detail);

        button = findViewById(R.id.btn_invoice_detail);
        invoiceDate = findViewById(R.id.txt_date_complete_invoice);
        receptionDate = findViewById(R.id.txt_send_date_invoice);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void getDetail(){
        showLoading();
        new VndrRequest(Volley.newRequestQueue(this))
                .patch(URL_PURCHASE_INVOICE + "/" + purchaseInvoice.getPurchaseId() , this);
    }

    @Override
    public void error(String message) {
        dismissLoading();
        Dialogs.showAlert(this, message);
    }

    @Override
    public void success(String response) {
        dismissLoading();

        try {
            JSONObject jsonObject = new JSONObject(response).getJSONObject("info");
            purchaseInvoice = new PurchaseInvoice(jsonObject);
            purchaseInvoice.setTotalMerch();
            PurchaseInvoice.Selected.current.setPurchaseInvoice(purchaseInvoice);
            fillData();

            if (jsonObject.has("reception_status")) {
                if (!jsonObject.getBoolean("reception_status")) {
                    receptionDate.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_cancel_red),null,null,null);
                    button.setVisibility(View.VISIBLE);
                } else {
                    receptionDate.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_ok_green),null,null,null);
                    button.setVisibility(View.GONE);
                    receptionDate.setText("Fecha de recepción " + purchaseInvoice.getSendDateStr());
                }
            }

            invoiceDate.setText("Fecha de factura " + purchaseInvoice.getOrderDateStr());

        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud.");
        } catch (ParseException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud.");
        }
    }


    private void fillData(){
        List<String> valuesList = new ArrayList<>();
        List<String> keyList = new ArrayList<>();

        keyList.add("Proveedor");
        valuesList.add( purchaseInvoice.getSupplier().getName());

        keyList.add("Productos");
        valuesList.add(String.valueOf(purchaseInvoice.getProductList().size()));

        keyList.add("Total mercancía");
        valuesList.add(purchaseInvoice.getTotalMerchFormat());

        /*
        keyList.add("Cargo por envío");
        valuesList.add(purchaseInvoice.getShippingCostFormat());
         */

        keyList.add("Subtotal");
        valuesList.add(purchaseInvoice.getSubtotalAmount());

        keyList.add("IVA");
        valuesList.add(purchaseInvoice.getIvaAmount());

        keyList.add("Total de la factura");
        valuesList.add(purchaseInvoice.getTotalOrderAmount());

        keyList.add("Número de factura");
        valuesList.add(purchaseInvoice.getPurchaseNumber());

        keyList.add("Fecha de factura");
        valuesList.add(purchaseInvoice.getOrderDateStr());

        keyList.add("Fecha de entrega");
        valuesList.add(purchaseInvoice.getSendDateStr());

        keyList.add("Medio de pago");
        valuesList.add(purchaseInvoice.getPaymentMethod());

        keyList.add("Recompensas");
        valuesList.add(purchaseInvoice.getRewards());

        keyList.add("Tipo recompensa");
        valuesList.add(purchaseInvoice.getRewardType());

        PurchaseInvoiceDetailAdapter adapter = new PurchaseInvoiceDetailAdapter(valuesList, keyList, this);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onClick(View view) {
        new OrderProductsFragment(purchaseInvoice.getProductList()).show(getSupportFragmentManager(),"productlist");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    public void onClickStatus(View view) {
        startActivity(new Intent(this, SetReceptionDateActivity.class));
    }
}