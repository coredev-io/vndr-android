package mx.com.vndr.vndrapp.orders;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.abhinay.input.CurrencyEditText;
import mx.com.vndr.vndrapp.PickDateActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.ShoppingCartProductAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDERS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDER_DETAIL;

public class ShoppingCartActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    TextView customerName, totalOrder, emptyCart;
    CurrencyEditText shippingAmount;
    Button confirm;
    ProgressBar progressBar;
    Toolbar toolbar;



    Boolean isNewOrder;
    public Boolean orderWasUpdated = false;
    ShoppingCartProductAdapter adapter;
    List<ShoppingCartProduct> productList = new ArrayList<>();
    Order order;
    Double mTotalOrder = 0.00;
    Double mShippingPayAux = 0.00;
    int aux = 0;

    /*
        Override Methods
     */

    /*
        Happy PATH
     */

    //  Paso 0: Se inicia la actividad
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        setUpUI();

        isNewOrder = getIntent().getBooleanExtra("isNewOrder",true);

        if (isNewOrder){
            order = new Order();
            order.setCustomer((Customer) getIntent().getSerializableExtra("customer"));
            checkListProducts();

        } else {    //  Get order detail
            order = (Order) getIntent().getSerializableExtra("order");
            getOrderDetail(order.getIdOrder());
        }

        customerName.setText(order.getCustomer().getCustomerFullName());


    }

    //  Paso 1: Inicia el flujo para agregar un producto dando clic a addProduct
    public void onClickAddProduct(View view){
        startActivityForResult(new Intent(this, PickBrandActivity.class),0);
    }

    //  Paso 2: Se recibe el producto agregado y se añade a la lista.
    //  Iteraciones entre el paso 1 y 2 hasta que se agreguen los productos deseados
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            ShoppingCartProduct product = (ShoppingCartProduct) data.getSerializableExtra("product");
            productList.add(product);
            adapter.notifyDataSetChanged();
            orderWasUpdated = true;
            updateTotalOrderUI(true, product.getSubtotal());
            checkListProducts();
        } else if (requestCode == 1 && resultCode == RESULT_OK){
            this.finish();
        }
    }

    //  Paso 3: Se da clic en confirmar carrito
    public void onClickConfirmShoppingCart(View view){
        progressBar.setVisibility(View.VISIBLE);
        confirm.setEnabled(false);

        if (orderWasUpdated && isNewOrder){
            Log.e("TAG","Created");
            createOrder(false);
        } else if (orderWasUpdated && !isNewOrder){
            Log.e("TAG","Updated");
            updateOrder(false);
        } else{
            Log.e("TAG"," Not Updated");
            goToNextView(false);
        }

    }

    //  Paso 4: Se solicita crear el pedido en el back
    private void createOrder(final Boolean shouldDismiss){
        StringRequest request = new StringRequest(Request.Method.POST, URL_ORDERS , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                isNewOrder = false;
                System.out.println(response);

                progressBar.setVisibility(View.GONE);
                confirm.setEnabled(true);

                try {
                    JSONObject object = new JSONObject(response);
                    order.setIdOrder(object.getString("_id"));
                    order.setOrderAmount(object.getDouble("order_amount"));
                    order.setTotalOrderAmount(object.getDouble("total_order_amount"));
                    order.setOrderNumber(object.getInt("order_number"));
                    order.setShippingAmount(shippingAmount.getCleanDoubleValue());
                    order.setProducts(productList);
                    goToNextView(shouldDismiss);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ShoppingCartActivity.this, "Error al procesar la solicitud", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(new String(error.networkResponse.data));
                progressBar.setVisibility(View.GONE);
                confirm.setEnabled(true);
                try {
                    Toast.makeText(ShoppingCartActivity.this, new JSONObject(new String(error.networkResponse.data)).getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ShoppingCartActivity.this, "Ocurrió un error inesperado, por favor inténtalo de nuevo", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return createBody(false);
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    //  Paso 4.1: Se crea el body para el ws
    private byte[] createBody(boolean isUpdate){

        JSONObject map = new JSONObject();

        //JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {
            for (ShoppingCartProduct product: productList){
                JSONObject jsonProduct = new JSONObject();
                jsonProduct.put("product", product.getProduct().getIdProduct());
                jsonProduct.put("sale_price", product.getSalePrice());
                jsonProduct.put("products_amount", product.getProductsCount());
                jsonProduct.put("adjustment", product.getAdjustments());
                jsonProduct.put("notes", product.getNotes());
                jsonArray.put(jsonProduct);
            }
            if (!isUpdate)
                map.put("id_client",order.getCustomer().getIdCustomer());
            map.put("order_products",jsonArray);
            map.put("shipping_pay", String.valueOf(shippingAmount.getCleanDoubleValue()));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("TAG", map.toString());

        return map.toString().getBytes();
    }

    //  Paso 5: De acuerdo al caso lo llevamos a la vista siguiente
    private void goToNextView(Boolean shouldDismiss){
        progressBar.setVisibility(View.GONE);
        confirm.setEnabled(true);

        if(shouldDismiss){  // Go to active orders view
            setResult(RESULT_OK, new Intent().putExtra("shouldRefresh", true));
            finish();
        } else{
            Intent intent = new Intent(this, PickDateActivity.class);
            intent.putExtra("type", PickDateActivity.Type.SALE_DATE);
            intent.putExtra("order", order);
            startActivityForResult(intent, 1);
        }

    }

    /*
        Si no es una orden nueva
     */

    private void getOrderDetail(String idOrder){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_ORDER_DETAIL + idOrder, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(View.GONE);
                processOrderDetail(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    Toast.makeText(ShoppingCartActivity.this, new JSONObject(new String(error.networkResponse.data)).getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ShoppingCartActivity.this, "Ocurrió un error inesperado, por favor inténtalo de nuevo", Toast.LENGTH_LONG).show();
                }
                System.out.println(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void processOrderDetail(String response){
        try {
            JSONObject objectResponse = new JSONObject(response);
            JSONArray productsArray = objectResponse.getJSONArray("order_products");

            for (int i = 0; i < productsArray.length(); i++){

                JSONObject productObject = productsArray.getJSONObject(i).getJSONObject("product");

                Product product = new Product(
                        productObject.getString("_id"),
                        productObject.getString("sku"),
                        productObject.getString("product_name"),
                        productObject.getString("standard_classification"),
                        productObject.getString("notes"),
                        productObject.getString("sub_brand"),
                        productObject.getString("category"),
                        productObject.getString("subcategory"),
                        productObject.getString("product_key"),
                        productObject.getString("size"),
                        productObject.getString("color"),
                        productObject.getString("description"),
                        productObject.getString("url_image"),
                        productObject.getString("product_points"),
                        productObject.getString("barcode"),
                        productObject.getString("qrcode"),
                        productObject.getDouble("list_price"),
                        productObject.getBoolean("discontinued"),
                        new Catalog(
                                productObject.getJSONObject("catalog").getString("_id"),
                                productObject.getJSONObject("catalog").getString("catalog_name")
                        )
                );

                ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct(
                        product,
                        productsArray.getJSONObject(i).getDouble("sale_price"),
                        productsArray.getJSONObject(i).getDouble("adjustment"),
                        productsArray.getJSONObject(i).getDouble("subtotal"),
                        productsArray.getJSONObject(i).getInt("products_amount"),
                        productsArray.getJSONObject(i).getString("notes")
                );
                productList.add(shoppingCartProduct);
            }

            //shippingAmount.setText(String.valueOf(objectResponse.getDouble("shipping_pay")));
            if (objectResponse.getDouble("shipping_pay") != 0)
                shippingAmount.setText(new DecimalFormat("#.00").format(objectResponse.getDouble("shipping_pay")));

            totalOrder.setText(NumberFormat.getCurrencyInstance().format(objectResponse.getDouble("total_order_amount")));
            mTotalOrder = objectResponse.getDouble("total_order_amount");
            adapter.notifyDataSetChanged();
            order.setProducts(productList);
            order.setShippingAmount(objectResponse.getDouble("shipping_pay"));

            if (objectResponse.has("delivery")){
                Delivery delivery = new Delivery();
                JSONObject deliveryJSON = objectResponse.getJSONObject("delivery");
                delivery.setType(deliveryJSON.getString("delivery_type"));

                if (delivery.isPersonalDelivery() && deliveryJSON.has("estimate_delivery_date")){
                    delivery.setEstimateDeliveryDate(deliveryJSON.getString("estimate_delivery_date"));
                } else if (!delivery.isPersonalDelivery()){

                    if (deliveryJSON.has("estimate_delivery_date"))
                        delivery.setEstimateDeliveryDate(deliveryJSON.getString("estimate_delivery_date"));
                    if (deliveryJSON.has("messenger_service"))
                        delivery.setMessengerService(deliveryJSON.getString("messenger_service"));
                    if (deliveryJSON.has("traking_code"))
                        delivery.setTrackingCode(deliveryJSON.getString("traking_code"));
                    if (deliveryJSON.has("estimate_shipping_date"))
                        delivery.setEstimateShippingDate(deliveryJSON.getString("estimate_shipping_date"));
                }

                order.setDelivery(delivery);
            }

            /*
            if (objectResponse.has("delivery")){
                if (objectResponse.getJSONObject("delivery").has("personal_delivery")){

                    order.setDelivery(
                            new Delivery(
                                    true,
                                    objectResponse.getJSONObject("delivery").getString("estimate_delivery_date")
                            )
                    );

                } else if (objectResponse.getJSONObject("delivery").has("shipping_by_courier")){
                    order.setDelivery(
                            new Delivery(
                                    true,
                                    objectResponse.getJSONObject("delivery").getString("estimate_delivery_date"),
                                    objectResponse.getJSONObject("delivery").getString("estimate_shipping_date"),
                                    objectResponse.getJSONObject("delivery").getString("messenger_service"),
                                    objectResponse.getJSONObject("delivery").getString("traking_code")
                            )
                    );
                }
            }

             */
            checkListProducts();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shopping_cart_menu,menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (orderWasUpdated){
            Dialogs.showAlertQuestion(this, "¿Deseas guardar el pedido y continuar después?", new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    onClickSaveAndContinueLater();
                }
            }, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    ShoppingCartActivity.this.finish();
                }
            });
        } else this.finish();
    }

    public void onClickSaveAndContinueLater(){
        progressBar.setVisibility(View.VISIBLE);
        confirm.setEnabled(false);
        if (orderWasUpdated && isNewOrder){
            Log.e("TAG","Updated");
            //updateTotal(true, shippingAmount.getCleanDoubleValue());
            createOrder(true);
        } else if (!isNewOrder && orderWasUpdated){
            Log.e("TAG","Updated");
            //updateTotal(true, shippingAmount.getCleanDoubleValue());
            updateOrder(true);
        } else{
            Log.e("TAG"," Not Updated");
            goToNextView(true);
        }
    }


    public void onClickDeleteShoppingCart(){
        if (isNewOrder)
            this.finish();
        else{
            // delete from ws
            deleteOrder();
        }
    }


    /*
        WS Action Methods
     */


    private void updateOrder(final Boolean shouldDismiss){
        StringRequest request = new StringRequest(Request.Method.PUT, URL_ORDERS  + "/" + order.getIdOrder(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(View.GONE);
                confirm.setEnabled(true);
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    order.setOrderAmount(object.getDouble("order_amount"));
                    order.setTotalOrderAmount(object.getDouble("total_order_amount"));
                    order.setOrderNumber(object.getInt("order_number"));
                    order.setShippingAmount(shippingAmount.getCleanDoubleValue());
                    order.setProducts(productList);
                    goToNextView(shouldDismiss);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ShoppingCartActivity.this, "Ocurrió un error inesperado, al procesar la solicitud.", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                confirm.setEnabled(true);
                try {
                    Toast.makeText(ShoppingCartActivity.this, new JSONObject(new String(error.networkResponse.data)).getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ShoppingCartActivity.this, "Ocurrió un error inesperado, por favor inténtalo de nuevo", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                return createBody(true);
            }

            @Override
            public String getBodyContentType() {
                return "application/json";
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void deleteOrder(){
        StringRequest request = new StringRequest(Request.Method.DELETE, URL_ORDERS + "/" + order.getIdOrder() , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                ShoppingCartActivity.this.setResult(RESULT_OK, new Intent().putExtra("shouldRefresh", true));
                ShoppingCartActivity.this.finish();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(new String(error.networkResponse.data));
                try {
                    Toast.makeText(ShoppingCartActivity.this, new JSONObject(new String(error.networkResponse.data)).getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ShoppingCartActivity.this, "Ocurrió un error inesperado, por favor inténtalo de nuevo", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }


    /*
        UI Methods
     */

    private void setUpUI(){
        customerName = findViewById(R.id.name_order);
        totalOrder = findViewById(R.id.total_order);
        shippingAmount = findViewById(R.id.shipping_order);
        confirm = findViewById(R.id.btn_confirm_order);
        progressBar = findViewById(R.id.pb_shopping_cart);
        toolbar = findViewById(R.id.tb_shopping_cart);
        emptyCart = findViewById(R.id.txt_empty_cart);
        setSupportActionBar(toolbar);

        emptyCart.setVisibility(View.GONE);


        shippingAmount.setCurrency("$");
        shippingAmount.setDelimiter(false);
        shippingAmount.setSpacing(true);
        shippingAmount.setDecimals(true);

        recyclerView = findViewById(R.id.rv_shopping_cart_items);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ShoppingCartProductAdapter(productList, this);
        recyclerView.setAdapter(adapter);

        addListners();
    }

    private void addListners(){

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_delete_order){
                    Dialogs.showAlertQuestion(ShoppingCartActivity.this, "¿Estás seguro que ya no requieres este carrito?"
                            , new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    onClickDeleteShoppingCart();
                                }
                            });
                }
                return false;
            }
        });

        /*

        shippingAmount.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                shippingAmount.setSelection(shippingAmount.getText().toString().length());
                return false;
            }
        });

         */

        //shippingAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});

        shippingAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //shippingAmount.setText();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                orderWasUpdated = true;
                if (mTotalOrder != 0)
                    mTotalOrder = mTotalOrder - mShippingPayAux;
                mShippingPayAux = shippingAmount.getCleanDoubleValue();
                updateTotalOrderUI(true, mShippingPayAux);
                System.out.println(mShippingPayAux);
                //System.out.println(editable.toString());

            }
        });


    }

    public void updateTotalOrderUI(boolean isAdd, Double value){
        if (isAdd)
            mTotalOrder = mTotalOrder + value;
        else if (!isAdd && mTotalOrder != 0.00)
            mTotalOrder = mTotalOrder - value;

        totalOrder.setText(NumberFormat.getCurrencyInstance().format(mTotalOrder));
    }

    //  Si la lista esta vacia desabilita los botones de accion
    public void checkListProducts(){
        if (productList.isEmpty()){
            //save.setEnabled(false);
            confirm.setEnabled(false);
            emptyCart.setVisibility(View.VISIBLE);
        } else {
            //save.setEnabled(true);
            confirm.setEnabled(true);
            emptyCart.setVisibility(View.GONE);

        }
    }

}
