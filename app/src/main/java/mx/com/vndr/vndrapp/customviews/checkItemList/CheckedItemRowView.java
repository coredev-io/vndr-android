package mx.com.vndr.vndrapp.customviews.checkItemList;

import android.view.View;

public interface CheckedItemRowView {
    void setTitle(String title);
    void setChecked(Boolean isChecked);
    void setClickListener(View.OnClickListener listener);
}
