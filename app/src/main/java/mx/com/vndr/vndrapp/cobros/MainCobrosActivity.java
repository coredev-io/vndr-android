package mx.com.vndr.vndrapp.cobros;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.CxCQuery;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.cobros.movements.MovementsActivity;
import mx.com.vndr.vndrapp.cobros.notifications.NotificationsActivity;
import mx.com.vndr.vndrapp.customer.SelectCustomerActivity;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.models.Customer;

public class MainCobrosActivity extends AppCompatActivity {

    private final int PICK_CLIENT_REQUEST_CODE = 9;
    private Toolbar toolbar;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_cobros);
        setupUI();


    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();
        List<String> headers = Arrays.asList(getResources().getStringArray(R.array.menuCobrosHeaderTitle));

        optionList.add(new Option(headers.get(0), true));

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuCobrosOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToNotificacions));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCobrosOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToResume));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCobrosOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToMovimientos));


        optionList.add(new Option(headers.get(1), true));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCobrosOption4));
        //optionList.add(new Option(options.get(0), options.get(1), this::navigateToPickClient));
        optionList.add(new Option(options.get(0), options.get(1), () -> {
            MovementType.Query.current.setSelected(MovementType.Query.A);
            navigateToPickClient();
        }));

        options = Arrays.asList(getResources().getStringArray(R.array.menuCobrosOption5));
        //optionList.add(new Option(options.get(0), options.get(1), this::navigateToPickClient));

        optionList.add(new Option(options.get(0), options.get(1), () -> {
            MovementType.Query.current.setSelected(MovementType.Query.C);
            navigateToPickClient();
        }));

        return optionList;
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_main_cobros);
        recyclerView = findViewById(R.id.rv_main_cobros);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));

    }

    private void navigateToResume(){
        MovementType.Query.current.setSelected(null);
        startActivity(
                new Intent(
                        this,
                        ResumeCxcActivity.class
                ).putExtra("query", CxCQuery.GENERAL)
        );
    }

    private void navigateToPickClient(){

        startActivityForResult(
                new Intent(
                        this,
                        SelectCustomerActivity.class
                ),
                PICK_CLIENT_REQUEST_CODE
        );

    }

    private void navigateToCustomerResume(Customer customer){
        CxCQuery query = CxCQuery.CUSTOMER;
        query.setQueryParam(customer.getIdCustomer());
        startActivity(
                new Intent(
                        this,
                        ResumeCxcActivity.class
                ).putExtra("query", query)
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_CLIENT_REQUEST_CODE && resultCode == RESULT_OK){
            Customer customer = (Customer) data.getSerializableExtra("customer");
            navigateToCustomerResume(customer);
        }
    }

    private void navigateToMovimientos(){
        startActivity(
                new Intent(
                        this,
                        MovementsActivity.class
                )
        );

    }

    private void navigateToNotificacions(){
        startActivity(
                new Intent(
                        this,
                        NotificationsActivity.class
                )
        );
    }


}
