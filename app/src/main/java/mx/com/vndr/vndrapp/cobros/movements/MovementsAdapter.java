package mx.com.vndr.vndrapp.cobros.movements;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.card.MaterialCardView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;

public class MovementsAdapter extends RecyclerView.Adapter<MovementsAdapter.MoveViewHolder> {

    private List<CxCMovement> movementList;
    private OnMoveSelected onMoveSelected;
    private Context context;

    public MovementsAdapter(List<CxCMovement> movementList) {
        this.movementList = movementList;
    }

    public void setOnMoveSelected(OnMoveSelected onMoveSelected) {
        this.onMoveSelected = onMoveSelected;
    }

    @NonNull
    @Override
    public MoveViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new MoveViewHolder(
                LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_cxc_movement, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull MoveViewHolder holder, int position) {
        CxCMovement movement = movementList.get(position);

        holder.amount.setText(movement.getCurrencyAmount());
        holder.title.setText(movement.getDescription());
        holder.desc.setText(movement.getDateFormatted());
        holder.type.setText(movement.getType().getTypeTag());
        holder.createDate.setText(movement.getCreateDateFormatted());
        GradientDrawable shapeStatus = (GradientDrawable) holder.type.getBackground();
        int res;
        if (movement.getType().getType().equals(MovementType.Query.A.toString()))
            res = R.color.colorBlue;
        else
            res = R.color.colorRed;
        shapeStatus.setColor(context.getResources().getColor(res));
        holder.cardView.setOnClickListener(view -> {
            if (onMoveSelected != null)
                onMoveSelected.moveSelected(movement);
        });
    }

    @Override
    public int getItemCount() {
        return movementList.size();
    }

    class MoveViewHolder extends RecyclerView.ViewHolder{

        TextView title, desc, amount, type, createDate;
        MaterialCardView cardView;
        MoveViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.txt_mov_title);
            desc = itemView.findViewById(R.id.txt_mov_date);
            amount = itemView.findViewById(R.id.txt_mov_amount);
            cardView = itemView.findViewById(R.id.card_move);
            type = itemView.findViewById(R.id.txt_mov_type);
            createDate = itemView.findViewById(R.id.txt_create_date_mov);
        }
    }

    public interface OnMoveSelected {
        void moveSelected(CxCMovement movement);
    }
}
