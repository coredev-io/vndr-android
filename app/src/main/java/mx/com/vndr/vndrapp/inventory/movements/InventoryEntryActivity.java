package mx.com.vndr.vndrapp.inventory.movements;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_E_TICKET_TYPES;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.TicketType;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Product;

public class InventoryEntryActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    Toolbar toolbar;
    TextView brandName, catalog, productName, sku;
    TextView size, color, descripcion;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    Button button;
    RadioGroup radioGroup;

    Product product;

    ImageButton add, rm;
    EditText count, otherType;
    TextInputLayout textInputLayout;

    int c = 1; //   Products

    List<TicketType> typeList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_entry);
        product = (Product) getIntent().getSerializableExtra("product");
        setUpUI();
        getData();
    }

    private void setUpUI(){
        brandName = findViewById(R.id.txt_brand_name_entry);
        productName = findViewById(R.id.product_name_entry);
        catalog = findViewById(R.id.txt_catalog_name_entry);
        sku = findViewById(R.id.sku_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        progressBar = findViewById(R.id.pb_inventory_entry);
        linearLayout = findViewById(R.id.ll_inventory_entry);
        button = findViewById(R.id.btn_inventory_entry);
        add = findViewById(R.id.btn_add);
        rm = findViewById(R.id.btn_rm);
        count = findViewById(R.id.count_add_product);
        radioGroup = findViewById(R.id.rg_cause);
        otherType = findViewById(R.id.other_type);
        textInputLayout = findViewById(R.id.other);

        brandName.setText(product.getBrandName());
        catalog.setText(product.getCatalog().getCatalogName());
        productName.setText(product.getProductName());
        sku.setText(product.getProduct_key());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_inventory_entry);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        radioGroup.setOnCheckedChangeListener(this);

    }

    public void addProduct(View view){
        c += 1;
        count.setText(String.valueOf(c));
        updateBalance();
    }

    public void removeProduct(View view){
        if (c != 1){
            c -= 1;
            count.setText(String.valueOf(c));
            updateBalance();
        }
    }


    private void updateBalance(){

    }

    public void onClickButton(View view){
        TicketType ticketType = typeList.get(radioGroup.getCheckedRadioButtonId());

        if (ticketType.getCode().equals("T006")) {
            ticketType.setDescription(otherType.getText().toString());
        }

        startActivityForResult(new Intent(this, InventoryMovementReviewActivity.class)
                .putExtra("product", product)
                .putExtra("count", c)
                .putExtra("isEntry", true)
                .putExtra("type", ticketType)
                , 0
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK){
            this.finish();
        }
    }

    public void getData(){
        new VndrRequest(Volley.newRequestQueue(this))
                .get(URL_E_TICKET_TYPES, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {

                    }

                    @SuppressLint("ResourceType")
                    @Override
                    public void success(String response) {
                        try {

                            JSONArray jsonArray = new JSONArray(response);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                TicketType ticketType = new TicketType(jsonObject.getString("code"), jsonObject.getString("description"));
                                typeList.add(ticketType);

                                RadioButton radioButton = new RadioButton(InventoryEntryActivity.this);
                                radioButton.setText(ticketType.getDescription());

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    radioButton.setTextAppearance(R.style.text_bold);
                                }

                                radioButton.setId(i);
                                RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.WRAP_CONTENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                                radioGroup.addView(radioButton, params);
                            }

                            radioGroup.check(0);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (typeList.get(i).getCode().equals("T006")) {
            textInputLayout.setVisibility(View.VISIBLE);
        } else {
            textInputLayout.setVisibility(View.GONE);
            otherType.setText("");
        }
    }
}