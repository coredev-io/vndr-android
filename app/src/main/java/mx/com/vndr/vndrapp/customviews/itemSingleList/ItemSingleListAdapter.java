package mx.com.vndr.vndrapp.customviews.itemSingleList;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;

public class ItemSingleListAdapter extends RecyclerView.Adapter<ItemSingleListViewHolder> {

    private List<String> items;
    private OnSingleListClickItemListener listener;

    public ItemSingleListAdapter(List<String> items, OnSingleListClickItemListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ItemSingleListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemSingleListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_single_list,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ItemSingleListViewHolder holder, final int position) {
        holder.textView.setText(items.get(position));
        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickListener(items.get(position), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
