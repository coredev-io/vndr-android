package mx.com.vndr.vndrapp.inventory.movements;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_TICKET_ARQUEO;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_TICKET_ENTRY;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_TICKET_EXIT;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.TicketType;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class InventoryMovementReviewActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView brandName, catalog, productName, sku;
    TextView size, color, descripcion;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    Button button;
    TextView date, initStock, movement, currentStock;
    TextView cost, total, reason, notes;

    Product product;
    TicketType ticketType;
    boolean isEntry = false;
    boolean isArqueo = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventory_movement_review);
        product = (Product) getIntent().getSerializableExtra("product");
        ticketType = (TicketType) getIntent().getSerializableExtra("type");
        isEntry = getIntent().getBooleanExtra("isEntry", true);
        isArqueo = getIntent().getBooleanExtra("isArqueo", false);
        setUpUI();
        fillData();
    }

    private void setUpUI(){
        brandName = findViewById(R.id.txt_brand_name_review);
        productName = findViewById(R.id.product_name_review);
        catalog = findViewById(R.id.txt_catalog_name_review);
        sku = findViewById(R.id.sku_add_product);
        size = findViewById(R.id.size_add);
        color = findViewById(R.id.color_add);
        descripcion = findViewById(R.id.desc);
        progressBar = findViewById(R.id.pb_inventory_review);
        linearLayout = findViewById(R.id.ll_inventory_review);
        button = findViewById(R.id.btn_inventory_review);
        date = findViewById(R.id.date_mv_review);
        initStock = findViewById(R.id.init_stock_mv_review);
        movement = findViewById(R.id.mv_review);
        currentStock = findViewById(R.id.current_stock_mv_review);
        cost = findViewById(R.id.cost_mv_review);
        total = findViewById(R.id.total_mv_review);
        reason = findViewById(R.id.type);
        notes = findViewById(R.id.notes);


        brandName.setText(product.getBrandName());
        catalog.setText(product.getCatalog().getCatalogName());
        productName.setText(product.getProductName());
        sku.setText(product.getProduct_key());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            descripcion.setText(product.getDescription());
            descripcion.setVisibility(View.VISIBLE);
        }

        toolbar = findViewById(R.id.tb_inventory_review);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

    }

    public void onClickButton(View view){
        if (isArqueo) {
            setArqueo();
            return;
        }

        if (isEntry) {
            addUnits();
        } else {
            removeUnits();
        }
    }

    public void fillData(){
        date.setText(VndrDateFormat.dateToString(new Date()));
        initStock.setText(String.valueOf(product.getInventory().getCurrentStock()));
        movement.setText(
                isArqueo ?
                String.valueOf(getIntent().getIntExtra("count", 0) - product.getInventory().getCurrentStock()) :
                String.valueOf(getIntent().getIntExtra("count", 0))
        );
        int stock = isEntry ? product.getInventory().getCurrentStock() + getIntent().getIntExtra("count", 0) :
                product.getInventory().getCurrentStock() - getIntent().getIntExtra("count", 0);

        if (isArqueo) {
            stock = getIntent().getIntExtra("count", 0);
        }

        currentStock.setText(String.valueOf(
                stock
        ));
        cost.setText(
                NumberFormat.getCurrencyInstance().format(
                        product.getInventory().getAcquisitionCost()
                )
        );
        total.setText(
                NumberFormat.getCurrencyInstance().format(
                        product.getInventory().getAcquisitionCost() * getIntent().getIntExtra("count", 0)
                )
        );
        reason.setText(ticketType.getDescription());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK){
            this.setResult(RESULT_OK);
            this.finish();
        }
    }

    public void onClickBack(View view){
        this.finish();
    }

    private void addUnits(){

        Map<String, String> map = new HashMap<>();

        map.put("product_id",product.getIdProduct());
        map.put("units", String.valueOf(getIntent().getIntExtra("count", 0)));
        map.put("type_code", ticketType.getCode());
        map.put("type_description_other", ticketType.getDescription());
        map.put("date",VndrDateFormat.dateToWSFormat(new Date()));
        map.put("notes",notes.getText().toString());

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(map)
                .post(URL_TICKET_ENTRY, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Dialogs.showAlert(InventoryMovementReviewActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            startActivityForResult(new Intent(InventoryMovementReviewActivity.this, MovementSuccessActivity.class)
                                    .putExtra("ticket", jsonObject.getInt("ticket")), 0
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void setArqueo(){

        Map<String, String> map = new HashMap<>();

        map.put("product_id",product.getIdProduct());
        map.put("units", currentStock.getText().toString());
        map.put("date",VndrDateFormat.dateToWSFormat(new Date()));
        map.put("notes",notes.getText().toString());

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(map)
                .post(URL_TICKET_ARQUEO, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Dialogs.showAlert(InventoryMovementReviewActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response).getJSONObject("ticket");
                            startActivityForResult(new Intent(InventoryMovementReviewActivity.this, MovementSuccessActivity.class)
                                    .putExtra("ticket", jsonObject.getInt("ticket")), 0
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void removeUnits(){

        Map<String, String> map = new HashMap<>();

        map.put("product_id",product.getIdProduct());
        map.put("units", String.valueOf(getIntent().getIntExtra("count", 0)));
        map.put("type_code", ticketType.getCode());
        map.put("type_description_other", ticketType.getDescription());
        map.put("date",VndrDateFormat.dateToWSFormat(new Date()));
        map.put("notes",notes.getText().toString());

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(map)
                .post(URL_TICKET_EXIT, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        Dialogs.showAlert(InventoryMovementReviewActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            startActivityForResult(new Intent(InventoryMovementReviewActivity.this, MovementSuccessActivity.class)
                                    .putExtra("ticket", jsonObject.getInt("ticket")), 0
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}