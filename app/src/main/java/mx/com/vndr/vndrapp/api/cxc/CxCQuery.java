package mx.com.vndr.vndrapp.api.cxc;

public enum CxCQuery {
    GENERAL, CUSTOMER;

    private String queryParam;

    public String getQueryParam() {
        return queryParam;
    }

    public void setQueryParam(String queryParam) {
        this.queryParam = queryParam;
    }
}
