package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mx.com.vndr.vndrapp.PickDateActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;

public class POSuccessActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textViewOrderNumber;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_o_success);
        setupUI();
    }

    private void setupUI(){

        textViewOrderNumber = findViewById(R.id.po_number_success);

        textViewOrderNumber.setText(ShoppingOrder.Selected.current.getShoppingOrder().getPurchaseNumber());
    }

    public void onClickFinish(View view){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setOnClickListener(this);
        alertFragment.setNotificationAlertData(
                ShoppingOrder.Selected.current.getShoppingOrder().getNotificationAlertData()
        );
        alertFragment.show(getSupportFragmentManager(),"notifications");

    }

    @Override
    public void onClick(View view) {
        startActivityForResult(
                new Intent(
                        this,
                        PickOrderDateActivity.class)
                        .putExtra("isSendDate", true),
                1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1  && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {

    }
}