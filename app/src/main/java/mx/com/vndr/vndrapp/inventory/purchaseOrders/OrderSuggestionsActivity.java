package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders.OnSuggestionsResponse;
import mx.com.vndr.vndrapp.api.inventory.SuggestionProducts;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.SuggestionsAdapter.OnSuggestionSelected;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class OrderSuggestionsActivity extends AppCompatActivity implements OnSuggestionsResponse, OnSuggestionSelected {

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_suggestions);
        setupUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showLoading();
        recyclerView.setAdapter(null);
        new APIShoppingOrders(Volley.newRequestQueue(this))
                .getProductSuggestions(this);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_order_suggestions);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        recyclerView = findViewById(R.id.rv_order_suggestions);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressBar = findViewById(R.id.pb_suggestions_po);
        textView = findViewById(R.id.txt_empty_po);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
        textView.setVisibility(View.GONE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    public void onClickBuyProduct(View view){
        startActivityForResult(new Intent(this, PickBrandActivity.class).putExtra("shoppingOrder", true),0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            this.setResult(resultCode, data);
            this.finish();
        }
    }

    @Override
    public void onSuccess(List<SuggestionProducts> products) {
        dismissLoading();
        recyclerView.setAdapter(new SuggestionsAdapter(products).setOnSuggestionSelected(this));
    }

    @Override
    public void onError(String messageError) {
        dismissLoading();
        textView.setVisibility(View.VISIBLE);
        textView.setText(messageError);
    }

    @Override
    public void onSuggestionSelected(SuggestionProducts product) {
        startActivityForResult(new Intent(this, ProductQuantityActivity.class).putExtra("productSuggestion", product),0);
    }
}