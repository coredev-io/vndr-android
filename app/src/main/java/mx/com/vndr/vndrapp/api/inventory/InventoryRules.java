package mx.com.vndr.vndrapp.api.inventory;

import org.json.JSONException;
import org.json.JSONObject;

public class InventoryRules {

    boolean registerIsValid = false;
    int currentStock = 0;
    int availableStock = 0;
    int inventoryAdjustment = 0;
    int optimalConfiguration = 0;
    int buyBackConfiguration = 0;
    int minimunConfiguration = 0;
    int initialStock = 0;
    int initialCalculatedStock = 0;
    int initialAdjustedStock = 0;
    double acquisitionCost = 0;
    double iva = 0;
    double acquisitionTotal = 0;
    double acquisitionCostInitial = 0;
    double acquisitionTotalInitial = 0;

    public InventoryRules(JSONObject jsonObject) throws JSONException {
        registerIsValid = jsonObject.getBoolean("valid_register");
        currentStock = jsonObject.getInt("current_stock");
        availableStock = jsonObject.getInt("available_stock");
        optimalConfiguration = jsonObject.getInt("config_optimum");
        buyBackConfiguration = jsonObject.getInt("config_buy_back");
        minimunConfiguration = jsonObject.getInt("config_minimal");
        initialStock = jsonObject.getInt("initial_stock");
        initialCalculatedStock = jsonObject.getInt("initial_calculated_stock");

        if (registerIsValid) {
            inventoryAdjustment = availableStock - currentStock;
            acquisitionCost = jsonObject.getDouble("acquisition_cost");
            iva = jsonObject.getDouble("acquisition_iva");
            acquisitionTotal = jsonObject.getDouble("acquisition_total");
            acquisitionCostInitial = jsonObject.getDouble("acquisition_cost_initial");
            acquisitionTotalInitial = jsonObject.getDouble("acquisition_total_initial");
            initialAdjustedStock = jsonObject.getInt("initial_adjusted_stock");

        } else {
            //inventoryAdjustment = availableStock - 0;
            acquisitionTotal = acquisitionCost+ iva;
            acquisitionCostInitial = acquisitionCost * availableStock;
            acquisitionTotalInitial = acquisitionTotal * availableStock;
        }

    }

    public boolean isRegisterIsValid() {
        return registerIsValid;
    }

    public int getCurrentStock() {
        return currentStock;
    }

    public int getAvailableStock() {
        return availableStock;
    }

    public int getInventoryAdjustment() {
        return inventoryAdjustment;
    }

    public int getOptimalConfiguration() {
        return optimalConfiguration;
    }

    public int getBuyBackConfiguration() {
        return buyBackConfiguration;
    }

    public int getMinimunConfiguration() {
        return minimunConfiguration;
    }

    public double getAcquisitionCost() {
        return acquisitionCost;
    }

    public double getIva() {
        return iva;
    }

    public double getAcquisitionTotal() {
        return acquisitionTotal;
    }

    public double getAcquisitionCostInitial() {
        return acquisitionCostInitial;
    }

    public double getAcquisitionTotalInitial() {
        return acquisitionTotalInitial;
    }

    public int getInitialStock() {
        return initialStock;
    }

    public int getInitialCalculatedStock() {
        return initialCalculatedStock;
    }

    public int getInitialAdjustedStock() {
        return initialAdjustedStock;
    }
}
