package mx.com.vndr.vndrapp.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.NumberFormat;

public class ShoppingCartProduct implements Serializable {
    Product product;
    Double salePrice, adjustments, subtotal;
    int productsCount;
    String notes;
    Double buyPrice;

    public ShoppingCartProduct(Product product, Double salePrice, Double adjustments, Double subtotal, int productsCount, String notes) {
        this.product = product;
        this.salePrice = salePrice;
        this.adjustments = adjustments;
        this.subtotal = subtotal;
        this.productsCount = productsCount;
        this.notes = notes;
    }

    public ShoppingCartProduct(Product product, int productsCount, String notes, Double buyPrice, Double subtotal) {
        this.product = product;
        this.productsCount = productsCount;
        this.notes = notes;
        this.buyPrice = buyPrice;
        this.subtotal = subtotal;
    }

    //  For shoppingOrders
    public ShoppingCartProduct(JSONObject jsonShoppingOrderObject) throws JSONException {
        this.product = new Product(jsonShoppingOrderObject.getJSONObject("product"));
        this.productsCount = jsonShoppingOrderObject.getInt("quantity");
        this.notes = jsonShoppingOrderObject.getString("notes");
        this.buyPrice = jsonShoppingOrderObject.getDouble("purchase_price");
        this.subtotal =jsonShoppingOrderObject.getDouble("total_cost");
    }


    public Product getProduct() {
        return product;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public String getSalePriceStr(){
        return NumberFormat.getCurrencyInstance().format(salePrice);
    }

    public Double getAdjustments() {
        return adjustments;
    }

    public String getAdjustmentsStr(){
        return NumberFormat.getCurrencyInstance().format(adjustments);
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public String getSubtotalString(){
        return NumberFormat.getCurrencyInstance().format(subtotal);
    }

    public int getProductsCount() {
        return productsCount;
    }

    public String getNotes() {
        return notes;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public String getBuyPriceStr(){
        return NumberFormat.getCurrencyInstance().format(buyPrice);
    }
}
