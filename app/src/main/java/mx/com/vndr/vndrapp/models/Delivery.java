package mx.com.vndr.vndrapp.models;

import androidx.annotation.NonNull;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Delivery implements Serializable {
    Boolean isPersonalDelivery, isShippingByCourier;
    Date estimateDeliveryDate, estimateShippingDate;
    String messengerService, trackingCode, type;

    public Delivery() {
    }

    public Delivery(Boolean isPersonalDelivery, Date estimateDeliveryDate) {
        this.isPersonalDelivery = isPersonalDelivery;
        this.estimateDeliveryDate = estimateDeliveryDate;
    }

    public Delivery(Boolean isShippingByCourier, Date estimateDeliveryDate, Date estimateShippingDate, String messengerService, String trackingCode) {
        this.isShippingByCourier = isShippingByCourier;
        this.estimateDeliveryDate = estimateDeliveryDate;
        this.estimateShippingDate = estimateShippingDate;
        this.messengerService = messengerService;
        this.trackingCode = trackingCode;
    }

    public Delivery(String type) {
        this.type = type;
    }

    public Delivery(Boolean isPersonalDelivery, @NonNull String estimateDeliveryDate) {
        this.isPersonalDelivery = isPersonalDelivery;
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date d = null;
        try
        {
            d = input.parse(estimateDeliveryDate);
            this.estimateDeliveryDate = d;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public Delivery(Boolean isShippingByCourier, @NonNull String estimateDeliveryDate, @NonNull String estimateShippingDate, String messengerService, String trackingCode) {
        this.isShippingByCourier = isShippingByCourier;
        this.messengerService = messengerService;
        this.trackingCode = trackingCode;


        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date d = null;
        try
        {
            d = input.parse(estimateDeliveryDate);
            this.estimateDeliveryDate = d;
            this.estimateShippingDate = d;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public Boolean getPersonalDelivery() {
        return isPersonalDelivery;
    }

    public void setPersonalDelivery(Boolean personalDelivery) {
        isPersonalDelivery = personalDelivery;
    }

    public Boolean getShippingByCourier() {
        return isShippingByCourier;
    }

    public void setShippingByCourier(Boolean shippingByCourier) {
        isShippingByCourier = shippingByCourier;
    }

    public Date getEstimateDeliveryDate() {
        return estimateDeliveryDate;
    }

    public String getEstimateDeliveryDateFormatt(){
        SimpleDateFormat output = new SimpleDateFormat("dd/MMM/yyyy");
        return output.format(getEstimateDeliveryDate());
    }

    public void setEstimateDeliveryDate(Date estimateDeliveryDate) {
        this.estimateDeliveryDate = estimateDeliveryDate;
    }

    public void setEstimateDeliveryDate(String estimateDeliveryDate) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date d = null;
        try
        {
            d = input.parse(estimateDeliveryDate);
            this.estimateDeliveryDate = d;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public Date getEstimateShippingDate() {
        return estimateShippingDate;
    }

    public String getEstimateShippingDateFormatt(){
        SimpleDateFormat output = new SimpleDateFormat("dd/MMM/yyyy");
        return output.format(getEstimateShippingDate());
    }

    public void setEstimateShippingDate(Date estimateShippingDate) {
        this.estimateShippingDate = estimateShippingDate;
    }

    public void setEstimateShippingDate(String estimateShippingDate) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date d = null;
        try
        {
            d = input.parse(estimateShippingDate);
            this.estimateShippingDate = d;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
    }

    public String getMessengerService() {
        return messengerService;
    }

    public void setMessengerService(String messengerService) {
        this.messengerService = messengerService;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isPersonalDelivery(){
        return getType().equals("personal");
    }

}
