package mx.com.vndr.vndrapp.api.cxc;

import android.util.Log;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.cobros.movements.NewMovement;
import mx.com.vndr.vndrapp.cobros.notifications.Notification;
import mx.com.vndr.vndrapp.cobros.notifications.NotificationType;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList3;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListType;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_ACTIVE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_ACTIVE_CUSTOMER;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_COMPLETED;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_COMPLETED_CUSTOMER;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_DETAIL;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_MOVEMENT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_MOVEMENT_REVERSE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_MOVEMENT_TYPE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_NOTIFICATIONS;

import androidx.annotation.NonNull;

public class APIVndrCxc {

    private final static String TAG = "APIVndrCxc";

    private RequestQueue queue;

    public APIVndrCxc(RequestQueue queue) {
        this.queue = queue;
    }

    public void getActiveCxcList(CXCResponse cxcResponse){
        new VndrRequest(queue)
                .get(URL_CXC_ACTIVE, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        cxcResponse.cxcError(message);
                    }

                    @Override
                    public void success(String response) {
                        List<VndrList> vndrLists = new ArrayList<>();
                        try {
                            JSONObject object = new JSONObject(response);
                            boolean isEmpty = true;

                            JSONArray resumeJSONArray = object.getJSONArray("resume");

                            for (int i = 0; i < resumeJSONArray.length() ; i++) {
                                JSONObject resumeObject = resumeJSONArray.getJSONObject(i);

                                vndrLists.add(
                                        new VndrList(
                                                new SimpleList3(
                                                        resumeObject.getString("status"),
                                                        String.valueOf(resumeObject.getInt("count")),
                                                        NumberFormat.getCurrencyInstance().format(
                                                                resumeObject.getDouble("amount")
                                                        )
                                                )
                                        )
                                );

                                if (resumeObject.getString("status").equals("Cuentas con retraso"))
                                    vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                if (resumeObject.getString("status").equals("Retraso grave"))
                                    vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                if (resumeObject.getInt("count") != 0)
                                    isEmpty = false;


                            }

                            JSONArray detailJSONArray = object.getJSONArray("detail");

                            if (detailJSONArray.length() == 0){
                                cxcResponse.cxcError("El usuario no cuenta con cuentas por cobrar activas");
                                return;
                            }


                            for (int i = 0; i < detailJSONArray.length(); i++) {
                                JSONObject sectionObject = detailJSONArray.getJSONObject(i);

                                JSONArray cxcJSONArray = sectionObject.getJSONArray("cxcs");

                                if (cxcJSONArray.length() != 0)
                                    vndrLists.add(new VndrList(sectionObject.getString("status")));

                                for (int j = 0; j < cxcJSONArray.length(); j++) {
                                    JSONObject cxcObject = cxcJSONArray.getJSONObject(j);

                                    CxCCard cxc= new CxCCard(cxcObject.getString("_id"));
                                    cxc.setStatus(cxcObject.getString("status"));

                                    cxc.setCustomerName(cxcObject.getString("customer_name"));
                                    cxc.setOrderNumber(cxcObject.getInt("order_number"));
                                    cxc.setBillingDate(cxcObject.getString("billing_date"));
                                    cxc.setDueDate(cxcObject.getString("due_date"));
                                    cxc.setTotalAmount(cxcObject.getDouble("total_amount"));
                                    cxc.setBalanceOverdue(cxcObject.getDouble("balance_overdue"));
                                    cxc.setBalanceDue(cxcObject.getDouble("balance_due"));
                                    cxc.setPmtAmount(cxcObject.getDouble("ar_pmt_amount"));

                                    vndrLists.add(new VndrList(cxc));
                                }
                            }

                            if (isEmpty)
                                cxcResponse.cxcError("Aún no tienes cuentas por cobrar activas");
                            else
                                cxcResponse.cxcSuccess(vndrLists);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        }
                    }
                });
    }

    public void getCustomerActiveCxcList(CXCResponse cxcResponse, String customerId){
        new VndrRequest(queue)
                .get(URL_CXC_ACTIVE_CUSTOMER + customerId, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        cxcResponse.cxcError(message);
                    }

                    @Override
                    public void success(String response) {
                        List<VndrList> vndrLists = new ArrayList<>();

                        try {
                            boolean isEmpty = true;
                            JSONObject object = new JSONObject(response);

                            JSONArray resumeJSONArray = object.getJSONArray("resume");

                            for (int i = 0; i < resumeJSONArray.length() ; i++) {
                                JSONObject resumeObject = resumeJSONArray.getJSONObject(i);

                                vndrLists.add(
                                        new VndrList(
                                                new SimpleList3(
                                                        resumeObject.getString("status"),
                                                        String.valueOf(resumeObject.getInt("count")),
                                                        NumberFormat.getCurrencyInstance().format(
                                                                resumeObject.getDouble("amount")
                                                        )
                                                )
                                        )
                                );

                                if (resumeObject.getString("status").equals("Cuentas con retraso"))
                                    vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                                if (resumeObject.getString("status").equals("Retraso grave"))
                                    vndrLists.add(new VndrList(VndrListType.SEPARATOR));


                                if (resumeObject.getInt("count") != 0)
                                    isEmpty = false;
                            }

                            JSONArray detailJSONArray = object.getJSONArray("detail");

                            if (detailJSONArray.length() == 0){
                                cxcResponse.cxcError("El cliente no cuenta con cuentas por cobrar activas");
                                return;
                            }

                            for (int i = 0; i < detailJSONArray.length(); i++) {
                                JSONObject sectionObject = detailJSONArray.getJSONObject(i);

                                JSONArray cxcJSONArray = sectionObject.getJSONArray("cxcs");

                                if (cxcJSONArray.length() != 0)
                                    vndrLists.add(new VndrList(sectionObject.getString("status")));

                                for (int j = 0; j < cxcJSONArray.length(); j++) {
                                    JSONObject cxcObject = cxcJSONArray.getJSONObject(j);

                                    CxCCard cxc= new CxCCard(cxcObject.getString("_id"));
                                    cxc.setStatus(cxcObject.getString("status"));
                                    cxc.setCustomerName(cxcObject.getString("customer_name"));
                                    cxc.setOrderNumber(cxcObject.getInt("order_number"));
                                    cxc.setBillingDate(cxcObject.getString("billing_date"));
                                    cxc.setDueDate(cxcObject.getString("due_date"));                                    cxc.setTotalAmount(cxcObject.getDouble("total_amount"));
                                    cxc.setBalanceOverdue(cxcObject.getDouble("balance_overdue"));
                                    cxc.setBalanceDue(cxcObject.getDouble("balance_due"));
                                    cxc.setPmtAmount(cxcObject.getDouble("ar_pmt_amount"));

                                    vndrLists.add(new VndrList(cxc));
                                }
                            }

                            if (isEmpty)
                                cxcResponse.cxcError("El cliente no tiene cuentas por cobrar activas");
                            else
                                cxcResponse.cxcSuccess(vndrLists);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        }
                    }
                });
    }

    public void getCompledtCxcList(CXCResponse cxcResponse){
        new VndrRequest(queue)
                .get(URL_CXC_COMPLETED, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        cxcResponse.cxcError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            List<VndrList> vndrLists = new ArrayList<>();
                            JSONObject responseObject = new JSONObject(response);

                            if (responseObject.has("statusCode") && responseObject.getInt("statusCode") == 404){
                                cxcResponse.cxcError(responseObject.getString("message"));
                                return;
                            }

                            JSONArray detailArray = responseObject.getJSONArray("detail");

                            if (detailArray.getJSONObject(0).getInt("count") == 0
                                    && detailArray.getJSONObject(1).getInt("count") == 0
                                    && detailArray.getJSONObject(2).getInt("count") == 0 ){
                                cxcResponse.cxcError("Aún no tienes cuentas por cobrar al corriente, liquidadas o quebrantadas.");
                                return;
                            }

                            for (int i = 0; i < detailArray.length(); i++) {
                                JSONObject sectionObject = detailArray.getJSONObject(i);
                                JSONArray cxcJSONArray = sectionObject.getJSONArray("cxcs");

                                if (cxcJSONArray.length() != 0)
                                    vndrLists.add(new VndrList(sectionObject.getString("status")));

                                for (int j = 0; j <  cxcJSONArray.length(); j++) {
                                    JSONObject cxcObject = cxcJSONArray.getJSONObject(j);

                                    CxCCard cxc= new CxCCard(cxcObject.getString("_id"));
                                    cxc.setStatus(cxcObject.getString("status"));
                                    cxc.setCustomerName(cxcObject.getString("customer_name"));
                                    cxc.setOrderNumber(cxcObject.getInt("order_number"));
                                    if (cxcObject.has("billing_date"))
                                        cxc.setBillingDate(cxcObject.getString("billing_date"));
                                    if (cxcObject.has("due_date"))
                                        cxc.setDueDate(cxcObject.getString("due_date"));
                                    cxc.setTotalAmount(cxcObject.getDouble("total_amount"));
                                    cxc.setBalanceOverdue(cxcObject.getDouble("balance_overdue"));
                                    cxc.setBalanceDue(cxcObject.getDouble("balance_due"));
                                    cxc.setPmtAmount(cxcObject.getDouble("ar_pmt_amount"));

                                    vndrLists.add(new VndrList(cxc));
                                }

                            }
                            cxcResponse.cxcSuccess(vndrLists);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        }
                    }
                });
    }

    public void getCustomerCompledtCxcList(CXCResponse cxcResponse, String customerId){
        new VndrRequest(queue)
                .get(URL_CXC_COMPLETED_CUSTOMER + customerId, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        cxcResponse.cxcError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            List<VndrList> vndrLists = new ArrayList<>();
                            JSONObject responseObject = new JSONObject(response);

                            if (responseObject.has("statusCode") && responseObject.getInt("statusCode") == 404){
                                cxcResponse.cxcError(responseObject.getString("message"));
                                return;
                            }

                            JSONArray detailArray = responseObject.getJSONArray("detail");

                            if (detailArray.getJSONObject(0).getInt("count") == 0
                            && detailArray.getJSONObject(1).getInt("count") == 0
                            && detailArray.getJSONObject(2).getInt("count") == 0 ){
                                cxcResponse.cxcError("El cliente no cuenta con cuentas por cobrar al corriente, liquidadas o quebrantadas.");
                                return;
                            }


                            for (int i = 0; i < detailArray.length(); i++) {
                                JSONObject sectionObject = detailArray.getJSONObject(i);
                                JSONArray cxcJSONArray = sectionObject.getJSONArray("cxcs");

                                if (cxcJSONArray.length() != 0)
                                    vndrLists.add(new VndrList(sectionObject.getString("status")));

                                for (int j = 0; j <  cxcJSONArray.length(); j++) {
                                    JSONObject cxcObject = cxcJSONArray.getJSONObject(j);

                                    CxCCard cxc= new CxCCard(cxcObject.getString("_id"));
                                    cxc.setStatus(cxcObject.getString("status"));
                                    cxc.setCustomerName(cxcObject.getString("customer_name"));
                                    cxc.setOrderNumber(cxcObject.getInt("order_number"));
                                    if (cxcObject.has("billing_date"))
                                        cxc.setBillingDate(cxcObject.getString("billing_date"));
                                    if (cxcObject.has("due_date"))
                                        cxc.setDueDate(cxcObject.getString("due_date"));
                                    cxc.setTotalAmount(cxcObject.getDouble("total_amount"));
                                    cxc.setBalanceOverdue(cxcObject.getDouble("balance_overdue"));
                                    cxc.setBalanceDue(cxcObject.getDouble("balance_due"));
                                    cxc.setPmtAmount(cxcObject.getDouble("ar_pmt_amount"));

                                    vndrLists.add(new VndrList(cxc));
                                }

                            }
                            cxcResponse.cxcSuccess(vndrLists);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un problema al procesar la solicitud.");
                        }
                    }
                });
    }

    public void getCxCDetail(CXCResponse cxcResponse, String cxcId){
        NewMovement.me.getMovement().setIdCxc(cxcId);
        new VndrRequest(queue)
                .get(URL_CXC_DETAIL + cxcId, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        cxcResponse.cxcError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            List<VndrList> vndrLists = new ArrayList<>();

                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.has("collections_notice")){
                                AccountReceivable.current.setGracePeriod(
                                        jsonObject
                                                .getJSONObject("collections_notice")
                                                .getInt("ar_grace_period")
                                );
                                AccountReceivable.current.setPaymentEarlyNotice(
                                        jsonObject
                                                .getJSONObject("collections_notice")
                                                .getInt("ar_pmt_early_notice")
                                );
                                AccountReceivable.current.setPaymentEarlyNoticeAlert(
                                        jsonObject
                                                .getJSONObject("collections_notice")
                                                .getBoolean("ar_pmt_early_notice_alert")
                                );
                                AccountReceivable.current.setPaymentReminderAlert(
                                        jsonObject
                                                .getJSONObject("collections_notice")
                                                .getBoolean("ar_payment_reminder_alert")
                                );
                            }

                            SimpleList3 list3;

                            if (jsonObject.has("customer_name")){
                                list3 = new SimpleList3(
                                        "Cliente:",
                                        "",
                                        jsonObject.getString("customer_name")
                                );

                                NewMovement.me.setCustomerName(
                                        jsonObject.getString("customer_name")
                                );

                                vndrLists.add(new VndrList(list3));
                            }



                            if (jsonObject.has("order_number")){
                                list3 = new SimpleList3(
                                        "Pedido:",
                                        "",
                                        String.valueOf(jsonObject.getInt("order_number"))
                                );
                                NewMovement.me.setOrderId(
                                        String.valueOf(jsonObject.getInt("order_number"))
                                );
                                vndrLists.add(new VndrList(list3));
                            }

                            if (jsonObject.has("total_order")){
                                list3 = new SimpleList3(
                                        "Total del Pedido:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("total_order"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }


                            if (jsonObject.has("ar_downpayment_amount")){
                                list3 = new SimpleList3(
                                        "Anticipo pagado:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("ar_downpayment_amount"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            if (jsonObject.has("ar_loan_amount")){
                                list3 = new SimpleList3(
                                        "Total Saldo Inicial:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("ar_loan_amount"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                            list3 = new SimpleList3(
                                    "Resumen de movimientos",
                                    "",
                                    ""
                            );

                            vndrLists.add(new VndrList(list3));


                            if (jsonObject.has("ar_total_debits")){
                                list3 = new SimpleList3(
                                        "Pagos y abonos:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("ar_total_debits"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }


                            if (jsonObject.has("ar_total_credits")){
                                list3 = new SimpleList3(
                                        "Cargos Adicionales:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("ar_total_credits"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            vndrLists.add(new VndrList(VndrListType.SEPARATOR));

                            if(jsonObject.has("ar_balance_due")){
                                list3 = new SimpleList3(
                                        "Saldo Actual:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("ar_balance_due"))
                                );

                                NewMovement.me.setSaldoActual(
                                        jsonObject.getDouble("ar_balance_due")
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            if (jsonObject.has("ar_balance_due")){
                                NewMovement.me.setMaxMovementAmount(jsonObject.getDouble("ar_balance_due"));
                            }

                            vndrLists.add(new VndrList(VndrListType.SEPARATOR));


                            if (jsonObject.has("due_date")){
                                list3 = new SimpleList3(
                                        "Fecha límite de pago:",
                                        "",
                                        new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault())
                                        .format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
                                                .parse(jsonObject.getString("due_date")))
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            if (jsonObject.has("ar_amount_due_total")){
                                list3 = new SimpleList3(
                                        "Total a pagar:",
                                        "",
                                        NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("ar_amount_due_total"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            if (jsonObject.has("ar_status_count")){
                                list3 = new SimpleList3(
                                        "No. pagos atrasados:",
                                        "",
                                        String.valueOf(jsonObject.getInt("ar_status_count"))
                                );

                                vndrLists.add(new VndrList(list3));
                            }

                            if (jsonObject.has("ar_total_amount_due"))
                                NewMovement.me.setExigible(
                                        jsonObject.getDouble("ar_total_amount_due")
                                );

                            if (jsonObject.has("sale_date")){
                                Log.e("TAG", "SALE_DARE");
                                NewMovement.me.setSaleDate(
                                        new SimpleDateFormat(
                                                "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
                                                Locale.getDefault()
                                        ).parse(
                                                jsonObject.getString("sale_date")
                                        )
                                );
                            }

                            if (jsonObject.has("ar_pmt_amount")){
                                NewMovement.me.setPmtAmount(jsonObject.getDouble("ar_pmt_amount"));
                            }

                            if (jsonObject.has("ar_unpaid_amount_due")){
                                NewMovement.me.setUnpaidAmountDue(jsonObject.getDouble("ar_unpaid_amount_due"));
                            }

                            if (jsonObject.has("ar_pmt_frequency")){
                                int freq = jsonObject.getInt("ar_pmt_frequency");
                                if (freq == 1)
                                    NewMovement.me.setUniquePaymentCxc(true);
                                else
                                    NewMovement.me.setUniquePaymentCxc(false);
                            }


                            vndrLists.add(new VndrList("Movimientos"));


                            JSONArray movementsArray = jsonObject.getJSONArray("ar_billing_movements");

                            if (movementsArray.length() == 0){
                                vndrLists.add(new VndrList("Aún no hay movimientos registrados.",true));
                            }

                            for (int i = 0; i < movementsArray.length(); i++) {
                                JSONObject movementObject = movementsArray.getJSONObject(i);


                                CxCMovement movement = new CxCMovement(movementObject.getString("_id"));
                                movement.setComment(movementObject.getString("mv_comments"));
                                movement.setDescription(movementObject.getString("mt_description"));
                                movement.setPaymentMethod(movementObject.getString("mv_payment_method_name"));
                                movement.setCreateDate(movementObject.getString("create_date"));

                                movement.setNotificationAlertData(
                                        new NotificationAlertData(
                                                movementObject.getJSONObject("mv_notifications"),
                                                movementObject.getJSONObject("contact_info")
                                        )
                                );

                                movement.setAmount(movementObject.getDouble("mv_amount"));
                                movement.setStringDate(movementObject.getString("mv_date"));
                                movement.setCustomerName(movementObject.getString("customer_name"));
                                movement.setOrderNumber(String.valueOf(movementObject.getInt("order_number")));
                                movement.setOldBalance(movementObject.getDouble("ar_initial_balance_ticket"));
                                movement.setNewBalance(movementObject.getDouble("ar_ending_balance_ticket"));
                                movement.setNewExigible(movementObject.getDouble("ar_ending_amount_due_ticket"));
                                movement.setOldExigible(movementObject.getDouble("ar_initial_amount_due_ticket"));
                                movement.setReversable(movementObject.getBoolean("mt_reversible"));

                                MovementType type = new MovementType();
                                type.setCode(movementObject.getString("mt_code"));
                                type.setTypeTag(movementObject.getString("mt_tag"));
                                type.setType(movementObject.getString("mt_type"));
                                movement.setType(type);

                                vndrLists.add(new VndrList(movement));
                            }

                            JSONArray notificationsArray = jsonObject.getJSONArray("notifications");
                            List<NotificationAlertData> notificationAlerts = new ArrayList<>();
                            for (int i = 0; i < notificationsArray.length(); i++) {
                                JSONObject notification = notificationsArray.getJSONObject(i);
                                Log.e("TAG", notification.toString());
                                notificationAlerts.add(
                                        new NotificationAlertData(
                                                notification.getString("title"),
                                                notification.getJSONObject("alert"),
                                                notification.getJSONObject("contact_info")
                                        )
                                );
                            }
                            NotificationAlertData.SelectedCxC.current.setDataList(notificationAlerts);  // Mover a propiedades de CxC


                            cxcResponse.cxcSuccess(vndrLists);




                        } catch (JSONException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un error al tratar de procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            cxcResponse.cxcError("Ocurrió un error al tratar de procesar la solicitud.");
                        }
                    }
                });

    }

    public void getMovementTypeList(String idCxc, MovementResponse movementResponse){

        String url = URL_MOVEMENT_TYPE + "?cxc=" + idCxc;
        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        movementResponse.movementError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONArray jsonArray = new JSONArray(response);
                            List<MovementType> movementTypeList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                MovementType type = new MovementType(
                                        object.getString("mt_code"),
                                        object.getString("mt_type"),
                                        object.getString("mt_description"),
                                        object.getBoolean("mt_request_payment_method")
                                );

                                movementTypeList.add(type);

                            }

                            if (movementTypeList.isEmpty())
                                movementResponse.movementError("No se encontraron movimientos.");
                            else
                                movementResponse.movementTypeList(movementTypeList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            movementResponse.movementError("Ocurrió un error al tratar de procesar la solicitud.");
                        }
                    }
                });

    }

    public void addMovement(CxCMovement movement, MovementResponse movementResponse){
        Map<String, String> params = new HashMap<>();
        params.put("id_cxc", movement.getIdCxc());
        params.put("type", movement.getType().getCode());
        params.put("amount", movement.getStringAmount());
        params.put("date", String.valueOf(movement.getDate().getTime()));
        if (movement.getType().isRequestPayment())
            params.put("payment_method", movement.getPaymentMethod());

        if (movement.getComment() != null && !movement.getComment().isEmpty())
            params.put("comments", movement.getComment());
        else
            params.put("comments", "");



        new VndrRequest(queue)
                .setRequestParams(params)
                .post(URL_MOVEMENT, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        movementResponse.movementError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject movementObject = new JSONObject(response);
                            movementObject = movementObject.getJSONObject("response");

                            movement.setId(movementObject.getString("_id"));
                            movement.setComment(movementObject.getString("mv_comments"));
                            movement.setDescription(movementObject.getString("mt_description"));
                            if (movementObject.has("mv_payment_method_name"))
                               movement.setPaymentMethod(movementObject.getString("mv_payment_method_name"));

                            movement.setCreateDate(movementObject.getString("create_date"));

                            movement.setNotificationAlertData(
                                    new NotificationAlertData(
                                            movementObject.getJSONObject("mv_notifications"),
                                            movementObject.getJSONObject("contact_info")
                                    )
                            );

                            movement.setAmount(movementObject.getDouble("mv_amount"));
                            movement.setStringDate(movementObject.getString("mv_date"));
                            movement.setCustomerName(movementObject.getString("customer_name"));
                            movement.setOrderNumber(String.valueOf(movementObject.getInt("order_number")));
                            movement.setOldBalance(movementObject.getDouble("ar_initial_balance_ticket"));
                            movement.setNewBalance(movementObject.getDouble("ar_ending_balance_ticket"));
                            movement.setNewExigible(movementObject.getDouble("ar_ending_amount_due_ticket"));
                            movement.setOldExigible(movementObject.getDouble("ar_initial_amount_due_ticket"));
                            if (movementObject.has("mt_reversible"))
                                movement.setReversable(movementObject.getBoolean("mt_reversible"));

                            movementResponse.addMovementSuccess(movement);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            movementResponse.movementError("Ocurrió un error inesperado, revisa los movimientos de tu cuenta para verificar que el movimiento ha sido creado correctamente.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            movementResponse.movementError("Ocurrió un error inesperado, revisa los movimientos de tu cuenta para verificar que el movimiento ha sido creado correctamente.");
                        }

                    }
                });
    }

    public void getMovemnts(MovementResponse movementResponse, @NonNull String iniDate, @NonNull String endDate){


        String url = URL_MOVEMENT;

        url += "?initial_date=" + iniDate + "&final_date=" + endDate;

        if (MovementType.Query.current.getSelected() != null && !MovementType.Query.current.getSelected().equals(MovementType.Query.ALL))
            url += "&type=" + MovementType.Query.current.getSelected().toString();

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        movementResponse.movementError(message);
                    }

                    @Override
                    public void success(String response) {
                        Log.e("TAG", response);
                        try {
                            List<CxCMovement> movementList = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.has("info")){
                                Movement.current.setSumAbonos(jsonObject.getJSONObject("info").getDouble("mt_type_a"));
                                Movement.current.setSumCargos(jsonObject.getJSONObject("info").getDouble("mt_type_c"));
                            }

                            JSONArray movementsArray = jsonObject.getJSONArray("results");

                            for (int i = 0; i < movementsArray.length(); i++) {
                                JSONObject movementObject = movementsArray.getJSONObject(i);
                                Log.e("TAG", movementObject.toString());

                                CxCMovement movement = new CxCMovement(movementObject.getString("_id"));
                                movement.setComment(movementObject.getString("mv_comments"));
                                movement.setDescription(movementObject.getString("mt_description"));
                                movement.setPaymentMethod(movementObject.getString("mv_payment_method_name"));
                                movement.setCreateDate(movementObject.getString("create_date"));
                                movement.setAmount(movementObject.getDouble("mv_amount"));
                                movement.setNotificationAlertData(
                                        new NotificationAlertData(
                                                movementObject.getJSONObject("mv_notifications"),
                                                movementObject.getJSONObject("contact_info")
                                        )
                                );
                                movement.setStringDate(movementObject.getString("mv_date"));
                                movement.setCustomerName(movementObject.getString("customer_name"));
                                movement.setOrderNumber(String.valueOf(movementObject.getInt("order_number")));
                                movement.setOldBalance(movementObject.getDouble("ar_initial_balance_ticket"));
                                movement.setNewBalance(movementObject.getDouble("ar_ending_balance_ticket"));
                                movement.setNewExigible(movementObject.getDouble("ar_ending_amount_due_ticket"));
                                movement.setOldExigible(movementObject.getDouble("ar_initial_amount_due_ticket"));
                                movement.setReversable(movementObject.getBoolean("mt_reversible"));

                                MovementType type = new MovementType();
                                type.setCode(movementObject.getString("mt_code"));
                                type.setTypeTag(movementObject.getString("mt_tag"));
                                type.setType(movementObject.getString("mt_type"));
                                movement.setType(type);


                                movementList.add(movement);
                            }

                            if (movementList.isEmpty())
                                movementResponse.movementError("No se encontraron movimientos con los criterios seleccionados");
                            else
                                movementResponse.movementList(movementList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            movementResponse.movementError("Ocurrió un error al tratar de procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            movementResponse.movementError("Ocurrió un error al tratar de procesar la solicitud.");
                        }


                    }
                });
    }

    public void reverseMovement(String id, MovementResponse movementResponse){
        Map<String, String> params = new HashMap<>();
        params.put("mv_id", id);
        new VndrRequest(queue)
                .setRequestParams(params)
                .post(URL_MOVEMENT_REVERSE, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        movementResponse.movementError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject movementObject = new JSONObject(response);
                            movementObject = movementObject.getJSONObject("response");

                            CxCMovement movement = new CxCMovement(movementObject.getString("_id"));
                            movement.setComment(movementObject.getString("mv_comments"));
                            movement.setDescription(movementObject.getString("mt_description"));
                            movement.setPaymentMethod(movementObject.getString("mv_payment_method_name"));
                            movement.setCreateDate(movementObject.getString("create_date"));

                            movement.setNotificationAlertData(
                                    new NotificationAlertData(
                                            movementObject.getJSONObject("mv_notifications"),
                                            movementObject.getJSONObject("contact_info")
                                    )
                            );

                            movement.setAmount(movementObject.getDouble("mv_amount"));
                            movement.setStringDate(movementObject.getString("mv_date"));
                            movement.setCustomerName(movementObject.getString("customer_name"));
                            movement.setOrderNumber(String.valueOf(movementObject.getInt("order_number")));
                            movement.setOldBalance(movementObject.getDouble("ar_initial_balance_ticket"));
                            movement.setNewBalance(movementObject.getDouble("ar_ending_balance_ticket"));
                            movement.setNewExigible(movementObject.getDouble("ar_ending_amount_due_ticket"));
                            movement.setOldExigible(movementObject.getDouble("ar_initial_amount_due_ticket"));
                            if (movementObject.has("mt_reversible"))
                                movement.setReversable(movementObject.getBoolean("mt_reversible"));

                            MovementType type = new MovementType();
                            type.setCode(movementObject.getString("mt_code"));
                            type.setTypeTag(movementObject.getString("mt_tag"));
                            type.setType(movementObject.getString("mt_type"));
                            movement.setType(type);

                            movementResponse.reversemovementSuccess(movement);
                        } catch (JSONException | ParseException e) {
                            e.printStackTrace();
                            movementResponse.movementError("Ocurrió un error al procesar la solicitud. Revisa los movimientos para comprobar que el movimeinto haya sido aplicado");
                        }

                    }
                });
    }

    public void getNotifications(NotificationsResponse notificationsResponse, @NonNull APIVndrCxc.NotificationsQuery query){
        new VndrRequest(queue)
                .get(URL_NOTIFICATIONS + query.toString(), new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        notificationsResponse.notificationsError(message);
                    }

                    @Override
                    public void success(String response) {
                        Log.d(TAG, response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONObject infoObject = jsonObject.getJSONObject("info");
                            JSONArray array = jsonObject.getJSONArray("cxc");

                            if (array.length() == 0){
                                notificationsResponse.notificationsError("Aún no tienes notificaciones.");
                            }

                            List<Notification> notifications = new ArrayList<>();

                            for (int i = 0; i < array.length(); i++) {
                                jsonObject = array.getJSONObject(i);

                                Notification notification = new Notification();
                                notification.setType(NotificationType.valueOf(jsonObject.getString("type")));
                                notification.setDescriptionType(jsonObject.getString("tag"));
                                notification.setDescription(jsonObject.getString("description"));
                                notification.setPaymentDate(jsonObject.getString("payment_date"));
                                notification.setAmount(String.valueOf(jsonObject.getDouble("amount")));
                                notification.setDueDate(jsonObject.getString("end_date"));
                                notification.setCustomerName(jsonObject.getString("customer_name"));

                                if (jsonObject.getInt("cxc_frequency") == 1)
                                    notification.setCxCUniquePayment(true);
                                else
                                    notification.setCxCUniquePayment(false);

                                notification.setIdCxc(jsonObject.getString("cxc_id"));
                                notification.setOrderNumber(String.valueOf(jsonObject.getInt("order_number")));

                                notification.setPaymentPeriod(NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("term_amount")));
                                notification.setUnpaidAmount(NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("unpaid_amount")));
                                notification.setBalanceAmount(NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("balance_amount")));
                                notification.setStatusCount(String.valueOf(jsonObject.getInt("status_count")));

                                notification.setNotificationAlertData(
                                        new NotificationAlertData(
                                                jsonObject.getJSONObject("notifications"),
                                                jsonObject.getJSONObject("contact_info")
                                        )
                                );

                                notifications.add(notification);
                            }
                            Notification.totalNotificationsNoticesAmount =
                                    NumberFormat
                                            .getCurrencyInstance()
                                            .format(infoObject.getDouble("total_notices"));

                            Notification.totalNotificationsRequirementsAmount =
                                    NumberFormat
                                            .getCurrencyInstance()
                                            .format(infoObject.getDouble("total_requirements"));

                            notificationsResponse.notifications(notifications);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            notificationsResponse.notificationsError("Ocurrió un error al procesar la solicitud.");
                        }
                    }
                });
    }

    public interface CXCResponse {
        void cxcSuccess(List<VndrList> list);
        void cxcError(String messageError);
    }

    public interface MovementResponse {
        default void movementTypeList(List<MovementType> types){

        }
        default void addMovementSuccess(CxCMovement movement){

        }
        default void movementList(List<CxCMovement> movements){

        }
        default void reversemovementSuccess(CxCMovement movement){

        }
        void movementError(String messageError);
    }

    public interface NotificationsResponse{
        void notifications(List<Notification> notifications);
        void notificationsError(String messageError);
    }

    public enum NotificationsQuery {
        A,R
    }
}
