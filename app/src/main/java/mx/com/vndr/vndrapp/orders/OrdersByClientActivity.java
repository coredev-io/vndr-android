package mx.com.vndr.vndrapp.orders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.ActiveOrdersRecyclerViewAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.api.URLVndr;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;

public class OrdersByClientActivity extends AppCompatActivity {

    Toolbar toolbar;
    Customer customer;
    RecyclerView recyclerViewHeader;
    RecyclerView recyclerViewOrders;
    TextView emptyOrders;
    ProgressBar progressBar;

    HeaderOrderAdapter headerAdapter;
    List<List<String>> headers = new ArrayList<>();
    List<Order> orders = new ArrayList<>();
    ActiveOrdersRecyclerViewAdapter ordersAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_by_client);
        customer = (Customer) getIntent().getSerializableExtra("customer");
        setUpUI();

        headerAdapter = new HeaderOrderAdapter(headers);
        ordersAdapter = new ActiveOrdersRecyclerViewAdapter(orders,this);

        recyclerViewHeader.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewHeader.setAdapter(headerAdapter);
        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewOrders.setAdapter(ordersAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

        headers.clear();
        orders.clear();
        ordersAdapter.notifyDataSetChanged();
        headerAdapter.notifyDataSetChanged();

        getOrdersByClient(customer.getIdCustomer());

    }

    private void getOrdersByClient(String idClient){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URLVndr.URL_ORDERS + "?client=" + idClient,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressBar.setVisibility(View.GONE);
                        System.out.println(response);
                        processResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrdersByClientActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));
        Volley.newRequestQueue(this).add(request);
    }

    private void processResponse(String response){
        try {
            if (new JSONObject(response).has("orders")){
                JSONObject orderObject = new JSONObject(response).getJSONObject("orders");

                if (orderObject.has("incomplete")){
                    JSONArray incompleteOrders = orderObject.getJSONArray("incomplete");
                    for (int i = 0; i < incompleteOrders.length(); i++){
                        JSONObject jsonOrder = incompleteOrders.getJSONObject(i);
                        Order order = new Order();
                        order.setCustomer(customer);
                        order.setIdOrder(jsonOrder.getString("_id"));
                        order.setOrderAmount(jsonOrder.getDouble("order_amount"));
                        order.setTotalOrderAmount(jsonOrder.getDouble("total_order_amount"));
                        order.setOrderNumber(jsonOrder.getInt("order_number"));
                        order.setStatus(
                                jsonOrder.getJSONObject("status").getBoolean("complete"),
                                jsonOrder.getJSONObject("status").getBoolean("sended"),
                                jsonOrder.getJSONObject("status").getBoolean("delivered")
                        );

                        if (jsonOrder.has("sale_date")){
                            order.setSaleDateFormmat(jsonOrder.getString("sale_date"));
                        }
                        if (jsonOrder.has("delivery")){
                            order.setDelivery(new Delivery(jsonOrder.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orders.add(order);
                    }
                }
                if (orderObject.has("not_sended")) {
                    JSONArray notSendedOrders = orderObject.getJSONArray("not_sended");
                    for (int i = 0; i < notSendedOrders.length(); i++){
                        JSONObject jsonOrder = notSendedOrders.getJSONObject(i);
                        Order order = new Order();
                        order.setCustomer(customer);
                        order.setIdOrder(jsonOrder.getString("_id"));
                        order.setOrderAmount(jsonOrder.getDouble("order_amount"));
                        order.setTotalOrderAmount(jsonOrder.getDouble("total_order_amount"));
                        order.setOrderNumber(jsonOrder.getInt("order_number"));
                        order.setStatus(
                                jsonOrder.getJSONObject("status").getBoolean("complete"),
                                jsonOrder.getJSONObject("status").getBoolean("sended"),
                                jsonOrder.getJSONObject("status").getBoolean("delivered")
                        );

                        if (jsonOrder.has("sale_date")){
                            order.setSaleDateFormmat(jsonOrder.getString("sale_date"));
                        }
                        if (jsonOrder.has("delivery")){
                            order.setDelivery(new Delivery(jsonOrder.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orders.add(order);
                    }
                }

                if (orderObject.has("not_delivery")){
                    JSONArray notDeliveryOrders = orderObject.getJSONArray("not_delivery");
                    for (int i = 0; i < notDeliveryOrders.length(); i++){
                        JSONObject jsonOrder = notDeliveryOrders.getJSONObject(i);
                        Order order = new Order();
                        order.setCustomer(customer);
                        order.setIdOrder(jsonOrder.getString("_id"));
                        order.setOrderAmount(jsonOrder.getDouble("order_amount"));
                        order.setTotalOrderAmount(jsonOrder.getDouble("total_order_amount"));
                        order.setOrderNumber(jsonOrder.getInt("order_number"));
                        order.setStatus(
                                jsonOrder.getJSONObject("status").getBoolean("complete"),
                                jsonOrder.getJSONObject("status").getBoolean("sended"),
                                jsonOrder.getJSONObject("status").getBoolean("delivered")
                        );

                        if (jsonOrder.has("sale_date")){
                            order.setSaleDateFormmat(jsonOrder.getString("sale_date"));
                        }
                        if (jsonOrder.has("delivery")){
                            order.setDelivery(new Delivery(jsonOrder.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orders.add(order);
                    }
                }

                if (orderObject.has("finish")){
                    JSONArray finishOrders = orderObject.getJSONArray("finish");

                    for (int i = 0; i < finishOrders.length(); i++){
                        JSONObject jsonOrder = finishOrders.getJSONObject(i);
                        Order order = new Order();
                        order.setCustomer(customer);
                        order.setIdOrder(jsonOrder.getString("_id"));
                        order.setOrderAmount(jsonOrder.getDouble("order_amount"));
                        order.setTotalOrderAmount(jsonOrder.getDouble("total_order_amount"));
                        order.setOrderNumber(jsonOrder.getInt("order_number"));
                        order.setStatus(
                                jsonOrder.getJSONObject("status").getBoolean("complete"),
                                jsonOrder.getJSONObject("status").getBoolean("sended"),
                                jsonOrder.getJSONObject("status").getBoolean("delivered")
                        );

                        if (jsonOrder.has("sale_date")){
                            order.setSaleDateFormmat(jsonOrder.getString("sale_date"));
                        }
                        if (jsonOrder.has("delivery")){
                            order.setDelivery(new Delivery(jsonOrder.getJSONObject("delivery").getString("delivery_type")));
                        }

                        orders.add(order);
                    }
                }

                ordersAdapter.notifyDataSetChanged();

                JSONObject incomplete = new JSONObject(response).getJSONObject("incomplete");

                List<String> incompleteHeader = new ArrayList<>();
                incompleteHeader.add("Pedidos incompletos");
                incompleteHeader.add(String.valueOf(incomplete.getInt("count")));
                incompleteHeader.add(NumberFormat.getCurrencyInstance().format(incomplete.getDouble("amount")));
                headers.add(incompleteHeader);

                JSONObject notSended = new JSONObject(response).getJSONObject("not_sended");

                List<String> notSendedHeader = new ArrayList<>();
                notSendedHeader.add("Pedidos no enviados");
                notSendedHeader.add(String.valueOf(notSended.getInt("count")));
                notSendedHeader.add(NumberFormat.getCurrencyInstance().format(notSended.getDouble("amount")));
                headers.add(notSendedHeader);

                JSONObject notDelivered = new JSONObject(response).getJSONObject("not_delivered");

                List<String> notDeliveredeHeader = new ArrayList<>();
                notDeliveredeHeader.add("Pedidos no entregados");
                notDeliveredeHeader.add(String.valueOf(notDelivered.getInt("count")));
                notDeliveredeHeader.add(NumberFormat.getCurrencyInstance().format(notDelivered.getDouble("amount")));
                headers.add(notDeliveredeHeader);


                JSONObject finish = new JSONObject(response).getJSONObject("finish");

                List<String> finishHeader = new ArrayList<>();
                finishHeader.add("Pedidos terminados");
                finishHeader.add(String.valueOf(finish.getInt("count")));
                finishHeader.add(NumberFormat.getCurrencyInstance().format(finish.getDouble("amount")));
                headers.add(finishHeader);



                headerAdapter.notifyDataSetChanged();



            } else {
                //  No hay pedidos para el cliente
                emptyOrders.setVisibility(View.VISIBLE);

                List<String> incompleteHeader = new ArrayList<>();
                incompleteHeader.add("Pedidos incompletos");
                incompleteHeader.add("0");
                incompleteHeader.add("$0.00");
                headers.add(incompleteHeader);


                List<String> notSendedHeader = new ArrayList<>();
                notSendedHeader.add("Pedidos no enviados");
                notSendedHeader.add("0");
                notSendedHeader.add("$0.00");
                headers.add(notSendedHeader);


                List<String> notDeliveredeHeader = new ArrayList<>();
                notDeliveredeHeader.add("Pedidos no entregados");
                notDeliveredeHeader.add("0");
                notDeliveredeHeader.add("$0.00");
                headers.add(notDeliveredeHeader);

                List<String> finishHeader = new ArrayList<>();
                finishHeader.add("Pedidos terminados");
                finishHeader.add("0");
                finishHeader.add("$0.00");
                headers.add(finishHeader);



                headerAdapter.notifyDataSetChanged();

            }






        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_orders_by_client);
        toolbar.setTitle(customer.getCustomerFullName());
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        recyclerViewHeader = findViewById(R.id.rv_header_orders);
        recyclerViewOrders = findViewById(R.id.rv_orders_user);
        emptyOrders = findViewById(R.id.txt_empty_orders);
        emptyOrders.setVisibility(View.GONE);
        progressBar = findViewById(R.id.pb_orders_client);
        progressBar.setVisibility(View.GONE);
    }
}
