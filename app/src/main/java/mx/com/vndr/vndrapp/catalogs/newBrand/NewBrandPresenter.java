package mx.com.vndr.vndrapp.catalogs.newBrand;

import mx.com.vndr.vndrapp.models.Brand;

public class NewBrandPresenter implements NewBrandInteractor.OnFinishRequest {

    private NewBrandInteractor interactor;
    private NewBrandView view;

    public NewBrandPresenter(NewBrandInteractor interactor, NewBrandView view) {
        this.interactor = interactor;
        this.view = view;
    }

    public void createNewBrand(Brand brand){
        view.showProgress();
        interactor.createNewBrand(brand,this);
    }

    public void updateBrand(Brand newBrand){
        view.showProgress();
        interactor.updateBrand(newBrand, this);
    }

    @Override
    public void onSuccess(Brand brand) {
        view.dismissProgress();
        view.navigateToSuccess(brand);
    }

    @Override
    public void onUpdateSuccess() {
        view.dismissProgress();
        view.showMessage("Marca actualizada exitosamente");
        view.finishActivity();
    }

    @Override
    public void onError(String error) {
        view.dismissProgress();
        view.showMessage(error);
    }
}
