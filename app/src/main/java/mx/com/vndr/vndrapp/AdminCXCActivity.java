package mx.com.vndr.vndrapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.CustomAdminCXCActivity;
import mx.com.vndr.vndrapp.orders.OrderDetailActivity;

import static android.graphics.Typeface.BOLD;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class AdminCXCActivity extends AppCompatActivity {

    int diasAviso, diasCortesia;
    int freq;
    Toolbar toolbar;
    Order order;
    TextView txtPreventiveReminderDesc, txtCourtesyDaysDesc;
    ImageButton btn_normal, btn_cuidado, btn_confianza;
    ImageButton btn_selected;
    Switch recordatorioPagoSwitch, faltaPagoSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_cxc);


        order = (Order) getIntent().getSerializableExtra("order");

        toolbar = findViewById(R.id.tb_admin_cxc);
        txtCourtesyDaysDesc = findViewById(R.id.courtesy_days_desc);
        txtPreventiveReminderDesc = findViewById(R.id.preventive_reminder_desc);
        btn_normal = findViewById(R.id.btn_normal);
        btn_confianza = findViewById(R.id.btn_confianza);
        btn_cuidado = findViewById(R.id.btn_cuidado);
        recordatorioPagoSwitch = findViewById(R.id.switch1);
        faltaPagoSwitch = findViewById(R.id.switch2);



        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });

        freq = order.getCxc().getPayFrequency();
        onComercial(txtCourtesyDaysDesc);


    }


    public void onConfianza(View view){

        if (btn_selected != null){
            if (btn_selected.getId() == R.id.btn_confianza)
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_confianza));
            else if (btn_selected.getId() == R.id.btn_cuidado)
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_normal));
            else
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_comercial));
        }
        btn_selected = btn_confianza;
        btn_confianza.setBackground(getResources().getDrawable(R.drawable.ic_confianza_selected));

        recordatorioPagoSwitch.setChecked(false);
        switch (freq){
            case 7:

                diasAviso = 1;
                diasCortesia = 1;
                break;
            case 15:
                diasAviso = 1;
                diasCortesia = 2;
                break;
            case 30:
                diasAviso = 1;
                diasCortesia = 5;
                break;
        }
        updateUI();
    }

    public void onComercial(View view){    //   Antes onNormal

        if (btn_selected != null){
            if (btn_selected.getId() == R.id.btn_confianza)
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_confianza));
            else if (btn_selected.getId() == R.id.btn_cuidado)
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_normal));
            else
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_comercial));
        }
        btn_selected = btn_normal;
        btn_normal.setBackground(getResources().getDrawable(R.drawable.ic_comercial_selected));
        recordatorioPagoSwitch.setChecked(true);


        switch (freq){
            case 7:

                diasAviso = 2;
                diasCortesia = 1;
                break;
            case 15:

                diasAviso = 3;
                diasCortesia = 1;
                break;
            case 30:

                diasAviso = 5;
                diasCortesia = 3;
                break;
        }
        updateUI();
    }

    public void onNormal(View view){   //  Antes onCuidado
        if (btn_selected != null){
            if (btn_selected.getId() == R.id.btn_confianza)
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_confianza));
            else if (btn_selected.getId() == R.id.btn_cuidado)
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_normal));
            else
                btn_selected.setBackground(getResources().getDrawable(R.drawable.ic_comercial));
        }        btn_selected = btn_cuidado;
        btn_cuidado.setBackground(getResources().getDrawable(R.drawable.ic_normal_selected));
        recordatorioPagoSwitch.setChecked(true);

        switch (freq){
            case 7:
            case 15:
                diasAviso = 2;
                diasCortesia = 0;
                break;
            case 30:

                diasAviso = 2;
                diasCortesia = 1;
                break;
        }
        updateUI();
    }

    public void onContinue(View view){
        order.getCxc().setDiasAviso(diasAviso);
        order.getCxc().setDiasCortesia(diasCortesia);
        //order.getCxc().setAlertas(true);
        order.getCxc().setEarlyNoticeAlert(faltaPagoSwitch.isChecked());    //  Dias de aviso preventivo
        order.getCxc().setPaymentReminderAlert(recordatorioPagoSwitch.isChecked()); //  Recordatorio de pago

        startActivityForResult(
                new Intent(this, OrderDetailActivity.class)
                        .putExtra("order", order),
                1
        );

    }

    public void onPersonalizar(View view){
        startActivityForResult(
                new Intent(this, CustomAdminCXCActivity.class)
                        .putExtra("order", order),
                1
        );
    }

    private void updateUI(){

        SpannableString a = new SpannableString(getResources().getString(R.string.preventive_reminder_desc,diasAviso));

        a.setSpan(new StyleSpan(BOLD),54,56,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        a.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)),54,56,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        SpannableString b = new SpannableString(getResources().getString(R.string.courtesy_days_desc,diasCortesia));

        b.setSpan(new StyleSpan(BOLD),8,10,Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        b.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorAccent)),8,10,Spanned.SPAN_EXCLUSIVE_INCLUSIVE);

        txtPreventiveReminderDesc.setText(a, TextView.BufferType.SPANNABLE);
        txtCourtesyDaysDesc.setText(b, TextView.BufferType.SPANNABLE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }
}
