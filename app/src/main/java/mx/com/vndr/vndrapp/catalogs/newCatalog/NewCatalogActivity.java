package mx.com.vndr.vndrapp.catalogs.newCatalog;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.customviews.DatePickerFragment;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.models.Catalog;

public class NewCatalogActivity extends AppCompatActivity implements View.OnClickListener, NewCatalogView, CompoundButton.OnCheckedChangeListener {

    Toolbar toolbar;
    EditText catalogName, catalogKey;
    Switch vigenciaSwitch;
    TextView initDate, endDate, alert;
    CheckBox shareCheckBox, publicCheckBox;
    Button button;
    ProgressBar progressBar;
    LinearLayout layoutEnd, layoutInit;

    Boolean isCreateMode, isNewCatalog;
    Date init,end;
    NewCatalogPresenter presenter;
    Brand brand;
    Catalog catalog;
    Boolean brandOwnerIsVndr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_catalog);

        isCreateMode = getIntent().getBooleanExtra("createMode",false);

        if (isCreateMode)
            brand = (Brand) getIntent().getSerializableExtra("brand");
        else
            catalog = (Catalog) getIntent().getSerializableExtra("catalog");

        brandOwnerIsVndr = getIntent().getBooleanExtra("isOwner", false);

        setUpUI();

        init = new Date();
        end = new Date();
        presenter = new NewCatalogPresenter(
                this,
                new NewCatalogInteractor(
                        VndrAuth.getInstance().getCurrentUser().getSessionToken(),
                        Volley.newRequestQueue(this)
                )
        );

        isNewCatalog = true;
        if (!isCreateMode){
            setUpDataOnUI();
            isNewCatalog = false;
            setViewMode();
        } else {
            activateEditMode();
            button.setEnabled(false);
        }

        Log.e("TAG", String.valueOf(brandOwnerIsVndr));

        if (brandOwnerIsVndr){

            //vigenciaSwitch.setVisibility(View.GONE);
            //layoutEnd.setVisibility(View.GONE);
            //layoutInit.setVisibility(View.GONE);
            publicCheckBox.setVisibility(View.GONE);
            shareCheckBox.setVisibility(View.GONE);
            alert.setVisibility(View.VISIBLE);
        }

    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_catalog_view);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        catalogName = findViewById(R.id.edtxt_catalog_name);
        catalogKey = findViewById(R.id.edtxt_catalog_key);
        vigenciaSwitch = findViewById(R.id.switch_vigencia_catalog);
        initDate = findViewById(R.id.txt_init_date);
        endDate = findViewById(R.id.txt_end_date);
        shareCheckBox = findViewById(R.id.collaborative_catalog);
        publicCheckBox = findViewById(R.id.public_catalog);
        button = findViewById(R.id.btn_new_catalog);
        progressBar = findViewById(R.id.pb_new_catalog);
        vigenciaSwitch.setOnCheckedChangeListener(this);
        layoutEnd = findViewById(R.id.date_end_layout);
        layoutInit = findViewById(R.id.date_init_layout);
        alert = findViewById(R.id.txt_alert);
        alert.setVisibility(View.GONE);

        button.setOnClickListener(this);

        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (catalogName.getText().toString().isEmpty() || catalogKey.getText().toString().isEmpty()){
                    button.setEnabled(false);
                } else {
                    button.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
        catalogName.addTextChangedListener(textWatcher);
        catalogKey.addTextChangedListener(textWatcher);

        layoutInit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickInitDateButton(view);
            }
        });

        layoutEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickEndDateButton(view);
            }
        });
        shareCheckBox.setVisibility(View.GONE);

        publicCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    shareCheckBox.setVisibility(View.VISIBLE);
                } else {
                    shareCheckBox.setVisibility(View.GONE);
                    if (shareCheckBox.isChecked())
                        shareCheckBox.setChecked(false);
                }
            }
        });


    }

    private void setUpDataOnUI(){
        catalogName.setText(catalog.getCatalogName());
        catalogKey.setText(catalog.getKey());
        vigenciaSwitch.setChecked(catalog.isValid());

        if (catalog.isValid() && catalog.getInitDate() != null && catalog.getEndDate() != null){

            initDate.setText(catalog.getStringInitDate());
            endDate.setText(catalog.getStringEndDate());
            init = catalog.getInitDate();
            end = catalog.getEndDate();

        }

        shareCheckBox.setChecked(catalog.isCollaborative());
        publicCheckBox.setChecked(catalog.isPublic());
    }

    @Override
    public void onClick(View view) {

        if(!isCreateMode){
            activateEditMode();
        } else {
            if (catalog == null) catalog = new Catalog();


            if (!brandOwnerIsVndr){

                catalog.setCollaborative(shareCheckBox.isChecked());
                catalog.setPublic(publicCheckBox.isChecked());
            }
            catalog.setValid(vigenciaSwitch.isChecked());

            if (catalog.isValid()){
                catalog.setInitDate(init);
                catalog.setEndDate(end);
            }
            catalog.setCatalogName(catalogName.getText().toString());
            catalog.setKey(catalogKey.getText().toString());

            if (brand != null)
                catalog.setIdBrand(brand.getBrandId());

            if (isNewCatalog)
                presenter.createCatalog(catalog, brandOwnerIsVndr);
            else
                presenter.updateCatalog(catalog, brandOwnerIsVndr);
        }


    }

    private void activateEditMode(){
        isCreateMode = true;
        button.setVisibility(View.VISIBLE);
        if (isNewCatalog)
            button.setText("Agregar");
        else
            button.setText("Actualizar");
        vigenciaSwitch.setEnabled(true);
        shareCheckBox.setEnabled(true);
        publicCheckBox.setEnabled(true);
        catalogName.setEnabled(true);
        catalogKey.setEnabled(true);
    }

    @Override
    public void setViewMode() {
        isCreateMode = false;
        button.setText("Editar");
        vigenciaSwitch.setEnabled(false);
        shareCheckBox.setEnabled(false);
        publicCheckBox.setEnabled(false);
        catalogName.setEnabled(false);
        catalogKey.setEnabled(false);
    }

    public void onClickInitDateButton(View view){

        if (isCreateMode){
            new DatePickerFragment(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    Calendar c = Calendar.getInstance();
                    c.set(i, i1, i2);
                    Date d = c.getTime();
                    init = d;
                    SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                    initDate.setText(format.format(d));
                }
            }).show(getSupportFragmentManager(),"picker");
        }
    }

    public void onClickEndDateButton(View view){
        if (isCreateMode){
            new DatePickerFragment(new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    Calendar c = Calendar.getInstance();
                    c.set(i, i1, i2);
                    Date d = c.getTime();
                    end = d;
                    SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy");
                    endDate.setText(format.format(d));
                }
            },init.getTime()).show(getSupportFragmentManager(),"picker");
        }
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void finishActivity() {
        this.finish();
    }



    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if (b){
            layoutInit.setVisibility(View.VISIBLE);
            layoutEnd.setVisibility(View.VISIBLE);
        } else {
            layoutInit.setVisibility(View.GONE);
            layoutEnd.setVisibility(View.GONE);
        }
    }
}
