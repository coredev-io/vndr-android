package mx.com.vndr.vndrapp.catalogs.newBrand;

import mx.com.vndr.vndrapp.models.Brand;

public interface NewBrandView {
    void showProgress();
    void dismissProgress();
    void showMessage(String message);
    void navigateToSuccess(Brand brand);
    void finishActivity();
}
