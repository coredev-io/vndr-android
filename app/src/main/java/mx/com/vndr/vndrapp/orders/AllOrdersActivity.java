package mx.com.vndr.vndrapp.orders;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.ActiveOrdersRecyclerViewAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Customer;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;

import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDERS;

public class AllOrdersActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    List<Order> orderList = new ArrayList<>();
    ActiveOrdersRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_orders);
        toolbar = findViewById(R.id.tb_all_orders);

        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView = findViewById(R.id.rv_all_orders);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new ActiveOrdersRecyclerViewAdapter(orderList,this);

        recyclerView.setAdapter(adapter);

        getAllOrders();


    }



    public void getAllOrders(){
        //progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("TAG",response);
                processResponse(response);
                //progressBar.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                //progressBar.setVisibility(View.GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));
        Volley.newRequestQueue(this).add(request);

    }

    private void processResponse(String response){
        try {
            JSONObject jsonResponse = new JSONObject(response);

            if (jsonResponse.has("orders")){

                JSONObject orders = jsonResponse.getJSONObject("orders");

                if (orders.has("incomplete")){
                    JSONArray array = orders.getJSONArray("incomplete");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }
                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orderList.add(order);
                    }
                }

                if (orders.has("not_sended")){
                    JSONArray array = orders.getJSONArray("not_sended");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }
                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orderList.add(order);
                    }
                }

                if (orders.has("not_delivery")){
                    JSONArray array = orders.getJSONArray("not_delivery");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }
                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orderList.add(order);
                    }
                }

                if (orders.has("finish")){
                    JSONArray array = orders.getJSONArray("finish");

                    for (int i = 0 ; i < array.length() ; i++){
                        JSONObject orderObject = array.getJSONObject(i);
                        JSONObject jsonCustomer = orderObject.getJSONObject("client");

                        Customer customer = new Customer(
                                jsonCustomer.getString("_id"),
                                jsonCustomer.getString("full_name"),
                                jsonCustomer.getString("first_given_name"),
                                jsonCustomer.getString("first_family_name"),
                                jsonCustomer.getString("second_given_name"),
                                jsonCustomer.getString("second_family_name")

                        );

                        Order order = new Order(
                                orderObject.getString("_id"),
                                customer,
                                orderObject.getJSONObject("status").getBoolean("complete"),
                                orderObject.getJSONObject("status").getBoolean("sended"),
                                orderObject.getJSONObject("status").getBoolean("delivered"),
                                orderObject.getDouble("order_amount"),
                                orderObject.getDouble("total_order_amount"),
                                orderObject.getInt("order_number")
                        );

                        if (orderObject.has("sale_date")){
                            order.setSaleDateFormmat(orderObject.getString("sale_date"));
                        }
                        if (orderObject.has("delivery")){
                            order.setDelivery(new Delivery(orderObject.getJSONObject("delivery").getString("delivery_type")));
                        }
                        orderList.add(order);
                    }
                }
                adapter.notifyDataSetChanged();



            }


        } catch (JSONException e) {
            e.printStackTrace();
            //progressBar.setVisibility(View.GONE);
            JSONObject jsonResponse = null;
            try {
                jsonResponse = new JSONObject(response);
                if (jsonResponse.has("statusCode") && jsonResponse.getInt("statusCode") == 404){
                    Log.e("TAG", jsonResponse.getString("message"));

                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }


        }
    }

}
