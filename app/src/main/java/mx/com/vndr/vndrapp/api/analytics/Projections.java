package mx.com.vndr.vndrapp.api.analytics;

import java.util.List;

public class Projections {
    String label;
    List<Projection> projectionList;

    public Projections(String label, List<Projection> projectionList) {
        this.label = label;
        this.projectionList = projectionList;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<Projection> getProjectionList() {
        return projectionList;
    }

    public void setProjectionList(List<Projection> projectionList) {
        this.projectionList = projectionList;
    }
}