package mx.com.vndr.vndrapp.brands;

import mx.com.vndr.vndrapp.models.Brand;

public interface BrandListView {
    void showProgress();
    void hideProgress();
    void showEmptyBrands(String message);
    void setRecyclerViewData(BrandListAdapter adapter);
    void navigateToCatalogList(Brand brand);
}
