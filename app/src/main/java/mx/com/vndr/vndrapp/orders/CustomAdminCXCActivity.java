package mx.com.vndr.vndrapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Order;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class CustomAdminCXCActivity extends AppCompatActivity {

    Order order;
    int diasAviso = 1, diasCortesia = 1;
    int freq;
    EditText aviso, cortesia;
    Toolbar toolbar;
    Switch recordatorioPagoSwitch, faltaPagoSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_admin_cxc);
        order = (Order) getIntent().getSerializableExtra("order");

        toolbar = findViewById(R.id.tb_custom_admin_cxc);
        aviso = findViewById(R.id.count_aviso);
        cortesia = findViewById(R.id.count_cortesia);
        recordatorioPagoSwitch = findViewById(R.id.switch4);
        faltaPagoSwitch = findViewById(R.id.switch3);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        freq = order.getCxc().getPayFrequency();

        updateUI();

    }




    public void addAviso(View view){
        if ((freq == 7 && diasAviso < 2) || (freq ==15 && diasAviso < 2) || (freq ==30 && diasAviso < 5)){
            diasAviso += 1;
            updateUI();
        }
    }
    public void removeAviso(View view){
        if (diasAviso > 1 ){
            diasAviso -= 1;
            updateUI();
        }
    }

    public void addCortesia(View view){
        if ((freq == 7 && diasCortesia < 3) || (freq ==15 && diasCortesia < 5) || (freq ==30 && diasCortesia < 10)){
            diasCortesia += 1;
            updateUI();
        }
    }

    public void removeCortesia(View view){
        if (diasCortesia > 1) {
            diasCortesia -= 1;
            updateUI();
        }
    }


    private void updateUI(){
        aviso.setText(String.valueOf(diasAviso));
        cortesia.setText(String.valueOf(diasCortesia));

    }

    public void onContinue(View view){
        order.getCxc().setDiasAviso(diasAviso);
        order.getCxc().setDiasCortesia(diasCortesia);
        //order.getCxc().setAlertas(true);
        order.getCxc().setEarlyNoticeAlert(faltaPagoSwitch.isChecked());    //  Dias de aviso preventivo
        order.getCxc().setPaymentReminderAlert(recordatorioPagoSwitch.isChecked()); //  Recordatorio de pago

        startActivityForResult(
                new Intent(this, OrderDetailActivity.class)
                        .putExtra("order", order),
                1
        );

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }
}
