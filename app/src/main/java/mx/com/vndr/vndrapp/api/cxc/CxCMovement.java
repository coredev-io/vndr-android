package mx.com.vndr.vndrapp.api.cxc;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import mx.com.vndr.vndrapp.api.analytics.Invoice;
import mx.com.vndr.vndrapp.cobros.movements.MovementType;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class CxCMovement implements Serializable {
    private String id;
    private String idCxc;
    private Date date;
    private String createDate;
    private String comment;
    private String description;
    private MovementType type;
    private double amount;
    private String paymentMethod;
    private NotificationAlertData notificationAlertData;
    private String customerName;

    private String orderNumber;
    private Double oldBalance;
    private Double newBalance;
    private Double oldExigible;
    private Double newExigible;
    private boolean isReversable = false;
    private boolean isInvoice = false;
    private Invoice invoice;


    public CxCMovement(String id) {
        this.id = id;
    }

    public CxCMovement() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdCxc() {
        return idCxc;
    }

    public void setIdCxc(String idCxc) {
        this.idCxc = idCxc;
    }

    public String getDateFormatted() {
        return VndrDateFormat.dateToString(date);
    }

    public void setStringDate(String date) throws ParseException {
        this.date = VndrDateFormat.stringToDate(date);
    }

    public void setDate(Date date){
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public String getStringAmount(){
        return String.valueOf(amount);
    }

    public String getCurrencyAmount(){
        return NumberFormat.getCurrencyInstance().format(amount);
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public MovementType getType() {
        return type;
    }

    public void setType(MovementType type) {
        this.type = type;
    }

    public Double getOldBalance() {
        return oldBalance;
    }

    public String getCurrencyOldBalance() {
        return NumberFormat.getCurrencyInstance().format(oldBalance);
    }

    public void setOldBalance(double oldBalance) {
        this.oldBalance = oldBalance;
    }

    public Double getNewBalance() {
        return newBalance;
    }

    public String getCurrencyNewBalance() {
        return NumberFormat.getCurrencyInstance().format(newBalance);
    }

    public void setNewBalance(double newBalance) {
        this.newBalance = newBalance;
    }

    public void setOldBalance(Double oldBalance) {
        this.oldBalance = oldBalance;
    }

    public void setNewBalance(Double newBalance) {
        this.newBalance = newBalance;
    }

    public Double getOldExigible() {
        return oldExigible;
    }

    public void setOldExigible(Double oldExigible) {
        this.oldExigible = oldExigible;
    }

    public Double getNewExigible() {
        return newExigible;
    }

    public String getCurrencyNewExigible() {
        return NumberFormat.getCurrencyInstance().format(newExigible);
    }

    public String getCurrencyOldExigible() {
        return NumberFormat.getCurrencyInstance().format(oldExigible);
    }

    public void setNewExigible(Double newExigible) {
        this.newExigible = newExigible;
    }

    public boolean isReversable() {
        return isReversable;
    }

    public void setReversable(boolean reversable) {
        isReversable = reversable;
    }

    public String getCreateDateFormatted() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = VndrDateFormat.dateWSFormatToAppFormat(createDate);
    }

    public NotificationAlertData getNotificationAlertData() {
        return notificationAlertData;
    }

    public void setNotificationAlertData(NotificationAlertData notificationAlertData) {
        this.notificationAlertData = notificationAlertData;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isInvoice() {
        return isInvoice;
    }

    public void setIsInvoice(boolean invoice) {
        isInvoice = invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
}
