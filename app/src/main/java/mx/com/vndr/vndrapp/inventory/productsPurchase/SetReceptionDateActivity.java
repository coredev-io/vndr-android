package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDERS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE_SET_DATE;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.customviews.datepPicker.DatePickerFragment;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.CompleteOrderStatusActivity;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class SetReceptionDateActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, MaterialDialog.SingleButtonCallback {

    Toolbar toolbar;
    TextView sendDate;
    ProgressBar progressBar;
    CardView cardViewSend;
    Date newDate;
    PurchaseInvoice invoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_reception_date);

        toolbar = findViewById(R.id.tb_invoice_status);
        sendDate = findViewById(R.id.txt_send_date_invoice);
        progressBar = findViewById(R.id.pb_invoice_status);
        cardViewSend = findViewById(R.id.cv_invoice_send);

        progressBar.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> {
            this.finish();
        });
        invoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();

    }



    public void launchCalendarSend(View view){
        if (!sendDate.getText().toString().equals("Sin asignar")){
            return;
        }
        new DatePickerFragment(this).show(getSupportFragmentManager(), "datePicker");
    }

    public void completeOrder(final Date date){
        progressBar.setVisibility(View.VISIBLE);
        Map<String, String> params = new HashMap<>();
        params.put("reception_date", VndrDateFormat.dateToWSFormat(date));

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .patch(URL_PURCHASE_INVOICE_SET_DATE + invoice.getPurchaseId() , new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        progressBar.setVisibility(View.GONE);
                        Dialogs.showAlert(SetReceptionDateActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        progressBar.setVisibility(View.GONE);
                        sendDate.setText(VndrDateFormat.dateToString(date));
                    }
                });

    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar c = Calendar.getInstance();
        c.set(i, i1, i2);
        newDate = c.getTime();
        confirmationDialog();
    }

    public void confirmationDialog(){
        Dialogs.showAlertQuestion(this,"Esta fecha ya no la podrás cambiar más adelante, ¿Quieres continuar?",this);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        completeOrder(newDate);
    }
}