package mx.com.vndr.vndrapp.catalogs.associateBrand;



import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.models.Brand;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PROFILE_BRANDS;

public class AssociatedBrandDialogInteractor {

    private RequestQueue queue;
    private String token;

    public AssociatedBrandDialogInteractor(RequestQueue queue, String token) {
        this.queue = queue;
        this.token = token;
    }

    public void associateBrand(final OnFinishRequestListener listener, final Brand brand){
        StringRequest request = new StringRequest(Request.Method.PATCH, URL_PROFILE_BRANDS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                listener.onSuccess();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    listener.onError(new JSONObject(new String(error.networkResponse.data)).getString("message"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onError("Ocurrió un error inesperado, por favor inténtalo de nuevo");
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> params = new HashMap<>();
                params.put("brand_id",brand.getBrandId());
                params.put("active",String.valueOf(brand.isActiveInUser()));
                if (brand.isActiveInUser()){
                    params.put("register_key",brand.getClaveRegistro());
                    params.put("supervisor_key",brand.getClaveSupervisor());
                    params.put("supervisor_name",brand.getNombreSupervisor());
                }

                if (brand.isAfiliacion()){
                    params.put("brand_code", brand.getPwdAfiliacion());
                }
                System.out.println(params);
                return params;
            }
        };

        queue.add(request);


    }

    interface OnFinishRequestListener{
        void onSuccess();
        void onError(String message);
    }
}
