package mx.com.vndr.vndrapp.membership;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.AppState;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrMembership;
import mx.com.vndr.vndrapp.util.Dialogs;

public class MembershipActivity extends AppCompatActivity {
    private static final String TAG = "MembershipActivity";


    private VndrMembership vndrMembership;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership);

        vndrMembership = VndrMembership.getInstance();
    }

    public void startFreeTrial(View view){
        Dialogs.showAlert(this, "La versión actual es de pruebas cerradas, no es necesario actualizar la licencia por lo que no se te hará ningún cargo.");
        /*
        vndrMembership.startFreeTrial(this, new VndrMembership.FreeTrialResult() {
            @Override
            public void onFreeTrialSuccess() {
                saveStateAndBack();
            }

            @Override
            public void onFreeTrialFail(String messageError) {
                Dialogs.showAlert(MembershipActivity.this, messageError);
            }
        });

         */
    }

    public void saveStateAndBack(){
        AppState.setAppState(MembershipActivity.this, AppState.ENROLLED);
        this.setResult(RESULT_OK);
        this.finish();
    }

    public void onBasicPlanClick(View view){
        startActivity(new Intent(this, BasicMembershipActivity.class));
    }

    public void onProfesionalPlanClick(View view){
        startActivity(new Intent(this, ProfesionalMembershipActivity.class));
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_CANCELED);
        this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        vndrMembership.endMembershipConnection();
    }
}
