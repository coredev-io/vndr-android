package mx.com.vndr.vndrapp.inventory.supplier;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.volley.toolbox.Volley;
import java.util.List;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APISupplier;
import mx.com.vndr.vndrapp.api.inventory.Supplier;

public class SuppliersActivity extends AppCompatActivity implements APISupplier.OnSupplierListResponse {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textViewEmptySuppliers;
    ProgressBar progressBar;
    boolean isPickMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suppliers);
        isPickMode = getIntent().getBooleanExtra("pickMode", false);
        setupUI();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSuppliers();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_suppliers);
        recyclerView = findViewById(R.id.rv_suppliers);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        progressBar = findViewById(R.id.pb_suppliers);
        progressBar.setVisibility(View.GONE);

        textViewEmptySuppliers = findViewById(R.id.txt_empty_suppliers);
    }

    private void getSuppliers(){
        showLoader();
        new APISupplier(Volley.newRequestQueue(this))
                .getSuppliers(this);
    }

    public void onClickNewSupplier(View view){
        startActivity(
                new Intent(
                        this,
                        SupplierActivity.class
                )
        );
    }

    @Override
    public void onSuccess(List<Supplier> suppliers) {
        dismissLoader();
        displaySuppliers(suppliers);
    }

    @Override
    public void onEmptyList() {
        dismissLoader();
        displayMessage("Aún no tienes ningún proveedor registrado.");
    }

    @Override
    public void onError(String messageError) {
        dismissLoader();
        displayMessage(messageError);
    }

    private void displaySuppliers(List<Supplier> suppliers){
        recyclerView.setAdapter(new SuppliersAdapter(suppliers, supplier -> {
            Supplier.Selected.current.setSupplier(supplier);
            if (isPickMode){
                this.setResult(RESULT_OK);
                this.finish();
            } else {
                navigateToSupplierMenu();
            }
        }));
    }

    private void navigateToSupplierMenu(){
        startActivity(new Intent(this, SupplierMenuActivity.class));
    }

    private void displayMessage(String message){
        textViewEmptySuppliers.setText(message);
        textViewEmptySuppliers.setVisibility(View.VISIBLE);
    }

    private void showLoader(){
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
        textViewEmptySuppliers.setVisibility(View.GONE);
    }

    private void dismissLoader(){
        progressBar.setVisibility(View.GONE);
    }

    public static class SuppliersAdapter extends RecyclerView.Adapter<SuppliersAdapter.SupplierViewHolder> {


        List<Supplier> items;
        OnSupplierSelected supplierSelectedListener;


        public SuppliersAdapter(List<Supplier> items, OnSupplierSelected supplierSelectedListener) {
            this.items = items;
            this.supplierSelectedListener = supplierSelectedListener;
        }

        @NonNull
        @Override
        public SupplierViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.select_customer_item_card, parent, false);
            return new SupplierViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull SupplierViewHolder holder, final int position) {
            holder.name.setText(items.get(position).getName());
            holder.initials.setText(items.get(position).getInitials());
            holder.phone.setText(items.get(position).getSalesPhoneNumberFormatted());
            holder.email.setText(items.get(position).getSalesEmail());
            holder.item.setOnClickListener((view -> supplierSelectedListener.onSelected(items.get(position))));
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class SupplierViewHolder extends RecyclerView.ViewHolder{

            TextView name, initials, phone, email;
            View item;

            public SupplierViewHolder(@NonNull View itemView) {
                super(itemView);
                item = itemView;
                name = itemView.findViewById(R.id.name_customer_item);
                initials = itemView.findViewById(R.id.initials_customer_item);
                phone = itemView.findViewById(R.id.phone_customer_item);
                email = itemView.findViewById(R.id.email_customer_item);
            }
        }

        public interface OnSupplierSelected {
            void onSelected(Supplier supplier);
        }
    }

}
