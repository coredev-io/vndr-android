package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_FINISH_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.OrderProductsFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.OrderDetailAdapter;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.util.Dialogs;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class InvoiceReviewActivity extends AppCompatActivity implements VndrRequest.VndrResponse, View.OnClickListener {

    Toolbar toolbar;
    RecyclerView recyclerView;
    EditText editTextNotes;
    ProgressBar progressBar;

    PurchaseInvoice purchaseInvoice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_review);
        setupUI();
        purchaseInvoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getDetail();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_invoice_review);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView = findViewById(R.id.rv_invoice_review);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        editTextNotes = findViewById(R.id.edtxt_invoice_review);
        progressBar = findViewById(R.id.pb_invoice_review);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
    }

    private void getDetail(){
        showLoading();
        new VndrRequest(Volley.newRequestQueue(this))
                .patch(URL_PURCHASE_INVOICE + "/" + purchaseInvoice.getPurchaseId() , this);
    }

    @Override
    public void error(String message) {
        dismissLoading();
        Dialogs.showAlert(this, message);
    }

    @Override
    public void success(String response) {
        dismissLoading();

        try {
            JSONObject jsonObject = new JSONObject(response).getJSONObject("info");
            purchaseInvoice = new PurchaseInvoice(jsonObject);
            purchaseInvoice.setTotalMerch();
            PurchaseInvoice.Selected.current.setPurchaseInvoice(purchaseInvoice);
            fillData();

        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud.");
        } catch (ParseException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud.");
        }
    }


    private void fillData(){
        List<String> valuesList = new ArrayList<>();
        List<String> keyList = new ArrayList<>();

        keyList.add("Proveedor");
        valuesList.add( purchaseInvoice.getSupplier().getName());

        keyList.add("Productos");
        valuesList.add(String.valueOf(purchaseInvoice.getProductList().size()));

        keyList.add("Total mercancía");
        valuesList.add(purchaseInvoice.getTotalMerchFormat());

        /*
        keyList.add("Cargo por envío");
        valuesList.add(purchaseInvoice.getShippingCostFormat());
*/

        keyList.add("Subtotal");
        valuesList.add(purchaseInvoice.getSubtotalAmount());

        keyList.add("IVA");
        valuesList.add(purchaseInvoice.getIvaAmount());

        keyList.add("Total de la factura");
        valuesList.add(purchaseInvoice.getTotalOrderAmount());

        keyList.add("Número de factura");
        valuesList.add(purchaseInvoice.getPurchaseNumber());

        keyList.add("Fecha de factura");
        valuesList.add(purchaseInvoice.getOrderDateStr());

        keyList.add("Fecha de entrega");
        valuesList.add(purchaseInvoice.getSendDateStr());

        keyList.add("Medio de pago");
        valuesList.add(purchaseInvoice.getPaymentMethod());

        keyList.add("Recompensas");
        valuesList.add(purchaseInvoice.getRewards());

        keyList.add("Tipo recompensa");
        valuesList.add(purchaseInvoice.getRewardType());

        PurchaseInvoiceDetailAdapter adapter = new PurchaseInvoiceDetailAdapter(valuesList, keyList, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        new OrderProductsFragment(purchaseInvoice.getProductList()).show(getSupportFragmentManager(),"productlist");
    }

    public void onClickModify(View view) {
        this.setResult(RESULT_BACK);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    public void onClickFinish(View view) {
        showLoading();
        Map<String, String> params = new HashMap<>();
        params.put("notes", editTextNotes.getText().toString());

        new VndrRequest(Volley.newRequestQueue(this))
                .setRequestParams(params)
                .post(URL_FINISH_PURCHASE_INVOICE + purchaseInvoice.getPurchaseId() , new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        dismissLoading();
                        Dialogs.showAlert(InvoiceReviewActivity.this, message);
                    }

                    @Override
                    public void success(String response) {
                        dismissLoading();
                        startActivityForResult(
                                new Intent(InvoiceReviewActivity.this, FinishInvoiceActivity.class),
                                1
                        );
                    }
                });
    }
}