package mx.com.vndr.vndrapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.toolbox.Volley;

import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.api.analytics.Invoice;
import mx.com.vndr.vndrapp.api.cxc.APIVndrCxc;
import mx.com.vndr.vndrapp.api.cxc.CxCMovement;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.util.Dialogs;

public class TicketMovementActivity extends AppCompatActivity implements
        APIVndrCxc.MovementResponse,
        MaterialDialog.SingleButtonCallback,
        Toolbar.OnMenuItemClickListener {

    Toolbar toolbar;
    RecyclerView recyclerView;
    Button button, continueButton;
    TextView textView;
    ProgressBar progressBar;
    CxCMovement movement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_movimiento);
        toolbar = findViewById(R.id.tb_ticket_mov);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener((view -> onBackPressed()));
        toolbar.setOnMenuItemClickListener(this);

        recyclerView = findViewById(R.id.rv_ticket_mov);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        button = findViewById(R.id.btn_ticket_mov);
        continueButton = findViewById(R.id.btn_continuar_ticket_mov);
        textView = findViewById(R.id.disclaimer);
        progressBar = findViewById(R.id.pb_ticket);
        button.setOnClickListener((view -> reverseMovement()));
        continueButton.setOnClickListener((view -> finish()));

        movement = (CxCMovement) getIntent().getSerializableExtra("move");

        if (movement.isInvoice())
            getInvoiceData();
        else {
            getData();
        }
    }

    private void getData(){
        List<VndrList> vndrLists = new ArrayList<>();
        SimpleList2 list3;

        list3 = new SimpleList2(
                "Cliente",
                movement.getCustomerName()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Fecha",
                movement.getDateFormatted()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Fecha de registro",
                movement.getCreateDateFormatted()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Pedido",
                movement.getOrderNumber()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Tipo de movimiento",
                movement.getDescription()
        );
        vndrLists.add(new VndrList(list3));


        if (movement.getType().getCode().equals("M001")){
            list3 = new SimpleList2(
                    "Método de pago",
                    movement.getPaymentMethod()
            );
            vndrLists.add(new VndrList(list3));
        }

        if (movement.getOldBalance() != null){
            list3 = new SimpleList2(
                    "Saldo anterior",
                    movement.getCurrencyOldBalance()
            );
            vndrLists.add(new VndrList(list3));
        }

        if (movement.getOldExigible() != null){
            list3 = new SimpleList2(
                    "Exigible anterior",
                    movement.getCurrencyOldExigible()
            );
            vndrLists.add(new VndrList(list3));
        }

        list3 = new SimpleList2(
                "Importe",
                movement.getCurrencyAmount()
        );
        vndrLists.add(new VndrList(list3));

        if (movement.getOldBalance() != null){
            list3 = new SimpleList2(
                    "Saldo actual",
                    movement.getCurrencyNewBalance()
            );
            vndrLists.add(new VndrList(list3));
        }

        if (movement.getNewExigible() != null){
            list3 = new SimpleList2(
                    "Exigible actual",
                    movement.getCurrencyNewExigible()
            );
            vndrLists.add(new VndrList(list3));
        }

        list3 = new SimpleList2(
                "Notas",
                movement.getComment()
        );
        vndrLists.add(new VndrList(list3));

        recyclerView.setAdapter(new VndrListAdapter(vndrLists));

        if (movement.isReversable()){
            button.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);
        } else {
            button.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
        }
    }

    private void getInvoiceData(){
        Invoice invoice = movement.getInvoice();

        List<VndrList> vndrLists = new ArrayList<>();
        SimpleList2 list3;

        list3 = new SimpleList2(
                "Proveedor",
                invoice.getSupplierName()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Total mercancía",
                invoice.getAmount()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Subtotal",
                invoice.getAmount()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "IVA",
                invoice.getIva()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Total de la factura",
                invoice.getTotalInvoice()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Número de la factura",
                invoice.getInvoiceNumber()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Fecha de factura",
                invoice.getInvoiceDate()
        );
        vndrLists.add(new VndrList(list3));

        list3 = new SimpleList2(
                "Fecha de entrega",
                invoice.getReceptionDate()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Método de pago",
                invoice.getPaymentMethod()
        );
        vndrLists.add(new VndrList(list3));


        list3 = new SimpleList2(
                "Notas",
                invoice.getNotes()
        );
        vndrLists.add(new VndrList(list3));

        recyclerView.setAdapter(new VndrListAdapter(vndrLists));

        button.setVisibility(View.GONE);
        textView.setVisibility(View.GONE);
        continueButton.setVisibility(View.GONE);

        getSupportActionBar().setTitle("Factura de compra");

    }


    private void reverseMovement(){

        Dialogs.showAlertQuestion(
                this,
                "Estás a punto de reversar este movimiento. Es una operación que ya no podrás deshacer. ¿Deseas continuar?",
                this);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (movement.isInvoice()) { return true; }
        getMenuInflater().inflate(R.menu.share_menu,menu);
        return true;
    }

    @Override
    public void reversemovementSuccess(CxCMovement movement) {
        dismissProgress();
        Toast.makeText(this, "Se reversó el movmiento con éxito", Toast.LENGTH_LONG).show();
        startActivity(
                new Intent(
                        this,
                        TicketMovementActivity.class
                ).putExtra("move", movement)
        );
        this.finish();
    }

    @Override
    public void movementError(String messageError) {
        dismissProgress();
        Dialogs.showAlert(this, messageError);
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        showProgress();
        new APIVndrCxc(Volley.newRequestQueue(this))
                .reverseMovement(movement.getId(), this);
    }

    private void showProgress(){
        button.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissProgress(){
        button.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    private void navigateToShareInformation(){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setNotificationAlertData(movement.getNotificationAlertData());

        alertFragment.show(getSupportFragmentManager(),"alert");
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        if (item.getItemId() == R.id.menu_share){
            navigateToShareInformation();
            return true;
        } else {
            return false;
        }
    }
}
