package mx.com.vndr.vndrapp.inventory.productsPurchase;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;

public class FinishInvoiceActivity extends AppCompatActivity {

    TextView textViewOrderNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_invoice);


        textViewOrderNumber = findViewById(R.id.txt_invoice_number_success);

        textViewOrderNumber.setText(PurchaseInvoice.Selected.current.getPurchaseInvoice().getPurchaseNumber());

    }

    public void onClickFinish(View view){
        this.setResult(RESULT_OK);
        this.finish();
    }

}