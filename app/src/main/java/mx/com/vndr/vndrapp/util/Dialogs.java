package mx.com.vndr.vndrapp.util;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import mx.com.vndr.vndrapp.R;

public class Dialogs {


    public static void showAlert(Context contex, String message){
        new MaterialDialog.Builder(contex)
                .title(contex.getResources().getString(R.string.app_name))
                .positiveText("Entendido")
                .content(message)
                .cancelable(false)
                .build()
                .show();
    }

    public static void showAlert(Context contex, String message, MaterialDialog.SingleButtonCallback callback){
        new MaterialDialog.Builder(contex)
                .title(contex.getResources().getString(R.string.app_name))
                .positiveText("Entendido")
                .onPositive(callback)
                .content(message)
                .cancelable(false)
                .build()
                .show();
    }

    public static void showAlertQuestion(Context contex, String message, MaterialDialog.SingleButtonCallback callbackPositive, MaterialDialog.SingleButtonCallback callbackNegative){
        new MaterialDialog.Builder(contex)
                .title(contex.getResources().getString(R.string.app_name))
                .positiveText("SI")
                .onPositive(callbackPositive)
                .negativeText("NO")
                .onNegative(callbackNegative)
                .content(message)
                .build()
                .show();
    }
    public static void showAlertQuestion(Context contex, String message, MaterialDialog.SingleButtonCallback callbackPositive){
        new MaterialDialog.Builder(contex)
                .title(contex.getResources().getString(R.string.app_name))
                .positiveText("SI")
                .onPositive(callbackPositive)
                .negativeText("NO")
                .content(message)
                .build()
                .show();
    }

    public static MaterialDialog.Builder showCustomAlert(Context context){
        return new MaterialDialog.Builder(context).title(R.string.app_name);
    }


}
