package mx.com.vndr.vndrapp.customviews.itemList;

import android.view.View;

public interface ItemRowView {
    void setTitle(String title);
    void setDescription(String description);
    void setOnClickListener(View.OnClickListener listener);
}
