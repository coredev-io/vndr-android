package mx.com.vndr.vndrapp.customviews.notificationalertfragment;

import android.content.Intent;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import mx.com.vndr.vndrapp.cobros.notifications.Notification;

public class NotificationAlertData implements Serializable {

    private String customerEmail = "";
    private String customerPhone = "";
    private String smsContent = "";
    private String whatsAppContent = "";
    private String emailContent = "";
    private String emailSubject = "";
    private String title = "Notificación";
    private String subtitle = "No olvides notificar a tu cliente";
    private String detailSMS = "Notificación de operación realizada";
    private String detailMail = "Notificación de operación realizada";
    private String alertMessage = "Advertencia";

    public NotificationAlertData() {
    }

    public NotificationAlertData(String title, JSONObject notificationJsonObject, JSONObject contactInfoJsonObject) throws JSONException {
        smsContent = notificationJsonObject.getString("sms");
        emailContent = notificationJsonObject.getString("mail");
        whatsAppContent = notificationJsonObject.getString("whatsapp");
        this.title = title;
        subtitle = notificationJsonObject.getString("subtitle");
        emailSubject = notificationJsonObject.getString("subject");
        detailMail = notificationJsonObject.getString("mail_label");
        detailSMS = notificationJsonObject.getString("sms_label");
        alertMessage = notificationJsonObject.getString("alert_message");

        customerEmail = contactInfoJsonObject.getString("email");
        customerPhone = contactInfoJsonObject.getString("mobile_phone");
    }

    public NotificationAlertData(JSONObject notificationJsonObject, JSONObject contactInfoJsonObject) throws JSONException {
        smsContent = notificationJsonObject.getString("sms");
        emailContent = notificationJsonObject.getString("mail");
        whatsAppContent = notificationJsonObject.getString("whatsapp");
        title = notificationJsonObject.getString("title");
        subtitle = notificationJsonObject.getString("subtitle");
        emailSubject = notificationJsonObject.getString("subject");
        detailMail = notificationJsonObject.getString("mail_label");
        detailSMS = notificationJsonObject.getString("sms_label");
        alertMessage = notificationJsonObject.getString("alert_message");

        customerEmail = contactInfoJsonObject.getString("email");
        customerPhone = contactInfoJsonObject.getString("mobile_phone");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDetailSMS() {
        return detailSMS;
    }

    public String getDetailMail() {
        return detailMail;
    }

    public String getAlertMessage() {
        return alertMessage;
    }

    public Intent getSMSIntent(){
        Uri sms_uri = Uri.parse("smsto:" + customerPhone);
        Intent sms_intent = new Intent(Intent.ACTION_SENDTO, sms_uri);
        sms_intent.putExtra("sms_body", smsContent);
        return  sms_intent;
    }

    public Intent getEmailIntent(){
        String[] addresses = new String[]{customerEmail};
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, addresses);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, emailSubject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, emailContent);
        return emailIntent;
    }

    public Intent getWhatsAppIntent(){
        Intent whatsAppIntent = new Intent();
        whatsAppIntent.setAction(Intent.ACTION_VIEW);
        whatsAppIntent.setPackage("com.whatsapp");
        String url = "https://api.whatsapp.com/send?phone=" + customerPhone + "&text=" + whatsAppContent;
        whatsAppIntent.setData(Uri.parse(url));
        return whatsAppIntent;
    }

    public enum SelectedCxC {
        current;

        private List<NotificationAlertData> dataList;

        public List<NotificationAlertData> getDataList() {
            return dataList;
        }

        public void setDataList(List<NotificationAlertData> dataList) {
            this.dataList = dataList;
        }
    }
}
