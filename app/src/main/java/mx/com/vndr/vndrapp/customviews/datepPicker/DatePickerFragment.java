package mx.com.vndr.vndrapp.customviews.datepPicker;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;

import mx.com.vndr.vndrapp.R;


public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    DatePickerDialog.OnDateSetListener listener;
    OnSelectedDate onSelectedDate;
    long minDate = 0;
    long maxDate = 0;
    Date initDate;

    public DatePickerFragment(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    public DatePickerFragment(DatePickerDialog.OnDateSetListener listener, Date initDate) {
        this.listener = listener;
        this.initDate = initDate;
    }

    public DatePickerFragment(DatePickerDialog.OnDateSetListener listener, long minDate, Date initDate) {
        this.listener = listener;
        this.minDate = minDate;
        this.initDate = initDate;
    }

    public DatePickerFragment(DatePickerDialog.OnDateSetListener listener, long minDate, long maxDate, Date initDate) {
        this.listener = listener;
        this.minDate = minDate;
        this.maxDate = maxDate;
        this.initDate = initDate;
    }

    public DatePickerFragment(OnSelectedDate onSelectedDate, Date initDate){
        this.onSelectedDate = onSelectedDate;
        this.initDate = initDate;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        if (initDate != null)
            c.setTime(initDate);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog;
        if (listener != null)
            dialog = new DatePickerDialog(getActivity(), listener, year, month, day);
        else
            dialog = new DatePickerDialog(getActivity(), this, year, month, day);

        if (minDate != 0)
            dialog.getDatePicker().setMinDate(minDate);
        if (maxDate != 0)
            dialog.getDatePicker().setMaxDate(maxDate);

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(i, i1, i2);
        onSelectedDate.selectedDate(calendar.getTime());
    }


    public interface OnSelectedDate {
        void selectedDate(Date date);
    }
}