package mx.com.vndr.vndrapp.inventory.movements;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.inventory.ProductInventoryActivity;
import mx.com.vndr.vndrapp.inventory.SelectStdClassActivity;
import mx.com.vndr.vndrapp.inventory.rules.InventoryRulesActivity;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class MenuMovesInventoryActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_moves_inventory);
        setupUI();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_inventory_moves);
        recyclerView = findViewById(R.id.rv_inventory_moves);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuMovesOption1));
        optionList.add(new Option(options.get(0), options.get(1), this::askForMovementQuery));

        options = Arrays.asList(getResources().getStringArray(R.array.menuMovesOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::pickProductForEntry));

        options = Arrays.asList(getResources().getStringArray(R.array.menuMovesOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::pickProductForExit));

        options = Arrays.asList(getResources().getStringArray(R.array.menuMovesOption4));
        optionList.add(new Option(options.get(0), options.get(1), this::navigateToArqueoFlow));

        return optionList;
    }

    private void navigateToArqueoFlow(){
        startActivityForResult(new Intent(this, PickBrandActivity.class)
                .putExtra("pickProduct", true)
                .putExtra("shoppingOrder", true),4);
    }

    private void askForMovementQuery(){
        new MaterialDialog.Builder(this)
                .title("Movimientos de inventario")
                .items(R.array.items_query_inventory_menu)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    if (which == 0) {
                        startActivity(new Intent(MenuMovesInventoryActivity.this, InventoryMovementsActivity.class));
                    } else {
                        askForMovementProductQuery();
                    }
                    return false;
                })
                .show();
    }

    private void askForMovementProductQuery(){
        new MaterialDialog.Builder(this)
                .title("Movimientos de inventario")
                .items(R.array.items_product_query_inventory_menu)
                .itemsCallbackSingleChoice(-1, (dialog, itemView, which, text) -> {
                    if (which == 0) {
                        startActivityForResult(new Intent(this, PickBrandActivity.class)
                                .putExtra("pickProduct", true)
                                .putExtra("shoppingOrder", true),2);
                    } else {
                        startActivityForResult(new Intent(this, PickBrandActivity.class)
                                .putExtra("pickProduct", true)
                                .putExtra("shoppingOrder", true),3);
                    }
                    return false;
                })
                .show();
    }



    private void pickProductForEntry(){
        startActivityForResult(new Intent(this, PickBrandActivity.class)
                .putExtra("pickProduct", true)
                .putExtra("shoppingOrder", true),0);

    }

    private void pickProductForExit(){
        startActivityForResult(new Intent(this, PickBrandActivity.class)
                .putExtra("pickProduct", true)
                .putExtra("shoppingOrder", true),1);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, InventoryEntryActivity.class)
                    .putExtra("product", product)
            );
        }

        if (requestCode == 1  && resultCode == RESULT_OK){
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, InventoryExitActivity.class)
                    .putExtra("product", product));
        }

        if (requestCode == 2  && resultCode == RESULT_OK){
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, MovementResumeActivity.class)
                    .putExtra("product", product)
            );
        }

        if (requestCode == 3  && resultCode == RESULT_OK){
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, MovementResumeActivity.class)
                    .putExtra("product", product)
                    .putExtra("isQueryByTicket", true)
            );
        }

        if (requestCode == 4  && resultCode == RESULT_OK){
            Product product = (Product) data.getSerializableExtra("product");
            startActivity(new Intent(this, ArqueoActivity.class)
                    .putExtra("product", product)
            );
        }
    }

    public void onClickButton(View view){
        startActivity(new Intent(this, MovementSuccessActivity.class)
                .putExtra("ticket", 0)
        );
    }

}