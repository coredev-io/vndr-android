package mx.com.vndr.vndrapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.NumberFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.abhinay.input.CurrencyEditText;
import mx.com.vndr.vndrapp.models.Cxc;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.ShoppingCartActivity;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class ConfigPaymentsActivity extends AppCompatActivity {

    Toolbar toolbar;
    int c = 1;
    EditText count;
    CurrencyEditText anticipo;
    TextView total, payment, balance;
    TextInputLayout textInputLayout;
    Order order;
    Double pedido, aPagar, eachPayment;
    double mAnticipoAux = 0.00;

    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config_payments);
        order = (Order) getIntent().getSerializableExtra("order");

        setupUI();
        setupData();
    }

    public void onClickModifyOrder(View view){
        this.setResult(RESULT_CANCELED);
        this.finish();
    }

    public void onClickNext(View view){

        //if (anticipo.getText().toString().isEmpty() || anticipo.getText().equals("0") || anticipo.getText().equals("0.00"))
        if (anticipo.getCleanDoubleValue() == 0.00)
            validaAnticipoVacio();
        else{
            //order.getCxc().setAnticipo(getCleanDoubleValue(anticipo.getText().toString()));
            order.getCxc().setAnticipo(anticipo.getCleanDoubleValue());
            order.getCxc().setNumPagos(c);
            order.getCxc().setMontoPago(eachPayment);
            order.getCxc().setSaldoPlazo(aPagar);


            startActivityForResult(
                    new Intent(ConfigPaymentsActivity.this, PickDateActivity.class)
                            .putExtra("type",PickDateActivity.Type.ANTICIPO)
                            .putExtra("order", order)
                    ,1);
        }

    }

    public void validaAnticipoVacio(){
        new MaterialDialog.Builder(this)
                .content("Este pedido no tiene anticipo ¿Quieres continuar?")
                .negativeText("Cancelar")
                .positiveText("Continuar")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //order.getCxc().setAnticipo(getCleanDoubleValue(anticipo.getText().toString()));
                        order.getCxc().setAnticipo(anticipo.getCleanDoubleValue());
                        order.getCxc().setNumPagos(c);
                        order.getCxc().setMontoPago(eachPayment);
                        order.getCxc().setSaldoPlazo(aPagar);


                        startActivityForResult(new Intent(ConfigPaymentsActivity.this, PickDateActivity.class)
                                        .putExtra("type", PickDateActivity.Type.FIRST_PAY)
                                        .putExtra("order",order)
                                        .putExtra("isPaid",true)
                                , 1);
                    }
                })
                .cancelable(false)
                .build()
                .show();
    }

    public void onRemove(View view){
        if (c != 1){
            c -= 1;
            updateBalance();
        }
    }

    public void onAdd(View view){
        c += 1;
        updateBalance();
    }

    private void setupUI(){
        count = findViewById(R.id.count_config);
        anticipo = findViewById(R.id.anticipo_config);
        total = findViewById(R.id.total_config);
        payment = findViewById(R.id.payment_config);
        balance = findViewById(R.id.balance_config);
        toolbar = findViewById(R.id.tb_config_pay);
        button = findViewById(R.id.btn_continuar_config_pay);
        textInputLayout = findViewById(R.id.textInputLayoutAnticipo);


        anticipo.setCurrency("$");
        anticipo.setDelimiter(false);
        anticipo.setSpacing(true);
        anticipo.setDecimals(true);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        anticipo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                anticipo.setSelection(anticipo.getText().toString().length());
                return false;
            }
        });

        //anticipo.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});


        anticipo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                //if (aPagar != 0)
                    aPagar = aPagar + mAnticipoAux;
                //mAnticipoAux = getCleanDoubleValue(anticipo.getText().toString());
                mAnticipoAux = anticipo.getCleanDoubleValue();
                aPagar = aPagar - mAnticipoAux;
                updateBalance();

            }
        });
    }

    private void setupData(){
        pedido = order.getTotalOrderAmount();
        aPagar = order.getTotalOrderAmount();
        eachPayment = order.getTotalOrderAmount();

        updateBalance();
    }

    private void updateBalance(){
        eachPayment = aPagar / c;
        total.setText(NumberFormat.getCurrencyInstance().format(pedido));
        balance.setText(NumberFormat.getCurrencyInstance().format(aPagar));
        payment.setText(NumberFormat.getCurrencyInstance().format(eachPayment));
        count.setText(String.valueOf(c));

        if (aPagar <= 0.00){
            button.setEnabled(false);
            textInputLayout.setError("El anticipo no puede ser mayor al total del pedido.");
        } else {
            button.setEnabled(true);
            textInputLayout.setError("");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }

    private double getCleanDoubleValue(String value){
        if (value.isEmpty() || value.equals("."))
            return 0;
        else
            return Double.parseDouble(value);
    }
}
