package mx.com.vndr.vndrapp.customviews.itemList;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mx.com.vndr.vndrapp.R;

public class ItemListAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private ItemListPresenter presenter;

    public ItemListAdapter(ItemListPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ItemViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,parent,false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        presenter.onBindItemRowViewAtPosition(holder,position);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
