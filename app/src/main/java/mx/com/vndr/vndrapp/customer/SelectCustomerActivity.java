package mx.com.vndr.vndrapp.customer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.CustomerAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Customer;

import static android.view.View.GONE;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CUSTOMERS;

public class SelectCustomerActivity extends AppCompatActivity implements CustomerAdapter.OnCustomerSelected {

    RecyclerView recyclerView;

    List<Customer> customerList = new ArrayList<>();
    CustomerAdapter adapter;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView emptyCustomers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_customer);

        recyclerView = findViewById(R.id.rv_select_customer);
        progressBar = findViewById(R.id.pb_pick_customer);
        emptyCustomers = findViewById(R.id.txt_empty_customers);
        emptyCustomers.setVisibility(GONE);

        toolbar = findViewById(R.id.tb_pick_customer);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
    }

    @Override
    protected void onResume() {
        super.onResume();
        emptyCustomers.setVisibility(GONE);
        emptyCustomers.setVisibility(GONE);
        customerList = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter = new CustomerAdapter(customerList, this);

        recyclerView.setAdapter(adapter);
        getCustomers();

    }

    public void onCreateNew(View view){
        startActivity(
                new Intent(this,AddCustomerActivity.class)
        );
    }

    private void getCustomers(){
        progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_CUSTOMERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(GONE);
                processResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
                progressBar.setVisibility(GONE);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };


        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void processResponse(String response){



        try {
            JSONArray responseArray = new JSONArray(response);

            for (int i = 0; i < responseArray.length(); i ++) {
                JSONObject customerObject = responseArray.getJSONObject(i);
                Customer customer = new Customer(
                        customerObject.getString("_id"),
                        customerObject.getString("full_name"),
                        customerObject.getString("first_given_name"),
                        customerObject.getString("first_family_name"),
                        customerObject.getString("email"),
                        customerObject.getString("mobile_phone"),
                        customerObject.getString("zipcode"),
                        customerObject.getString("gender"),
                        customerObject.getString("second_given_name"),
                        customerObject.getString("second_family_name")
                    );

                customerList.add(customer);

            }
            System.out.println(
                    customerList.size());
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
            emptyCustomers.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSelected(Customer customer) {
        this.setResult(RESULT_OK, new Intent().putExtra("customer", customer));
        this.finish();
    }
}
