package mx.com.vndr.vndrapp;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import mx.com.vndr.vndrapp.adapters.PaymentMethodAdapter;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.orders.DeliveryActivity;
import mx.com.vndr.vndrapp.orders.OrderDetailActivity;

import static android.view.View.GONE;
import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class PickDateActivity extends AppCompatActivity {


    public enum Type{
        SALE_DATE, PAY_DATE, ANTICIPO, FIRST_PAY
    }
    CalendarView calendarView;
    Toolbar toolbar;
    TextView detail;
    Date selectedDate = new Date(System.currentTimeMillis());
    Type selectedType = Type.SALE_DATE;
    MaterialButton button;
    Boolean isPaid = false;
    RadioButton firstDate, secondDate;
    Date first, second;
    CardView firstVC, secondCV;

    Order order;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_date);

        if (getIntent().hasExtra("type")){
            selectedType = (Type) getIntent().getSerializableExtra("type");
        }


        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
        }
        isPaid = getIntent().getBooleanExtra("isPaid",false);


        setupUI();

        if (order.getSaleDate() != null)
            calendarView.setDate(order.getSaleDate().getTime());
    }

    @Override
    protected void onStart() {
        super.onStart();
        customUI(selectedType);
    }

    private void customUI(Type type){
        switch (type){

            case FIRST_PAY:

                toolbar.setTitle("Primer pago a plazos");
                detail.setText("Seleccione la fecha del primer pago a plazos");
                button.setText("Confirmar fecha");
                if (order.getCxc().getPayFrequency() == 15){
                    calendarView.setVisibility(GONE);
                    button.setVisibility(GONE);
                    firstVC.setVisibility(View.VISIBLE);
                    secondCV.setVisibility(View.VISIBLE);
                    selectedDate = order.getSaleDate();
                    int day = Integer.valueOf(new SimpleDateFormat("dd").format(selectedDate));
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(selectedDate);
                    if (day >= 16){
                        calendar.add(Calendar.MONTH,1);
                        firstDate.setText(new SimpleDateFormat("1/MMM/yyyy").format(calendar.getTime()));
                        secondDate.setText(new SimpleDateFormat("16/MMM/yyyy").format(calendar.getTime()));
                        calendar.set(Calendar.DAY_OF_MONTH,1);
                        first = calendar.getTime();
                        calendar.set(Calendar.DAY_OF_MONTH, 16);
                        second = calendar.getTime();
                    } else {
                        firstDate.setText(new SimpleDateFormat("16/MMM/yyyy").format(calendar.getTime()));
                        calendar.set(Calendar.DAY_OF_MONTH,16);
                        first = calendar.getTime();
                        calendar.add(Calendar.MONTH,1);
                        secondDate.setText(new SimpleDateFormat("1/MMM/yyyy").format(calendar.getTime()));
                        calendar.set(Calendar.DAY_OF_MONTH, 1);
                        second = calendar.getTime();
                    }



                } else {

                    //  Si hay anticipo la fecha de primer pago minima es el día siguiente
                    if (order.getCxc().getPayDate()!= null) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.setTime(order.getCxc().getPayDate());
                        calendar.add(Calendar.DAY_OF_YEAR, 1);
                        calendarView.setMinDate(calendar.getTimeInMillis());
                        selectedDate = calendar.getTime();
                    }
                    //  Si no, la fecha minima para primer pago es fecha de venta
                    else {
                        calendarView.setMinDate(order.getSaleDate().getTime());
                        selectedDate = order.getSaleDate();
                    }


                    int day = Integer.valueOf(new SimpleDateFormat("dd").format(selectedDate));
                    if (selectedType == Type.FIRST_PAY && day > 28 && order.getCxc().getPayFrequency() == 30){
                        button.setEnabled(false);
                        Toast.makeText(PickDateActivity.this, "No se permite día de pago mayor a 28", Toast.LENGTH_LONG).show();
                    }

                }




                break;
            case ANTICIPO:
                toolbar.setTitle("Fecha de pago de anticipo");
                detail.setText("Seleccione la fecha de pago de anticipo");
                calendarView.setDate(order.getSaleDate().getTime());
                selectedDate = order.getSaleDate();
                calendarView.setMinDate(order.getSaleDate().getTime());

                if (selectedDate.after(new Date())){
                    changeButton(false);
                } else {
                    changeButton(true);
                }
                break;

            case SALE_DATE:
                calendarView.setMaxDate(System.currentTimeMillis());

                if (order.getSaleDate() != null){
                    calendarView.setDate(order.getSaleDate().getTime());
                    selectedDate = order.getSaleDate();
                }

                break;

            case PAY_DATE:
                toolbar.setTitle("Fecha de pago");
                detail.setText("Seleccione la fecha de pago");
                calendarView.setDate(order.getSaleDate().getTime());
                selectedDate = order.getSaleDate();
                calendarView.setMinDate(order.getSaleDate().getTime());

                if (selectedDate.after(new Date())){
                    changeButton(false);
                } else {
                    changeButton(true);
                }

                break;
        }
    }

    public void onClickNextPickDate(View view){

        switch (selectedType){
            case FIRST_PAY:
                order.getCxc().setFirstPayDate(selectedDate);
                startActivityForResult(
                        new Intent(PickDateActivity.this, PaymentCalendarActivity.class)
                                .putExtra("order", order)
                                .putExtra("isPaid",getIntent().getBooleanExtra("isPaid",false)),
                        1
                );
                break;
            case ANTICIPO:
                order.getCxc().setPayDate(selectedDate);
                if (isPaid){
                    startActivityForResult(new Intent(this, PickPaymentMethodActivity.class)
                            , 9);
                } else {
                    order.getCxc().setMethodPayment("Pendiente de pago");
                    startActivityForResult(new Intent(this, PickDateActivity.class)
                                    .putExtra("type",Type.FIRST_PAY)
                                    .putExtra("order",order)
                            , 1);
                }
                break;
            case PAY_DATE:
                order.getCxc().setPayDate(selectedDate);

                if (isPaid){
                    startActivityForResult(
                            new Intent(
                                    this,
                                    PickPaymentMethodActivity.class
                            ),
                            9
                    );
                } else {
                    order.getCxc().setMethodPayment("Pendiente de pago");
                    startActivityForResult(new Intent(this, OrderDetailActivity.class)
                                    .putExtra("order",order)
                                    .putExtra("isPaid",isPaid)
                            , 1);
                }

                break;
            case SALE_DATE:
                order.setSaleDate(selectedDate);
                startActivityForResult(new Intent(this, DeliveryActivity.class)
                        .putExtra("order",order)
                        , 1);
                break;
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        } else if (requestCode == 9 && resultCode == RESULT_OK){
            order.getCxc().setMethodPayment( data.getStringExtra("method"));
            Log.e("TAG", "asdfasd");
            if (order.getCxc().getPayFrequency() != 1){
                startActivityForResult(new Intent(this, PickDateActivity.class)
                                .putExtra("type", PickDateActivity.Type.FIRST_PAY)
                                .putExtra("order",order)
                                .putExtra("isPaid",true)

                        ,1);
            } else{
                startActivityForResult(new Intent( this, OrderDetailActivity.class)
                        .putExtra("order",order),1);
            }
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }


    private void changeButton(Boolean isNext){

        if (isNext){
            isPaid = true;
            button.setBackgroundTintList(ContextCompat.getColorStateList(this,R.color.colorAccent));
            button.setText("Siguiente");
        } else{
            isPaid = false;
            button.setBackgroundTintList(ContextCompat.getColorStateList(this,R.color.colorPrimary));
            button.setText("Aún no recibo el pago");
        }

    }


    private void setupUI(){
        calendarView = findViewById(R.id.calendarView);
        toolbar = findViewById(R.id.tb_pick_date);
        detail = findViewById(R.id.detail_pick_date);
        button = findViewById(R.id.btn_pick_date);

        firstDate = findViewById(R.id.rb_first_pay);
        secondDate = findViewById(R.id.rb_second_pay);
        firstVC = findViewById(R.id.cv_first_pay);
        secondCV = findViewById(R.id.cv_second_pay);
        firstVC.setVisibility(GONE);
        secondCV.setVisibility(GONE);


        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendarView, int i, int i1, int i2) {
                Calendar c = Calendar.getInstance();
                c.set(i, i1, i2);
                Date d = c.getTime();

                if (selectedType == Type.FIRST_PAY && i2 > 28 && order.getCxc().getPayFrequency() == 30){
                    Toast.makeText(PickDateActivity.this, "No se permite día de pago mayor a 28", Toast.LENGTH_LONG).show();
                    button.setEnabled(false);
                } else{
                    button.setEnabled(true);
                    selectedDate = d;
                }


                if (selectedType == Type.PAY_DATE || selectedType == Type.ANTICIPO){
                    if (selectedDate.after(new Date())){
                        changeButton(false);
                    } else {
                        changeButton(true);
                    }
                }
            }
        });

        firstDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    selectedDate = first;
                    order.getCxc().setFirstPayDate(selectedDate);
                    startActivityForResult(
                            new Intent(PickDateActivity.this, PaymentCalendarActivity.class)
                            .putExtra("order", order)
                                    .putExtra("isPaid",isPaid),
                            1
                    );
                    firstDate.setChecked(false);
                }
            }
        });

        secondDate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    selectedDate = second;
                    order.getCxc().setFirstPayDate(selectedDate);
                    startActivityForResult(
                            new Intent(PickDateActivity.this, PaymentCalendarActivity.class)
                                    .putExtra("order", order)
                                    .putExtra("isPaid",isPaid),
                    1
                    );
                    secondDate.setChecked(false);

                }
            }
        });
    }
}
