package mx.com.vndr.vndrapp.analytics.collections;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.APICollections;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;

public class StatusCollectionActivity extends AppCompatActivity implements APICollections.OnCollectionResponse {

    Toolbar toolbar;
    RecyclerView recyclerView;
    TextView textViewError;
    ProgressBar progressBar;
    Query query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_collection);

        setupUI();
        if (getIntent().hasExtra("query")){
            query = (Query) getIntent().getSerializableExtra("query");
        } else {
            query = Query.CURRENT;
        }
        getData();
        if (query == Query.CXC_REC){
            getSupportActionBar().setTitle("Cuentas activas");
        }
    }

    public void getData(){
        showLoader();
        new APICollections(Volley.newRequestQueue(this))
                .getCollection(this, query);

    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_status_coll);
        recyclerView = findViewById(R.id.rv_status_coll);
        textViewError = findViewById(R.id.txt_error_status_colletion);
        progressBar = findViewById(R.id.pb_status_collection);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onSuccess(VndrListAdapter adapter) {
        dismissLoader();
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onError(String error) {
        showError(error);
    }

    private void showLoader(){
        textViewError.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    private void dismissLoader(){
        progressBar.setVisibility(View.GONE);
    }

    private void showError(String error){
        dismissLoader();
        textViewError.setVisibility(View.VISIBLE);
        textViewError.setText(error);
    }

    public enum Query {
        CURRENT, CXC_REC
    }
}