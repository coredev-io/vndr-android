package mx.com.vndr.vndrapp.api.inventory;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import mx.com.vndr.vndrapp.models.Product;

public class SuggestionProducts implements Serializable {

    private Product product;
    private String countMissing;
    private String countIncomplete;
    private String countPendingShipping;
    private String countPendingDelivery;

    public SuggestionProducts(JSONObject jsonSuggestion) throws JSONException {
        this.product = new Product(jsonSuggestion);
        this.countMissing = String.valueOf(jsonSuggestion.getInt("cnt_products"));
        this.countIncomplete = String.valueOf(jsonSuggestion.getInt("cnt_incomplete"));
        this.countPendingShipping = String.valueOf(jsonSuggestion.getInt("cnt_pendding_shipping_supplier"));
        this.countPendingDelivery = String.valueOf(jsonSuggestion.getInt("cnt_pendding_shipping_personal"));
        this.product.setInventory(new Inventory(jsonSuggestion.getJSONObject("inventory")));
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getCountMissing() {
        return countMissing;
    }

    public void setCountMissing(String countMissing) {
        this.countMissing = countMissing;
    }

    public String getCountIncomplete() {
        return countIncomplete;
    }

    public void setCountIncomplete(String countIncomplete) {
        this.countIncomplete = countIncomplete;
    }

    public String getCountPendingShipping() {
        return countPendingShipping;
    }

    public void setCountPendingShipping(String countPendingShipping) {
        this.countPendingShipping = countPendingShipping;
    }

    public String getCountPendingDelivery() {
        return countPendingDelivery;
    }

    public void setCountPendingDelivery(String countPendingDelivery) {
        this.countPendingDelivery = countPendingDelivery;
    }
}
