package mx.com.vndr.vndrapp.inventory.productsPurchase;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_DELETE_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_FINISH_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASES_CART;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASE_INVOICE_CART;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.POCartActivity;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.util.DecimalDigitsInputFilter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class CartRegisterPurchaseActivity extends AppCompatActivity implements VndrRequest.VndrResponse {

    Toolbar toolbar;
    RecyclerView recyclerView;
    Button button;
    EditText editTextShippingAmount;
    TextView textViewNoItems;
    TextView textViewTotalAmount;
    TextView textViewSupplierName;
    ProgressBar progressBar;

    private double shoppingOrderAmount = 0;
    private double shippingAmountAux = 0;
    private List<ShoppingCartProduct> productList = new ArrayList<>();
    CartRegisterPurchaseActivity.ShoppingCartProductAdapter adapter;
    boolean orderWasUpdated = false;
    Supplier supplier;
    PurchaseInvoice purchaseInvoice;
    boolean updateMode = false;
    boolean continueLater = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_register_purchase);
        setupUI();

        supplier = Supplier.Selected.current.getSupplier();


        if (getIntent().hasExtra("purchase_invoice")){
            updateMode = true;
            purchaseInvoice = (PurchaseInvoice) getIntent().getSerializableExtra("purchase_invoice");
            supplier = purchaseInvoice.getSupplier();
            getInvoiceDetail();
        }

        textViewSupplierName.setText(supplier.getName());

        checkListProducts();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.shopping_cart_menu,menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (orderWasUpdated){
            Dialogs.showAlertQuestion(
                    this,
                    "¿Deseas guardar la factura y continuar después?",
                    ((dialog, which) -> {onClickSaveAndContinueLater(); continueLater = true; }),
                    (dialog, which) -> CartRegisterPurchaseActivity.this.finish()
            );
        } else this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0  && resultCode == RESULT_OK){
            //  Add product to list
            ShoppingCartProduct product = (ShoppingCartProduct) data.getSerializableExtra("product");
            productList.add(product);
            adapter.notifyDataSetChanged();
            orderWasUpdated = true;
            updateTotalOrderUI(true, product.getSubtotal());
            checkListProducts();
        }

        if (requestCode == 1 && resultCode == RESULT_OK){
            this.finish();
        }

        if (requestCode == 1 && resultCode != RESULT_OK){
            purchaseInvoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();
            updateMode = true;
        }
    }

    private void onClickSaveAndContinueLater() {
        showLoading();
        buildShoppingOrder();
        createUpdateShoppingOrder(purchaseInvoice, updateMode);
    }

    private void buildShoppingOrder(){
        if (purchaseInvoice == null){
            purchaseInvoice = new PurchaseInvoice();
            purchaseInvoice.setSupplier(supplier);
        }
        purchaseInvoice.setShippingCost(getCleanDoubleValue(editTextShippingAmount.getText().toString()));
        purchaseInvoice.setProductList(productList);
        purchaseInvoice.setTotalOrderAmount(shoppingOrderAmount);
        purchaseInvoice.setTotalMerch();
        PurchaseInvoice.Selected.current.setPurchaseInvoice(purchaseInvoice);
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_rp_cart);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.menu_delete_order){
                Dialogs.showAlertQuestion(
                        this,
                        "¿Estás seguro que ya no requieres este carrito?",
                        (dialog, which) -> onClickDeleteShoppingCart());
            }
            return false;
        });

        recyclerView = findViewById(R.id.rv_rp_cart);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        textViewSupplierName = findViewById(R.id.txt_rp_cart_supplier_name);
        progressBar = findViewById(R.id.pb_rp_cart);

        button = findViewById(R.id.btn_rp_cart);
        editTextShippingAmount = findViewById(R.id.et_rp_shipping_order);
        editTextShippingAmount.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
        editTextShippingAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                orderWasUpdated = true;
                if (shoppingOrderAmount != 0)
                    shoppingOrderAmount = shoppingOrderAmount - shippingAmountAux;
                shippingAmountAux = getCleanDoubleValue(editable.toString());
                updateTotalOrderUI(true, shippingAmountAux);

            }
        });

        textViewNoItems = findViewById(R.id.txt_rp_no_items);
        textViewTotalAmount = findViewById(R.id.txt_rp_total);

        adapter = new CartRegisterPurchaseActivity.ShoppingCartProductAdapter(productList, this);
        recyclerView.setAdapter(adapter);
    }

    private void onClickDeleteShoppingCart() {

        if (purchaseInvoice == null || purchaseInvoice.getPurchaseId() == null)
            this.finish();
        else{
            new VndrRequest(Volley.newRequestQueue(this))
                    .delete(URL_DELETE_PURCHASE_INVOICE + "/" +  purchaseInvoice.getPurchaseId() , new VndrRequest.VndrResponse() {
                        @Override
                        public void error(String message) {
                            Dialogs.showAlert(CartRegisterPurchaseActivity.this, message);
                        }

                        @Override
                        public void success(String response) {
                            finish();
                        }
                    });

        }
    }

    public void updateTotalOrderUI(boolean isAdd, Double value){
        if (isAdd)
            shoppingOrderAmount = shoppingOrderAmount + value;
        else if (!isAdd && shoppingOrderAmount != 0.00)
            shoppingOrderAmount = shoppingOrderAmount - value;

        textViewTotalAmount.setText(NumberFormat.getCurrencyInstance().format(shoppingOrderAmount));
    }

    public void checkListProducts(){
        if (productList.isEmpty()){
            button.setEnabled(false);
            textViewNoItems.setVisibility(View.VISIBLE);
        } else {
            button.setEnabled(true);
            textViewNoItems.setVisibility(View.GONE);
        }
    }

    private double getCleanDoubleValue(String value){
        if (value.isEmpty() || value.equals("."))
            return 0;
        else
            return Double.parseDouble(value);
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }

    private void dismiss(){
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    private void showError(String messageError){
        dismiss();
        Dialogs.showAlert(this, messageError);
    }

    public void onClickAddProduct(View view){
        startActivityForResult(new Intent(this, PickBrandActivity.class).putExtra("shoppingOrder", true),0);
    }

    public void onClickContinuar(View view){
        onClickSaveAndContinueLater();
    }

    private void getInvoiceDetail(){
        showLoading();
        new VndrRequest(Volley.newRequestQueue(this))
                .patch(URL_PURCHASE_INVOICE + "/" + purchaseInvoice.getPurchaseId() , this);
    }


    @Override
    public void error(String message) {
        dismiss();
        Dialogs.showAlert(this, message);
    }

    @Override
    public void success(String response) {
        dismiss();

        try {
            JSONObject jsonObject = new JSONObject(response).getJSONObject("info");
            purchaseInvoice = new PurchaseInvoice(jsonObject);
            purchaseInvoice.setTotalMerch();
            PurchaseInvoice.Selected.current.setPurchaseInvoice(purchaseInvoice);
            fillData();

        } catch (JSONException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud.");
        } catch (ParseException e) {
            e.printStackTrace();
            Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud.");
        }
    }

    private void fillData(){
        shoppingOrderAmount = purchaseInvoice.getTotalOrderAmountDouble();
        textViewTotalAmount.setText(purchaseInvoice.getTotalOrderAmount());
        if (purchaseInvoice.getShippingCost() != null)
            editTextShippingAmount.setText(purchaseInvoice.getShippingCostDecimalFormat());
        productList = purchaseInvoice.getProductList();
        adapter = new ShoppingCartProductAdapter(productList, this);
        recyclerView.setAdapter(adapter);
        checkListProducts();
    }

    private void createUpdateShoppingOrder(PurchaseInvoice purchaseInvoice, boolean isUpdate){

        try {
            new VndrRequest(Volley.newRequestQueue(this))
                    .setRequestByteParams(purchaseInvoice.getCreateUpdateOrderBody(isUpdate))
                    .post(URL_PURCHASE_INVOICE_CART, new VndrRequest.VndrResponse() {
                        @Override
                        public void error(String message) {
                            showError(message);
                        }

                        @Override
                        public void success(String response) {
                            PurchaseInvoice invoice = PurchaseInvoice.Selected.current.getPurchaseInvoice();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getInt("statusCode") != 200){
                                    showError(jsonObject.getString("message"));
                                    return;
                                }
                                JSONObject orderInfo = jsonObject.getJSONObject("info");
                                invoice.setPurchaseId(orderInfo.getString("_id"));
                                invoice.setPurchaseNumber(String.valueOf(orderInfo.getInt("purchase_invoice_number")));

                                PurchaseInvoice.Selected.current.setPurchaseInvoice(invoice);
                                dismiss();
                                if (continueLater) finish();
                                else navigateToTotalInvoice();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                showError("Ocurrió un error inesperado al enviar la solicitud");
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            showError("Ocurrió un error inesperado al enviar la solicitud");
        }
    }

    private void navigateToTotalInvoice(){
        startActivityForResult(new Intent(this, TotalInvoiceActivity.class), 1);
    }


    public class ShoppingCartProductAdapter extends RecyclerView.Adapter<CartRegisterPurchaseActivity.ShoppingCartProductAdapter.ProductViewHolder> {

        private List<ShoppingCartProduct> items;
        private Context context;

        public ShoppingCartProductAdapter(List<ShoppingCartProduct> items, Context context) {
            this.items = items;
            this.context = context;
        }

        @NonNull
        @Override
        public CartRegisterPurchaseActivity.ShoppingCartProductAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.shopping_cart_item, parent, false);
            return new CartRegisterPurchaseActivity.ShoppingCartProductAdapter.ProductViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull CartRegisterPurchaseActivity.ShoppingCartProductAdapter.ProductViewHolder holder, final int position) {
            holder.count.setText(String.valueOf(position + 1));
            holder.productName.setText(items.get(position).getProduct().getProductName());
            holder.productKey.setText(items.get(position).getProduct().getProduct_key());
            holder.subtotal.setText(items.get(position).getSubtotalString());
            holder.salePrice.setText(
                    items.get(position).getProductsCount() +
                            " piezas x " +
                            items.get(position).getBuyPriceStr()
            );


            if (items.get(position).getProduct().getColor().isEmpty())
                holder.color.setVisibility(View.GONE);
            else
                holder.color.setText(items.get(position).getProduct().getColor());

            if (items.get(position).getProduct().getSize().isEmpty())
                holder.size.setVisibility(View.GONE);
            else
                holder.size.setText(items.get(position).getProduct().getSize());

            if (items.get(position).getNotes().isEmpty())
                holder.notes.setVisibility(View.GONE);
            else {
                holder.notes.setVisibility(View.VISIBLE);

                holder.notes.setText(items.get(position).getNotes());
            }

            holder.delete.setOnClickListener(view -> {

                ((CartRegisterPurchaseActivity) context).orderWasUpdated = true;
                ((CartRegisterPurchaseActivity) context).updateTotalOrderUI(false, items.get(position).getSubtotal());
                items.remove(position);
                CartRegisterPurchaseActivity.ShoppingCartProductAdapter.this.notifyDataSetChanged();
                ((CartRegisterPurchaseActivity) context).checkListProducts();
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        public class ProductViewHolder extends RecyclerView.ViewHolder {

            TextView count, productName, productKey, salePrice, subtotal;
            TextView size, color, notes;
            ImageButton delete;

            public ProductViewHolder(@NonNull View itemView) {
                super(itemView);

                count = itemView.findViewById(R.id.count_cart_item);
                productName = itemView.findViewById(R.id.product_name_cart_item);
                productKey = itemView.findViewById(R.id.product_key_item);
                salePrice = itemView.findViewById(R.id.sale_price_cart_item);
                subtotal = itemView.findViewById(R.id.subtotal_cart_item);
                delete = itemView.findViewById(R.id.delete_cart_item);
                size = itemView.findViewById(R.id.size_item);
                color = itemView.findViewById(R.id.color_item);
                notes = itemView.findViewById(R.id.notes_item);
            }
        }
    }

}