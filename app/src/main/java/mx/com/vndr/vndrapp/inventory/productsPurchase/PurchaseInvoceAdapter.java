package mx.com.vndr.vndrapp.inventory.productsPurchase;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.PurchaseInvoice;


public class PurchaseInvoceAdapter extends RecyclerView.Adapter<PurchaseInvoceAdapter.PurchaseInvoiceViewHolder> {

    public interface OnPurchaseInvoceSelected {
        void onSelected(PurchaseInvoice purchaseInvoice);
    }

    private List<PurchaseInvoice> items;
    private Context context;
    private OnPurchaseInvoceSelected onPurchaseInvoceSelected;

    public PurchaseInvoceAdapter(List<PurchaseInvoice> purchaseInvoices, Context context) {
        this.items = purchaseInvoices;
        this.context = context;
    }

    public PurchaseInvoceAdapter setOnShoppingOrderSelected(OnPurchaseInvoceSelected onPurchaseInvoceSelected) {
        this.onPurchaseInvoceSelected = onPurchaseInvoceSelected;
        return this;
    }

    @NonNull
    @Override
    public PurchaseInvoiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_card, parent, false);
        return new PurchaseInvoiceViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PurchaseInvoiceViewHolder holder, int position) {
        PurchaseInvoice purchaseInvoice = items.get(position);
        holder.name.setText(purchaseInvoice.getSupplier().getName());
        holder.initials.setText(purchaseInvoice.getSupplier().getInitials());
        holder.order.setText(purchaseInvoice.getPurchaseNumber());
        holder.balance.setText(purchaseInvoice.getTotalOrderAmount());
        holder.saleDate.setText(purchaseInvoice.getOrderDateStr());
        holder.status.setText(purchaseInvoice.getStatusLabel());
        setColor(holder.status.getBackground(), purchaseInvoice.getStatusCode());
        if (onPurchaseInvoceSelected != null)
            holder.cardView.setOnClickListener(view -> onPurchaseInvoceSelected.onSelected(purchaseInvoice));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setColor(Drawable background, String status){

        int color = 0;

        switch (status){
            case "I01":
                color = ContextCompat.getColor(context,R.color.colorYellow);
                break;
            case "I02":
                color = ContextCompat.getColor(context,R.color.colorComplete);
                break;
            default:
                color = ContextCompat.getColor(context,R.color.colorRed);
                break;
        }

        if (background instanceof ShapeDrawable) {
            // cast to 'ShapeDrawable'
            ShapeDrawable shapeDrawable = (ShapeDrawable) background;
            shapeDrawable.getPaint().setColor(color);
        } else if (background instanceof GradientDrawable) {
            // cast to 'GradientDrawable'
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            gradientDrawable.setColor(color);
        } else if (background instanceof ColorDrawable) {
            // alpha value may need to be set again after this call
            ColorDrawable colorDrawable = (ColorDrawable) background;
            colorDrawable.setColor(color);
        }
    }

    public class PurchaseInvoiceViewHolder extends RecyclerView.ViewHolder{
        TextView status, name, initials, order, saleDate, balance;
        CardView cardView;
        public PurchaseInvoiceViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.status_order_item);
            name = itemView.findViewById(R.id.fullname_order_item);
            initials = itemView.findViewById(R.id.txt_initial_name_order);
            order = itemView.findViewById(R.id.order_number_order_item);
            saleDate = itemView.findViewById(R.id.sale_date_order_item);
            balance = itemView.findViewById(R.id.balance_order_item);
            cardView = itemView.findViewById(R.id.cardview_order_item);
        }
    }
}