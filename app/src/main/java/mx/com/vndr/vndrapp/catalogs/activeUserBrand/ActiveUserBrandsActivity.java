package mx.com.vndr.vndrapp.catalogs.activeUserBrand;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.catalogs.CatalogsListActivity;
import mx.com.vndr.vndrapp.catalogs.util.CatalogActivity;
import mx.com.vndr.vndrapp.customviews.checkItemList.CheckedItemAdapter;
import mx.com.vndr.vndrapp.models.Brand;

public class ActiveUserBrandsActivity extends AppCompatActivity implements ActiveUserBrandView{

    Toolbar toolbar;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView textView;
    private ActiveUserBrandsPresenter presenter;
    CatalogActivity activity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_user_brands);
        activity = (CatalogActivity) getIntent().getSerializableExtra("activity");
        setUpUI();
        presenter = new ActiveUserBrandsPresenter(this,
                new ActiveUserBrandsInteractor(
                        VndrAuth.getInstance().getCurrentUser().getSessionToken(),
                        Volley.newRequestQueue(this)
                ),activity
        );
        presenter.getActiveUserBrands();
    }

    private void setUpUI(){
        toolbar = findViewById(R.id.tb_active_user_brands);
        recyclerView = findViewById(R.id.rv_active_user_brands);
        progressBar = findViewById(R.id.pb_active_user_brands);
        textView = findViewById(R.id.txt_empty_brands);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
    }

    @Override
    public void setAdapter(CheckedItemAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyBrands() {
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void navigateToCatalogList(Brand brand, CatalogActivity activity) {
        startActivity(new Intent(
                this,
                CatalogsListActivity.class
            ).putExtra("brand",brand)
                .putExtra("activity",activity)
        );
    }
}
