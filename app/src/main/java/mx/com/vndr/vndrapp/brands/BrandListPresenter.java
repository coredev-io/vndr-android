package mx.com.vndr.vndrapp.brands;

import java.util.List;

import mx.com.vndr.vndrapp.models.Brand;

import static mx.com.vndr.vndrapp.brands.BrandListActivity.BrandListType.BRANDS_TO_ADD_PRODUCTS;
import static mx.com.vndr.vndrapp.brands.BrandListActivity.BrandListType.BRANDS_TO_EDIT_CATALOGS;
import static mx.com.vndr.vndrapp.brands.BrandListActivity.BrandListType.OWN_BRANDS;

public class BrandListPresenter implements BrandInteractor.BrandCallback, BrandListAdapter.OnBrandListClick {

    BrandListView view;
    BrandInteractor interactor;
    BrandListActivity.BrandListType customType;

    public BrandListPresenter(BrandListView view, BrandInteractor interactor, BrandListActivity.BrandListType type) {
        this.view = view;
        this.interactor = interactor;
        customType = type;
    }

    public void getBrandListToEditCatalogs(){
        view.showEmptyBrands("");
        view.showProgress();
        interactor.getBrandsToEditCatalogs(this);
    }




    @Override
    public void error(String message) {
        view.hideProgress();
        view.showEmptyBrands(message);
    }

    @Override
    public void success(List<Brand> brandList) {
        view.hideProgress();
        view.setRecyclerViewData(new BrandListAdapter(brandList, this));
    }

    @Override
    public void emptyBrandList() {
        view.hideProgress();
        view.showEmptyBrands("No se encontraron marcas");
    }

    @Override
    public void onClick(Brand brand) {
        switch (customType){

            case OWN_BRANDS:

                break;
            case BRANDS_TO_EDIT_CATALOGS:
                view.navigateToCatalogList(brand);
                break;

            case BRANDS_TO_ADD_PRODUCTS:

                break;
        }
    }
}
