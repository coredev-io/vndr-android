package mx.com.vndr.vndrapp.registro;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.facebook.appevents.AppEventsLogger;
import com.google.android.material.textfield.TextInputLayout;

import mx.com.vndr.vndrapp.MainActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.SignInActivity;
import mx.com.vndr.vndrapp.account.ValidateEmailActivity;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.util.Dialogs;

public class InitialSurveyActivity extends AppCompatActivity implements
        RadioGroup.OnCheckedChangeListener, VndrUser.VndrUserCallback {

    Toolbar toolbar;
    RadioGroup radioGroup;
    TextInputLayout textInputLayout;
    EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_survey);
        toolbar = findViewById(R.id.tb_survey);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        radioGroup = findViewById(R.id.rg_survey);
        radioGroup.setOnCheckedChangeListener(this);
        textInputLayout = findViewById(R.id.il_user_recom);
        editText = findViewById(R.id.edtxt_user_recome);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        /*
        if (i == R.id.rb_recomendacion){
            textInputLayout.setVisibility(View.VISIBLE);
        } else {
            textInputLayout.setVisibility(View.GONE);
        }
         */
    }

    public void onFinishSignup(View view){
        SignUpData.getInstance().setSurvey(getSurveyAnswer());
        VndrAuth.getInstance().getCurrentUser().grantPermissionDataUsageWithSignUpData(this);
    }

    private String getSurveyAnswer(){
        switch (radioGroup.getCheckedRadioButtonId()){
            case R.id.rb_facebook:
                return "Facebook";
            case R.id.rb_instagram:
                return "Instagram";
            case R.id.rb_youtube:
                return "Youtube";
            case R.id.rb_internet:
                return "Internet";
            case R.id.rb_apps_store:
                return "Tienda de aplicaciones";
            case R.id.rb_evnt_partner:
                return "Evento de una marca";
            case R.id.rb_envt_vndr:
                return "Evento de vndr+";
            case R.id.rb_recomendacion:
                return "Recomendacion";
            default:
                return "";
        }
    }

    @Override
    public void onVndrUserCallbackSuccess() {
        Bundle bundle =  new Bundle();
        bundle.putString("uuid", VndrAuth.getInstance().getCurrentUser().getUid());
        bundle.putString("survey", getSurveyAnswer());
        AppEventsLogger.newLogger(this).logEvent("initialSurvey", bundle);
        if (VndrAuth.getInstance().getCurrentUser().isEmailVerified())
            navigateToMainActivity();
        else
            navigateToValidateEmail();
    }

    @Override
    public void onVndrUserCallbackError(String messageError) {
        Dialogs.showAlert(this, messageError);
    }

    private void navigateToMainActivity(){
        startActivity(new Intent(this, MainActivity.class));
        this.setResult(RESULT_OK);
        this.finish();
    }

    private void navigateToValidateEmail(){
        VndrAuth.getInstance().logout(null);
        startActivity(new Intent(this, ValidateEmailActivity.class));
        this.setResult(RESULT_OK);
        this.finish();
    }
}
