package mx.com.vndr.vndrapp.catalogs;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.models.Catalog;

public class CatalogListAdapter extends RecyclerView.Adapter<CatalogListAdapter.CatalogListViewHolder> {

    private List<Catalog> items;
    private OnClickCatalogListener listener;

    public CatalogListAdapter(List<Catalog> items, OnClickCatalogListener listener) {
        this.items = items;
        this.listener = listener;
    }

    @NonNull
    @Override
    public CatalogListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CatalogListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_catalog_list,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CatalogListViewHolder holder, final int position) {
        holder.title.setText(items.get(position).getCatalogName());
        holder.key.setText(items.get(position).getKey());

        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onSelect(items.get(position));
            }
        });

        if (items.get(position).isExpired()){
            //holder.imageView.setImageResource(R.drawable.ic_group);
            holder.status.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.shape_vigencia));
            holder.status.setText("Vigente");
        } else {
            //holder.imageView.setImageResource(R.drawable.ic_no_public);
            holder.status.setBackground(holder.itemView.getContext().getResources().getDrawable(R.drawable.shape_satatus));
            holder.status.setText("Expirado");
        }

        Catalog catalog = items.get(position);
        if (catalog.isPublic()){
            holder.status.setText(holder.status.getText().toString() + " - Público");

        } else {
            holder.status.setText(holder.status.getText().toString() + " - Privado");
        }

        if (catalog.isCollaborative()){
            holder.status.setText(holder.status.getText().toString() + " - Editable");

        } else {
            holder.status.setText(holder.status.getText().toString() + " - No editable");
        }
        /*
        if (catalog.isItsMine()){

            if (catalog.isCollaborative()){
                holder.imageView.setImageResource(R.drawable.ic_02);
            } else {
                holder.imageView.setImageResource(R.drawable.ic_01);
            }

        } else {
            if (catalog.isCollaborative()){
                holder.imageView.setImageResource(R.drawable.ic_04);
            } else {
                holder.imageView.setImageResource(R.drawable.ic_03);
            }
        }

         */
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CatalogListViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView title;
        TextView key;
        TextView status;
        ConstraintLayout layout;

        public CatalogListViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.ic_status);
            title = itemView.findViewById(R.id.txt_catalog_name);
            key = itemView.findViewById(R.id.ic_catalog_key);
            layout = itemView.findViewById(R.id.layout_catalog_list);
            status = itemView.findViewById(R.id.txt_status);
        }
    }

    interface OnClickCatalogListener{
        void onSelect(Catalog catalog);
    }
}
