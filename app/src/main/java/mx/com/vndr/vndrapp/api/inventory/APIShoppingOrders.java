package mx.com.vndr.vndrapp.api.inventory;

import android.util.Log;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertData;
import mx.com.vndr.vndrapp.inventory.purchaseOrders.ShoppingOrdersActivity;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASES;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASES_CART;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASES_SET_DATE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASES_SET_SEND_DATE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PURCHASES_SUGGESTIONS;

public class APIShoppingOrders {

    private final static String TAG = "APIShoppingOrders";

    private RequestQueue queue;

    public APIShoppingOrders(RequestQueue queue) {
        this.queue = queue;
    }


    public void getShoppingOrders(OnShoppingOrdersResponse shoppingOrdersResponse, ShoppingOrdersActivity.Query query){
        String url = URL_PURCHASES;

        switch (query){
            case BY_SUPPLIER:
                url += "?supplier=" + query.getSupplierId() + "&type=pending";
                break;
            case ALL_SUPPLIERS:
                url += "?type=pending";
                break;
            case ALL_COMPLETE:
                url += "?type=completed";
                break;
            case COMPLETE_BY_SUPPLIERS:
                url += "?supplier=" + query.getSupplierId() + "&type=completed";
                break;
        }
        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        shoppingOrdersResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200){
                                shoppingOrdersResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            JSONArray jsonArrayPurchases = jsonObject.getJSONArray("orders");
                            List<ShoppingOrder> shoppingOrders = new ArrayList<>();

                            for (int i = 0; i < jsonArrayPurchases.length(); i++) {
                                ShoppingOrder shoppingOrder = new ShoppingOrder(
                                        jsonArrayPurchases.getJSONObject(i)
                                );
                                shoppingOrders.add(shoppingOrder);
                            }

                            JSONArray jsonArrayResume = jsonObject.getJSONArray("resume");
                            Map<String, String> amountsHeader = new HashMap<>();

                            for (int i = 0; i < jsonArrayResume.length(); i++) {
                                JSONObject jsonHeader = jsonArrayResume.getJSONObject(i);
                                amountsHeader.put(
                                        jsonHeader.getString("type"),
                                        NumberFormat
                                                .getCurrencyInstance()
                                                .format(
                                                        jsonHeader.get("amount")
                                                )
                                );
                            }

                            shoppingOrdersResponse.onSuccess(shoppingOrders, amountsHeader);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            shoppingOrdersResponse.onError("Ocurrió un error al parsear los datos.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            shoppingOrdersResponse.onError("Ocurrió un error al parsear los datos.");
                        }
                    }
                });
    }

    public void getShoppingOrderDetail(OnShoppingOrdersResponse shoppingOrdersResponse, String shoppingOrderId){
        new VndrRequest(queue)
                .get(URL_PURCHASES + "?order=" + shoppingOrderId, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        shoppingOrdersResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        Log.e(TAG,response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") != 200){
                                shoppingOrdersResponse.onError(jsonObject.getString("message"));
                                return;
                            }
                            ShoppingOrder shoppingOrder = ShoppingOrder.Selected.current.getShoppingOrder();

                            if(shoppingOrder == null)
                                shoppingOrder = new ShoppingOrder(jsonObject.getJSONObject("data"));

                            List<ShoppingCartProduct> productList = new ArrayList<>();
                            JSONArray array = jsonObject.getJSONObject("data").getJSONArray("order_products");

                            for (int i = 0; i < array.length(); i++) {
                                productList.add(new ShoppingCartProduct(array.getJSONObject(i)));
                            }
                            shoppingOrder.setProductList(productList);
                            ShoppingOrder.Selected.current.setShoppingOrder(shoppingOrder);
                            ShoppingOrder
                                    .Selected
                                    .current
                                    .getShoppingOrder()
                                    .setNotificationAlertData(new NotificationAlertData(
                                            jsonObject.getJSONObject("data").getJSONObject("notifications"),
                                            jsonObject.getJSONObject("data").getJSONObject("contact_info")
                                    ));
                            shoppingOrdersResponse.onSuccess(shoppingOrder);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            shoppingOrdersResponse.onError("Ocurrió un error al procesar la solicitud.");
                        } catch (ParseException e) {
                            e.printStackTrace();
                            shoppingOrdersResponse.onError("Ocurrió un error al procesar la solicitud.");
                        }
                    }
                });
    }

    //  STEP 1 Create Shopping Order
    public void createUpdateShoppingOrder(ShoppingOrder shoppingOrder, OnCreateShoppingOrder onCreateShoppingOrder, boolean isUpdate){
        try {
            new VndrRequest(queue)
                    .setRequestByteParams(shoppingOrder.getCreateUpdateOrderBody(isUpdate))
                    .post(URL_PURCHASES_CART, new VndrRequest.VndrResponse() {
                        @Override
                        public void error(String message) {
                            onCreateShoppingOrder.onError(message);
                        }

                        @Override
                        public void success(String response) {
                            ShoppingOrder order = ShoppingOrder.Selected.current.getShoppingOrder();
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                if (jsonObject.getInt("statusCode") != 200){
                                    onCreateShoppingOrder.onError(jsonObject.getString("message"));
                                    return;
                                }
                                JSONObject orderInfo = jsonObject.getJSONObject("info");
                                order.setPurchaseId(orderInfo.getString("purchase_id"));
                                order.setPurchaseNumber(String.valueOf(orderInfo.getInt("purchase_number")));

                                ShoppingOrder.Selected.current.setShoppingOrder(order);
                                onCreateShoppingOrder.onSuccess();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                onCreateShoppingOrder.onError("Ocurrió un error inesperado al procesar la respuesta de la solicitud.");
                            }
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            onCreateShoppingOrder.onError("Ocurrió un error inesperado al enviar la solicitud");
        }

    }

    //  STEP 2 Create Shopping Order
    public void setOrderDate(OnCreateShoppingOrder onCreateShoppingOrder){
        new VndrRequest(queue)
                .setRequestParams(
                        ShoppingOrder
                                .Selected
                                .current
                                .getShoppingOrder()
                                .getUpdateOrderDateBody()
                )
                .post(URL_PURCHASES_SET_DATE, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onCreateShoppingOrder.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") != 200){
                                onCreateShoppingOrder.onError(jsonObject.getString("message"));
                                return;
                            }

                            ShoppingOrder
                                    .Selected
                                    .current
                                    .getShoppingOrder()
                                    .setNotificationAlertData(new NotificationAlertData(
                                            jsonObject.getJSONObject("notifications").getJSONObject("alerts"),
                                            jsonObject.getJSONObject("notifications").getJSONObject("contact_info")
                                    ));

                            onCreateShoppingOrder.onSuccess();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onCreateShoppingOrder.onError("Ocurrió un error inesperado al procesar la respuesta de la solicitud.");
                        }
                    }
                });
    }

    // STEP 3 Create Shopping Order
    public void setSendedDate(OnCreateShoppingOrder onCreateShoppingOrder){
        new VndrRequest(queue)
                .setRequestParams(
                        ShoppingOrder
                                .Selected
                                .current
                                .getShoppingOrder()
                                .getUpdateSendOrderDateBody()
                )
                .post(URL_PURCHASES_SET_SEND_DATE, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onCreateShoppingOrder.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") != 200){
                                onCreateShoppingOrder.onError(jsonObject.getString("message"));
                                return;
                            }

                            ShoppingOrder
                                    .Selected
                                    .current
                                    .setShoppingOrder(null);

                            onCreateShoppingOrder.onSuccess();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onCreateShoppingOrder.onError("Ocurrió un error inesperado al procesar la respuesta de la solicitud.");
                        }
                    }
                });
    }

    public void getProductSuggestions(OnSuggestionsResponse onSuggestionsResponse){
        new VndrRequest(queue)
                .get(URL_PURCHASES_SUGGESTIONS, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onSuggestionsResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") != 200){
                                onSuggestionsResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            List<SuggestionProducts> products = new ArrayList<>();

                            JSONArray jsonArraySuggestions = jsonObject.getJSONArray("data");

                            for (int i = 0; i < jsonArraySuggestions.length(); i++) {
                                products.add(new SuggestionProducts(jsonArraySuggestions.getJSONObject(i)));
                            }

                            if (products.isEmpty()){
                                onSuggestionsResponse.onError(jsonObject.getString("message"));
                                return;
                            }
                            onSuggestionsResponse.onSuccess(products);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            onSuggestionsResponse.onError("Ocurrió un error al procesar la solicitud");

                        }
                    }
                });

    }

    public void deleteShoppingOrder(OnCreateShoppingOrder onCreateShoppingOrder, String id){
        new VndrRequest(queue)
                .delete(URL_PURCHASES + "/" + id, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onCreateShoppingOrder.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getInt("statusCode") != 200) {
                                onCreateShoppingOrder.onError(jsonObject.getString("message"));
                                return;
                            }
                            onCreateShoppingOrder.onSuccessDelete();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            onCreateShoppingOrder.onError("Ocurrió un error al procesar la solicitud, comprueba que los cambios hayan sido aplicados antes de volver a intentar.");
                        }
                    }
                });
    }

    public interface OnCreateShoppingOrder {
        void onSuccess();
        default void onSuccessDelete(){

        }
        void onError(String messageError);
    }

    public interface OnSuggestionsResponse {
        void onSuccess(List<SuggestionProducts> products);
        void onError(String messageError);
    }

    public interface OnShoppingOrdersResponse {
        default void onSuccess(List<ShoppingOrder> shoppingOrders, Map<String,String> amountsHeader){

        }
        default void onSuccess(ShoppingOrder shoppingOrder){

        }
        default void onEmptyList(String message){

        }
        void onError(String messageError);
    }

}
