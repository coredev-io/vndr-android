package mx.com.vndr.vndrapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.Projections;

public class ProjectionsAdapter extends RecyclerView.Adapter<ProjectionsAdapter.ProjectionsViewHolder> {

    List<Projections> projectionsList;
    Context context;

    public ProjectionsAdapter(List<Projections> projectionsList, Context context) {
        this.projectionsList = projectionsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProjectionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProjectionsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_projection_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectionsViewHolder holder, int position) {

        holder.textView.setText(projectionsList.get(position).getLabel());
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.recyclerView.setAdapter(new ProjectionAdapter(projectionsList.get(position).getProjectionList(),context));
    }

    @Override
    public int getItemCount() {
        return projectionsList.size();
    }

    public class ProjectionsViewHolder extends RecyclerView.ViewHolder{

        TextView textView;
        RecyclerView recyclerView;

        public ProjectionsViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.txt_title_projection_cards);
            recyclerView = itemView.findViewById(R.id.rv_projections_card);
        }
    }
}
