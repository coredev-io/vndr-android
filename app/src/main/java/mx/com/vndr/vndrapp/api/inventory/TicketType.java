package mx.com.vndr.vndrapp.api.inventory;

import java.io.Serializable;

public class TicketType implements Serializable {

    String code;
    String description;

    public TicketType(String code, String description) {
        this.code = code;
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}