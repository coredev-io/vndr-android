package mx.com.vndr.vndrapp.customviews.checkItemList;

import android.util.Log;

import java.util.List;


public class CheckedItemPresenter {

    List<CheckedItem> items;

    public CheckedItemPresenter(List<CheckedItem> items) {
        this.items = items;
    }

    public void onBindCheckedItemRowViewAtPosition(CheckedItemRowView rowView, int position){
        CheckedItem item = items.get(position);

        rowView.setTitle(item.getTitle());
        rowView.setChecked(item.getChecked());
        rowView.setClickListener(item.getListener());
    }

    public int getItemCount(){
        return  items.size();
    }
}
