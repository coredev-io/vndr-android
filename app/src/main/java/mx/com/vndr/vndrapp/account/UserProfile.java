package mx.com.vndr.vndrapp.account;


import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import mx.com.vndr.vndrapp.api.VndrAuth;

public class UserProfile {

    private static UserProfile instance;

    public static UserProfile getInstance(){
        synchronized (UserProfile.class) {
            if(instance == null){
                instance = new UserProfile();
            }
        }
        return instance;
    }

    private String firstName = "";
    private String secondName = "";
    private String firstLastName = "";
    private String secondLastName = "";
    private String gender = "";
    private String birthDay = "";
    private String email = "";
    private String phoneNumber = "";
    private String cellPhoneNumber = "";
    private String zipCode = "";
    private String colonia = "";
    private String streetAddress = "";
    private boolean emailWasUpdated = false;

    public void parseWithJSONObject(JSONObject jsonObject) throws JSONException {

        //  Required

        if (jsonObject.has("firstGivenName"))
            this.firstName = jsonObject.getString("firstGivenName");
        if (jsonObject.has("firstFamilyName"))
            this.firstLastName = jsonObject.getString("firstFamilyName");
        if (jsonObject.has("mail"))
            this.email = jsonObject.getString("mail");
        if (jsonObject.has("mobilePhone"))
            this.cellPhoneNumber = jsonObject.getString("mobilePhone");
        if (jsonObject.has("zipCode"))
            this.zipCode = jsonObject.getString("zipCode");

        // Optionals

        if (jsonObject.has("secondGivenName"))
            this.secondName = jsonObject.getString("secondGivenName");
        if (jsonObject.has("secondFamilyName"))
            this.secondLastName = jsonObject.getString("secondFamilyName");
        if (jsonObject.has("gender"))
            this.gender = jsonObject.getString("gender");
        if (jsonObject.has("birthDay"))
            this.birthDay = jsonObject.getString("birthDay");
        if (jsonObject.has("phone"))
            this.phoneNumber = jsonObject.getString("phone");
        if (jsonObject.has("colonia"))
            this.colonia = jsonObject.getString("colonia");
        if (jsonObject.has("addresss"))
            this.streetAddress = jsonObject.getString("addresss");
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public String getGender() {
        return gender;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getColonia() {
        return colonia;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public boolean isEmailUpdated(){
        if (!VndrAuth.getInstance().getCurrentUser().getEmail().equals(email))
            return true;
        else
            return false;
    }

    public boolean emailWasUpdated() {
        return emailWasUpdated;
    }

    public void setEmailWasUpdated(boolean emailWasUpdated) {
        this.emailWasUpdated = emailWasUpdated;
    }

    public Map<String, String> getParamsBody(){
        Map<String, String> params = new HashMap<>();
        params.put("name", getFirstName());
        params.put("lastName", getFirstLastName());
        params.put("mail", getEmail());
        params.put("mobilePhone", getCellPhoneNumber());
        params.put("zipCode", getZipCode());
        params.put("secondName", getSecondName());
        params.put("secondLastName", getSecondLastName());
        params.put("gender", getGender());
        params.put("birthDay", getBirthDay());
        params.put("phoneNumber", getPhoneNumber());
        params.put("colonia", getColonia());
        params.put("streetAddress", getStreetAddress());
        return params;
    }
}
