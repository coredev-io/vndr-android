package mx.com.vndr.vndrapp.orders;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import mx.com.vndr.vndrapp.R;

public class TestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        Log.e("TAG",String.valueOf(getResources().getDisplayMetrics().density));
    }
}
