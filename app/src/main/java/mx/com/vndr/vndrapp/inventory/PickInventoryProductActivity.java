package mx.com.vndr.vndrapp.inventory;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_PRODUCTS_INVENTORY;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_PRODUCTS_STD_CLASS;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import mx.com.vndr.vndrapp.ProductEnum;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.CustomExpandableListAdapter;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.Inventory;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.StandardClassification;
import mx.com.vndr.vndrapp.orders.PickBrandActivity;
import mx.com.vndr.vndrapp.products.PickProductActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

public class PickInventoryProductActivity extends AppCompatActivity implements VndrRequest.VndrResponse {

    ExpandableListView expandableListView;
    ProgressBar progressBar;
    Toolbar toolbar;
    TextView empty;


    HashMap<String, List<Product>> expandableListDetail;
    List<String> expandableListTitle;
    List<Product> products;

    ProductExpandableListAdapter adapter;
    String idBrand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_inventory_product);
        expandableListView = findViewById(R.id.exp_lv_brands);
        progressBar = findViewById(R.id.pb_pick_brand);
        empty = findViewById(R.id.txt_empty_product_brand);

        toolbar = findViewById(R.id.tb_pick_brand);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        idBrand = getIntent().getStringExtra("idBrand");

        expandableListView.setOnChildClickListener((expandableListView, view, i, i1, l) -> {
            Product product = expandableListDetail.get(expandableListTitle.get(i)).get(i1);

            startActivity(new Intent(this, ProductInventoryActivity.class)
                    .putExtra("product", product));
            return false;
        });

        getData();
    }

    private void getData(){
        progressBar.setVisibility(View.VISIBLE);
        String url = URL_PRODUCTS_INVENTORY;
        if (idBrand != null){
            url += "&id_brand=" + idBrand;
        }
        url += "&standard_classification_id=" + getIntent().getStringExtra("idClass");
        new VndrRequest(Volley.newRequestQueue(this))
                .get(url, this);
    }

    @Override
    public void error(String message) {
        progressBar.setVisibility(View.GONE);
        Dialogs.showAlert(this, message);
    }

    @Override
    public void success(String response) {
        progressBar.setVisibility(View.GONE);

        try {
            JSONArray jsonArray = new JSONArray(response);
            products = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject productObject = jsonArray.getJSONObject(i);

                Product product = new Product(
                        productObject.getString("_id"),
                        productObject.getString("sku"),
                        productObject.getString("product_name"),
                        productObject.getString("standard_classification"),
                        productObject.getString("notes"),
                        productObject.getString("sub_brand"),
                        productObject.getString("category"),
                        productObject.getString("subcategory"),
                        productObject.getString("product_key"),
                        productObject.getString("size"),
                        productObject.getString("color"),
                        productObject.getString("description"),
                        productObject.getString("url_image"),
                        productObject.getString("product_points"),
                        productObject.getString("barcode"),
                        productObject.getString("qrcode"),
                        productObject.getDouble("list_price"),
                        productObject.getBoolean("active"),
                        new Catalog(
                                productObject.getJSONObject("catalog").getString("_id"),
                                productObject.getJSONObject("catalog").getString("catalog_name")

                        ));

                product.setBrandName(productObject.getString("brand_name"));
                if (productObject.has("inventory")) {
                    product.setInventory(new Inventory(productObject.getJSONObject("inventory")));
                }
                products.add(product);
            }
            if (products.isEmpty()){
                empty.setVisibility(View.VISIBLE);
                empty.setText("No se encontraron productos.");
            } else {
                generateList();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getInt("statusCode") == 404) {
                    //products = new ArrayList<>();
                    //generateList();
                    empty.setVisibility(View.VISIBLE);
                    empty.setText(jsonObject.getString("message"));
                }
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
                Dialogs.showAlert(this, "Ocurrió un error al procesar la solicitud");
            }
        }
    }

    private void generateList(){
        expandableListTitle = new ArrayList<>();
        expandableListDetail = new HashMap<>();

        for (int i = 0; i < products.size(); i++) {

            if (!expandableListTitle.contains(products.get(i).getBrandName())) {
                expandableListTitle.add(products.get(i).getBrandName());
                List<Product> p = new ArrayList<>();
                p.add(products.get(i));
                expandableListDetail.put(products.get(i).getBrandName(), p);
            } else {
                List<Product> p = expandableListDetail.get(products.get(i).getBrandName());
                p.add(products.get(i));
                expandableListDetail.put(products.get(i).getBrandName(), p);
            }
        }

        adapter = new ProductExpandableListAdapter(
                this,
                expandableListTitle,
                expandableListDetail
        );
        expandableListView.setAdapter(adapter);

        if (expandableListDetail.size() == 1) {
            expandableListView.expandGroup(0, true);
        }
    }
}

class ProductExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<Product>> expandableListDetail;

    public ProductExpandableListAdapter(Context context, List<String> expandableListTitle,
                                       HashMap<String, List<Product>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return this.expandableListDetail.get(this.expandableListTitle.get(i))
                .size();
    }

    @Override
    public Object getGroup(int i) {
        return this.expandableListTitle.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return this.expandableListDetail.get(this.expandableListTitle.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        String listTitle = (String) getGroup(i);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_blue_title, null);
        }
        TextView listTitleTextView = (TextView) view
                .findViewById(R.id.blueTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        final Product product = (Product) getChild(i, i1);
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_inventory_product, null);
        }

        TextView inventory, productName, sku;
        TextView size, color, description;

        inventory = (TextView) view
                .findViewById(R.id.inventory);
        productName = (TextView) view
                .findViewById(R.id.product_name);
        sku = (TextView) view
                .findViewById(R.id.sku_add_product);
        size = (TextView) view
                .findViewById(R.id.size_add);
        color = (TextView) view
                .findViewById(R.id.color_add);
        description = (TextView) view
                .findViewById(R.id.desc);


        productName.setText(product.getProductName());
        Log.e("TAG", product.getProduct_key());
        sku.setText(product.getProduct_key());
        inventory.setText("Inventario actual " + product.getInventory().getCurrentStock());

        if (!product.getSize().isEmpty()){
            size.setText(product.getSize());
            size.setVisibility(View.VISIBLE);
        }

        if (!product.getColor().isEmpty()){
            color.setText(product.getColor());
            color.setVisibility(View.VISIBLE);
        }

        if (!product.getDescription().isEmpty()){
            description.setText(product.getDescription());
            description.setVisibility(View.VISIBLE);
        }

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}