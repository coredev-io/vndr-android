package mx.com.vndr.vndrapp.orders;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.vndr.vndrapp.OrderProductsFragment;
import mx.com.vndr.vndrapp.PaymentCalendarActivity;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.adapters.OrderDetailAdapter;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.models.Catalog;
import mx.com.vndr.vndrapp.models.Cxc;
import mx.com.vndr.vndrapp.models.Delivery;
import mx.com.vndr.vndrapp.models.Order;
import mx.com.vndr.vndrapp.models.Product;
import mx.com.vndr.vndrapp.models.ShoppingCartProduct;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;
import static mx.com.vndr.vndrapp.util.Constants.RETRY_COUNT;
import static mx.com.vndr.vndrapp.util.Constants.TIME_OUT;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_SINGLE;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_CXC_TERMS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDERS;
import static mx.com.vndr.vndrapp.api.URLVndr.URL_ORDER_DETAIL;

public class OrderDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView recyclerView;
    OrderDetailAdapter adapter;
    List<String> valueList = new ArrayList<>();
    List<String> keysList = new ArrayList<>();
    TextView alert;
    ProgressBar progressBar;
    Button primary, secondary;
    Toolbar toolbar;
    EditText notes;

    Order order;
    Boolean isPaid, isComplete, isCxc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        alert = findViewById(R.id.alert_order_detail);
        progressBar = findViewById(R.id.pb_order_detail);
        primary = findViewById(R.id.btn_primary_order_detail);
        secondary = findViewById(R.id.btn_sec_order_detail);
        toolbar = findViewById(R.id.tb_order_detail);
        notes = findViewById(R.id.edtxt_notes);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        toolbar.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.all){
                if (order.getCxc().getPayFrequency() == 1)
                    Dialogs.showAlert(this, "Este pedido no cuenta con un calendario de pagos.");
                else
                    startActivity(
                            new Intent(
                                    this,
                                    PaymentCalendarActivity.class)
                                    .putExtra("order", order)
                                    .putExtra("isReadMode", true)
                    );
            }
            return false;
        });

        isComplete = getIntent().getBooleanExtra("isComplete", false);

        if (getIntent().hasExtra("order")){
            order = (Order) getIntent().getSerializableExtra("order");
            isPaid = getIntent().getBooleanExtra("isPaid",true);
        }


        recyclerView = findViewById(R.id.rv_order_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //keysList = new ArrayList<>(detailMap.keySet());
        if (!isPaid)
            alert.setVisibility(View.VISIBLE);

        if (order.getCxc().getPayFrequency() == 1) isCxc = false;
        else isCxc = true;



        getData();
    }



    public void getData(){

        if (isComplete){
            getOrderDetail(order.getIdOrder());
        } else {

            keysList.add("Cliente");
            valueList.add( order.getCustomer().getCustomerFullName());

            keysList.add("Productos");
            valueList.add(String.valueOf(order.getProducts().size()));

            keysList.add( "Total mercancía");
            valueList.add(NumberFormat.getCurrencyInstance().format(order.getOrderAmount()));

            keysList.add("Cargo por envío");
            valueList.add( NumberFormat.getCurrencyInstance().format(order.getShippingAmount()));

            keysList.add("Total del pedido");
            valueList.add(NumberFormat.getCurrencyInstance().format(order.getTotalOrderAmount()));

            keysList.add("Fecha de venta");
            valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getSaleDate()));



            if (!order.getDelivery().isPersonalDelivery()){

                if (order.getDelivery().getEstimateShippingDate() != null){
                    keysList.add( "Fecha de envío");
                    valueList.add(order.getDelivery().getEstimateShippingDateFormatt());
                }
            }

            if (order.getDelivery().getEstimateDeliveryDate() != null){
                keysList.add( "Fecha de entrega");
                valueList.add(order.getDelivery().getEstimateDeliveryDateFormatt());
            }

            if (!order.getDelivery().isPersonalDelivery()){

                if (order.getDelivery().getMessengerService() != null){
                    keysList.add( "Forma de entrega");
                    valueList.add(order.getDelivery().getMessengerService());
                }
                if (order.getDelivery().getTrackingCode() != null){
                    keysList.add( "Código de rastreo");
                    valueList.add(order.getDelivery().getTrackingCode());
                }

            }

            if (order.getCxc().getPayFrequency() != 1){


                keysList.add("Monto anticipo");
                valueList.add( NumberFormat.getCurrencyInstance().format(order.getCxc().getAnticipo()));

                if (order.getCxc().getPayDate() != null){

                    keysList.add("Fecha de pago anticipo");
                    valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getPayDate()));


                    keysList.add("Medio de pago");
                    valueList.add(order.getCxc().getMethodPayment());
                }



                keysList.add("Saldo a pagar en abonos");
                valueList.add(NumberFormat.getCurrencyInstance().format(order.getCxc().getSaldoPlazo()));

                keysList.add("Periodicidad de pago");

                switch (order.getCxc().getPayFrequency()){
                    case 7:
                        valueList.add("Semanal");
                        break;
                    case 15:
                        valueList.add("Quincenal");
                        break;
                    case 30:
                        valueList.add("Mensual");
                        break;
                }

                keysList.add("Número de pagos");
                valueList.add(String.valueOf(order.getCxc().getNumPagos()));


                keysList.add("Importe de cada pago");
                valueList.add(NumberFormat.getCurrencyInstance().format(order.getCxc().getMontoPago()));


                keysList.add("Fecha de primer pago");
                valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getFirstPayDate()));


            } else{

                keysList.add("Medio de pago");
                valueList.add(order.getCxc().getMethodPayment());

                if (!order.getCxc().getMethodPayment().equals("Pendiente de pago")){
                    keysList.add( "Fecha de pago");
                    valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getPayDate()));
                    keysList.add("Saldo pendiente");
                    valueList.add("$0.00");

                }

                else{

                    if (order.getCxc().getPayDate() != null){
                        keysList.add( "Fecha esperada de pago");
                        valueList.add(new SimpleDateFormat("dd/MMM/yyyy").format(order.getCxc().getPayDate()));
                    }

                    keysList.add("Saldo pendiente");
                    valueList.add( order.getTotalOrderAmountStr());

                }


            }


            adapter = new OrderDetailAdapter(valueList,keysList, this);
            recyclerView.setAdapter(adapter);

        }


    }

    private void getOrderDetail(String idOrder){
        //progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.GET, URL_ORDER_DETAIL + idOrder, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                //progressBar.setVisibility(View.GONE);
                Log.e("RESPONSE", response);
                processOrderDetail(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //progressBar.setVisibility(View.GONE);
                System.out.println(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void processOrderDetail(String response){
        List<ShoppingCartProduct> productList = new ArrayList<>();
        try {
            JSONObject objectResponse = new JSONObject(response);
            JSONArray productsArray = objectResponse.getJSONArray("order_products");

            for (int i = 0; i < productsArray.length(); i++){

                JSONObject productObject = productsArray.getJSONObject(i).getJSONObject("product");

                Product product = new Product(
                        productObject.getString("_id"),
                        productObject.getString("sku"),
                        productObject.getString("product_name"),
                        productObject.getString("standard_classification"),
                        productObject.getString("notes"),
                        productObject.getString("sub_brand"),
                        productObject.getString("category"),
                        productObject.getString("subcategory"),
                        productObject.getString("product_key"),
                        productObject.getString("size"),
                        productObject.getString("color"),
                        productObject.getString("description"),
                        productObject.getString("url_image"),
                        productObject.getString("product_points"),
                        productObject.getString("barcode"),
                        productObject.getString("qrcode"),
                        productObject.getDouble("list_price"),
                        productObject.getBoolean("discontinued"),
                        new Catalog(
                                productObject.getJSONObject("catalog").getString("_id"),
                                productObject.getJSONObject("catalog").getString("catalog_name")
                        )
                );

                ShoppingCartProduct shoppingCartProduct = new ShoppingCartProduct(
                        product,
                        productsArray.getJSONObject(i).getDouble("sale_price"),
                        productsArray.getJSONObject(i).getDouble("adjustment"),
                        productsArray.getJSONObject(i).getDouble("subtotal"),
                        productsArray.getJSONObject(i).getInt("products_amount"),
                        productsArray.getJSONObject(i).getString("notes")
                );
                productList.add(shoppingCartProduct);
            }



            order.setProducts(productList);


            order.setTotalOrderAmount(objectResponse.getDouble("total_order_amount"));
            order.setShippingAmount(objectResponse.getDouble("shipping_pay"));
            order.setOrderNumber(objectResponse.getInt("order_number"));
            order.setSaleDateFormmat(objectResponse.getString("sale_date"));
            order.setOrderAmount(objectResponse.getDouble("order_amount"));


            if (objectResponse.has("delivery")){
                if (objectResponse.getJSONObject("delivery").has("personal_delivery")){

                    order.setDelivery(
                            new Delivery(
                                    true,
                                    objectResponse.getJSONObject("delivery").getString("estimate_delivery_date")
                            )
                    );

                } else if (objectResponse.getJSONObject("delivery").has("shipping_by_courier")){
                    order.setDelivery(
                            new Delivery(
                                    true,
                                    objectResponse.getJSONObject("delivery").getString("estimate_delivery_date"),
                                    objectResponse.getJSONObject("delivery").getString("estimate_shipping_date"),
                                    objectResponse.getJSONObject("delivery").getString("messenger_service"),
                                    objectResponse.getJSONObject("delivery").getString("traking_code")
                            )
                    );
                }
            }

            if (objectResponse.has("cxc")){

                order.setCxc(new Cxc());


                order.getCxc().setPayFrequency(objectResponse.getJSONObject("cxc").getInt("pay_frequency"));

                if (objectResponse.getJSONObject("cxc").has("payment_method")){
                    order.getCxc().setMethodPayment(objectResponse.getJSONObject("cxc").getString("payment_method"));
                    order.getCxc().setPayDate(objectResponse.getJSONObject("cxc").getString("pay_date"));
                    order.getCxc().setAnticipo(objectResponse.getJSONObject("cxc").getDouble("advance_payment"));
                }
                else {
                    order.getCxc().setMethodPayment("Pendiente de pago");
                }


                if(order.getCxc().getPayFrequency() != 1){
                    order.getCxc().setAnticipo(objectResponse.getJSONObject("cxc").getDouble("advance_payment"));
                    order.getCxc().setSaldoPlazo(objectResponse.getJSONObject("cxc").getDouble("total_debt"));
                    order.getCxc().setNumPagos(objectResponse.getJSONObject("cxc").getInt("payments_number"));
                    order.getCxc().setMontoPago(objectResponse.getJSONObject("cxc").getDouble("payment_amount"));

                    order.getCxc().setDiasAviso(objectResponse.getJSONObject("cxc").getInt("preventive_reminder_days"));
                    order.getCxc().setDiasCortesia(objectResponse.getJSONObject("cxc").getInt("courtesy_days"));
                    //order.getCxc().setAlertas(objectResponse.getJSONObject("cxc").getBoolean("alerts"));

                    order.getCxc().setFirstPayDate(objectResponse.getJSONObject("cxc").getString("first_pay_date"));


                }

            }
            isComplete = false;
            getData();


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private void createCxc(){

        String url = isCxc ? URL_CXC_TERMS : URL_CXC_SINGLE;

        Log.e("TAG",url);

        showLoader(true);
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.has("_id")){
                        order.getCxc().setIdCxc(jsonObject.getString("_id"));
                        updateStatus();
                    } else if (jsonObject.has("statusCode") && jsonObject.getInt("statusCode") == 400){
                        Toast.makeText(OrderDetailActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                        showLoader(false);

                    } else {
                        Toast.makeText(OrderDetailActivity.this, "Ocurrió un error inesperado al procesar la respuesta.", Toast.LENGTH_LONG).show();
                        showLoader(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(OrderDetailActivity.this, "Ocurrió un error inesperado al procesar la respuesta.", Toast.LENGTH_LONG).show();
                    showLoader(false);
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if (error.networkResponse.statusCode == 409){
                    updateStatus();

                } else {

                    showLoader(false);
                    Toast.makeText(OrderDetailActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                    System.out.println(new String(error.networkResponse.data));
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return createBody(false);
            }

        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private void updateStatus(){
        //progressBar.setVisibility(View.VISIBLE);
        StringRequest request = new StringRequest(Request.Method.PATCH, URL_ORDERS + "/" + order.getIdOrder(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                progressBar.setVisibility(View.GONE);
                //processOrderDetail(response);
                showLoader(false);

                startActivityForResult(new Intent(OrderDetailActivity.this, SuccesOrderActivity.class)
                        .putExtra("order", order)
                        ,1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                showLoader(false);
                Toast.makeText(OrderDetailActivity.this, new String(error.networkResponse.data), Toast.LENGTH_LONG).show();
                System.out.println(new String(error.networkResponse.data));
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + VndrAuth.getInstance().getCurrentUser().getSessionToken());
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return createBody(true);
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(TIME_OUT,RETRY_COUNT,1f));

        Volley.newRequestQueue(this).add(request);
    }

    private HashMap<String, String> createBody(boolean isUpdate){

        HashMap<String, String> map = new HashMap<>();

        if (isUpdate){

            map.put("is_complete","true");
            map.put("update_date",String.valueOf(new Date().getTime()));
            map.put("notes", notes.getText().toString());
            //map.put("cxc",order.getCxc().getIdCxc());
        } else {




            map.put("id_order", order.getIdOrder());
            map.put("pay_frequency",String.valueOf(order.getCxc().getPayFrequency()));
            map.put("is_cxc", String.valueOf(isCxc));

            if (order.getCxc().getPayDate() != null){ // Ya está pagado
                map.put("pay_date",String.valueOf(order.getCxc().getPayDate().getTime()));
                map.put("payment_method",order.getCxc().getMethodPayment());
            }

            if (isCxc){


                if (order.getCxc().getAnticipo() != null){
                    map.put("advance_payment",String.valueOf(order.getCxc().getAnticipo()));

                }
                map.put("payments_number",String.valueOf(order.getCxc().getNumPagos()));
                map.put("first_pay_date",String.valueOf(order.getCxc().getFirstPayDate().getTime()));

                map.put("courtesy_days",String.valueOf(order.getCxc().getDiasCortesia()));
                map.put("preventive_reminder_days",String.valueOf(order.getCxc().getDiasAviso()));
                //map.put("alerts",String.valueOf(order.getCxc().isAlertas()));
                map.put("payment_reminder_alert", String.valueOf(order.getCxc().isPaymentReminderAlert()));
                map.put("early_notice_alert", String.valueOf(order.getCxc().isEarlyNoticeAlert()));

            }

        }

        Log.e("TAG", map.toString());
        return map;
    }

    public void showLoader(Boolean show){
        if (show){
            progressBar.setVisibility(View.VISIBLE);
            primary.setVisibility(View.GONE);
            secondary.setVisibility(View.GONE);
        } else{
            progressBar.setVisibility(View.GONE);

            primary.setVisibility(View.VISIBLE);
            secondary.setVisibility(View.VISIBLE);
        }
    }

    public void onClickFinishOrder(View view){
        if (!isComplete) createCxc();
        else {

        }
    }

    public void onClickEditOrder(View view){
        this.setResult(RESULT_CANCELED);
        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode!= RESULT_BACK){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        this.setResult(RESULT_BACK);
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        new OrderProductsFragment(order.getProducts()).show(getSupportFragmentManager(),"productlist");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.active_orders_menu,menu);
        return true;
    }
}
