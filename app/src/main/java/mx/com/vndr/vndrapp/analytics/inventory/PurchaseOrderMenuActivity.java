package mx.com.vndr.vndrapp.analytics.inventory;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.SelectBrandActivity;
import mx.com.vndr.vndrapp.analytics.inventory.PurchaseResumeActivity.Query;
import mx.com.vndr.vndrapp.customviews.OptionList.OnClickListener;
import mx.com.vndr.vndrapp.customviews.OptionList.Option;
import mx.com.vndr.vndrapp.customviews.OptionList.OptionListAdapter;
import mx.com.vndr.vndrapp.inventory.supplier.SuppliersActivity;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.util.Dialogs;

public class PurchaseOrderMenuActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_order_menu);
        setupUI();
    }


    private void setupUI(){
        toolbar = findViewById(R.id.tb_po_menu);
        recyclerView = findViewById(R.id.rv_po_menu);

        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        recyclerView.setAdapter(new OptionListAdapter(getListOptions()));
    }

    private List<Option> getListOptions(){
        List<Option> optionList = new ArrayList<>();

        List<String> options = Arrays.asList(getResources().getStringArray(R.array.menuPurchaseOrderOption1));
        optionList.add(new Option(options.get(0), options.get(1), () -> navigateToShoppingOrderResume(Query.ALL)));

        options = Arrays.asList(getResources().getStringArray(R.array.menuPurchaseOrderOption2));
        optionList.add(new Option(options.get(0), options.get(1), this::pickSupplier));

        options = Arrays.asList(getResources().getStringArray(R.array.menuPurchaseOrderOption3));
        optionList.add(new Option(options.get(0), options.get(1), this::pickBrand));

        return optionList;
    }

    public void navigateToShoppingOrderResume(Query query){
        startActivity(new Intent(this, PurchaseResumeActivity.class).putExtra("query", query));
    }

    private void pickSupplier(){
        startActivityForResult(new Intent(this, SuppliersActivity.class).putExtra("pickMode",true),0);
    }

    private void pickBrand(){
        startActivityForResult(new Intent(this, SelectBrandActivity.class),1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK){
            navigateToShoppingOrderResume(Query.BY_SUPPLIER);
        }
        if (requestCode == 1 && resultCode == RESULT_OK){
            navigateToShoppingOrderResume(Query.BY_BRAND);
        }
    }

    public void displayAlertUnavailable(){
        Dialogs.showAlert(this, "Esta opción estará disponible muy pronto.\n\n¡Espérala!");
    }
}