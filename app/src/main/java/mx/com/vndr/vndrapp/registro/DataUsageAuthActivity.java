package mx.com.vndr.vndrapp.registro;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.VndrAuth;
import mx.com.vndr.vndrapp.api.VndrUser;
import mx.com.vndr.vndrapp.membership.ProfesionalMembershipActivity;
import mx.com.vndr.vndrapp.util.Dialogs;

import static android.view.View.GONE;

public class DataUsageAuthActivity extends AppCompatActivity implements VndrUser.VndrUserCallback {

    private final int MEMBERSHIP_INTENT = 1;

    Toolbar toolbar;
    Switch switchPersonalData;
    Switch switchCustomerData;
    TextView textViewDisclaimer;
    ProgressBar progressBar;
    Button button;

    boolean isUpdateMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_usage_auth);
        toolbar = findViewById(R.id.tb_data_usage_auth);
        switchCustomerData = findViewById(R.id.switch_customer_data);
        switchPersonalData = findViewById(R.id.switch_personal_data);
        setSupportActionBar(toolbar);
        textViewDisclaimer = findViewById(R.id.txt_data_usage_auth_disclaimer);
        progressBar = findViewById(R.id.pb_data_usage_auth);
        button = findViewById(R.id.btn_data_usage_auth);

        isUpdateMode = getIntent().getBooleanExtra("updateMode", false);
        customActivity();
    }

    public void onClickContinue(View view){
        if (isUpdateMode)
            updateDataUsageAuth();
        else {
            SignUpData.getInstance().setShareMyData(switchPersonalData.isChecked());
            SignUpData.getInstance().setShareCustomerData(switchCustomerData.isChecked());
            navigateToMembership();
        }
    }

    private void navigateToMembership() {
        startActivityForResult(new Intent(this, ProfesionalMembershipActivity.class), MEMBERSHIP_INTENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MEMBERSHIP_INTENT && resultCode == RESULT_OK){
            this.finish();
        }
    }

    private void displayCurrentValues(){
        switchPersonalData.setChecked(VndrAuth
                .getInstance()
                .getCurrentUser()
                .getPersonalDataUsageAuth()
        );

        switchCustomerData.setChecked(VndrAuth
                .getInstance()
                .getCurrentUser()
                .getCustomersDataUsageAuth()
        );
    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(GONE);
    }

    private void dismissLoading(){
        progressBar.setVisibility(GONE);
        button.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if (isUpdateMode)
            super.onBackPressed();
    }

    private void updateDataUsageAuth(){
        showLoading();
        VndrAuth.getInstance().getCurrentUser().grantPermissionDataUsage(
                switchPersonalData.isChecked(),
                switchCustomerData.isChecked(),
                this);
    }

    private void customActivity(){
        if (isUpdateMode) {
            toolbar.setNavigationOnClickListener(view -> onBackPressed());
            toolbar.setNavigationIcon(R.drawable.ic_nav);
            textViewDisclaimer.setVisibility(GONE);
            displayCurrentValues();
        }
    }

    @Override
    public void onVndrUserCallbackSuccess() {
        dismissLoading();
        Dialogs.showAlert(this, "Se actualizaron los permisos exitosamente.", (dialog, which) -> {
            onBackPressed();
        });
    }

    @Override
    public void onVndrUserCallbackError(String messageError) {
        dismissLoading();
        displayCurrentValues();
        Dialogs.showAlert(this, messageError);
    }
}
