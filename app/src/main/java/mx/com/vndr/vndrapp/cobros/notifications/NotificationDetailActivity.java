package mx.com.vndr.vndrapp.cobros.notifications;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import mx.com.vndr.vndrapp.customviews.notificationalertfragment.NotificationAlertFragment;
import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.cobros.DetailCxcActivity;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.util.Dialogs;

public class NotificationDetailActivity extends AppCompatActivity {

    Toolbar toolbar;
    ImageView imageView;
    TextView description;
    TextView callToAction;
    RecyclerView recyclerView;

    private Notification notification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_detail);
        notification = Notification.Selected.current.getNotificationSelected();

        setupUI();
        customUI();
        setData();
    }



    public void onClickDetail(View view){
        navigateToCxcDetail();
    }

    public void onClickSend(View view){
        navigateToShareInformation();
    }

    private void setupUI(){
        toolbar = findViewById(R.id.tb_notification_detail);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());

        imageView = findViewById(R.id.img_notification_icon);
        description = findViewById(R.id.txt_notification_description);
        callToAction = findViewById(R.id.txt_notification_call_action);

        recyclerView = findViewById(R.id.rv_noti_detail);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
    }

    private void customUI(){

        if (notification.getType().equals(NotificationType.N001)){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_calendar_red));
            description.setText("Aviso preventivo de pago");
            callToAction.setText("Notifica a tu cliente de su próximo pago");
        } else if (notification.getType().equals(NotificationType.N002)){
            imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_alert));
            description.setText("No se recibió el total adeudado");
            description.setTextColor(getResources().getColor(R.color.colorRed));
            callToAction.setText("Envía un requerimiento de pago");
        } else {
            Dialogs.showAlert(
                    this,
                    "Ocurrió un error inesperado al mostrar la información, inténtalo de nuevo.",
                    (dialog, which) -> onBackPressed()
            );
            return;
        }
        Objects.requireNonNull(getSupportActionBar()).setTitle(notification.getDescriptionType());
    }

    private void setData(){
        List<VndrList> list = new ArrayList<>();

        list.add(new VndrList(new SimpleList2("Cliente", notification.getCustomerName())));
        list.add(new VndrList(new SimpleList2("Pedido", notification.getOrderNumber())));
        list.add(new VndrList(new SimpleList2("Fecha de pago", notification.getPaymentDate())));

        if (!notification.isCxCUniquePayment())
            list.add(new VndrList(new SimpleList2("Fecha de límite pago", notification.getDueDate())));

        list.add(new VndrList(new SimpleList2("Total a pagar", notification.getAmount())));

        if (!notification.isCxCUniquePayment())
            list.add(new VndrList(new SimpleList2("Pago de periodo", notification.getPaymentPeriod())));

        if (!notification.isCxCUniquePayment())
            list.add(new VndrList(new SimpleList2("Importe vencido", notification.getUnpaidAmount())));

        list.add(new VndrList(new SimpleList2("Adeudo a la fecha", notification.getBalanceAmount())));

        if (!notification.isCxCUniquePayment())
            list.add(new VndrList(new SimpleList2("Pagos vencidos", notification.getStatusCount())));

        VndrListAdapter adapter = new VndrListAdapter(list);
        recyclerView.setAdapter(adapter);
    }

    private void navigateToShareInformation(){
        NotificationAlertFragment alertFragment = new NotificationAlertFragment();
        alertFragment.setNotificationAlertData(notification.getNotificationAlertData());

        alertFragment.show(getSupportFragmentManager(),"alert");
    }

    private void navigateToCxcDetail(){
        startActivity(
                new Intent(
                        this,
                        DetailCxcActivity.class
                ).putExtra("id_cxc", notification.getIdCxc())
        );
    }
}
