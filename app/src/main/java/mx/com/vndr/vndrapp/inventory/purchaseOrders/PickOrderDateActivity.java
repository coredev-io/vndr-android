package mx.com.vndr.vndrapp.inventory.purchaseOrders;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.toolbox.Volley;

import java.util.Calendar;
import java.util.Date;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders;
import mx.com.vndr.vndrapp.api.inventory.APIShoppingOrders.OnCreateShoppingOrder;
import mx.com.vndr.vndrapp.api.inventory.ShoppingOrder;
import mx.com.vndr.vndrapp.util.Dialogs;

import static mx.com.vndr.vndrapp.util.Constants.RESULT_BACK;

public class PickOrderDateActivity extends AppCompatActivity implements OnCreateShoppingOrder, OnDateChangeListener {

    Toolbar toolbar;
    CalendarView calendarView;
    ProgressBar progressBar;
    Button button;
    TextView textView;
    boolean isSendDate;
    boolean isSended;
    Date date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_order_date);
        isSendDate = getIntent().getBooleanExtra("isSendDate", false);
        setupUI();
    }

    /**
     * UI Methods
     */

    private void setupUI(){
        toolbar = findViewById(R.id.tb_date_po);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> onBackPressed());
        calendarView = findViewById(R.id.cv_po);
        button = findViewById(R.id.btn_pick_date_po);
        progressBar = findViewById(R.id.pb_pick_date_po);
        textView = findViewById(R.id.detail_pick_date);
        calendarView.setOnDateChangeListener(this);

        if (isSendDate) {
            customUIToSendedData();
            calendarView.setDate(ShoppingOrder.Selected.current.getShoppingOrder().getOrderDate().getTime());
            calendarView.setMinDate(
                    ShoppingOrder.Selected.current.getShoppingOrder().getOrderDate().getTime()
            );
            date = ShoppingOrder.Selected.current.getShoppingOrder().getOrderDate();
            validateDates();
        } else {
            date = new Date();  //hoy
            calendarView.setDate(date.getTime());
            calendarView.setMinDate(date.getTime());
            calendarView.setMaxDate(date.getTime());
        }

    }

    private void customUIToSendedData(){
        getSupportActionBar().setTitle("Fecha de envío");
        textView.setText("Selecciona la fecha de envío");
    }

    private void makeButtonNotSendedAppearance(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            button.setBackgroundTintList(ContextCompat.getColorStateList(this,R.color.colorPrimary));
        }
        button.setText("Aún no la envío");
        isSended = false;
    }

    private void makeButtonSendedAppearance(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            button.setBackgroundTintList(ContextCompat.getColorStateList(this,R.color.colorAccent));
        }
        button.setText("Siguiente");
        isSended = true;
    }

    /**
     * Data methods
     */

    private void updateSendOrderDate(){
        ShoppingOrder
                .Selected
                .current
                .getShoppingOrder()
                .setSendDate(date);
        new APIShoppingOrders(Volley.newRequestQueue(this))
                .setSendedDate(this);
    }


    private void updateOrderDate(){
        ShoppingOrder
                .Selected
                .current
                .getShoppingOrder()
                .setOrderDate(date);
        navigateToOrderDetail();
    }

    @Override
    public void onSuccess() {
        dismissLoading();
        this.setResult(RESULT_OK);
        this.finish();
    }

    @Override
    public void onError(String messageError) {
        dismissLoading();
        Dialogs.showAlert(this, messageError);
    }

    private void validateDates(){
        if (date.after(new Date()))
            makeButtonNotSendedAppearance();
        else
            makeButtonSendedAppearance();
    }

    /**
     * Action UI Methods
     */

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        button.setVisibility(View.GONE);
    }


    private void dismissLoading(){
        progressBar.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
    }

    public void onClick(View view){
        if (isSendDate){
            showLoading();
            if (isSended){
                updateSendOrderDate();
            } else {
                dismissLoading();
                this.setResult(RESULT_OK);
                this.finish();
            }
        } else {
            updateOrderDate();
        }
    }

    private void navigateToOrderDetail(){
        startActivityForResult(new Intent(this, PODetailActivity.class),1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            this.setResult(resultCode);
            this.finish();
        }
    }

    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
        Calendar c = Calendar.getInstance();
        c.set(i, i1, i2);
        date = c.getTime();

        if (isSendDate){
            validateDates();
        }
    }
}