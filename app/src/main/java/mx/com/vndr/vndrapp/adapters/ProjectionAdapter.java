package mx.com.vndr.vndrapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.api.analytics.Projection;
import mx.com.vndr.vndrapp.api.analytics.Projections;

public class ProjectionAdapter extends RecyclerView.Adapter<ProjectionAdapter.ProjectionViewHolder> {

    List<Projection> projectionList;
    Context context;

    public ProjectionAdapter(List<Projection> projectionList, Context context) {
        this.projectionList = projectionList;
        this.context = context;
    }

    @NonNull
    @Override
    public ProjectionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ProjectionViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_projections, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectionViewHolder holder, int position) {
        holder.textViewLabel.setText(projectionList.get(position).getMonth());
        holder.textViewAmount.setText(projectionList.get(position).getAmountStr());
        holder.progressBar.setProgress(projectionList.get(position).getPercent());
    }

    @Override
    public int getItemCount() {
        return projectionList.size();
    }

    public class ProjectionViewHolder extends RecyclerView.ViewHolder{

        TextView textViewLabel;
        TextView textViewAmount;
        ProgressBar progressBar;

        public ProjectionViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewLabel = itemView.findViewById(R.id.txt_label_projections_item);
            progressBar = itemView.findViewById(R.id.pb_projections_item);
            textViewAmount = itemView.findViewById(R.id.txt_amnt_projections_item);
        }
    }
}
