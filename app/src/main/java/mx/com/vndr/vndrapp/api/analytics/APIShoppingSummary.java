package mx.com.vndr.vndrapp.api.analytics;

import static mx.com.vndr.vndrapp.api.URLVndr.URL_ANALYTICS_PURCHASE;

import com.android.volley.RequestQueue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mx.com.vndr.vndrapp.R;
import mx.com.vndr.vndrapp.analytics.inventory.ShoppingSummaryActivity;
import mx.com.vndr.vndrapp.api.VndrRequest;
import mx.com.vndr.vndrapp.api.inventory.Supplier;
import mx.com.vndr.vndrapp.customviews.VndrList.ProductAnalytic;
import mx.com.vndr.vndrapp.customviews.VndrList.SimpleList2;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrList;
import mx.com.vndr.vndrapp.customviews.VndrList.VndrListAdapter;
import mx.com.vndr.vndrapp.models.Brand;
import mx.com.vndr.vndrapp.util.VndrDateFormat;

public class APIShoppingSummary {

    private final static String TAG = "APIShoppingSummary";

    private RequestQueue queue;

    public APIShoppingSummary(RequestQueue queue) {
        this.queue = queue;
    }

    public void getShoppingSummary(OnShoppingSummaryResponse onShoppingSummaryResponse, Date minDate, Date maxDate, ShoppingSummaryActivity.Query query){

        String url = URL_ANALYTICS_PURCHASE +
                "?start_date=" +
                VndrDateFormat.dateToWSFormat(minDate) +
                "&end_date=" +
                VndrDateFormat.dateToWSFormat(maxDate);

        switch (query){

            case ALL:
                break;
            case BY_SUPPLIER:
                url += "&supplier=" + Supplier.Selected.current.getSupplier().getId();
                break;
            case PRODUCT_BY_SUPPLIER:
                break;
            case BY_BRAND:
                break;
        }

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onShoppingSummaryResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200){
                                onShoppingSummaryResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            List<VndrList> vndrLists = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data");

                            vndrLists.add(new VndrList(
                                    new SimpleList2(
                                            "Total",
                                            NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("total_invoices"))
                                    ).setBGColor(R.color.colorPrimary)
                            ));

                            JSONArray jsonArray;

                            if (query.equals(ShoppingSummaryActivity.Query.ALL)){
                                jsonArray = jsonObject.getJSONArray("suppliers");

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    vndrLists.add(new VndrList(
                                            new SimpleList2(
                                                    object.getString("name"),
                                                    NumberFormat.getCurrencyInstance().format(object.getDouble("amount"))
                                            ).setBGColor(R.color.colorAccent)
                                    ));
                                }
                            }


                            jsonArray = jsonObject.getJSONArray("invoices");
                            if (jsonArray.length() == 0){
                                onShoppingSummaryResponse.onError("No se encontraron datos con el criterio de búsqueda.");
                                return;
                            }

                            for (int i = 0; i < jsonArray.length(); i++) {
                                vndrLists.add(new VndrList(new Invoice(jsonArray.getJSONObject(i))));
                            }

                            onShoppingSummaryResponse.onSuccess(new VndrListAdapter(vndrLists));


                        } catch (JSONException e) {
                            e.printStackTrace();
                            onShoppingSummaryResponse.onError("Ocurrió un problema inesperado, por favor inténtalo de nuevo.");
                        }
                    }
                });

    }

    public void getProductsBySupplierResume(OnShoppingSummaryResponse onShoppingSummaryResponse, Date minDate, Date maxDate){

        String url = URL_ANALYTICS_PURCHASE +
                "?start_date=" +
                VndrDateFormat.dateToWSFormat(minDate) +
                "&end_date=" +
                VndrDateFormat.dateToWSFormat(maxDate);

        url += "&supplier=" + Supplier.Selected.current.getSupplier().getId();
        url += "&products=true";

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onShoppingSummaryResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200){
                                onShoppingSummaryResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            List<VndrList> vndrLists = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data");


                            if (jsonObject.getDouble("total_invoices") == 0){
                                onShoppingSummaryResponse.onError("No se encontraron datos con el criterio de búsqueda.");
                                return;
                            }

                            vndrLists.add(new VndrList(
                                    new SimpleList2(
                                            "Total productos",
                                            NumberFormat.getCurrencyInstance().format(jsonObject.getDouble("total_invoices"))
                                    ).setBGColor(R.color.colorAccent)
                            ));

                            JSONArray jsonArray = jsonObject.getJSONArray("brand");

                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonObject = jsonArray.getJSONObject(i);
                                vndrLists.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        jsonObject.getString("name"),
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        jsonObject
                                                                                .getDouble("total_brand")
                                                                )
                                                ).setBGColor(R.color.colorPrimary)
                                        )
                                );

                                JSONArray categoriesArray = jsonObject.getJSONArray("categories");

                                for (int j = 0; j < categoriesArray.length(); j++) {
                                    JSONObject object = categoriesArray.getJSONObject(j);
                                    vndrLists.add(
                                            new VndrList(
                                                    new SimpleList2(
                                                            object.getString("name"),
                                                            NumberFormat
                                                                    .getCurrencyInstance()
                                                                    .format(
                                                                            object
                                                                                    .getDouble("total")
                                                                    )
                                                    ).setBGColor(R.color.textColor)
                                            )
                                    );
                                    JSONArray arrayProducts = object.getJSONArray("products");
                                    for (int k = 0; k < arrayProducts.length(); k++) {
                                        JSONObject objectProduct = arrayProducts.getJSONObject(k);
                                        ProductAnalytic productAnalytic = new ProductAnalytic(
                                                objectProduct.getString("name"),
                                                objectProduct.getString("product_key"),
                                                String.valueOf(objectProduct.getInt("quantity")),
                                                NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("total")),
                                                NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("average_price")),
                                                NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("catalog_price")),
                                                objectProduct.getString("discount") + "%"

                                        );
                                        productAnalytic.setPurchase(true);
                                        productAnalytic.setInvoice(true);

                                        productAnalytic.setOrderNumber("#" +  objectProduct.getString("invoice_document_number"));
                                        productAnalytic.setOrderDate(VndrDateFormat.stringDateParamsFormatToAppFormat(objectProduct.getString("invoice_date")));
                                        vndrLists.add(new VndrList(productAnalytic));
                                    }

                                }

                            }


                            onShoppingSummaryResponse.onSuccess(new VndrListAdapter(vndrLists));


                        } catch (JSONException e) {
                            e.printStackTrace();
                            onShoppingSummaryResponse.onError("Ocurrió un problema inesperado, por favor inténtalo de nuevo.");
                        }
                    }
                });

    }

    public void getResumeByBrand(OnShoppingSummaryResponse onShoppingSummaryResponse, Date minDate, Date maxDate){

        String url = URL_ANALYTICS_PURCHASE +
                "?start_date=" +
                VndrDateFormat.dateToWSFormat(minDate) +
                "&end_date=" +
                VndrDateFormat.dateToWSFormat(maxDate);

        url += "&brand=" + Brand.Selected.current.getBrand().getBrandId();

        new VndrRequest(queue)
                .get(url, new VndrRequest.VndrResponse() {
                    @Override
                    public void error(String message) {
                        onShoppingSummaryResponse.onError(message);
                    }

                    @Override
                    public void success(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            if (jsonObject.getInt("statusCode") != 200){
                                onShoppingSummaryResponse.onError(jsonObject.getString("message"));
                                return;
                            }

                            List<VndrList> vndrLists = new ArrayList<>();

                            jsonObject = jsonObject.getJSONObject("data");

                            if (jsonObject.getDouble("total_brand") == 0){
                                onShoppingSummaryResponse.onError("No se encontraron datos con el criterio de búsqueda.");
                                return;
                            }

                            vndrLists.add(
                                    new VndrList(
                                            new SimpleList2(
                                                    jsonObject.getString("brand_name"),
                                                    NumberFormat
                                                            .getCurrencyInstance()
                                                            .format(
                                                                    jsonObject
                                                                            .getDouble("total_brand")
                                                            )
                                            ).setBGColor(R.color.colorPrimary)
                                    )
                            );



                            JSONArray categoriesArray = jsonObject.getJSONArray("categories");

                            for (int j = 0; j < categoriesArray.length(); j++) {
                                JSONObject object = categoriesArray.getJSONObject(j);
                                vndrLists.add(
                                        new VndrList(
                                                new SimpleList2(
                                                        object.getString("name"),
                                                        NumberFormat
                                                                .getCurrencyInstance()
                                                                .format(
                                                                        object
                                                                                .getDouble("total")
                                                                )
                                                ).setBGColor(R.color.textColor)
                                        )
                                );
                                JSONArray arrayProducts = object.getJSONArray("products");

                                for (int k = 0; k < arrayProducts.length(); k++) {
                                    JSONObject objectProduct = arrayProducts.getJSONObject(k);
                                    ProductAnalytic productAnalytic = new ProductAnalytic(
                                            objectProduct.getString("name"),
                                            objectProduct.getString("product_key"),
                                            String.valueOf(objectProduct.getInt("quantity")),
                                            NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("total")),
                                            NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("average_price")),
                                            NumberFormat.getCurrencyInstance().format(objectProduct.getDouble("catalog_price")),
                                            objectProduct.getString("discount") + "%"

                                    );
                                    productAnalytic.setPurchase(true);
                                    productAnalytic.setInvoice(true);
                                    vndrLists.add(new VndrList(productAnalytic));
                                }

                            }


                            onShoppingSummaryResponse.onSuccess(new VndrListAdapter(vndrLists));


                        } catch (JSONException e) {
                            e.printStackTrace();
                            onShoppingSummaryResponse.onError("Ocurrió un problema inesperado, por favor inténtalo de nuevo.");
                        }
                    }
                });

    }




    public interface OnShoppingSummaryResponse {
        void onSuccess(VndrListAdapter adapter);
        void onError(String error);
    }
}
