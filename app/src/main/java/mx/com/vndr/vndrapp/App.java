package mx.com.vndr.vndrapp;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

//import com.instabug.library.Instabug;
//import com.instabug.library.invocation.InstabugInvocationEvent;

import mx.com.vndr.vndrapp.util.Dialogs;
import zendesk.android.SuccessCallback;
import zendesk.android.Zendesk;
import zendesk.messaging.android.DefaultMessagingFactory;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
/*
        if (!Instabug.isBuilt()){
            new Instabug.Builder(this, BuildConfig.INSTABUG_TOKEN)
                    .setInvocationEvents(
                            InstabugInvocationEvent.SHAKE,
                            InstabugInvocationEvent.SCREENSHOT,
                            InstabugInvocationEvent.TWO_FINGER_SWIPE_LEFT)
                    .build();
        }
 */

        Zendesk.initialize(
                this,
                BuildConfig.ZENDESK_CHANNEL_KEY,
                zendesk -> Log.i("Zendesk", "Initialization successful"),
                error -> Log.e("Zendesk", "Messaging failed to initialize", error),
                new DefaultMessagingFactory());

    }
}
